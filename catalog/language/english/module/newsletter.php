<?php
// Heading
$_['heading_title']     = 'Sign up for e-mail newsletter and get<br><span>discount -5% from Nikola </span>';
$_['text_subscribe']     = 'Subscribe';
$_['text_email']     = 'Your email';
$_['text_name']     = 'Your name';

$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';

$_['isset_email']    = 'This Email already exists in subscribers';
$_['success_subscribe']    = 'A confirmation letter has been sent to your email address';
$_['error_subscribe']    = 'Subscription error';




