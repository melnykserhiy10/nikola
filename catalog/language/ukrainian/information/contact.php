<?php
// Heading
$_['heading_title']  = 'Зв’язатися з нами';

// Text
$_['text_location']  = 'Наша Адреса';
$_['text_store']     = 'Наші магазини';
$_['text_contact']   = 'Форма зв’язку';
$_['text_address']   = 'Адреса';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Час роботи';
$_['text_comment']   = 'Коментар';
$_['text_success']   = '<p>Ваш запит був успішно відправлений адміністрації магазину!</p>';

$_['text_contacts'] = 'Контакти <span>Nikola.ua</span>';
$_['text_contact_with'] = 'Контакт з <span>Nikola.ua</span>';
$_['text_shops'] = 'Магазини <span>Nikola</span>';

// Entry
$_['entry_first_name']     = 'Ваше ім’я';
$_['entry_last_name']     = 'Прізвище';
$_['entry_email']    = 'E-Mail для зв’язку';
$_['entry_enquiry']  = 'Повідомлення';
$_['entry_telephone']  = 'Контактний телефон';


// Email
$_['email_subject']  = 'Повідомлення %s';

// Errors
$_['error_first_name']     = 'Ім’я має бути від 3 до 32 символів!';
$_['error_last_name']     = 'Прізвище має бути від 3 до 32 символів!';
$_['error_email']    = 'E-Mail вказано некоректно!';
$_['error_enquiry']  = 'Повідомлення має бути від 10 до 3000 символів!';
$_['error_telephone'] = 'Телефон повинен бути від 3 до 32 символів!';