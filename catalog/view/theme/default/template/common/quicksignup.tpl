<div id="login_register-wrapp" class="login_register-wrapp">
    <div class="content_popup">
        <div id="quick-login" class="col-sm-4 col-xs-12">
            <div class="row"><div class="heading"><?php echo $text_login; ?></div></div>
            <div class="content">
                <div class="title"><?php echo $text_login_title1; ?></div>
                <div class="under_title"><?php echo $text_login_title2; ?></div>
                <div class="form-group">
                    <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email1" class="form-control" />
                </div>
                <div class="form-group">
                    <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-passwordl" class="form-control" />
                </div>
                <div class="button_wrapp">
                    <button type="button" class="btn btn-primary loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>
                </div>
            </div>
        </div>
        <div id="quick-register" class="col-sm-8 col-xs-12">
            <div class="row"><div class="heading"><?php echo $text_register; ?></div></div>
            <div class="content">
                <div class="title"><?php echo $text_register_title1; ?></div>
                <div class="under_title"><?php echo $text_register_title2; ?></div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <input type="text" name="first_name" value="" placeholder="<?php echo $entry_name; ?>" id="input-firstname" class="form-control" />
                        </div>
                        <div class="form-group required">
                            <input type="text" name="last_name" value="" placeholder="<?php echo $entry_last_name; ?>" id="input-lastname" class="form-control" />
                        </div>
                        <div class="form-group required">
                            <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <input type="text" name="telephone" value="" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                        </div>
                        <div class="form-group required">
                            <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                        </div>
                        <div class="form-group required">
                            <input type="password" name="confpassword" value="" placeholder="<?php echo $entry_confpassword; ?>" id="input-confpassword" class="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                    <div class="form-group required">
                        <label class="for_select2">
                            <select name="country_id" id="input-country" class="form-control">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $country_id) { ?>
                                        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </label>
                    </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group radio_group hidden">
                        <?php foreach ($customer_group as $info) { ?>
                            <div class="radio register_radio">
                                <input type="radio" name="customer_group_id" id="radio_button-<?php echo $info['id'] ?>" value="<?php echo $info['id'] ?>" <?php echo $default_group == $info['id'] ? 'checked="checked"' : '' ?>>
                                <label for="<?php echo $info['id'] ?>" class="label_button-<?php echo $info['id'] ?>">
                                    <?php echo $info['text'] ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="all_field"><?php echo $text_all_field; ?></div>
                </div>
                <div class="button_wrapp">
                    <button type="button" class="btn btn-primary createaccount" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue; ?></button>
                </div>
            </div>
        </div>
    </div>
    <div class="one_footer">
        <?php echo $ulogin; ?>
    </div>
</div>

<script type="text/javascript"><!--

    $(document).ready(function () {
        console.log('ror');
        $('.login_register-wrapp #input-country').on('change', function (e) {
            console.log($(this));
            if($(this).closest('.form-group ').hasClass('has-error')){
                $(this).closest('.form-group ').removeClass('has-error');
                $('#quick-register .alert-danger').remove();
            }
        });
    });

$('#quick-register input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-register .createaccount').trigger('click');
	}
});
$('#quick-register .createaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/register',
		type: 'post',
		data: $('#quick-register input[type=\'text\'], #quick-register select, #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked, #quick-register input[type=\'radio\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register .createaccount').button('loading');
			$('#quick-register .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register .createaccount').button('reset');
		},
		success: function(json) {
			$('#quick-register .form-group').removeClass('has-error');
			
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			if (json['error_firs_name']) {
				$('#quick-register #input-firstname').parent().addClass('has-error');
				$('#quick-register #input-firstname').focus();
			}
			if (json['error_email']) {
				$('#quick-register #input-email').parent().addClass('has-error');
				$('#quick-register #input-email').focus();
			}
			if (json['error_telephone']) {
				$('#quick-register #input-telephone').parent().addClass('has-error');
				$('#quick-register #input-telephone').focus();
			}
			if (json['error_last_name']) {
				$('#quick-register #input-lastname').parent().addClass('has-error');
				$('#quick-register #input-lastname').focus();
			}
			if (json['error_country']) {
				$('#quick-register #input-country').parent().parent().addClass('has-error');
				$('#quick-register #input-country').focus();
			}
			if (json['error_password']) {
				$('#quick-register #input-password').parent().addClass('has-error');
				$('#quick-register #input-password').focus();
			}
			if (json['error_confpassword']) {
				$('#quick-register #input-confpassword').parent().addClass('has-error');
				$('#quick-register #input-confpassword').focus();
			}
			if (json['error']) {
				$('#quick-register .under_title').after('<div class="alert alert-danger">' + json['error'] + '</div>');
			}

            if (json['confirm_account']) {
                $.fancybox.close({href:'#login_register-wrapp'});
                $('#thanks_modal .fancy_content').html(json['confirm_account']);
                $.fancybox.open({
                    href: '#thanks_modal',
                    wrapCSS: 'custom_fancy',
                    width: '600px',
                    autoSize: false,
                    height: 'auto',
                    padding: 0,
                    helpers: {
                        overlay: {
                            css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image':'none'} // or your preferred hex color value
                        }
                    },
                    'afterShow': function () {
                        setTimeout(function(){
                            $.fancybox.close({href:'#thanks_modal'});
                        },2500);
                    }
                });
            }
            if(json['login']){
                window.location.href="index.php?route=account/account";
            }
            if (json['success']) {
                $('#modal-lq').modal('hide');
            }
		}
	});
});
//--></script>
<script type="text/javascript"><!--

$('#quick-login input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-login .loginaccount').trigger('click');
	}
});
$('#quick-login .loginaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/login',
		type: 'post',
		data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-login .loginaccount').button('loading');
			$('#modal-login .alert-danger').remove();
		},
		complete: function() {
			$('#quick-login .loginaccount').button('reset');
		},
		success: function(json) {
			$('#modal-login .form-group').removeClass('has-error');
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			
			if (json['error']) {
			    $('#quick-login .alert-danger').remove();
				$('#quick-login #input-email1').parent().addClass('has-error');
				$('#quick-login #input-passwordl').parent().addClass('has-error');
				$('#quick-login #input-email1').focus();
				$('#quick-login .button_wrapp').before('<span class="error_mess">' + json['error'] + '</span>');
				console.log(json['error']);

//                $('#thanks_modal .fancy_content').html(json['error']);
//
//                $.fancybox.close({href:'#login_register-wrapp'});
//                $.fancybox.open({
//                    href: '#thanks_modal',
//                    wrapCSS: 'custom_fancy',
//                    width: '600px',
//                    autoSize: false,
//                    height: 'auto',
//                    padding: 0,
//                    helpers: {
//                        overlay: {
//                            css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image':'none'} // or your preferred hex color value
//                        }
//                    },
//                    'afterShow': function () {
//                        setTimeout(function(){
//                            $.fancybox.close({href:'#thanks_modal'});
//                        },2500);
//                    }
//                });
			}
			if(json['success']){
                window.location.href="index.php?route=account/account";
                $('#modal-login').modal('hide');
			}
			
		}
	});
});
//--></script>
<script type="text/javascript"><!--
function loacation() {
	location.reload();
}
//--></script>