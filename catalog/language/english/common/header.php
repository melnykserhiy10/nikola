<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = '%s products';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_page']          = 'page';
$_['text_gallery']       = 'Gallery';

$_['text_shop']          = 'Online Shop';
$_['text_sot']           = 'Cooperation';
$_['text_company']       = 'About the company';
$_['text_contact']       = 'Contacts';
$_['total_wishlist']     = '%s products';

