<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($products) { ?>
          <div class="row">
              <?php foreach ($products as $product) { ?>
                  <div class="product-layout col-lg-3 col-md-3 col-sm-4 col-xs-12">
                      <div class="product-thumb">
                          <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                          <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                          <div class="name"><?php echo $product['stock']; ?></div>
                          <?php if ($product['price']) { ?>
                              <div class="price">
                                  <?php if (!$product['special']) { ?>
                                      <div class="price-new"><?php echo $product['price']; ?></div>
                                  <?php } else { ?>
                                      <div class="price-new"><?php echo $product['special']; ?></div>
                                      <div class="price-old"><?php echo $product['price']; ?></div>
                                  <?php } ?>
                              </div>
                          <?php } ?>
                          <?php if($product['sizes']){ ?>
                              <div class="sizes">
                                  <ul>
                                      <?php foreach ($product['sizes'] as $sizes) { ?>
                                          <?php foreach ($sizes['product_option_value'] as $size) { ?>
                                              <li><?php echo $size['name']; ?></li>
                                          <?php } ?>
                                      <?php } ?>
                                  </ul>
                              </div>
                          <?php } ?>
                          <div class="wrapp_whbtn">
                              <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="remove"></a>
                          </div>
                      </div>
                  </div>
              <?php } ?>
          </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>