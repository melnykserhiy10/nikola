<?php
class ControllerModuleNewsletters extends Controller {
	public function index() {
		$this->load->language('module/newsletter');
		$this->load->model('module/newsletters');
		
		$this->model_module_newsletters->createNewsletter();

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_brands'] = $this->language->get('text_brands');
		$data['text_index'] = $this->language->get('text_index');

		$data['text_subscribe'] = $this->language->get('text_subscribe');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_name'] = $this->language->get('text_name');

		$data['brands'] = array();
		
		
		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/newsletters.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/newsletters.tpl', $data);
		} else {
			return $this->load->view('default/template/module/newsletters.tpl', $data);
		}
	}
	public function news()
	{
		$this->load->model('module/newsletters');
        $this->load->language('module/newsletter');

        $json = array();

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
                $json['error'] = true;
                $json['error_name'] = $this->language->get('error_name');
            }
            if (!preg_match($this->config->get('config_mail_regexp'), $this->request->post['email'])) {
                $json['error'] = true;
                $json['error_email'] = $this->language->get('error_email');
            }
            if(!isset($json['error'])){

                $json['message'] = $this->model_module_newsletters->subscribes($this->request->post);
                $json['success'] = true;
            }
        }

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	
}
