<script>
		function subscribe(){
//			var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//			var email = $('#txtemail').val();
//			if(email != "")
//			{
//				if(!emailpattern.test(email))
//				{
//					alert("Неправильный Email");
//					return false;
//				}
//				else
//				{
					$.ajax({
						url: 'index.php?route=module/newsletters/news',
						type: 'post',
						data: 'email=' + $('#txtemail').val()+ '&name=' + $('#txtname').val(),
						dataType: 'json',
						success: function(json) {
						    $('.error_mess').remove();
						    $('.subscribe-form .form-group').removeClass('has-error');
						    if(json['error']){
						        if(json['error_email']){
//                                    alert(json['error_email']);
                                    $('#txtemail').closest('.form-group').addClass('has-error');
//                                    $('#txtemail').after('<span class="error_mess">' + json['error_email'] + '</span>');

                                }
                                if(json['error_name']){
						            $('#txtname').closest('.form-group').addClass('has-error');
//						            $('#txtname').after('<span class="error_mess">' + json['error_name'] + '</span>');
//                                    alert(json['error_name']);
                                }
                            }
                            if(json['success']){
//                                alert(json['message']);
                                $('#thanks_modal .fancy_content').html(json['message']);
                                $.fancybox.open({
                                    href: '#thanks_modal',
                                    wrapCSS: 'custom_fancy',
                                    width: '600px',
                                    autoSize: false,
                                    height: 'auto',
                                    padding: 0,
                                    helpers: {
                                        overlay: {
                                            css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image':'none'}, // or your preferred hex color value
                                            locked: false
                                        }
                                    },
                                    'afterShow': function () {
                                        setTimeout(function(){$.fancybox.close({href:'#thanks_modal'})},2500);
                                    }
                                });
                            }
						}
						
					});
					return false;
//				}
//			}
//			else
//			{
//				alert("Email обязательно");
//				$(email).focus();
//				return false;
//			}
		}
	</script>
	
<div class="subscribe-box">
	<h3 class="subscribe-title"><?php echo $heading_title; ?></h3>
	<div class="subscribe-form">
		<form class="form-inline" method="post">
          <div class="form-group">
            <input type="text" class="form-control" name="txtname" id="txtname" placeholder="<?php echo $text_name; ?>">
          </div>
		  <div class="form-group">
		    <input type="email" class="form-control" name="txtemail" id="txtemail" placeholder="<?php echo $text_email; ?>">
		  </div>
          <div class="btn_wrapp">
            <button type="submit" class="subscribe_btn" onclick="return subscribe();"><?php echo $text_subscribe; ?></button>
          </div>
		</form>
	</div>
</div>
