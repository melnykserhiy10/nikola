<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';

$_['entry_name']      	= 'Name';
$_['entry_phone']       = 'Mobile phone number';
$_['text_call']      	= 'Request a call';
$_['text_send']      	= 'Send';
$_['text_loading']      = 'Treatment';

$_['text_cookie_fancy']  = 'Please indicate your buyer type';
$_['title_cookie_fancy'] = 'Welcome<br>In our online store!';
$_['text_wholesale_buyer'] = 'I am a wholesale buyer';
$_['text_retail_buyer']    = 'I am a retail buyer';