<?php
// Heading
$_['heading_title']            = 'Купити в 1 клік';

// Button
$_['button_shopping']		   = 'Продовжити покупки';
$_['button_checkout']		   = 'Оформити замовлення';
$_['button_upload']            = 'Загрузити замовлення';
$_['button_purchase_now']	   = 'Купити в 1 клік';

// Text
$_['text_price']               = 'Ціна:';
$_['text_reward']              = 'Бонусные балы:';
$_['text_points']              = 'Це    на с учетом балов:';
$_['text_tax']                 = 'Без налогов:';
$_['text_discount']            = ' или ';
$_['text_option']              = 'Доступные варианты';
$_['text_minimum']             = 'Минимальное кол-во товара для заказа %s';
$_['text_select']              = ' --- Сделайте выбор --- ';
$_['text_success_order']	   = '<p>Ваш заказ успешно оформлен.</p><p>Наш менеджер свяжется с Вами в ближайшее время.</p><p>Спасибо за покупки в нашем интернет-магазине!</p>';
$_['text_loading']             = 'Загрузка...';

// Enter
$_['enter_firstname']          = 'Введіть Ім\'я';
$_['enter_telephone']          = 'Введіть Телефон';
$_['enter_email']              = 'Введіть E-mail';
$_['enter_comment']            = 'Введіть коментар';
$_['entry_quantity']           = 'Введіть кількість:';


// Error
$_['error_firstname']          = 'Ім\'я повинно бути від 1 до 32 символів!';
$_['error_email']              = 'Е-mail адресу введений невірно!';
$_['error_telephone']          = 'Номер телефону повинен бути від 3 до 32 символів!';
$_['error_comment']            = 'Коментар повинен бути від 3 до 500 символів!';
$_['error_option']             = '%s обов\'язково!';