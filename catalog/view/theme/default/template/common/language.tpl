<?php if (count($languages) > 1) { ?>
<div class="language_wrapp">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
        <div class="languages_btn">
            <?php foreach ($languages as $language) { ?>
                <?php if ($language['code'] == $code) { ?>
                    <?php echo $language['code']; ?>
                <?php } ?>
            <?php } ?>
        </div>
        <ul class="all_languages">
            <?php foreach ($languages as $language) { ?>
                <?php if($language['code'] != $code) { ?>
                    <li><a href="<?php echo $language['code']; ?>"><?php echo $language['code']; ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
      <input type="hidden" name="code" value="" />
      <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
    </form>
    <div class="info"><?php echo $text_language; ?></div>
</div>
<?php } ?>
