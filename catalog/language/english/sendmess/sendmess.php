<?php
// Text
$_['text_name']  			     = 'Name:';
$_['text_email']  				 = 'E-mail:';
$_['text_message'] 				 = 'Сообщение:';
$_['text_phone']				 = 'Телефон:';

$_['error_name']   				 = 'The name must be between 3 and 25 characters!';
$_['error_email']   			 = 'E-mail is required!';
$_['error_message'] 			 = 'Message must be from 10 characters!';
$_['error_phone'] 			 	 = 'Phone must be from 5 characters!';
$_['success']      				 = 'Your request was successfully sent.<br>We will get back to you as soon as possible!';

$_['email_subject_message'] 	 = 'Новое сообщение';
$_['email_subject_callback'] 	 = 'Новый заказ звонка';
