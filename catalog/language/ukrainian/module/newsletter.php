<?php
// Heading
$_['heading_title']     = 'Підпишіться на e-mail розсилку і отримаєте<br><span>знижку -5% від Nikola</span>';
$_['text_subscribe']     = 'Підписатися';
$_['text_name']     = 'Ваше і\'мя';
$_['text_email']     = 'Ваш email';

$_['error_name']      = 'Ім’я має бути від 3 до 32 символів!';
$_['error_email']     = 'E-Mail вказано некоректно!';

$_['isset_email']    = 'Цей Email вже є в підписниках';
$_['success_subscribe']    = 'На вашу електрону адресу було надіслано лист підтвердження';
$_['error_subscribe']    = 'Помилка підписки';

