$(document).ready(function () {
    $('.search_link, .callback_btn').fancybox({
        wrapCSS: 'custom_fancy',
        width: '600px',
        autoSize: false,
        height: 'auto',
        padding: 0,
        helpers: {
            overlay: {
                css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image': 'none'} // or your preferred hex color value
            }
        }
    });
    $('.login_link').fancybox({
        wrapCSS: 'custom_fancy',
        width: '1030px',
        padding: 0,
        autoSize: false,
        height: 'auto',
        helpers: {
            overlay: {
                css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image': 'none'} // or your preferred hex color value
            }
        }
    });

    $(".thumbnail").fancybox({
        wrapCSS: 'custom_fancy custom_fancy_popup',
        openEffect	: 'none',
        closeEffect	: 'none',
        autoSize: true,
        helpers:  {
            title:  null,
            overlay: {
                css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image': 'none'} // or your preferred hex color value
            }
        }
    });

    $('.main_slideshow').slick({
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2500,
        dots: true,
        adaptiveHeight: true,
        prevArrow: '<div class="prev_arrow"></div>',
        nextArrow: '<div class="next_arrow"></div>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    dots: false
                }
            }
        ]
    });

    $('#manufacturer_carusel').slick({
        slidesToShow: 7,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: '<div class="prev_arrow"></div>',
        nextArrow: '<div class="next_arrow"></div>',
        responsive: [
            {
                breakpoint: 1440,
                settings: {
                    slidesToShow: 4
                }
            }
        ]
    });

    $('.currencies_btn').click(function () {
        $(this).toggleClass('active');
        $(this).next('.all_currencies').stop(true, false).slideToggle();
        $('.all_languages').slideUp();
    });

    $('.languages_btn').click(function () {
        $(this).toggleClass('active');
        $(this).next('.all_languages').stop(true, false).slideToggle();
        $('.all_currencies').slideUp();

    });

    if($(window).outerWidth() > 991) {
        (function () {
            var a = document.querySelector('#right_header'), b = null, P = 0;  // если ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом
            window.addEventListener('scroll', Ascroll, false);
            document.body.addEventListener('scroll', Ascroll, false);
            function Ascroll() {
                if (b == null) {
                    var Sa = getComputedStyle(a, ''), s = '';
                    for (var i = 0; i < Sa.length; i++) {
                        if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                            s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                        }
                    }
                    b = document.createElement('div');
                    b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                    a.insertBefore(b, a.firstChild);
                    var l = a.childNodes.length;
                    for (var i = 1; i < l; i++) {
                        b.appendChild(a.childNodes[1]);
                    }
                    a.style.height = b.getBoundingClientRect().height + 'px';
                    a.style.padding = '0';
                    a.style.border = '0';
                }
                var Ra = a.getBoundingClientRect(),
                    R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.f_footer').getBoundingClientRect().top + 0);  // селектор блока, при достижении верхнего края которого нужно открепить прилипающий элемент;  Math.round() только для IE; если ноль заменить на число, то блок будет прилипать до того, как нижний край элемента дойдёт до футера
                if ((Ra.top - P) <= 0) {
                    if ((Ra.top - P) <= R) {
                        b.className = 'stop';
                        b.style.top = -R + 'px';
                    } else {
                        b.className = 'sticky';
                        b.style.top = P + 'px';
                    }
                } else {
                    b.className = '';
                    b.style.top = '';
                }
                window.addEventListener('resize', function () {
                    a.children[0].style.width = getComputedStyle(a, '').width
                }, false);
            }
        })();
    }

    $('.latest_wrapp .product_wrapp').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<div class="prev_arrow"></div>',
        nextArrow: '<div class="next_arrow"></div>',
        responsive: [
            {
                breakpoint: 1210,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    $('.related_wrapp .related').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        // autoplay: true,
        // autoplaySpeed: 3000,
        prevArrow: '<div class="prev_arrow"></div>',
        nextArrow: '<div class="next_arrow"></div>',
        responsive: [
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.product-thumb .name').matchHeight();
    $('.news-ncategory .artblock .artblock_inside .name').matchHeight();



    $('#request_call-form #input-phone').mask('(999) 999 99 99');
    $('.login_register-wrapp #quick-register #input-telephone').mask('+38 (999) 999 99 99');
    $('.contacts_with_nikola #input-telephone').mask('+38 (999) 999 99 99');

    $('.label_button-2').click(function () {
        if ($('#radio_button-2').is(':checked')) {
            $('#radio_button-2').removeAttr('checked');
        } else {
            $('#radio_button-2').prop('checked', 'checked');
        }
    });

    $('.thumbnails').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        prevArrow: '<div class="prev_arrow"></div>',
        nextArrow: '<div class="next_arrow"></div>',
        asNavFor: '.wrapp_dots'
    });
    $('.wrapp_dots').slick({
        slidesToShow: 10,
        slidesToScroll: 1,
        asNavFor: '.thumbnails',
        focusOnSelect: true,
        vertical: true,
    });

    $('.radio_wrapp .control-label').click(function () {
        $(this).toggleClass('closed');
        $(this).next('.opt_wrapp').toggle();
    });
    $('.main_description .heading, .otpravka .heading').click(function () {
        $(this).toggleClass('closed');
        $(this).next('.description').toggle();
    });

    $(window).scroll(function () {
        fixed_menu();
        hide_header();
    });
    function fixed_menu() {
        var top = $(window).scrollTop();
        var menu = ($('.main_menu').offset().top + $('.main_menu').outerHeight())   ;
        if(top > menu){
            $('.fixed_menu').fadeIn();
        }else{
            $('.fixed_menu').fadeOut();
        }
    }

    $(window).resize(function () {
        rsz();
        right_header();
    });
    function rsz() {
        var rozn = window.outerWidth - document.documentElement.clientWidth;
        var w_width = $(window).outerWidth() + rozn;
        if(w_width < 992){
            if($('body').hasClass('resize')) {
                $('.cart_wrapp').appendTo('.right_logo');
                $('#right_header').css({'left':'0px'});
                console.log(w_width);

                $('body').removeClass('resize');
            }
        }else{
            console.log(w_width);
            if(!$('body').hasClass('resize')){
                $('.cart_wrapp').prependTo('#right_header');
                $('#right_header').css({'bottom':'inherit'});

                $('body').addClass('resize');
            }
        }
        if(w_width < 768){
            if($('body').hasClass('mob_resize')){
                $('.cart_wrapp').prependTo('#right_header');
                console.log('123');
                $('.main_about_as ul').slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    prevArrow: '<div class="prev_arrow"></div>',
                    nextArrow: '<div class="next_arrow"></div>',
                    responsive: [
                        {
                            breakpoint: 481,
                            settings: {
                                slidesToShow: 2
                            }
                        }
                    ]
                });
                $('.related_wrapp').insertAfter('.right-col-product');

                $('body').removeClass('mob_resize');
            }
        }else{
            if(!$('body').hasClass('mob_resize')){
                $('.cart_wrapp').appendTo('.right_logo');
                $('.main_about_as ul').slick('unslick');
                $('.related_wrapp').appendTo('.append_realated');

                $('body').addClass('mob_resize');
            }
        }
    }
    rsz();
    right_header();
    function right_header() {
        var posrh = $('.container').offset().left + $('.container').outerWidth();
        var poscallback = $('.container').offset().left - 101;
        $('.callback_btn').css({'left': poscallback + 'px'});
        if($(window).outerWidth() > 991) {
            $('#right_header').css({'left': posrh + 'px'});
            $('#right_header').show();
        }
    };

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
        $('.main_menu .first_level').slideToggle();
    });
    $('.shecter').click(function () {
        $(this).toggleClass('is-active');
        var elem = $('#right_header');
        if($('.shecter').attr("data-fall") == 1){
            elem.css({'bottom':'100%'});
            $('.shecter').attr("data-fall", 0);
        }else{
            $('.shecter').attr("data-fall", 1);
            elem.css({'bottom':'calc(100% - ' + elem.outerHeight() + 'px)'});
        }
    });
    function hide_header() {
        var top = $(window).scrollTop();
        var shecter = ($('.shecter').offset().top + $('.shecter').outerHeight());
        if(top > shecter){
            $('.shecter').attr('data-fall', 0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               );
            $('.shecter').removeClass('is-active');
            $('#right_header').css({'bottom':'100%'});
        }
    }
    $('.open_child').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('is-active');
        if($(this).closest('.dropdown').find('.dropdown-menu').is(':visible')){
            $(this).closest('.dropdown').find('.dropdown-menu').fadeOut(100);
        }else{
            $(this).closest('.dropdown').find('.dropdown-menu').fadeIn(100);
        }
    });

    $('.hide_filter').click(function () {
        var text1 = $(this).text();
        var text2 = $(this).data('text');

        $(this).text(text2);
        $(this).data('text', text1);

        if($(window).outerWidth() > 767){
            if($(this).hasClass('closed')) {
                $(this).removeClass('closed')
                $('.mfilter-box').slideDown(200);
                setTimeout(function () {
                    $('.container_product_res').attr('class', 'container_product_res col-sm-9');
                    $('.product-layout').attr('class', 'product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12');
                },0);
                localStorage.setItem('displays', '1');
            }else{
                $(this).addClass('closed');
                $('.mfilter-box').slideUp(200);
                setTimeout(function () {
                    $('.container_product_res').attr('class', 'container_product_res col-sm-12');
                    $('.product-layout').attr('class', 'product-layout col-lg-3 col-md-3 col-sm-4 col-xs-12');
                },200);
                localStorage.setItem('displays', '2');
            }
        }else{
            $(this).toggleClass('closed');
            $('.mfilter-box').slideToggle(200);
        }
    });
    if($(window).outerWidth() > 767){
        if (localStorage.getItem('displays') == '1') {
             $('.category-page').find('.container_product_res').attr('class', 'container_product_res col-sm-9');
            $('.category-page').find('.product-layout').attr('class', 'product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12');
        } else {
            var text = $('.hide_filter').text();
            $('.hide_filter').text($('.hide_filter').data('text'));
            $('.hide_filter').data('text', text);
            $('.hide_filter').addClass('closed');
            $('.mfilter-box').hide();
            $('.category-page').find('.container_product_res').attr('class', 'container_product_res col-sm-12');
            $('.category-page').find('.product-layout').attr('class', 'product-layout col-lg-3 col-md-3 col-sm-4 col-xs-12');
        }
    }
});
$(window).load(function(){

    $('.ulogin-buttons-container').removeAttr('style');
    $('.ulogin-button-facebook').addClass('fa fa-facebook-square authorization_modal-login_icons_item').removeAttr('style');
    $('.ulogin-button-google').addClass('fa fa-google-plus-square authorization_modal-login_icons_item').removeAttr('style');
    $('.ulogin-button-googleplus').addClass('fa fa-google-plus-square authorization_modal-login_icons_item').removeAttr('style');



    /**
     *
     * first_cokie_popup
     *
     **/

    function ggg() {
        if($.cookie("cookie_name") == null ||  $.cookie("cookie_name") == undefined ){

            console.log($.cookie("after nuul"));

            $.fancybox.open({
                href: '#cookie_fancy',
                wrapCSS: 'custom_fancy cookie_f',
                width: '630px',
                autoSize: false,
                height: 'auto',
                padding: 0,
                helpers: {
                    overlay: {
                        css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image':'none'} // or your preferred hex color value
                    }
                },
                // afterClose: function(){
                //     clear_cookie();
                // }
            });
        }
    }
    ggg();
    function clear_cookie() {
        var date = new Date();
        var minutes = 1440; // 24 hours
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        $.cookie("cookie_name", '1', {expires: date,  path: '/'});
    }
    $('#wholesale_buyer').click(function (e) {
        e.preventDefault();
        $.fancybox.close({href:'#cookie_fancy'});
        $.fancybox.open({
            href: '#login_register-wrapp',
            wrapCSS: 'custom_fancy',
            width: '1030px',
            autoSize: false,
            height: 'auto',
            padding: 0,
            helpers: {
                overlay: {
                    css: {'background-color': 'rgba(90, 61, 132, 0.6)', 'background-image':'none'} // or your preferred hex color value
                }
            }
        });
        clear_cookie();
    });
    $('#retail_buyer').click(function (e) {
        e.preventDefault();
        clear_cookie();
        setTimeout(function () {
             window.location.replace("/all_products");
        },1000);
    });
    /**
     *
     * end
     *
     **/

    // $.removeCookie('cookie_name', { path: '/' });

});