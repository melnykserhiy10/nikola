<?php
// Text
$_['text_items']     = '%s';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_cart2']     = 'Your Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';

$_['cart_text1'] = 'Your shopping cart is empty';
$_['cart_text2'] = 'View the basket';