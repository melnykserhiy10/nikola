<?php echo $header; ?>
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
  <div class="row">
    <div id="content" class="col-sm-12"><?php echo $content_top; ?>
        <div class="titles">
            <h1><?php echo $heading_title; ?></h1>
        </div>
        <?php if ($products) { ?>
          <div class="row wrapp_sort">
          <div class="col-md-4 sort_item">
              <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
              <label class="for_select">
                  <select id="input-sort" class="form-control" onchange="location = this.value;">
                      <?php foreach ($sorts as $sorts) { ?>
                          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </label>
          </div>
          <div class="col-md-4 sort_item">
              <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
              <label class="for_select">
                  <select id="input-limit" class="form-control" onchange="location = this.value;">
                      <?php foreach ($limits as $limits) { ?>
                          <?php if ($limits['value'] == $limit) { ?>
                              <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </label>
              <label class="control-label"><?php echo $text_limit2; ?></label>
          </div>
          <div class="col-sm-4 pagination_wrapp">
              <?php echo $pagination; ?>
          </div>
      </div>
      <br />
      <div class="row">
          <?php foreach ($products as $product) { ?>
              <div class="product-layout col-lg-3 col-md-3 col-sm-4 col-xs-12">
                  <div class="product-thumb">
                      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                      <?php if ($product['price']) { ?>
                          <div class="price">
                              <?php if (!$product['special']) { ?>
                                  <div class="price-new"><?php echo $product['price']; ?></div>
                              <?php } else { ?>
                                  <div class="price-new"><?php echo $product['special']; ?></div>
                                  <div class="price-old"><?php echo $product['price']; ?></div>
                              <?php } ?>
                              <?php if ($product['tax']) { ?>
                                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                              <?php } ?>
                          </div>
                      <?php } ?>
                      <?php if($product['sizes']){ ?>
                          <div class="sizes">
                              <ul>
                                  <?php foreach ($product['sizes'] as $sizes) { ?>
                                      <?php foreach ($sizes['product_option_value'] as $size) { ?>
                                          <li><?php echo $size['name']; ?></li>
                                      <?php } ?>
                                  <?php } ?>
                              </ul>
                          </div>
                      <?php } ?>
                      <div class="wrapp_whbtn">
                          <input type="hidden" id="pr-<?php echo $product['product_id']; ?>">
                          <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" <?php echo $product['wishlist']? 'disabled' : '' ?> onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></button>
                      </div>
                  </div>
              </div>
          <?php } ?>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>