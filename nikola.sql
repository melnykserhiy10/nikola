-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 01 2017 г., 08:56
-- Версия сервера: 5.7.19-0ubuntu0.16.04.1
-- Версия PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `nikola`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_address_simple_fields`
--

CREATE TABLE `oc_address_simple_fields` (
  `address_id` int(11) NOT NULL,
  `metadata` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_affiliate`
--

CREATE TABLE `oc_affiliate` (
  `affiliate_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_affiliate_activity`
--

CREATE TABLE `oc_affiliate_activity` (
  `activity_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_affiliate_login`
--

CREATE TABLE `oc_affiliate_login` (
  `affiliate_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_affiliate_transaction`
--

CREATE TABLE `oc_affiliate_transaction` (
  `affiliate_transaction_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `name`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'W9Lvvumr9ltxDQ9ICMgrGdhbDvUKAVjmhBql1WIMZGHGyA7AFWUqymFqBTTGoduJ8HZzO051wivl92q8zYIG8CINlUdWmJE93EZ7M7jYKykfvguRwif5Mn2ssM4KGqbkqQkWT2S1zLB77MqgCLgKlW01AZDxhgXoctxcvNLCOByWU7aO6ldoutCsbfB87hKf495nebFb62Zg1C1oa5qXdwKga3oOKTkcJtdl72PLiIB6dbisWWJolbLEHOa9qN8E', 1, '2016-11-06 17:15:44', '2016-11-06 17:15:44');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `session_name` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_attribute`
--

INSERT INTO `oc_attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(19, 1, 0),
(18, 1, 0),
(17, 1, 0),
(16, 7, 5),
(15, 7, 4),
(14, 7, 3),
(13, 7, 2),
(12, 7, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(16, 3, 'Сезон'),
(15, 2, 'Brand'),
(15, 3, 'Бренд'),
(15, 1, 'Бренд'),
(13, 2, 'Age'),
(13, 1, 'Возвраст'),
(12, 3, 'Стать'),
(16, 1, 'Сезон'),
(14, 2, 'Model'),
(14, 3, 'Модель'),
(14, 1, 'Модель'),
(13, 3, 'Вік'),
(12, 2, 'Sex'),
(12, 1, 'Пол'),
(16, 2, 'Season'),
(17, 1, 'Склад'),
(18, 1, 'Склад'),
(19, 1, 'Склад'),
(15, 4, 'Бренд'),
(13, 4, 'Возвраст'),
(16, 4, 'Сезон'),
(14, 4, 'Модель'),
(12, 4, 'Пол'),
(17, 4, 'Склад'),
(18, 4, 'Склад'),
(19, 4, 'Склад');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(8, 0),
(10, 0),
(9, 0),
(7, 1),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(9, 1, 'Свойства'),
(10, 1, 'Свойства'),
(8, 1, 'Свойства'),
(7, 3, 'Характеристики'),
(7, 2, 'Характеристики'),
(7, 1, 'Характеристики'),
(11, 1, 'Свойства'),
(12, 1, 'Свойства'),
(13, 1, 'Свойства'),
(14, 1, 'Свойства'),
(15, 1, 'Свойства'),
(16, 1, 'Свойства'),
(17, 1, 'Свойства'),
(18, 1, 'Свойства'),
(19, 1, 'Свойства'),
(20, 1, 'Свойства'),
(21, 1, 'Свойства'),
(22, 1, 'Свойства'),
(23, 1, 'Свойства'),
(24, 1, 'Свойства'),
(25, 1, 'Свойства'),
(26, 1, 'Свойства'),
(27, 1, 'Свойства'),
(28, 1, 'Свойства'),
(29, 1, 'Свойства'),
(30, 1, 'Свойства'),
(31, 1, 'Свойства'),
(32, 1, 'Свойства'),
(33, 1, 'Свойства'),
(34, 1, 'Свойства'),
(35, 1, 'Свойства'),
(36, 1, 'Свойства'),
(37, 1, 'Свойства'),
(9, 4, 'Свойства'),
(10, 4, 'Свойства'),
(8, 4, 'Свойства'),
(7, 4, 'Характеристики'),
(11, 4, 'Свойства'),
(12, 4, 'Свойства'),
(13, 4, 'Свойства'),
(14, 4, 'Свойства'),
(15, 4, 'Свойства'),
(16, 4, 'Свойства'),
(17, 4, 'Свойства'),
(18, 4, 'Свойства'),
(19, 4, 'Свойства'),
(20, 4, 'Свойства'),
(21, 4, 'Свойства'),
(22, 4, 'Свойства'),
(23, 4, 'Свойства'),
(24, 4, 'Свойства'),
(25, 4, 'Свойства'),
(26, 4, 'Свойства'),
(27, 4, 'Свойства'),
(28, 4, 'Свойства'),
(29, 4, 'Свойства'),
(30, 4, 'Свойства'),
(31, 4, 'Свойства'),
(32, 4, 'Свойства'),
(33, 4, 'Свойства'),
(34, 4, 'Свойства'),
(35, 4, 'Свойства'),
(36, 4, 'Свойства'),
(37, 4, 'Свойства'),
(38, 1, 'Свойства'),
(39, 1, 'Свойства'),
(40, 1, 'Свойства'),
(41, 1, 'Свойства'),
(42, 1, 'Свойства'),
(43, 1, 'Свойства'),
(44, 1, 'Свойства'),
(45, 1, 'Свойства'),
(46, 1, 'Свойства'),
(47, 1, 'Свойства'),
(48, 1, 'Свойства'),
(49, 1, 'Свойства'),
(50, 1, 'Свойства'),
(51, 1, 'Свойства'),
(52, 1, 'Свойства'),
(53, 1, 'Свойства'),
(54, 1, 'Свойства'),
(55, 1, 'Свойства'),
(56, 1, 'Свойства'),
(57, 1, 'Свойства'),
(58, 1, 'Свойства'),
(59, 1, 'Свойства'),
(60, 1, 'Свойства'),
(61, 1, 'Свойства'),
(62, 1, 'Свойства'),
(63, 1, 'Свойства'),
(64, 1, 'Свойства'),
(65, 1, 'Свойства'),
(66, 1, 'Свойства'),
(67, 1, 'Свойства');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_attribute_to_1c`
--

CREATE TABLE `oc_attribute_to_1c` (
  `attribute_id` int(11) NOT NULL,
  `1c_attribute_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_attribute_to_1c`
--

INSERT INTO `oc_attribute_to_1c` (`attribute_id`, `1c_attribute_id`) VALUES
(19, '6ec6fd07-ef28-11e4-826f-dc85deb034aa');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'Продукция HP', 1),
(7, 'Слайдшоу на главной', 1),
(8, 'Производители', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `link`, `image`, `image1`, `sort_order`) VALUES
(87, 6, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', '', 0),
(117, 8, '', 'catalog/manufacturer/bitmap_7.png', '', 0),
(116, 8, '', 'catalog/manufacturer/bitmap_12.png', '', 0),
(115, 8, '', 'catalog/manufacturer/bitmap_6.png', '', 0),
(114, 8, '', 'catalog/manufacturer/bitmap_9.png', '', 0),
(113, 8, '', 'catalog/manufacturer/bitmap_8.png', '', 0),
(112, 8, '', 'catalog/manufacturer/bitmap_7.png', '', 0),
(111, 8, '', 'catalog/manufacturer/bitmap_6.png', '', 0),
(110, 8, '', 'catalog/manufacturer/bitmap_5.png', '', 0),
(109, 8, '', 'catalog/manufacturer/bitmap_12.png', '', 0),
(108, 8, '', 'catalog/manufacturer/bitmap_11.png', '', 0),
(154, 7, '', 'catalog/bitmap3.jpg', 'catalog/bitmap.jpg', 3),
(153, 7, '', 'catalog/bitmap2.jpg', 'catalog/bitmap.jpg', 2),
(152, 7, '', 'catalog/bitmap1.jpg', 'catalog/bitmap.jpg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_banner_image_description`
--

CREATE TABLE `oc_banner_image_description` (
  `banner_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_banner_image_description`
--

INSERT INTO `oc_banner_image_description` (`banner_image_id`, `language_id`, `banner_id`, `title`) VALUES
(87, 1, 6, 'HP Banner'),
(115, 3, 8, 'pepe jecma'),
(115, 1, 8, 'pepe jecma'),
(114, 2, 8, 'mango'),
(114, 3, 8, 'mango'),
(114, 1, 8, 'mango'),
(113, 2, 8, 'tape a loeil'),
(113, 3, 8, 'tape a loeil'),
(113, 1, 8, 'tape a loeil'),
(87, 2, 6, 'HP Banner'),
(112, 2, 8, 'jamiks'),
(112, 3, 8, 'jamiks'),
(112, 1, 8, 'jamiks'),
(111, 2, 8, 'pepe jecma'),
(111, 3, 8, 'pepe jecma'),
(111, 1, 8, 'pepe jecma'),
(110, 2, 8, 'g-star raw'),
(87, 3, 6, 'HP Banner'),
(110, 3, 8, 'g-star raw'),
(110, 1, 8, 'g-star raw'),
(109, 2, 8, 'quksilver'),
(109, 3, 8, 'quksilver'),
(109, 1, 8, 'quksilver'),
(108, 2, 8, 'parfoais'),
(108, 3, 8, 'parfoais'),
(108, 1, 8, 'parfoais'),
(154, 4, 7, '   '),
(154, 2, 7, '   '),
(154, 3, 7, '   '),
(154, 1, 7, '   '),
(115, 2, 8, 'pepe jecma'),
(116, 1, 8, 'quksilver'),
(116, 3, 8, 'quksilver'),
(116, 2, 8, 'quksilver'),
(117, 1, 8, 'jamiks'),
(117, 3, 8, 'jamiks'),
(117, 2, 8, 'jamiks'),
(87, 4, 6, 'HP Banner'),
(115, 4, 8, 'pepe jecma'),
(114, 4, 8, 'mango'),
(113, 4, 8, 'tape a loeil'),
(153, 4, 7, '   '),
(112, 4, 8, 'jamiks'),
(111, 4, 8, 'pepe jecma'),
(110, 4, 8, 'g-star raw'),
(153, 2, 7, '   '),
(109, 4, 8, 'quksilver'),
(108, 4, 8, 'parfoais'),
(153, 3, 7, '   '),
(153, 1, 7, '   '),
(116, 4, 8, 'quksilver'),
(117, 4, 8, 'jamiks'),
(152, 4, 7, '   '),
(152, 2, 7, '   '),
(152, 3, 7, '   '),
(152, 1, 7, '   ');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_cart`
--

INSERT INTO `oc_cart` (`cart_id`, `customer_id`, `session_id`, `product_id`, `recurring_id`, `option`, `quantity`, `date_added`) VALUES
(45, 1, 'p8ejdpuojgljjsj1l14rlp5rp7', 56, 0, '[]', 1, '2017-08-08 15:48:19'),
(62, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 60, 0, '[]', 3, '2017-08-22 13:17:19'),
(63, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 2185, 0, '{"7984":"17677","7985":"17683"}', 1, '2017-08-22 13:17:19'),
(64, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 2185, 0, '{"7984":"17677","7985":"17684"}', 2, '2017-08-22 13:17:19'),
(65, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 2185, 0, '{"7984":"17677","7985":"17685"}', 3, '2017-08-22 13:17:19'),
(66, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 2185, 0, '{"7984":"17677","7985":"17686"}', 1, '2017-08-22 13:17:19'),
(67, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 2185, 0, '{"7984":"17677","7985":"17681"}', 1, '2017-08-22 13:17:19'),
(69, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 2183, 0, '{"7980":"17654","7981":"17656"}', 1, '2017-08-23 11:02:57'),
(71, 25, 'p8ejdpuojgljjsj1l14rlp5rp7', 2152, 0, '{"7932":"17476","7933":"17478"}', 1, '2017-08-23 11:17:57');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(67, '', 0, 1, 1, 0, 1, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(59, '', 0, 0, 1, 0, 1, '2017-08-29 11:11:44', '2017-08-29 16:19:35'),
(68, '', 0, 1, 1, 0, 1, '2017-08-31 18:59:32', '2017-08-31 18:59:32');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES
(67, 1, 'ШАПКИ Осень-зима 2015', '', '', '', '', ''),
(59, 1, 'Все', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(59, 3, 'Все', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(59, 2, 'Все', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(59, 4, 'Все', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(68, 1, '1123', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(68, 3, '1123', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(68, 2, '1123', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(68, 4, '1123', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(67, 67, 0),
(59, 59, 0),
(68, 68, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_1c`
--

CREATE TABLE `oc_category_to_1c` (
  `category_id` int(11) NOT NULL,
  `1c_category_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_to_1c`
--

INSERT INTO `oc_category_to_1c` (`category_id`, `1c_category_id`) VALUES
(67, 'a64672d0-3c16-11e5-8a4f-00ac06be3045');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(59, 0, 0),
(68, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(59, 0),
(67, 0),
(68, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Афганистан', 'AF', 'AFG', '', 0, 1),
(2, 'Албания', 'AL', 'ALB', '', 0, 1),
(3, 'Алжир', 'DZ', 'DZA', '', 0, 1),
(4, 'Восточное Самоа', 'AS', 'ASM', '', 0, 1),
(5, 'Андорра', 'AD', 'AND', '', 0, 1),
(6, 'Ангола', 'AO', 'AGO', '', 0, 1),
(7, 'Ангилья', 'AI', 'AIA', '', 0, 1),
(8, 'Антарктида', 'AQ', 'ATA', '', 0, 1),
(9, 'Антигуа и Барбуда', 'AG', 'ATG', '', 0, 1),
(10, 'Аргентина', 'AR', 'ARG', '', 0, 1),
(11, 'Армения', 'AM', 'ARM', '', 0, 1),
(12, 'Аруба', 'AW', 'ABW', '', 0, 1),
(13, 'Австралия', 'AU', 'AUS', '', 0, 1),
(14, 'Австрия', 'AT', 'AUT', '', 0, 1),
(15, 'Азербайджан', 'AZ', 'AZE', '', 0, 1),
(16, 'Багамские острова', 'BS', 'BHS', '', 0, 1),
(17, 'Бахрейн', 'BH', 'BHR', '', 0, 1),
(18, 'Бангладеш', 'BD', 'BGD', '', 0, 1),
(19, 'Барбадос', 'BB', 'BRB', '', 0, 1),
(20, 'Белоруссия (Беларусь)', 'BY', 'BLR', '', 0, 1),
(21, 'Бельгия', 'BE', 'BEL', '', 0, 1),
(22, 'Белиз', 'BZ', 'BLZ', '', 0, 1),
(23, 'Бенин', 'BJ', 'BEN', '', 0, 1),
(24, 'Бермудские острова', 'BM', 'BMU', '', 0, 1),
(25, 'Бутан', 'BT', 'BTN', '', 0, 1),
(26, 'Боливия', 'BO', 'BOL', '', 0, 1),
(27, 'Босния и Герцеговина', 'BA', 'BIH', '', 0, 1),
(28, 'Ботсвана', 'BW', 'BWA', '', 0, 1),
(29, 'Остров Буве', 'BV', 'BVT', '', 0, 1),
(30, 'Бразилия', 'BR', 'BRA', '', 0, 1),
(31, 'Британская территория в Индийском океане', 'IO', 'IOT', '', 0, 1),
(32, 'Бруней', 'BN', 'BRN', '', 0, 1),
(33, 'Болгария', 'BG', 'BGR', '', 0, 1),
(34, 'Буркина-Фасо', 'BF', 'BFA', '', 0, 1),
(35, 'Бурунди', 'BI', 'BDI', '', 0, 1),
(36, 'Камбоджа', 'KH', 'KHM', '', 0, 1),
(37, 'Камерун', 'CM', 'CMR', '', 0, 1),
(38, 'Канада', 'CA', 'CAN', '', 0, 1),
(39, 'Кабо-Верде', 'CV', 'CPV', '', 0, 1),
(40, 'Каймановы острова', 'KY', 'CYM', '', 0, 1),
(41, 'Центрально-Африканская Республика', 'CF', 'CAF', '', 0, 1),
(42, 'Чад', 'TD', 'TCD', '', 0, 1),
(43, 'Чили', 'CL', 'CHL', '', 0, 1),
(44, 'Китайская Народная Республика', 'CN', 'CHN', '', 0, 1),
(45, 'Остров Рождества', 'CX', 'CXR', '', 0, 1),
(46, 'Кокосовые острова', 'CC', 'CCK', '', 0, 1),
(47, 'Колумбия', 'CO', 'COL', '', 0, 1),
(48, 'Коморские острова', 'KM', 'COM', '', 0, 1),
(49, 'Конго', 'CG', 'COG', '', 0, 1),
(50, 'Острова Кука', 'CK', 'COK', '', 0, 1),
(51, 'Коста-Рика', 'CR', 'CRI', '', 0, 1),
(52, 'Кот д\'Ивуар', 'CI', 'CIV', '', 0, 1),
(53, 'Хорватия', 'HR', 'HRV', '', 0, 1),
(54, 'Куба', 'CU', 'CUB', '', 0, 1),
(55, 'Кипр', 'CY', 'CYP', '', 0, 1),
(56, 'Чехия', 'CZ', 'CZE', '', 0, 1),
(57, 'Дания', 'DK', 'DNK', '', 0, 1),
(58, 'Джибути', 'DJ', 'DJI', '', 0, 1),
(59, 'Доминика', 'DM', 'DMA', '', 0, 1),
(60, 'Доминиканская Республика', 'DO', 'DOM', '', 0, 1),
(61, 'Восточный Тимор', 'TP', 'TMP', '', 0, 1),
(62, 'Эквадор', 'EC', 'ECU', '', 0, 1),
(63, 'Египет', 'EG', 'EGY', '', 0, 1),
(64, 'Сальвадор', 'SV', 'SLV', '', 0, 1),
(65, 'Экваториальная Гвинея', 'GQ', 'GNQ', '', 0, 1),
(66, 'Эритрея', 'ER', 'ERI', '', 0, 1),
(67, 'Эстония', 'EE', 'EST', '', 0, 1),
(68, 'Эфиопия', 'ET', 'ETH', '', 0, 1),
(69, 'Фолклендские (Мальвинские) острова', 'FK', 'FLK', '', 0, 1),
(70, 'Фарерские острова', 'FO', 'FRO', '', 0, 1),
(71, 'Фиджи', 'FJ', 'FJI', '', 0, 1),
(72, 'Финляндия', 'FI', 'FIN', '', 0, 1),
(73, 'Франция', 'FR', 'FRA', '', 0, 1),
(74, 'Франция, Метрополия', 'FX', 'FXX', '', 0, 1),
(75, 'Французская Гвиана', 'GF', 'GUF', '', 0, 1),
(76, 'Французская Полинезия', 'PF', 'PYF', '', 0, 1),
(77, 'Французские Южные территории', 'TF', 'ATF', '', 0, 1),
(78, 'Габон', 'GA', 'GAB', '', 0, 1),
(79, 'Гамбия', 'GM', 'GMB', '', 0, 1),
(80, 'Грузия', 'GE', 'GEO', '', 0, 1),
(81, 'Германия', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(82, 'Гана', 'GH', 'GHA', '', 0, 1),
(83, 'Гибралтар', 'GI', 'GIB', '', 0, 1),
(84, 'Греция', 'GR', 'GRC', '', 0, 1),
(85, 'Гренландия', 'GL', 'GRL', '', 0, 1),
(86, 'Гренада', 'GD', 'GRD', '', 0, 1),
(87, 'Гваделупа', 'GP', 'GLP', '', 0, 1),
(88, 'Гуам', 'GU', 'GUM', '', 0, 1),
(89, 'Гватемала', 'GT', 'GTM', '', 0, 1),
(90, 'Гвинея', 'GN', 'GIN', '', 0, 1),
(91, 'Гвинея-Бисау', 'GW', 'GNB', '', 0, 1),
(92, 'Гайана', 'GY', 'GUY', '', 0, 1),
(93, 'Гаити', 'HT', 'HTI', '', 0, 1),
(94, 'Херд и Макдональд, острова', 'HM', 'HMD', '', 0, 1),
(95, 'Гондурас', 'HN', 'HND', '', 0, 1),
(96, 'Гонконг', 'HK', 'HKG', '', 0, 1),
(97, 'Венгрия', 'HU', 'HUN', '', 0, 1),
(98, 'Исландия', 'IS', 'ISL', '', 0, 1),
(99, 'Индия', 'IN', 'IND', '', 0, 1),
(100, 'Индонезия', 'ID', 'IDN', '', 0, 1),
(101, 'Иран', 'IR', 'IRN', '', 0, 1),
(102, 'Ирак', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ирландия', 'IE', 'IRL', '', 0, 1),
(104, 'Израиль', 'IL', 'ISR', '', 0, 1),
(105, 'Италия', 'IT', 'ITA', '', 0, 1),
(106, 'Ямайка', 'JM', 'JAM', '', 0, 1),
(107, 'Япония', 'JP', 'JPN', '', 0, 1),
(108, 'Иордания', 'JO', 'JOR', '', 0, 1),
(109, 'Казахстан', 'KZ', 'KAZ', '', 0, 1),
(110, 'Кения', 'KE', 'KEN', '', 0, 1),
(111, 'Кирибати', 'KI', 'KIR', '', 0, 1),
(112, 'Корейская Народно-Демократическая Республика', 'KP', 'PRK', '', 0, 1),
(113, 'Республика Корея', 'KR', 'KOR', '', 0, 1),
(114, 'Кувейт', 'KW', 'KWT', '', 0, 1),
(115, 'Киргизия (Кыргызстан)', 'KG', 'KGZ', '', 0, 1),
(116, 'Лаос', 'LA', 'LAO', '', 0, 1),
(117, 'Латвия', 'LV', 'LVA', '', 0, 1),
(118, 'Ливан', 'LB', 'LBN', '', 0, 1),
(119, 'Лесото', 'LS', 'LSO', '', 0, 1),
(120, 'Либерия', 'LR', 'LBR', '', 0, 1),
(121, 'Ливия', 'LY', 'LBY', '', 0, 1),
(122, 'Лихтенштейн', 'LI', 'LIE', '', 0, 1),
(123, 'Литва', 'LT', 'LTU', '', 0, 1),
(124, 'Люксембург', 'LU', 'LUX', '', 0, 1),
(125, 'Макао', 'MO', 'MAC', '', 0, 1),
(126, 'Македония', 'MK', 'MKD', '', 0, 1),
(127, 'Мадагаскар', 'MG', 'MDG', '', 0, 1),
(128, 'Малави', 'MW', 'MWI', '', 0, 1),
(129, 'Малайзия', 'MY', 'MYS', '', 0, 1),
(130, 'Мальдивы', 'MV', 'MDV', '', 0, 1),
(131, 'Мали', 'ML', 'MLI', '', 0, 1),
(132, 'Мальта', 'MT', 'MLT', '', 0, 1),
(133, 'Маршалловы острова', 'MH', 'MHL', '', 0, 1),
(134, 'Мартиника', 'MQ', 'MTQ', '', 0, 1),
(135, 'Мавритания', 'MR', 'MRT', '', 0, 1),
(136, 'Маврикий', 'MU', 'MUS', '', 0, 1),
(137, 'Майотта', 'YT', 'MYT', '', 0, 1),
(138, 'Мексика', 'MX', 'MEX', '', 0, 1),
(139, 'Микронезия', 'FM', 'FSM', '', 0, 1),
(140, 'Молдова', 'MD', 'MDA', '', 0, 1),
(141, 'Монако', 'MC', 'MCO', '', 0, 1),
(142, 'Монголия', 'MN', 'MNG', '', 0, 1),
(143, 'Монтсеррат', 'MS', 'MSR', '', 0, 1),
(144, 'Марокко', 'MA', 'MAR', '', 0, 1),
(145, 'Мозамбик', 'MZ', 'MOZ', '', 0, 1),
(146, 'Мьянма', 'MM', 'MMR', '', 0, 1),
(147, 'Намибия', 'NA', 'NAM', '', 0, 1),
(148, 'Науру', 'NR', 'NRU', '', 0, 1),
(149, 'Непал', 'NP', 'NPL', '', 0, 1),
(150, 'Нидерланды', 'NL', 'NLD', '', 0, 1),
(151, 'Антильские (Нидерландские) острова', 'AN', 'ANT', '', 0, 1),
(152, 'Новая Каледония', 'NC', 'NCL', '', 0, 1),
(153, 'Новая Зеландия', 'NZ', 'NZL', '', 0, 1),
(154, 'Никарагуа', 'NI', 'NIC', '', 0, 1),
(155, 'Нигер', 'NE', 'NER', '', 0, 1),
(156, 'Нигерия', 'NG', 'NGA', '', 0, 1),
(157, 'Ниуэ', 'NU', 'NIU', '', 0, 1),
(158, 'Остров Норфолк', 'NF', 'NFK', '', 0, 1),
(159, 'Северные Марианские острова', 'MP', 'MNP', '', 0, 1),
(160, 'Норвегия', 'NO', 'NOR', '', 0, 1),
(161, 'Оман', 'OM', 'OMN', '', 0, 1),
(162, 'Пакистан', 'PK', 'PAK', '', 0, 1),
(163, 'Палау', 'PW', 'PLW', '', 0, 1),
(164, 'Панама', 'PA', 'PAN', '', 0, 1),
(165, 'Папуа - Новая Гвинея', 'PG', 'PNG', '', 0, 1),
(166, 'Парагвай', 'PY', 'PRY', '', 0, 1),
(167, 'Перу', 'PE', 'PER', '', 0, 1),
(168, 'Филиппины', 'PH', 'PHL', '', 0, 1),
(169, 'Острова Питкэрн', 'PN', 'PCN', '', 0, 1),
(170, 'Польша', 'PL', 'POL', '', 0, 1),
(171, 'Португалия', 'PT', 'PRT', '', 0, 1),
(172, 'Пуэрто-Рико', 'PR', 'PRI', '', 0, 1),
(173, 'Катар', 'QA', 'QAT', '', 0, 1),
(174, 'Реюньон', 'RE', 'REU', '', 0, 1),
(175, 'Румыния', 'RO', 'ROM', '', 0, 1),
(176, 'Российская Федерация', 'RU', 'RUS', '', 0, 1),
(177, 'Руанда', 'RW', 'RWA', '', 0, 1),
(178, 'Сент-Китс и Невис', 'KN', 'KNA', '', 0, 1),
(179, 'Сент-Люсия', 'LC', 'LCA', '', 0, 1),
(180, 'Сент-Винсент и Гренадины', 'VC', 'VCT', '', 0, 1),
(181, 'Западное Самоа', 'WS', 'WSM', '', 0, 1),
(182, 'Сан-Марино', 'SM', 'SMR', '', 0, 1),
(183, 'Сан-Томе и Принсипи', 'ST', 'STP', '', 0, 1),
(184, 'Саудовская Аравия', 'SA', 'SAU', '', 0, 1),
(185, 'Сенегал', 'SN', 'SEN', '', 0, 1),
(186, 'Сейшельские острова', 'SC', 'SYC', '', 0, 1),
(187, 'Сьерра-Леоне', 'SL', 'SLE', '', 0, 1),
(188, 'Сингапур', 'SG', 'SGP', '', 0, 1),
(189, 'Словакия', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Словения', 'SI', 'SVN', '', 0, 1),
(191, 'Соломоновы острова', 'SB', 'SLB', '', 0, 1),
(192, 'Сомали', 'SO', 'SOM', '', 0, 1),
(193, 'Южно-Африканская Республика', 'ZA', 'ZAF', '', 0, 1),
(194, 'Южная Джорджия и Южные Сандвичевы острова', 'GS', 'SGS', '', 0, 1),
(195, 'Испания', 'ES', 'ESP', '', 0, 1),
(196, 'Шри-Ланка', 'LK', 'LKA', '', 0, 1),
(197, 'Остров Святой Елены', 'SH', 'SHN', '', 0, 1),
(198, 'Сен-Пьер и Микелон', 'PM', 'SPM', '', 0, 1),
(199, 'Судан', 'SD', 'SDN', '', 0, 1),
(200, 'Суринам', 'SR', 'SUR', '', 0, 1),
(201, 'Шпицберген и Ян Майен', 'SJ', 'SJM', '', 0, 1),
(202, 'Свазиленд', 'SZ', 'SWZ', '', 0, 1),
(203, 'Швеция', 'SE', 'SWE', '', 0, 1),
(204, 'Швейцария', 'CH', 'CHE', '', 0, 1),
(205, 'Сирия', 'SY', 'SYR', '', 0, 1),
(206, 'Тайвань (провинция Китая)', 'TW', 'TWN', '', 0, 1),
(207, 'Таджикистан', 'TJ', 'TJK', '', 0, 1),
(208, 'Танзания', 'TZ', 'TZA', '', 0, 1),
(209, 'Таиланд', 'TH', 'THA', '', 0, 1),
(210, 'Того', 'TG', 'TGO', '', 0, 1),
(211, 'Токелау', 'TK', 'TKL', '', 0, 1),
(212, 'Тонга', 'TO', 'TON', '', 0, 1),
(213, 'Тринидад и Тобаго', 'TT', 'TTO', '', 0, 1),
(214, 'Тунис', 'TN', 'TUN', '', 0, 1),
(215, 'Турция', 'TR', 'TUR', '', 0, 1),
(216, 'Туркменистан', 'TM', 'TKM', '', 0, 1),
(217, 'Острова Теркс и Кайкос', 'TC', 'TCA', '', 0, 1),
(218, 'Тувалу', 'TV', 'TUV', '', 0, 1),
(219, 'Уганда', 'UG', 'UGA', '', 0, 1),
(220, 'Украина', 'UA', 'UKR', '', 0, 1),
(221, 'Объединенные Арабские Эмираты', 'AE', 'ARE', '', 0, 1),
(222, 'Великобритания', 'GB', 'GBR', '', 1, 1),
(223, 'Соединенные Штаты Америки', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'Мелкие отдаленные острова США', 'UM', 'UMI', '', 0, 1),
(225, 'Уругвай', 'UY', 'URY', '', 0, 1),
(226, 'Узбекистан', 'UZ', 'UZB', '', 0, 1),
(227, 'Вануату', 'VU', 'VUT', '', 0, 1),
(228, 'Ватикан', 'VA', 'VAT', '', 0, 1),
(229, 'Венесуэла', 'VE', 'VEN', '', 0, 1),
(230, 'Вьетнам', 'VN', 'VNM', '', 0, 1),
(231, 'Виргинские острова (Британские)', 'VG', 'VGB', '', 0, 1),
(232, 'Виргинские острова (США)', 'VI', 'VIR', '', 0, 1),
(233, 'Уоллис и Футуна', 'WF', 'WLF', '', 0, 1),
(234, 'Западная Сахара', 'EH', 'ESH', '', 0, 1),
(235, 'Йемен', 'YE', 'YEM', '', 0, 1),
(236, 'Сербия и Черногория', 'CS', 'SCG', '', 0, 1),
(237, 'Заир', 'ZR', 'ZAR', '', 0, 1),
(238, 'Замбия', 'ZM', 'ZMB', '', 0, 1),
(239, 'Зимбабве', 'ZW', 'ZWE', '', 0, 1),
(242, 'Черногория', 'ME', 'MNE', '', 0, 1),
(243, 'Сербия', 'RS', 'SRB', '', 0, 1),
(244, 'Аландские острова', 'AX', 'ALA', '', 0, 1),
(245, 'Бонайре, Синт-Эстатиус и Саба', 'BQ', 'BES', '', 0, 1),
(246, 'Кюрасао', 'CW', 'CUW', '', 0, 1),
(247, 'Палестинская территория, оккупированная', 'PS', 'PSE', '', 0, 1),
(248, 'Южный Судан', 'SS', 'SSD', '', 0, 1),
(249, 'Санкт-Бартелеми', 'BL', 'BLM', '', 0, 1),
(250, 'Санкт-Мартин (французская часть)', 'MF', 'MAF', '', 0, 1),
(251, 'Канарские Острова', 'IC', 'ICA', '', 0, 1),
(252, 'Остров Вознесения (Великобритания)', 'AC', 'ASC', '', 0, 1),
(253, 'Косово, Республика', 'XK', 'UNK', '', 0, 1),
(254, 'Остров Мэн', 'IM', 'IMN', '', 0, 1),
(255, 'Тристан-да-Кунья', 'TA', 'SHN', '', 0, 1),
(256, 'Остров Гернси', 'GG', 'GGY', '', 0, 1),
(257, 'Остров Джерси', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(10) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(7, 'Скидка 5%', '1123', 'P', '5.0000', 0, 0, '1.0000', '2017-08-14', '2018-12-16', 123, '123', 1, '2017-08-15 14:32:13');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Рубль', 'RUB', '', ' р.', '', 1.00000000, 1, '2017-08-31 14:55:33'),
(2, 'US Dollar', 'USD', '$', '', '', 0.01720000, 1, '2017-08-31 14:55:33'),
(3, 'Euro', 'EUR', '', '€', '', 0.01440000, 1, '2017-08-31 14:55:33'),
(4, 'Гривня', 'UAH', '', 'UAH', '', 0.44130000, 1, '2017-08-31 14:55:33'),
(5, 'Злоти', 'PLN', '', 'zł', '', 0.06110000, 1, '2017-08-31 14:55:33');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `approved`, `safe`, `token`, `date_added`) VALUES
(29, 1, 0, 'TEST', 'TEST', 'melnykserhiy1@gmail.com', '+38 (111) 111 11 11', '', '10c8a4b204927012e4f5ff3c45cc6c36aaa1d4ef', '6346ba889', NULL, NULL, 0, 0, '', '127.0.0.1', 1, 1, 0, '', '2017-08-31 18:03:36');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_activity`
--

INSERT INTO `oc_customer_activity` (`activity_id`, `customer_id`, `key`, `data`, `ip`, `date_added`) VALUES
(1, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-03 13:06:20'),
(2, 2, 'register', '{"customer_id":"2","name":"TEST TEST"}', '127.0.0.1', '2017-08-03 14:42:20'),
(3, 3, 'register', '{"customer_id":"3","name":"TEST TEST"}', '127.0.0.1', '2017-08-03 14:43:11'),
(4, 4, 'register', '{"customer_id":"4","name":"TEST TEST"}', '127.0.0.1', '2017-08-03 14:45:05'),
(5, 5, 'register', '{"customer_id":"5","name":"TEST TEST"}', '127.0.0.1', '2017-08-03 14:46:18'),
(6, 6, 'register', '{"customer_id":"6","name":null}', '127.0.0.1', '2017-08-03 14:57:09'),
(7, 0, 'register', '{"customer_id":"","name":null}', '127.0.0.1', '2017-08-03 14:57:38'),
(8, 8, 'register', '{"customer_id":"8","name":null}', '127.0.0.1', '2017-08-03 15:21:26'),
(9, 9, 'register', '{"customer_id":"9","name":null}', '127.0.0.1', '2017-08-03 15:22:09'),
(10, 0, 'register', '{"customer_id":null,"name":null}', '127.0.0.1', '2017-08-03 15:24:00'),
(11, 11, 'register', '{"customer_id":"11","name":null}', '127.0.0.1', '2017-08-03 15:26:52'),
(12, 0, 'register', '{"customer_id":null,"name":null}', '127.0.0.1', '2017-08-03 15:30:10'),
(13, 13, 'register', '{"customer_id":"13","name":null}', '127.0.0.1', '2017-08-03 15:42:53'),
(14, 0, 'register', '{"customer_id":null,"name":null}', '127.0.0.1', '2017-08-03 15:53:00'),
(15, 15, 'register', '{"customer_id":"15","name":null}', '127.0.0.1', '2017-08-03 15:53:48'),
(16, 16, 'register', '{"customer_id":"16","name":null}', '127.0.0.1', '2017-08-03 15:54:47'),
(17, 17, 'register', '{"customer_id":"17","name":null}', '127.0.0.1', '2017-08-03 15:55:50'),
(18, 0, 'register', '{"customer_id":null,"name":null}', '127.0.0.1', '2017-08-03 15:57:49'),
(19, 0, 'register', '{"customer_id":null,"name":null}', '127.0.0.1', '2017-08-03 16:05:14'),
(20, 0, 'register', '{"customer_id":null,"name":null}', '127.0.0.1', '2017-08-03 16:07:42'),
(21, 0, 'register', '{"customer_id":null,"name":"TEST"}', '127.0.0.1', '2017-08-03 16:11:14'),
(22, 0, 'register', '{"customer_id":null,"name":"TEST"}', '127.0.0.1', '2017-08-03 16:13:39'),
(23, 0, 'register', '{"customer_id":null,"name":"TEST"}', '127.0.0.1', '2017-08-03 16:14:40'),
(24, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-03 16:50:52'),
(25, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-03 17:53:28'),
(26, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-04 12:09:17'),
(27, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-04 12:31:50'),
(28, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-07 09:24:00'),
(29, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-07 09:25:50'),
(30, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-07 09:26:52'),
(31, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-07 09:27:21'),
(32, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-07 09:27:41'),
(33, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-07 09:28:20'),
(34, 0, 'order_guest', '{"name":"TEST TEST","order_id":1}', '127.0.0.1', '2017-08-08 15:39:58'),
(35, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-08 15:41:17'),
(36, 1, 'order_account', '{"customer_id":"1","name":"TEST TEST","order_id":2}', '127.0.0.1', '2017-08-08 15:41:34'),
(37, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-08 15:44:18'),
(38, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-08 15:46:28'),
(39, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-14 09:45:44'),
(40, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-14 10:04:15'),
(41, 1, 'login', '{"customer_id":"1","name":"TEST TEST"}', '127.0.0.1', '2017-08-14 10:05:08'),
(42, 24, 'register', '{"customer_id":"24","name":"\\u0421\\u0435\\u0440\\u0433\\u0435\\u0439 \\u041c\\u0435\\u043b\\u044c\\u043d\\u0438\\u043a"}', '127.0.0.1', '2017-08-14 10:05:30'),
(43, 25, 'register', '{"customer_id":"25","name":"\\u0421\\u0435\\u0440\\u0433\\u0435\\u0439 \\u041c\\u0435\\u043b\\u044c\\u043d\\u0438\\u043a"}', '127.0.0.1', '2017-08-14 11:31:52'),
(44, 25, 'login', '{"customer_id":"25","name":"\\u0421\\u0435\\u0440\\u0433\\u0435\\u0439 \\u041c\\u0435\\u043b\\u044c\\u043d\\u0438\\u043a"}', '127.0.0.1', '2017-08-16 13:16:52'),
(45, 25, 'login', '{"customer_id":"25","name":"\\u0421\\u0435\\u0440\\u0433\\u0435\\u0439 \\u041c\\u0435\\u043b\\u044c\\u043d\\u0438\\u043a"}', '127.0.0.1', '2017-08-22 13:17:19'),
(46, 25, 'login', '{"customer_id":"25","name":"\\u0421\\u0435\\u0440\\u0433\\u0435\\u0439 \\u041c\\u0435\\u043b\\u044c\\u043d\\u0438\\u043a"}', '127.0.0.1', '2017-08-23 11:02:57'),
(47, 25, 'login', '{"customer_id":"25","name":"\\u0421\\u0435\\u0440\\u0433\\u0435\\u0439 \\u041c\\u0435\\u043b\\u044c\\u043d\\u0438\\u043a"}', '127.0.0.1', '2017-08-23 11:17:57'),
(48, 26, 'register', '{"customer_id":"26","name":"TEST"}', '127.0.0.1', '2017-08-25 17:57:10'),
(49, 0, 'register', '{"customer_id":null,"name":"TEST"}', '127.0.0.1', '2017-08-25 17:57:34'),
(50, 28, 'register', '{"customer_id":"28","name":"TEST"}', '127.0.0.1', '2017-08-25 17:58:06'),
(51, 29, 'register', '{"customer_id":"29","name":"TEST"}', '127.0.0.1', '2017-08-31 18:03:36'),
(52, 29, 'login', '{"customer_id":"29","name":"TEST TEST"}', '127.0.0.1', '2017-08-31 18:13:31'),
(53, 0, 'order_guest', '{"name":"TEST TEST","order_id":4}', '127.0.0.1', '2017-08-31 18:33:37'),
(54, 0, 'order_guest', '{"name":"TEST TEST","order_id":5}', '127.0.0.1', '2017-08-31 18:34:32');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1),
(2, 1, 2),
(3, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 3, 'Default', ''),
(1, 1, 'Default', ''),
(2, 3, 'Я гуртовий клієнт', ''),
(2, 2, 'I am a wholesale buyer', ''),
(1, 2, 'Default', ''),
(2, 4, 'Jestem hurtowym nabywcą', ''),
(3, 1, 'uLogin', 'uLogin - группа, для зарегестрированных с помощью uLogin пользователей. Создана модулем uLogin.'),
(3, 3, 'uLogin', 'uLogin - группа, для зарегестрированных с помощью uLogin пользователей. Создана модулем uLogin.'),
(3, 2, 'uLogin', 'uLogin - группа, для зарегестрированных с помощью uLogin пользователей. Создана модулем uLogin.'),
(1, 4, 'Default', ''),
(2, 1, 'Я оптовый покупатель', ''),
(3, 4, 'uLogin', 'uLogin - группа, для зарегестрированных с помощью uLogin пользователей. Создана модулем uLogin.');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_ip`
--

INSERT INTO `oc_customer_ip` (`customer_ip_id`, `customer_id`, `ip`, `date_added`) VALUES
(15, 29, '127.0.0.1', '2017-08-31 18:03:36');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_simple_fields`
--

CREATE TABLE `oc_customer_simple_fields` (
  `customer_id` int(11) NOT NULL,
  `metadata` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_customer_wishlist`
--

INSERT INTO `oc_customer_wishlist` (`customer_id`, `product_id`, `date_added`) VALUES
(1, 56, '2017-08-08 15:46:46');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `location` varchar(7) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(3, 'total', 'sub_total'),
(35, 'module', 'ulogin_sets'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(9, 'shipping', 'flat'),
(38, 'module', 'related_options'),
(37, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(36, 'module', 'ulogin'),
(34, 'shipping', 'free'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'module', 'lang_editor'),
(21, 'module', 'mega_filter'),
(23, 'module', 'popup_purchase'),
(24, 'module', 'simple'),
(25, 'module', 'csvprice_pro'),
(26, 'module', 'ncategory'),
(27, 'module', 'news'),
(28, 'module', 'watermark'),
(29, 'module', 'faq'),
(30, 'module', 'newsletters'),
(31, 'module', 'latest'),
(32, 'module', 'html'),
(33, 'module', 'exchange1c');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_modified`, `date_added`) VALUES
(3, 'НДС', 'Облагаемые НДС', '2010-02-26 22:33:24', '2009-01-06 23:26:25');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 0, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1),
(7, 0, 0, 1),
(8, 0, 0, 1),
(9, 0, 0, 1),
(10, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES
(4, 3, 'Про компанію', '&lt;p&gt;\r\n	Про компанію&lt;/p&gt;\r\n', '', '', '', ''),
(5, 1, 'Условия соглашения', '&lt;p&gt;\r\n	Условия соглашения&lt;/p&gt;\r\n', '', '', '', ''),
(3, 1, 'Политика Безопасности', '&lt;p&gt;\r\n	Политика Безопасности&lt;/p&gt;\r\n', '', '', '', ''),
(6, 1, 'Информация о доставке', '&lt;p&gt;\r\n	Информация о доставке&lt;/p&gt;\r\n', '', '', '', ''),
(5, 2, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n', '', '', '', ''),
(3, 2, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n', '', '', '', ''),
(6, 2, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n', '', '', '', ''),
(4, 1, 'О компании', '&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp;&lt;/p&gt;\r\n', '', '', '', ''),
(5, 3, 'Условия соглашения', '&lt;p&gt;\r\n	Условия соглашения&lt;/p&gt;\r\n', '', '', '', ''),
(3, 3, 'Политика Безопасности', '&lt;p&gt;\r\n	Политика Безопасности&lt;/p&gt;\r\n', '', '', '', ''),
(6, 3, 'Информация о доставке', '&lt;p&gt;\r\n	Информация о доставке&lt;/p&gt;\r\n', '', '', '', ''),
(4, 2, 'About company', '&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp;&lt;/p&gt;\r\n', '', '', '', ''),
(7, 1, 'Сотрудничество', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(7, 3, 'Співпрацця', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(7, 2, 'Cooperation', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(8, 1, 'Адреса', '&lt;ul class=&quot;citys&quot;&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Хмельницкий&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Оазис”, ул. С. Бандеры, 2а&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Агора”, Староконстантиновское шосе, 2/1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Квартал”, ул. Камянецкая, 17&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;Оптовый склад - ул. Геологов, 10&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Киев&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Dreamtown2”, Оболонский пр-т, 21Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Skymall”, ул. генерала Ватутина, 2Т&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ ”Район”, ул. Лаврухина, 4&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Проспект”, ул. Красногвардейская, 1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Дарынок”, ул. Магниторская, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Чернигов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Голивуд”, ул. 77 г-ской дивизии, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Житомир&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Глобал”, ул. Киевская, 77&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Караван”, ул. Нижнеднепровская, 17&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Львов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТВК “Южный”, цоколь “Карпаты”, ул. Щирецкая, 36&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Victoria Gardens”, ул. Кульпарковская, 226А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                &lt;/ul&gt;', '', '', '', ''),
(8, 3, 'Адреса', '&lt;ul class=&quot;citys&quot;&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Хмельницкий&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Оазис”, ул. С. Бандеры, 2а&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Агора”, Староконстантиновское шосе, 2/1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Квартал”, ул. Камянецкая, 17&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;Оптовый склад - ул. Геологов, 10&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Киев&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Dreamtown2”, Оболонский пр-т, 21Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Skymall”, ул. генерала Ватутина, 2Т&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ ”Район”, ул. Лаврухина, 4&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Проспект”, ул. Красногвардейская, 1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Дарынок”, ул. Магниторская, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Чернигов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Голивуд”, ул. 77 г-ской дивизии, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Житомир&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Глобал”, ул. Киевская, 77&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Караван”, ул. Нижнеднепровская, 17&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Львов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТВК “Южный”, цоколь “Карпаты”, ул. Щирецкая, 36&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Victoria Gardens”, ул. Кульпарковская, 226А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                &lt;/ul&gt;', '', '', '', ''),
(8, 2, 'Адреса', '&lt;ul class=&quot;citys&quot;&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Хмельницкий&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Оазис”, ул. С. Бандеры, 2а&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Агора”, Староконстантиновское шосе, 2/1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Квартал”, ул. Камянецкая, 17&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;Оптовый склад - ул. Геологов, 10&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Киев&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Dreamtown2”, Оболонский пр-т, 21Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Skymall”, ул. генерала Ватутина, 2Т&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ ”Район”, ул. Лаврухина, 4&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Проспект”, ул. Красногвардейская, 1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Дарынок”, ул. Магниторская, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Чернигов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Голивуд”, ул. 77 г-ской дивизии, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Житомир&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Глобал”, ул. Киевская, 77&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Караван”, ул. Нижнеднепровская, 17&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Львов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТВК “Южный”, цоколь “Карпаты”, ул. Щирецкая, 36&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Victoria Gardens”, ул. Кульпарковская, 226А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                &lt;/ul&gt;', '', '', '', ''),
(9, 1, 'Отправка', '&lt;p&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap; color: inherit;&quot;&gt;Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;/p&gt;', '', '', '', ''),
(9, 3, 'Отправка', '&lt;p&gt;&lt;span style=&quot;color: inherit; font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt;Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;/p&gt;', '', '', '', ''),
(10, 1, 'Адреса футер ', '&lt;ul&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. С. Бандеры, 2а, ТРЦ “Оазис”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, Староконстантиновское шосе, 2/1Б, ТЦ “Агора”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. Камянецкая, 17, ТРЦ “Квартал”&lt;/li&gt;\r\n          &lt;/ul&gt;', '', '', '', ''),
(10, 3, 'Адреса футер ', '&lt;ul&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. С. Бандеры, 2а, ТРЦ “Оазис”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, Староконстантиновское шосе, 2/1Б, ТЦ “Агора”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. Камянецкая, 17, ТРЦ “Квартал”&lt;/li&gt;\r\n          &lt;/ul&gt;', '', '', '', ''),
(10, 2, 'Адреса футер ', '&lt;ul&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. С. Бандеры, 2а, ТРЦ “Оазис”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, Староконстантиновское шосе, 2/1Б, ТЦ “Агора”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. Камянецкая, 17, ТРЦ “Квартал”&lt;/li&gt;\r\n          &lt;/ul&gt;', '', '', '', ''),
(9, 2, 'Отправка', '&lt;p&gt;&lt;span style=&quot;color: inherit; font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt;Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;/p&gt;', '', '', '', ''),
(5, 4, 'Условия соглашения', '&lt;p&gt;\r\n	Условия соглашения&lt;/p&gt;\r\n', '', '', '', ''),
(3, 4, 'Политика Безопасности', '&lt;p&gt;\r\n	Политика Безопасности&lt;/p&gt;\r\n', '', '', '', ''),
(6, 4, 'Информация о доставке', '&lt;p&gt;\r\n	Информация о доставке&lt;/p&gt;\r\n', '', '', '', ''),
(4, 4, 'О компании', '&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp;&lt;/p&gt;\r\n', '', '', '', ''),
(7, 4, 'Сотрудничество', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(8, 4, 'Адреса', '&lt;ul class=&quot;citys&quot;&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Хмельницкий&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Оазис”, ул. С. Бандеры, 2а&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Агора”, Староконстантиновское шосе, 2/1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Квартал”, ул. Камянецкая, 17&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;Оптовый склад - ул. Геологов, 10&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Киев&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Dreamtown2”, Оболонский пр-т, 21Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Skymall”, ул. генерала Ватутина, 2Т&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ ”Район”, ул. Лаврухина, 4&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Проспект”, ул. Красногвардейская, 1Б&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТЦ “Дарынок”, ул. Магниторская, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Чернигов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Голивуд”, ул. 77 г-ской дивизии, 1А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Житомир&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Глобал”, ул. Киевская, 77&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Караван”, ул. Нижнеднепровская, 17&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                    &lt;li class=&quot;city&quot;&gt;\r\n                        &lt;div class=&quot;name_city&quot;&gt;Львов&lt;/div&gt;\r\n                        &lt;ul class=&quot;shops&quot;&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТВК “Южный”, цоколь “Карпаты”, ул. Щирецкая, 36&lt;/li&gt;\r\n                            &lt;li class=&quot;shop&quot;&gt;ТРЦ “Victoria Gardens”, ул. Кульпарковская, 226А&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/li&gt;\r\n                &lt;/ul&gt;', '', '', '', ''),
(9, 4, 'Отправка', '&lt;p&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap; color: inherit;&quot;&gt;Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;span style=&quot;font-family: Roboto, Helvetica, sans-serif; font-size: medium; white-space: pre-wrap;&quot;&gt; Отправка&lt;/span&gt;&lt;/p&gt;', '', '', '', ''),
(10, 4, 'Адреса футер ', '&lt;ul&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. С. Бандеры, 2а, ТРЦ “Оазис”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, Староконстантиновское шосе, 2/1Б, ТЦ “Агора”&lt;/li&gt;\r\n              &lt;li&gt;г. Хмельницкий, ул. Камянецкая, 17, ТРЦ “Квартал”&lt;/li&gt;\r\n          &lt;/ul&gt;', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(4, 0, 0),
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(10, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'Russian', 'ru', 'ru_RU.UTF-8,ru_RU,russian', 'ru.png', 'russian', 1, 1),
(2, 'English', 'en', 'en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 2, 1),
(3, 'Українська', 'ua', 'ua_UA.UTF-8,ua_UA,ua-ua,ukrainian', 'ua.png', 'ukrainian', 1, 1),
(4, 'Polski', 'pl', 'pl_PL.UTF-8,pl_PL,pl-pl,polish', 'pl.png', 'poland', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Главная'),
(2, 'Продукт'),
(3, 'Категория'),
(4, 'По умолчанию'),
(5, 'Производитель'),
(6, 'Аккаунт'),
(7, 'Оформление заказ'),
(8, 'Контакты'),
(9, 'Карта сайта'),
(10, 'Партнерская программа'),
(11, 'Информация'),
(12, 'Сравнение'),
(13, 'Поиск'),
(14, 'Mega Filter PRO'),
(15, 'Manufacturer info');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'affiliate', 'column_right', 1),
(131, 6, 'account', 'column_right', 1),
(133, 6, 'ulogin.35', 'content_top', 0),
(155, 3, 'mega_filter.1', 'content_top', 0),
(154, 1, 'slideshow.27', 'content_top', 1),
(153, 1, 'latest.33', 'content_top', 3),
(152, 1, 'html.34', 'content_bottom', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(17, 10, 0, 'affiliate/%'),
(65, 3, 0, 'product/category'),
(87, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search'),
(54, 14, 0, 'module/mega_filter/results'),
(55, 15, 0, 'product/manufacturer/info'),
(80, 6, 0, 'account/address'),
(79, 6, 0, 'account/password'),
(78, 6, 0, 'account/simpleedit'),
(77, 6, 0, 'account/account'),
(81, 6, 0, 'account/edit'),
(82, 6, 0, 'account/order');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00'),
(2, '10.00'),
(3, '0.39');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Сантиметр', 'см'),
(2, 1, 'Миллиметр', 'мм'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(1, 3, 'Сантиметр', 'см'),
(2, 3, 'Миллиметр', 'мм'),
(1, 4, 'Сантиметр', 'см'),
(2, 4, 'Миллиметр', 'мм');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(5, 'HTC', 'catalog/demo/htc_logo.jpg', 0),
(6, 'Palm', 'catalog/demo/palm_logo.jpg', 0),
(7, 'Hewlett-Packard', 'catalog/demo/hp_logo.jpg', 0),
(8, 'Apple', 'catalog/demo/apple_logo.jpg', 0),
(9, 'Canon', 'catalog/demo/canon_logo.jpg', 0),
(10, 'Sony', 'catalog/demo/sony_logo.jpg', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_description`
--

CREATE TABLE `oc_manufacturer_description` (
  `manufacturer_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_manufacturer_description`
--

INSERT INTO `oc_manufacturer_description` (`manufacturer_id`, `language_id`, `name`, `description`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES
(5, 1, 'HTC', '', 'HTC', '', '', ''),
(5, 2, 'HTC', '', 'HTC', '', '', ''),
(6, 1, 'Palm', '', 'Palm', '', '', ''),
(6, 2, 'Palm', '', 'Palm', '', '', ''),
(7, 1, 'Hewlett-Packard', '', 'Hewlett-Packard', '', '', ''),
(7, 2, 'Hewlett-Packard', '', 'Hewlett-Packard', '', '', ''),
(8, 1, 'Apple', '', 'Apple', '', '', ''),
(8, 2, 'Apple', '', 'Apple', '', '', ''),
(9, 1, 'Canon', 'Пример текста в описания производителя', 'Canon', '', '', ''),
(9, 2, 'Canon', 'Example of manufacturer description text', 'Canon', '', '', ''),
(10, 1, 'Sony', '', 'Sony', '', '', ''),
(10, 2, 'Sony', '', 'Sony', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_mfilter_url_alias`
--

CREATE TABLE `oc_mfilter_url_alias` (
  `mfilter_url_alias_id` int(11) UNSIGNED NOT NULL,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `mfp` text COLLATE utf8_unicode_ci NOT NULL,
  `alias` text COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(1, 'Local copy OCMOD by iSenseLabs', 'isensealabs_quickfix_ocmod', 'iSenseLabs', '1.3', 'http://isenselabs.com', '<modification>\r\n    <name>Local copy OCMOD by iSenseLabs</name>\r\n	<version>1.3</version>\r\n	<link>http://isenselabs.com</link>\r\n	<author>iSenseLabs</author>\r\n	<code>isensealabs_quickfix_ocmod</code>\r\n\r\n	<file path="admin/controller/extension/installer.php">\r\n		<operation error="skip">\r\n			<search ><![CDATA[\'url\'  => str_replace(\'&amp;\', \'&\', $this->url->link(\'extension/installer/ftp\', \'token=\' . $this->session->data[\'token\'],]]></search>\r\n			<add position="replace"><![CDATA[\'url\'  => str_replace(\'&amp;\', \'&\', $this->url->link(\'extension/installer/localcopy\', \'token=\' . $this->session->data[\'token\'],]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[public function unzip() {]]></search>\r\n			<add position="before"><![CDATA[			\r\n	public function localcopy() {\r\n		$this->load->language(\'extension/installer\');\r\n\r\n		$json = array();\r\n\r\n		if (!$this->user->hasPermission(\'modify\', \'extension/installer\')) {\r\n			$json[\'error\'] = $this->language->get(\'error_permission\');\r\n		}\r\n\r\n		if (VERSION == \'2.0.0.0\') {\r\n		    $directory = DIR_DOWNLOAD  . str_replace(array(\'../\', \'..\\\\\', \'..\'), \'\', $this->request->post[\'path\']) . \'/upload/\';\r\n		} else {\r\n		    $directory = DIR_UPLOAD  . str_replace(array(\'../\', \'..\\\\\', \'..\'), \'\', $this->request->post[\'path\']) . \'/upload/\';\r\n		}\r\n\r\n		if (!is_dir($directory)) {\r\n			$json[\'error\'] = $this->language->get(\'error_directory\');\r\n		}\r\n\r\n		if (!$json) {\r\n			// Get a list of files ready to upload\r\n			$files = array();\r\n\r\n			$path = array($directory . \'*\');\r\n\r\n			while (count($path) != 0) {\r\n				$next = array_shift($path);\r\n\r\n				foreach (glob($next) as $file) {\r\n					if (is_dir($file)) {\r\n						$path[] = $file . \'/*\';\r\n					}\r\n\r\n					$files[] = $file;\r\n				}\r\n			}\r\n\r\n			$root = dirname(DIR_APPLICATION).\'/\';\r\n\r\n			foreach ($files as $file) {\r\n				// Upload everything in the upload directory\r\n				$destination = substr($file, strlen($directory));\r\n\r\n				// Update from newer OpenCart versions:\r\n				if (substr($destination, 0, 5) == \'admin\') {\r\n					$destination = DIR_APPLICATION . substr($destination, 5);\r\n				} else if (substr($destination, 0, 7) == \'catalog\') {\r\n					$destination = DIR_CATALOG . substr($destination, 7);\r\n				} else if (substr($destination, 0, 5) == \'image\') {\r\n					$destination = DIR_IMAGE . substr($destination, 5);\r\n				} else if (substr($destination, 0, 6) == \'system\') {\r\n					$destination = DIR_SYSTEM . substr($destination, 6);\r\n				} else {\r\n					$destination = $root.$destination;\r\n				}\r\n\r\n				if (is_dir($file)) {\r\n					if (!file_exists($destination)) {\r\n						if (!mkdir($destination)) {\r\n							$json[\'error\'] = sprintf($this->language->get(\'error_ftp_directory\'), $destination);\r\n						}\r\n					}\r\n				}\r\n\r\n				if (is_file($file)) {\r\n					if (!copy($file, $destination)) {\r\n						$json[\'error\'] = sprintf($this->language->get(\'error_ftp_file\'), $file);\r\n					}\r\n				}\r\n			}\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}]]></add>\r\n		</operation>\r\n	</file>	\r\n</modification>\r\n', 1, '2016-11-06 22:05:30'),
(2, 'Easy Language Editor', 'ele14', 'sorcer', '1.4', 'http://opencart2.bom.lt', '﻿<?xml version="1.0" encoding="utf-8"?>\r\n<modification>\r\n    <name>Easy Language Editor</name>\r\n    <version>1.4</version>\r\n    <author>sorcer</author>\r\n    <link>http://opencart2.bom.lt</link>\r\n	<code>ele14</code>\r\n<file path="admin/controller/localisation/language.php">\r\n	<operation>\r\n	<search><![CDATA[if (isset($this->error[\'warning\'])) {]]></search>\r\n	<add position="before"><![CDATA[\r\n$data[\'text_select\'] = $this->language->get(\'text_select\');\r\n$data[\'easy_path\'] = DIR_APPLICATION . \'view/template/localisation/easy_langs/\';\r\n$dir = $data[\'easy_path\'];\r\n$listDir = array ();\r\nif ($handler = opendir ( $dir )) {\r\n	while ( ($sub = readdir ( $handler )) !== FALSE ) {\r\n		if ($sub != "." && $sub != ".." && $sub != "Thumb.db" && $sub != "Thumbs.db") {\r\n			if (is_file ( $dir . "/" . $sub )) {\r\n				$path_info = pathinfo ( $sub );\r\n				if ($path_info [\'extension\'] == \'php\')\r\n					$listDir [] = $sub;\r\n			}\r\n		}\r\n	}\r\n	closedir ( $handler );\r\n}\r\nasort($listDir);\r\n$data[\'easy_langs\'] = $listDir;\r\n	]]></add>\r\n	</operation>\r\n</file>\r\n\r\n<file path="admin/view/template/localisation/language_form.tpl">\r\n	<operation>\r\n	<search><![CDATA[id="form-language" class="form-horizontal">]]></search>\r\n	<add position="after"><![CDATA[\r\n		<div class="form-group">\r\n			<label class="col-sm-2 control-label" for="input-code"></label>\r\n			<div class="col-sm-10">\r\n				<select id="lang" class="form-control">\r\n					<option><?php echo $text_select; ?></option>\r\n					<?php foreach ($easy_langs as $lang){ ?>\r\n						<option value="<?php echo $lang; ?>"><?php echo ucwords(substr($lang, 0, strlen($lang)-4)); ?></option>\r\n					<?php } ?>\r\n				</select>\r\n			  <?php if ($error_code) { ?>\r\n			  <div class="text-danger"><?php echo $error_code; ?></div>\r\n			  <?php } ?>\r\n			</div>\r\n		</div>\r\n			<script type="text/javascript">\r\n				$(document).ready(function(){\r\n					$(\'#lang\').change(function(){\r\n						$.ajax({\r\n							type: \'POST\',\r\n							url: \'view/template/localisation/easy_langs/\' + $(this).val(),\r\n							dataType: \'json\',\r\n							success: function(lang){\r\n								$(\'input[name="name"]\').val(lang.name);\r\n								$(\'input[name="code"]\').val(lang.code);\r\n								$(\'input[name="locale"]\').val(lang.locale);\r\n								$(\'input[name="image"]\').val(lang.image);\r\n								$(\'input[name="directory"]\').val(lang.directory);\r\n								$(\'input[name="filename"]\').val(lang.filename);\r\n							}\r\n						});\r\n					});\r\n				});\r\n			</script>\r\n	]]></add>\r\n	</operation>\r\n</file>\r\n\r\n	\r\n</modification>', 1, '2016-11-06 22:06:35'),
(4, 'Shopencart News/Blog integration OCmod', 'shopencart-newsblog', 'Flo from shopencart.com', '1.1', 'http://shopencart.com', '<?xml version="1.0" encoding="UTF-8"?>\r\n<modification>\r\n	<name>Shopencart News/Blog integration OCmod</name>\r\n	<version>1.1</version>\r\n	<link>http://shopencart.com</link>\r\n	<author>Flo from shopencart.com</author>\r\n	<code>shopencart-newsblog</code>\r\n	<file path="admin/controller/common/menu.php">\r\n		<operation>\r\n			<search><![CDATA[$data[\'openbay_link_amazonus_links\'] = $this->url->link(\'openbay/amazonus/itemlinks\', \'token=\' . $this->session->data[\'token\'], \'SSL\');]]></search>\r\n			<add position="after"><![CDATA[\r\n			\r\n			$this->language->load(\'common/newspanel\');\r\n			\r\n			$data[\'nmod\'] = $this->url->link(\'module/news\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\r\n		\r\n			$data[\'ncmod\'] = $this->url->link(\'module/ncategory\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\r\n		\r\n			$data[\'npages\'] = $this->url->link(\'catalog/news\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\r\n		\r\n			$data[\'ncategory\'] = $this->url->link(\'catalog/ncategory\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\r\n		\r\n			$data[\'tocomments\'] = $this->url->link(\'catalog/ncomments\', \'token=\' . $this->session->data[\'token\'], \'SSL\');	\r\n		\r\n			$data[\'nauthor\'] = $this->url->link(\'catalog/nauthor\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\r\n		\r\n			$data[\'text_commod\'] = $this->language->get(\'text_commod\');\r\n		\r\n			$data[\'entry_npages\'] = $this->language->get(\'entry_npages\');\r\n		\r\n			$data[\'entry_nmod\'] = $this->language->get(\'entry_nmod\');\r\n		\r\n			$data[\'entry_ncmod\'] = $this->language->get(\'entry_ncmod\');\r\n		\r\n			$data[\'entry_ncategory\'] = $this->language->get(\'entry_ncategory\');\r\n		\r\n			$data[\'text_nauthor\'] = $this->language->get(\'text_nauthor\');\r\n		\r\n		]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="admin/view/template/common/menu.tpl">\r\n		<operation>\r\n			<search><![CDATA[<li id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>]]></search>\r\n			<add position="before"><![CDATA[  \r\n			<li id="blog"><a class="parent"><i class="fa fa-book fa-fw"></i> <span>News/Blog</span></a>\r\n				<ul>\r\n					<li><a href="<?php echo $npages; ?>"><?php echo $entry_npages; ?></a></li>\r\n					<li><a href="<?php echo $ncategory; ?>"><?php echo $entry_ncategory; ?></a></li>\r\n					<li><a href="<?php echo $tocomments; ?>"><?php echo $text_commod; ?></a></li>\r\n					<li><a href="<?php echo $nauthor; ?>"><?php echo $text_nauthor; ?></a></li>\r\n					<li><a href="<?php echo $nmod; ?>"><?php echo $entry_nmod; ?></a></li>\r\n					<li><a href="<?php echo $ncmod; ?>"><?php echo $entry_ncmod; ?></a></li>\r\n				</ul>\r\n			</li>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="admin/controller/common/dashboard.php">\r\n		<operation>\r\n			<search><![CDATA[$data[\'footer\'] = $this->load->controller(\'common/footer\');]]></search>\r\n			<add position="after"><![CDATA[$data[\'newspanel\'] = $this->load->controller(\'common/newspanel\');]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="admin/view/template/common/dashboard.tpl">\r\n		<operation>\r\n			<search><![CDATA[<?php if ($error_install) { ?>]]></search>\r\n			<add position="before"><![CDATA[  <?php echo $newspanel; ?>\r\n							<br />]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/controller/common/column_left.php">\r\n		<operation>\r\n			<search><![CDATA[$this->load->model(\'catalog/information\');]]></search>\r\n			<add position="after"><![CDATA[$this->load->model(\'catalog/news\');\r\n			$this->load->model(\'catalog/ncategory\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($route == \'product/category\' && isset($this->request->get[\'path\'])) {]]></search>\r\n			<add position="before"><![CDATA[\r\n			if ($route == \'news/article\' && isset($this->request->get[\'news_id\'])) {\r\n				$layout_id = $this->model_catalog_news->getNewsLayoutId($this->request->get[\'news_id\']);\r\n			}\r\n			if ($route == \'news/ncategory\' && isset($this->request->get[\'ncat\'])) {\r\n				$ncat = explode(\'_\', (string)$this->request->get[\'ncat\']);\r\n				\r\n				$layout_id = $this->model_catalog_ncategory->getncategoryLayoutId(end($ncat));			\r\n			}\r\n		]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/controller/common/column_right.php">\r\n		<operation>\r\n			<search><![CDATA[$this->load->model(\'catalog/information\');]]></search>\r\n			<add position="after"><![CDATA[$this->load->model(\'catalog/news\');\r\n			$this->load->model(\'catalog/ncategory\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($route == \'product/category\' && isset($this->request->get[\'path\'])) {]]></search>\r\n			<add position="before"><![CDATA[\r\n			if ($route == \'news/article\' && isset($this->request->get[\'news_id\'])) {\r\n				$layout_id = $this->model_catalog_news->getNewsLayoutId($this->request->get[\'news_id\']);\r\n			}\r\n			if ($route == \'news/ncategory\' && isset($this->request->get[\'ncat\'])) {\r\n				$ncat = explode(\'_\', (string)$this->request->get[\'ncat\']);\r\n				\r\n				$layout_id = $this->model_catalog_ncategory->getncategoryLayoutId(end($ncat));			\r\n			}\r\n		]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/controller/common/content_top.php">\r\n		<operation>\r\n			<search><![CDATA[$this->load->model(\'catalog/information\');]]></search>\r\n			<add position="after"><![CDATA[$this->load->model(\'catalog/news\');\r\n			$this->load->model(\'catalog/ncategory\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($route == \'product/category\' && isset($this->request->get[\'path\'])) {]]></search>\r\n			<add position="before"><![CDATA[\r\n			if ($route == \'news/article\' && isset($this->request->get[\'news_id\'])) {\r\n				$layout_id = $this->model_catalog_news->getNewsLayoutId($this->request->get[\'news_id\']);\r\n			}\r\n			if ($route == \'news/ncategory\' && isset($this->request->get[\'ncat\'])) {\r\n				$ncat = explode(\'_\', (string)$this->request->get[\'ncat\']);\r\n				\r\n				$layout_id = $this->model_catalog_ncategory->getncategoryLayoutId(end($ncat));			\r\n			}\r\n		]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/controller/common/content_bottom.php">\r\n		<operation>\r\n			<search><![CDATA[$this->load->model(\'catalog/information\');]]></search>\r\n			<add position="after"><![CDATA[$this->load->model(\'catalog/news\');\r\n			$this->load->model(\'catalog/ncategory\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($route == \'product/category\' && isset($this->request->get[\'path\'])) {]]></search>\r\n			<add position="before"><![CDATA[\r\n			if ($route == \'news/article\' && isset($this->request->get[\'news_id\'])) {\r\n				$layout_id = $this->model_catalog_news->getNewsLayoutId($this->request->get[\'news_id\']);\r\n			}\r\n			if ($route == \'news/ncategory\' && isset($this->request->get[\'ncat\'])) {\r\n				$ncat = explode(\'_\', (string)$this->request->get[\'ncat\']);\r\n				\r\n				$layout_id = $this->model_catalog_ncategory->getncategoryLayoutId(end($ncat));			\r\n			}\r\n		]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/controller/common/seo_url.php">\r\n		<operation>\r\n			<search><![CDATA[if (isset($this->request->get[\'route\'])) {]]></search>\r\n			<add position="before"><![CDATA[\r\n        if ($this->request->get[\'route\'] != \'product/product\' && $this->request->get[\'route\'] != \'product/category\' && $this->request->get[\'route\'] != \'product/manufacturer/info\' && $this->request->get[\'route\'] != \'information/information\') {\r\n			$blog_headlines = $this->config->get(\'ncategory_bnews_headlines_url\') ? $this->config->get(\'ncategory_bnews_headlines_url\') : \'blog-headlines\';\r\n			\r\n			$blogparts = explode(\'/\', $this->request->get[\'_route_\']);\r\n			\r\n			if (utf8_strlen(end($blogparts)) == 0) {\r\n				array_pop($blogparts);\r\n			}\r\n\r\n			\r\n			foreach ($blogparts as $part) {\r\n					/* default article seo urls */\r\n					if (strpos($part, \'blogcat\') === 0) {\r\n						$ncatid = (int)str_replace("blogcat", "", $part);\r\n						if (!isset($this->request->get[\'ncat\'])) {\r\n							$this->request->get[\'ncat\'] = $ncatid;\r\n						} else {\r\n							$this->request->get[\'ncat\'] .= \'_\' . $ncatid;\r\n						}\r\n					}\r\n					if (strpos($part, \'blogart\') === 0) {\r\n						$this->request->get[\'news_id\'] = (int)str_replace("blogart", "", $part);\r\n					}\r\n					if (strpos($part, \'blogauthor\') === 0) {\r\n						$this->request->get[\'author\'] = (int)str_replace("blogauthor", "", $part);\r\n					}\r\n					/* end of default article urls */\r\n				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = \'" . $this->db->escape($part) . "\'");\r\n				\r\n				if ($part == $blog_headlines) {\r\n					$query->num_rows = true;\r\n					$query->row[\'query\'] = "-=-";\r\n				}\r\n\r\n				if ($query->num_rows) {\r\n					$url = explode(\'=\', $query->row[\'query\']);\r\n					/* custom article urls */\r\n					if ($url[0] == \'news_id\') {\r\n						$this->request->get[\'news_id\'] = $url[1];\r\n					}\r\n					if ($url[0] == \'nauthor_id\') {\r\n						$this->request->get[\'author\'] = $url[1];\r\n					}\r\n					if ($url[0] == \'ncategory_id\') {\r\n						if (!isset($this->request->get[\'ncat\'])) {\r\n							$this->request->get[\'ncat\'] = $url[1];\r\n						} else {\r\n							$this->request->get[\'ncat\'] .= \'_\' . $url[1];\r\n						}\r\n					}\r\n					/* end of custom article urls */\r\n				}\r\n			}\r\n			if (!isset($this->request->get[\'route\']) || (isset($this->request->get[\'route\']) && $this->request->get[\'route\'] == "error/not_found")) {\r\n					\r\n				if (isset($this->request->get[\'news_id\'])) {\r\n					$this->request->get[\'route\'] = \'news/article\';\r\n				} elseif (isset($this->request->get[\'ncat\']) || isset($this->request->get[\'author\']) || $this->request->get[\'_route_\'] ==  $blog_headlines) {\r\n					$this->request->get[\'route\'] = \'news/ncategory\';\r\n				}\r\n			}\r\n        }\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[} elseif ($key == \'path\') {]]></search>\r\n			<add position="replace"><![CDATA[\r\n			} elseif ($data[\'route\'] == \'news/article\' && $key == \'news_id\') { \r\n				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = \'" . $this->db->escape($key . \'=\' . (int)$value) . "\'");\r\n				if ($query->num_rows) {\r\n					$url .= \'/\' . $query->row[\'keyword\'];\r\n					unset($data[$key]);\r\n				} else {\r\n					$url .= \'/blogart\' . (int)$value;	\r\n					unset($data[$key]);\r\n				}\r\n			} elseif ($data[\'route\'] == \'news/ncategory\' && $key == \'author\') { \r\n				$realkey = "nauthor_id";\r\n				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = \'" . $this->db->escape($realkey . \'=\' . (int)$value) . "\'");\r\n				if ($query->num_rows) {\r\n					$url .= \'/\' . $query->row[\'keyword\'];\r\n					unset($data[$key]);\r\n				} else {\r\n					$url .= \'/blogauthor\' . (int)$value;	\r\n					unset($data[$key]);\r\n				}\r\n			} elseif ($key == \'ncat\') {\r\n				$ncategories = explode(\'_\', $value);\r\n						\r\n				foreach ($ncategories as $ncategory) {\r\n					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = \'ncategory_id=" . (int)$ncategory . "\'");\r\n					if ($query->num_rows) {\r\n						$url .= \'/\' . $query->row[\'keyword\'];\r\n					} else {\r\n						$url .= \'/blogcat\' . $ncategory;\r\n					}\r\n				}\r\n				unset($data[$key]);\r\n			} elseif ((isset($data[\'route\']) && $data[\'route\'] == \'news/ncategory\' && $key != \'ncat\' && $key != \'author\' && $key != \'page\') || (isset($data[\'route\']) && $data[\'route\'] == \'news/article\' && $key != \'page\')) { \r\n				$blog_headlines = $this->config->get(\'ncategory_bnews_headlines_url\') ? $this->config->get(\'ncategory_bnews_headlines_url\') : \'blog-headlines\';\r\n				$url .=  \'/\'.$blog_headlines;\r\n			} elseif ($key == \'path\') {]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/controller/common/footer.php">\r\n		<operation>\r\n			<search><![CDATA[$data[\'newsletter\'] = $this->url->link(\'account/newsletter\', \'\', \'SSL\');]]></search>\r\n			<add position="before"><![CDATA[\r\n		$this->language->load(\'module/news\');\r\n		$data[\'blog_url\'] = $this->url->link(\'news/ncategory\');\r\n		$data[\'blog_name\'] = $this->language->get(\'text_blogpage\');\r\n		]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/view/theme/*/template/common/footer.tpl">\r\n		<operation>\r\n			<search><![CDATA[<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>]]></search>\r\n			<add position="after"><![CDATA[\r\n			<li><a href="<?php echo $blog_url; ?>"><?php echo $blog_name; ?></a></li>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="system/library/document.php">\r\n		<operation>\r\n			<search><![CDATA[private $scripts = array();]]></search>\r\n			<add position="after"><![CDATA[\r\n			private $extra_tags = array();\r\n			public function addExtraTag($property, $content = \'\', $name=\'\'){\r\n				$this->extra_tags[md5($property)] = array(\r\n					\'property\' => $property,\r\n					\'content\'  => $content,\r\n					\'name\'     => $name,\r\n				);\r\n			}\r\n			\r\n			public function getExtraTags(){\r\n				return $this->extra_tags;\r\n			}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/controller/common/header.php">\r\n		<operation>\r\n			<search><![CDATA[$data[\'scripts\'] = $this->document->getScripts();]]></search>\r\n			<add position="after"><![CDATA[$data[\'extra_tags\'] = $this->document->getExtraTags();]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path="catalog/view/theme/*/template/common/header.tpl">\r\n		<operation>\r\n			<search><![CDATA[<?php if ($description) { ?>]]></search>\r\n			<add position="before"><![CDATA[<?php foreach($extra_tags as $extra_tag) {?>\r\n<meta <?php echo ($extra_tag[\'name\']) ? \'name="\' . $extra_tag[\'name\'] . \'" \' : \'\'; ?><?php echo (!in_array($extra_tag[\'property\'], array("noprop", "noprop1", "noprop2", "noprop3", "noprop4"))) ? \'property="\' . $extra_tag[\'property\'] . \'" \' : \'\'; ?> content="<?php echo addslashes($extra_tag[\'content\']); ?>" />\r\n<?php } ?>]]></add>\r\n		</operation>\r\n	</file>\r\n</modification>', 1, '2016-11-06 23:29:49'),
(5, 'SeoPro for Opencart v2.x', 'seopro-20150125', 'OpencartJazz.com', '1.0.2016-01-15', 'http://www.opencartjazz.com/opencart-module/seo/ocj-seopro-oc2', '<?xml version="1.0" encoding="utf-8"?>\r\n<modification>\r\n    <code>seopro-20150125</code>\r\n    <name>SeoPro for Opencart v2.x</name>\r\n    <version>1.0.2016-01-15</version>\r\n    <author>OpencartJazz.com</author>\r\n    <link>http://www.opencartjazz.com/opencart-module/seo/ocj-seopro-oc2</link>\r\n    \r\n    <file path="admin/controller/setting/setting.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$data[\'entry_seo_url\'] = $this->language->get(\'entry_seo_url\');]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[		$data[\'entry_seo_url_type\'] = $this->language->get(\'entry_seo_url_type\');\r\n		$data[\'entry_seo_url_include_path\'] = $this->language->get(\'entry_seo_url_include_path\');\r\n		$data[\'entry_seo_url_postfix\'] = $this->language->get(\'entry_seo_url_postfix\');\r\n]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$data[\'help_seo_url\'] = $this->language->get(\'help_seo_url\');]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[		$data[\'help_seo_url_include_path\'] = $this->language->get(\'help_seo_url_include_path\');\r\n		$data[\'help_seo_url_postfix\'] = $this->language->get(\'help_seo_url_postfix\');\r\n]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$data[\'config_seo_url\'] = $this->config->get(\'config_seo_url\');]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[		}\r\n\r\n		if (isset($this->request->post[\'config_seo_url_type\'])) {\r\n			$data[\'config_seo_url_type\'] = $this->request->post[\'config_seo_url_type\'];\r\n		} elseif ($this->config->get(\'config_seo_url_type\')) {\r\n			$data[\'config_seo_url_type\'] = $this->config->get(\'config_seo_url_type\');\r\n		} else {\r\n			$data[\'config_seo_url_type\'] = \'seo_url\';\r\n		}\r\n\r\n		$data[\'seo_types\'] = array();\r\n		$data[\'seo_types\'][] = array(\'type\' => \'seo_url\', \'name\' => $this->language->get(\'text_seo_url\'));\r\n		$data[\'seo_types\'][] = array(\'type\' => \'seo_pro\', \'name\' => $this->language->get(\'text_seo_pro\'));\r\n\r\n		if (isset($this->request->post[\'config_seo_url_include_path\'])) {\r\n			$data[\'config_seo_url_include_path\'] = $this->request->post[\'config_seo_url_include_path\'];\r\n		} else {\r\n			$data[\'config_seo_url_include_path\'] = $this->config->get(\'config_seo_url_include_path\');\r\n		}\r\n\r\n		if (isset($this->request->post[\'config_seo_url_postfix\'])) {\r\n			$data[\'config_seo_url_postfix\'] = $this->request->post[\'config_seo_url_postfix\'];\r\n		} else {\r\n			$data[\'config_seo_url_postfix\'] = $this->config->get(\'config_seo_url_postfix\');\r\n]]></add>\r\n        </operation>\r\n    </file>\r\n\r\n    <file path="admin/language/english/catalog/product.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'entry_recurring\']        = \'Recurring Profile\';]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'entry_main_category\']    = \'Main Category:\';]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path="admin/language/russian/catalog/product.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'entry_recurring\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'entry_main_category\']    = \'Главная категория:\';]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path="admin/language/ukrainian/catalog/product.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'entry_recurring\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'entry_main_category\']    = \'Головна категорія:\';]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path="admin/language/uk_UA/catalog/product.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'entry_recurring\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'entry_main_category\']    = \'Головна категорія:\';]]></add>\r\n        </operation>\r\n    </file>\r\n\r\n    <file path="admin/language/english/setting/setting.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'text_smtp\']                        = \'SMTP\';]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'text_seo_url\']                 = \'Default\';\r\n$_[\'text_seo_pro\']                 = \'SeoPro\';]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'entry_seo_url\']                    = \'Use SEO URLs\';]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'entry_seo_url_type\']               = \'SEO URL type\';\r\n$_[\'entry_seo_url_include_path\']       = \'SEO URL for product with categories\';\r\n$_[\'entry_seo_url_postfix\']            = \'SEO URL ending\';]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'help_seo_url\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'help_seo_url_include_path\']        = \'SEO URL format: <b>/category/subcategory/product</b> (only for SeoPro)\';\r\n$_[\'help_seo_url_postfix\']             = \'SEO URL extension, such as <b>.html</b> (only for SeoPro)\';]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path="admin/language/russian/setting/setting.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'text_smtp\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'text_seo_url\']                 = \'Стандартный Seo_Url\';\r\n$_[\'text_seo_pro\']                 = \'SeoPro\';]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'entry_seo_url\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'entry_seo_url_type\']               = \'Тип SEO URL\';\r\n$_[\'entry_seo_url_include_path\']       = \'SEO URL для товаров включают категории\';\r\n$_[\'entry_seo_url_postfix\']            = \'Окончание SEO URL\';]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'help_seo_url\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'help_seo_url_include_path\']        = \'Формат SEO URL: <b>/category/subcategory/product</b> (только для SeoPro)\';\r\n$_[\'help_seo_url_postfix\']             = \'Окончание SEO URL, например <b>.html</b> (только для SeoPro)\';]]></add>\r\n        </operation>\r\n    </file>\r\n    <file path="admin/language/ukrainian/setting/setting.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'text_smtp\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'text_seo_url\']                 = \'Звичайний Seo_Url з Опенкарт\';\r\n$_[\'text_seo_pro\']                 = \'SeoPro\';]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'entry_seo_url\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'entry_seo_url_type\']               = \'Тип SEO URL\';\r\n$_[\'entry_seo_url_include_path\']       = \'SEO URL для товарів містить категорії\';\r\n$_[\'entry_seo_url_postfix\']            = \'Фінальна частина SEO URL\';]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$_[\'help_seo_url\']]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[$_[\'help_seo_url_include_path\']        = \'Формат SEO URL: <b>/category/subcategory/product</b> (тільки для SeoPro)\';\r\n$_[\'help_seo_url_postfix\']             = \'Фінальна частина SEO URL, наприклад <b>.html</b> (тільки для SeoPro)\';]]></add>\r\n        </operation>\r\n    </file>\r\n\r\n    \r\n    <file path="admin/model/openbay/openbay.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[curl_setopt($curl, CURLOPT_RETURNTRANSFER, true)]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[			// curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);\r\n			// curl_setopt($curl, CURLOPT_POSTREDIR, 7);]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="admin/view/template/catalog/product_form.tpl">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[<input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" />]]></search>\r\n            <add position="after" trim="false" offset="2"><![CDATA[              <div class="form-group">\r\n                <label class="col-sm-2 control-label" for="input-main-category-id"><?php echo $entry_main_category; ?></label>\r\n                <div class="col-sm-10">\r\n                  <select name="main_category_id" id="input-main-category-id" class="form-control">\r\n                    <?php foreach ($product_categories as $category) { ?>\r\n	                <?php if ($category[\'category_id\'] == $main_category_id) { ?>\r\n	                <option value="<?php echo $category[\'category_id\']; ?>" selected="selected"><?php echo $category[\'name\']; ?></option>\r\n	                <?php } else { ?>\r\n	                <option value="<?php echo $category[\'category_id\']; ?>"><?php echo $category[\'name\']; ?></option>\r\n                    <?php } ?>\r\n                    <?php } ?>\r\n	              </select>\r\n                </div>\r\n              </div>]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="admin/view/template/setting/setting.tpl">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[<input type="radio" name="config_seo_url" value="0" />]]></search>\r\n            <add position="after" trim="false" offset="5"><![CDATA[              <div class="form-group">\r\n                <label class="col-sm-2 control-label" for="input-seo-url-type"><?php echo $entry_seo_url_type; ?></label>\r\n                <div class="col-sm-10">\r\n                  <select name="config_seo_url_type" id="input-seo-url-type" class="form-control">\r\n                    <?php foreach ($seo_types as $seo_type) { ?>\r\n	                <?php if ($seo_type[\'type\'] == $config_seo_url_type) { ?>\r\n	                <option value="<?php echo $seo_type[\'type\']; ?>" selected="selected"><?php echo $seo_type[\'name\']; ?></option>\r\n	                <?php } else { ?>\r\n	                <option value="<?php echo $seo_type[\'type\']; ?>"><?php echo $seo_type[\'name\']; ?></option>\r\n                    <?php } ?>\r\n                    <?php } ?>\r\n	              </select>\r\n                </div>\r\n              </div>\r\n\r\n              <div class="form-group">\r\n                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_seo_url_include_path; ?>"><?php echo $entry_seo_url_include_path; ?></span></label>\r\n                <div class="col-sm-10">\r\n                  <label class="radio-inline">\r\n                    <input type="radio" name="config_seo_url_include_path" value="1" <?php echo $config_seo_url_include_path ? \'checked="checked"\' : \'\'; ?>/>\r\n                    <?php echo $text_yes; ?>\r\n                  </label>\r\n                  <label class="radio-inline">\r\n                    <input type="radio" name="config_seo_url_include_path" value="0" <?php echo !$config_seo_url_include_path ? \'checked="checked"\' : \'\'; ?>/>\r\n                    <?php echo $text_no; ?>\r\n                  </label>\r\n                </div>\r\n              </div>\r\n              <div class="form-group">\r\n                <label class="col-sm-2 control-label" for="input-seo-url-postfix"><span data-toggle="tooltip" title="<?php echo $help_seo_url_postfix; ?>"><?php echo $entry_seo_url_postfix; ?></span></label>\r\n                <div class="col-sm-10">\r\n                  <input type="text" name="config_seo_url_postfix" value="<?php echo $config_seo_url_postfix; ?>" placeholder="<?php echo $entry_seo_url_postfix; ?>" id="input-seo-url-postfix" class="form-control" />\r\n                </div>\r\n              </div>]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="catalog/controller/common/header.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$data[\'name\'] = $this->config->get(\'config_name\');]]></search>\r\n            <add position="after" trim="false" offset="0"><![CDATA[		$data[\'alter_lang\'] = $this->getAlterLanguageLinks($this->document->getLinks());]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[return $this->load->view(\'default/template/common/header.tpl\', $data);]]></search>\r\n            <add position="after" trim="false" offset="2"><![CDATA[	\r\n	protected function getAlterLanguageLinks($links) {\r\n		$result = array();\r\n		if ($this->config->get(\'config_seo_url\')) {\r\n			foreach($links as $link) {\r\n				if($link[\'rel\']==\'canonical\') {\r\n					$url=$link[\'href\'];\r\n					$schema = parse_url($url,PHP_URL_SCHEME);\r\n					$server = strtolower($schema)==\'https\' ? HTTPS_SERVER : HTTP_SERVER; \r\n					$cur_lang = substr($url, strlen($server),2);\r\n					$query = substr($url, strlen($server)+2);\r\n					$this->load->model(\'localisation/language\');\r\n					$languages = $this->model_localisation_language->getLanguages();\r\n					$active_langs = array();\r\n					foreach($languages as $lang) {\r\n						if($lang[\'status\']) {\r\n							$active_langs[]=$lang[\'code\'];\r\n						} \r\n					}\r\n					if(in_array($cur_lang, $active_langs)) {\r\n						foreach($active_langs as $lang) {\r\n							$result[$lang] = $server.$lang.($query ? $query : \'\');\r\n						}\r\n					}\r\n				}\r\n			}\r\n		}\r\n		return $result;\r\n	}]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="catalog/controller/common/home.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$this->document->addLink(HTTP_SERVER, \'canonical\');]]></search>\r\n            <add position="replace" trim="false" offset="0"><![CDATA[$this->document->addLink($this->url->link(\'common/home\'), \'canonical\');]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="catalog/controller/common/language.php">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$data[\'redirect\'] = $this->url->link(\'common/home\');]]></search>\r\n            <add position="replace" trim="false" offset="0"><![CDATA[			$data[\'redirect_route\'] = \'common/home\';\r\n			$data[\'redirect_query\'] = \'\';\r\n			$data[\'redirect_ssl\']   = \'\';]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$data[\'redirect\'] = $this->url->link($route, $url, $this->request->server[\'HTTPS\']);]]></search>\r\n            <add position="replace" trim="false" offset="0"><![CDATA[	$data[\'redirect_route\']=$route;\r\n			$data[\'redirect_query\']=$url;\r\n			$data[\'redirect_ssl\']=$this->request->server[\'HTTPS\'];]]></add>\r\n        </operation>\r\n	<operation>\r\n            <search regex="true" limit="1" quote="true"><![CDATA[~		if (isset($this->request->post[\'redirect\'])) {\r\n			$this->response->redirect($this->request->post[\'redirect\']);~]]></search>\r\n            <add><![CDATA[		if (isset($this->request->post[\'redirect_route\'])) {\r\n			$url = $this->url->link($this->request->post[\'redirect_route\'],\r\n					isset($this->request->post[\'redirect_query\']) ? html_entity_decode($this->request->post[\'redirect_query\']) : \'\',\r\n					isset($this->request->post[\'redirect_ssl\']) ? $this->request->post[\'redirect_ssl\'] : \'\');\r\n			$this->response->redirect($url);]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="catalog/view/theme/default/template/common/header.tpl">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>]]></search>\r\n            <add position="before" trim="false" offset="0"><![CDATA[<?php foreach ($alter_lang as $lang=>$href) { ?>\r\n<link href="<?php echo $href; ?>" hreflang="<?php echo $lang; ?>" rel="alternate" />\r\n<?php } ?>]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="catalog/view/theme/default/template/common/language.tpl">\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />]]></search>\r\n            <add position="replace" trim="false" offset="0"><![CDATA[  <input type="hidden" name="redirect_route" value="<?php echo $redirect_route; ?>" />\r\n  <input type="hidden" name="redirect_query" value="<?php echo isset($redirect_query) ? $redirect_query : \'\'; ?>" />\r\n  <input type="hidden" name="redirect_ssl" value="<?php echo isset($redirect_ssl) ? $redirect_ssl : \'\'; ?>" />]]></add>\r\n        </operation>\r\n    </file>  \r\n    <file path="catalog/controller/common/home.php">\r\n	<operation>\r\n            <search regex="true" limit="1" quote="true"><![CDATA[~if (isset($this->request->get[\'route\'])) {\r\n			$this->document->addLink($this->url->link(\'common/home\'), \'canonical\');\r\n		}~]]></search>\r\n            <add><![CDATA[$this->document->addLink($this->getCanonical(), \'canonical\');]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search trim="true" index="0"><![CDATA[$this->response->setOutput($this->load->view(\'default/template/common/home.tpl\', $data));]]></search>\r\n            <add position="after" trim="false" offset="2"><![CDATA[	\r\n	protected function getCanonical() {\r\n		$url = HTTP_SERVER;\r\n		if( $this->config->get(\'config_seo_url\')\r\n			&& $this->config->get(\'config_seo_url_type\') == \'seo_pro\') {\r\n			$url = $this->url->link(\'common/home\');\r\n			$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = \'" . (int)$this->config->get(\'config_store_id\') . "\' AND `key` =\'config_language\'");\r\n				\r\n			$code = $this->session->data[\'language\'];\r\n			\r\n			// Do not show language code for home when default language is always shown\r\n			if( !$this->config->get(\'ocjazz_seopro_hide_default\') \r\n				&& $code == $query->row[\'value\']) \r\n			{\r\n				$component = parse_url($url);\r\n				if ($component[\'scheme\'] == \'https\') {\r\n					$domain = $this->config->get(\'config_ssl\');\r\n				} else {\r\n					$domain = $this->config->get(\'config_url\');\r\n				}\r\n					\r\n				$url = preg_replace(\'~(\'.$domain.\')(\'.$code.\'/)(.+)~i\', \'$1$3\',$url);\r\n			}\r\n		}\r\n		return $url;\r\n	}\r\n]]></add>\r\n        </operation>\r\n    </file>  \r\n</modification>\r\n', 1, '0216-11-06 00:00:00'),
(6, 'Popup Purchase', 'popup_purchase_001', 'Octemplates', '2.0.x', 'http://octemplates.net/', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n  <code>popup_purchase_001</code>\n  <name>Popup Purchase</name>\n  <version>2.0.x</version>\n  <author>Octemplates</author>\n  <link>http://octemplates.net/</link>\n  <file path="catalog/controller/common/header.php">\n     <operation error="skip">\n      <search><![CDATA[public function index() {]]></search>\n      <add position="after"><![CDATA[\n        // popup_purchase start\n        $data[\'popup_purchase_data\'] = $this->config->get(\'popup_purchase_data\');\n        $data[\'popup_purchase_text\'] = $this->language->load(\'module/popup_purchase\');\n        // popup_purchase end\n      ]]></add>\n    </operation>\n  </file>\n</modification>', 1, '2016-11-08 22:11:13');
INSERT INTO `oc_modification` (`modification_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(7, 'Simple Custom Data Admin', 'Simple Custom Data Admin', 'deeman', '1.0.0', 'http://simpleopencart.com', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n    <name>Simple Custom Data Admin</name>\n	<code>Simple Custom Data Admin</code>\n    <version>1.0.0</version>\n    <author>deeman</author>\n    <link>http://simpleopencart.com</link>\n\n    <file path="admin/controller/sale/order.php">\n        <operation>\n            <search><![CDATA[\n                $store_info = $this->model_setting_setting->getSetting(\'config\', $order_info[\'store_id\']);\n            ]]></search>\n            <add position="after" index="1"><![CDATA[\n                $this->load->model(\'module/simplecustom\');\n\n                $customInfo = $this->model_module_simplecustom->getCustomFields(\'order\', $order_info[\'order_id\'], $order_info[\'language_code\']);\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                $shipping_address = str_replace(\n            ]]></search>\n            <add position="before" index="1"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'shipping_company_id\']) ? $order_info[\'shipping_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'shipping_tax_id\']) ? $order_info[\'shipping_tax_id\'] : \'\';\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'shipping_\') === 0) {\n                        $id = str_replace(\'shipping_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'payment_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                $payment_address = str_replace\n            ]]></search>\n            <add position="before" index="1"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'payment_company_id\']) ? $order_info[\'payment_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'payment_tax_id\']) ? $order_info[\'payment_tax_id\'] : \'\';\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'payment_\') === 0) {\n                        $id = str_replace(\'payment_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'shipping_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                $customer_group_info = $this->model_customer_customer_group->getCustomerGroup\n            ]]></search>\n            <add position="after" index="1"><![CDATA[\n                $this->load->model(\'module/simplecustom\');\n\n                $customInfo = $this->model_module_simplecustom->getCustomFields(\'order\', $order_info[\'order_id\'], $order_info[\'language_code\']);\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                $data[\'shipping_address\'] = str_replace(\n            ]]></search>\n            <add position="before" index="1"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'shipping_company_id\']) ? $order_info[\'shipping_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'shipping_tax_id\']) ? $order_info[\'shipping_tax_id\'] : \'\';\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'shipping_\') === 0) {\n                        $id = str_replace(\'shipping_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'payment_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                $data[\'payment_address\'] = str_replace(\n            ]]></search>\n            <add position="before" index="1"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'payment_company_id\']) ? $order_info[\'payment_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'payment_tax_id\']) ? $order_info[\'payment_tax_id\'] : \'\';\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'payment_\') === 0) {\n                        $id = str_replace(\'payment_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'shipping_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                $data[\'ip\'] = $order_info[\'ip\'];\n            ]]></search>\n            <add position="before" index=""><![CDATA[\n                $this->load->model(\'module/simplecustom\');\n\n                $customAccountInfo = $this->model_module_simplecustom->getInfoWithValues(\'order\', $order_info[\'order_id\'], \'customer\');\n\n                foreach ($customAccountInfo as $key => $info) {\n                    $data[\'account_custom_fields\'][] = array(\n                        \'name\'  => $info[\'label\'],\n                        \'value\' => !empty($info[\'values\']) ? $info[\'values\'][$info[\'value\']] : $info[\'value\'],\n                        \'sort_order\' => 0\n                    );\n                }\n\n                $customShippingInfo = $this->model_module_simplecustom->getInfoWithValues(\'order\', $order_info[\'order_id\'], \'shipping_address\');\n\n                foreach ($customShippingInfo as $key => $info) {\n                    $data[\'account_custom_fields\'][] = array(\n                        \'name\'  => $info[\'label\'],\n                        \'value\' => !empty($info[\'values\']) ? $info[\'values\'][$info[\'value\']] : $info[\'value\'],\n                        \'sort_order\' => 0\n                    );\n                }\n\n                $customPaymentInfo = $this->model_module_simplecustom->getInfoWithValues(\'order\', $order_info[\'order_id\'], \'payment_address\');\n\n                foreach ($customPaymentInfo as $key => $info) {\n                    $data[\'account_custom_fields\'][] = array(\n                        \'name\'  => $info[\'label\'],\n                        \'value\' => !empty($info[\'values\']) ? $info[\'values\'][$info[\'value\']] : $info[\'value\'],\n                        \'sort_order\' => 0\n                    );\n                }\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="admin/view/template/customer/customer_form.tpl">\n        <operation>\n            <search><![CDATA[id="tab-customer"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    if (jQuery(\'#tab-customer table\').length) {\n                        jQuery(\'#tab-customer table\').after(\'<div id="simple_custom_customer" class="simple-container"></div>\');\n                    } else {\n                        jQuery(\'#tab-customer\').append(\'<div id="simple_custom_customer" class="simple-container"></div>\');\n                    }\n                    jQuery(\'#simple_custom_customer\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&token=<?php echo $token; ?>&set=customer&object=customer&id=<?php echo $customer_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n            <input type="hidden" name="address[<?php echo $address_row; ?>][address_id]" value="<?php echo $address[\'address_id\']; ?>" />\n            ]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    if (jQuery(\'#tab-address-<?php echo $address_row; ?> table\').length) {\n                        jQuery(\'#tab-address-<?php echo $address_row; ?> table\').after(\'<div id="simple_custom_address_<?php echo $address_row; ?>" class="simple-container"></div>\');\n                    } else {\n                        jQuery(\'#tab-address<?php echo $address_row; ?>\').append(\'<div id="simple_custom_address_<?php echo $address_row; ?>" class="simple-container"></div>\');\n                    }\n                    jQuery(\'#simple_custom_address_<?php echo $address_row; ?>\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=address&token=<?php echo $token; ?>&object=address&id=<?php echo $address[\'address_id\']; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="admin/view/template/sale/customer_form.tpl">\n        <operation>\n            <search><![CDATA[id="tab-customer"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    if (jQuery(\'#tab-customer table\').length) {\n                        jQuery(\'#tab-customer table\').after(\'<div id="simple_custom_customer" class="simple-container"></div>\');\n                    } else {\n                        jQuery(\'#tab-customer\').append(\'<div id="simple_custom_customer" class="simple-container"></div>\');\n                    }\n                    jQuery(\'#simple_custom_customer\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&token=<?php echo $token; ?>&set=customer&object=customer&id=<?php echo $customer_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n            <input type="hidden" name="address[<?php echo $address_row; ?>][address_id]" value="<?php echo $address[\'address_id\']; ?>" />\n            ]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    if (jQuery(\'#tab-address-<?php echo $address_row; ?> table\').length) {\n                        jQuery(\'#tab-address-<?php echo $address_row; ?> table\').after(\'<div id="simple_custom_address_<?php echo $address_row; ?>" class="simple-container"></div>\');\n                    } else {\n                        jQuery(\'#tab-address<?php echo $address_row; ?>\').append(\'<div id="simple_custom_address_<?php echo $address_row; ?>" class="simple-container"></div>\');\n                    }\n                    jQuery(\'#simple_custom_address_<?php echo $address_row; ?>\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=address&token=<?php echo $token; ?>&object=address&id=<?php echo $address[\'address_id\']; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="admin/view/template/sale/order_info.tpl">\n        <operation>\n            <search><![CDATA[id="tab-order"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    if (jQuery(\'#tab-order table\').length) {\n                        jQuery(\'#tab-order table\').after(\'<div id="simple_custom_order" class="simple-container"></div>\');\n                    } else {\n                        jQuery(\'#tab-order\').append(\'<div id="simple_custom_order" class="simple-container"></div>\');\n                    }\n                    jQuery(\'#simple_custom_order\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=order&token=<?php echo $token; ?>&object=order&id=<?php echo $order_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[id="tab-payment"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    if (jQuery(\'#tab-payment table\').length) {\n                        jQuery(\'#tab-payment table\').after(\'<div id="simple_custom_payment_address" class="simple-container"></div>\');\n                    } else {\n                        jQuery(\'#tab-payment\').append(\'<div id="simple_custom_payment_address" class="simple-container"></div>\');\n                    }\n                    jQuery(\'#simple_custom_payment_address\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=payment_address&token=<?php echo $token; ?>&object=order&id=<?php echo $order_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[id="tab-shipping"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    if (jQuery(\'#tab-payment table\').length) {\n                        jQuery(\'#tab-shipping table\').after(\'<div id="simple_custom_shipping_address" class="simple-container"></div>\');\n                    } else {\n                        jQuery(\'#tab-shipping\').append(\'<div id="simple_custom_shipping_address" class="simple-container"></div>\');\n                    }\n                    jQuery(\'#simple_custom_shipping_address\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=shipping_address&token=<?php echo $token; ?>&object=order&id=<?php echo $order_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n    </file>\n\n    <file path="admin/view/template/sale/order_form.tpl">\n        <operation>\n            <search><![CDATA[id="tab-customer"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    jQuery(\'#tab-customer\').append(\'<div id="simple_custom_order" class="simple-container"></div>\');\n                    jQuery(\'#simple_custom_order\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=order&token=<?php echo $token; ?>&object=order&id=<?php echo $order_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[id="tab-payment"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    jQuery(\'#tab-payment\').append(\'<div id="simple_custom_payment_address" class="simple-container"></div>\');\n                    jQuery(\'#simple_custom_payment_address\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=payment_address&token=<?php echo $token; ?>&object=order&id=<?php echo $order_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[id="tab-shipping"]]></search>\n            <add position="after"><![CDATA[\n            <script type="text/javascript">\n                jQuery(function(){\n                    jQuery(\'#tab-shipping\').append(\'<div id="simple_custom_shipping_address" class="simple-container"></div>\');\n                    jQuery(\'#simple_custom_shipping_address\').load(\'index.php?option=com_mijoshop&format=raw&tmpl=component&route=module/simple/custom&set=shipping_address&token=<?php echo $token; ?>&object=order&id=<?php echo $order_id; ?>\');\n                });\n            </script>\n            ]]></add>\n        </operation>\n    </file>\n\n</modification>\n\n', 0, '2016-11-08 22:20:40'),
(8, 'Simple Custom Data Catalog', 'Simple Custom Data Catalog', 'deeman', '1.0.0', 'http://simpleopencart.com', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n    <name>Simple Custom Data Catalog</name>\n	<code>Simple Custom Data Catalog</code>\n    <version>1.0.0</version>\n    <author>deeman</author>\n    <link>http://simpleopencart.com</link>\n\n    <file path="catalog/model/checkout/order.php">\n        <operation>\n            <search><![CDATA[\n                if (!$order_info[\'order_status_id\'] && $order_status_id) {\n            ]]></search>\n            <add position="after"><![CDATA[\n                $this->load->model(\'tool/simplecustom\');\n\n                $customInfo = $this->model_tool_simplecustom->getCustomFields(\'order\', $order_info[\'order_id\'], $order_info[\'language_code\']);\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                $data[\'title\'] = sprintf($language->get(\'text_new_\n            ]]></search>\n            <add position="after"><![CDATA[\n                $this->load->model(\'tool/simplecustom\');\n\n                $customInfo = $this->model_tool_simplecustom->getCustomFields(\'order\', $order_info[\'order_id\'], $order_info[\'language_code\']);\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                data[\'payment_address\']\n            ]]></search>\n            <add position="before"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'payment_company_id\']) ? $order_info[\'payment_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'payment_tax_id\']) ? $order_info[\'payment_tax_id\'] : \'\';\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'payment_\') === 0) {\n                        $id = str_replace(\'payment_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'shipping_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                data[\'shipping_address\']\n            ]]></search>\n            <add position="before"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'shipping_company_id\']) ? $order_info[\'shipping_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'shipping_tax_id\']) ? $order_info[\'shipping_tax_id\'] : \'\';\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'shipping_\') === 0) {\n                        $id = str_replace(\'shipping_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'payment_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n    </file>\n    <file path="catalog/controller/account/address.php">\n        <operation>\n            <search><![CDATA[\n                $find = array(\n            ]]></search>\n            <add position="before"><![CDATA[\n                $this->load->model(\'tool/simplecustom\');\n\n                if ($result[\'address_format\']) {\n                    $format = $result[\'address_format\'];\n                } else {\n                    $format = $this->model_tool_simplecustom->getAddressFormat();\n\n                    if (!$format) {\n                        $format = \'{firstname} {lastname}\' . "\\n" . \'{company}\' . "\\n" . \'{address_1}\' . "\\n" . \'{address_2}\' . "\\n" . \'{city} {postcode}\' . "\\n" . \'{zone}\' . "\\n" . \'{country}\';\n                    }\n                }\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                data[\'addresses\'][] = array(\n            ]]></search>\n            <add position="before"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($result[\'company_id\']) ? $result[\'company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($result[\'tax_id\']) ? $result[\'tax_id\'] : \'\';\n\n                $customInfo = $this->model_tool_simplecustom->getCustomFields(\'address\', $result[\'address_id\']);\n\n                foreach($customInfo as $id => $value) {\n                    $find[] = \'{\'.$id.\'}\';\n                    $replace[$id] = $value;\n                }\n            ]]></add>\n        </operation>\n    </file>\n    <file path="catalog/controller/account/order.php">\n        <operation>\n            <search><![CDATA[\n                data[\'payment_address\']\n            ]]></search>\n            <add position="before"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'payment_company_id\']) ? $order_info[\'payment_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'payment_tax_id\']) ? $order_info[\'payment_tax_id\'] : \'\';\n\n                $this->load->model(\'tool/simplecustom\');\n\n                $customInfo = $this->model_tool_simplecustom->getCustomFields(\'order\', $order_info[\'order_id\'], $this->config->get(\'config_language\'));\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'payment_\') === 0) {\n                        $id = str_replace(\'payment_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'shipping_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n        <operation>\n            <search><![CDATA[\n                data[\'shipping_address\']\n            ]]></search>\n            <add position="before"><![CDATA[\n                $find[] = \'{company_id}\';\n                $find[] = \'{tax_id}\';\n                $replace[\'company_id\'] = isset($order_info[\'shipping_company_id\']) ? $order_info[\'shipping_company_id\'] : \'\';\n                $replace[\'tax_id\'] = isset($order_info[\'shipping_tax_id\']) ? $order_info[\'shipping_tax_id\'] : \'\';\n\n                $this->load->model(\'tool/simplecustom\');\n\n                $customInfo = $this->model_tool_simplecustom->getCustomFields(\'order\', $order_info[\'order_id\'], $this->config->get(\'config_language\'));\n\n                foreach($customInfo as $id => $value) {\n                    if (strpos($id, \'shipping_\') === 0) {\n                        $id = str_replace(\'shipping_\', \'\', $id);\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    } elseif (strpos($id, \'payment_\') === false) {\n                        $find[] = \'{\'.$id.\'}\';\n                        $replace[$id] = $value;\n                    }\n                }\n            ]]></add>\n        </operation>\n    </file>\n\n</modification>\n', 0, '2016-11-08 22:20:44'),
(9, 'CSV Price Pro import/export 4 - Admin Menu', 'admin_menu_csvprice_pro4', 'costaslabs.com', '4.1.0', 'http://www.costaslabs.com', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n  <name>CSV Price Pro import/export 4 - Admin Menu</name>\n  <code>admin_menu_csvprice_pro4</code>\n  <version>4.1.0</version>\n  <author>costaslabs.com</author>\n  <link>http://www.costaslabs.com</link>\n\n  <file path="admin/controller/common/menu.php">\n    <operation>\n      <search><![CDATA[$this->load->language(\'common/menu\');]]></search>\n      <add position="after"><![CDATA[\n        if(isset($this->session->data[\'token\']) && $this->user->isLogged()){\n			$this->language->load(\'module/csvprice_pro\');\n\n			$data[\'text_csvprice_pro\'] = \'CSV Price Pro 4\';\n\n			$data[\'csvprice_pro_products\'] = array(\n			 \'text\' => $this->language->get(\'text_products\'),\n			 \'url\' =>  $this->url->link(\'csvprice_pro/app_product\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n			\n			$data[\'csvprice_pro_categories\'] = array(\n			 \'text\' => $this->language->get(\'text_categories\'),\n			 \'url\' =>  $this->url->link(\'csvprice_pro/app_category\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n			\n			$data[\'csvprice_pro_manufacturers\'] = array(\n			 \'text\' => $this->language->get(\'text_manufacturers\'),\n			 \'url\' =>  $this->url->link(\'csvprice_pro/app_manufacturer\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n			\n			$data[\'csvprice_pro_crontab\'] = array(\n			 \'text\' => $this->language->get(\'text_crontab\'),\n			 \'url\' =>  $this->url->link(\'csvprice_pro/app_crontab\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n			\n			\n			$data[\'csvprice_pro_customers\'] = array(\n			 \'text\' => $this->language->get(\'text_customers\'),\n			 \'url\' =>  $this->url->link(\'csvprice_pro/app_customer\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n			\n			$data[\'csvprice_pro_orders\'] = array(\n			 \'text\' => $this->language->get(\'text_orders\'),\n			 \'url\' =>  $this->url->link(\'csvprice_pro/app_order\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n			\n			$data[\'csvprice_pro_about\'] = array(\n			 \'text\' => $this->language->get(\'text_about\'),\n			 \'url\' =>  $this->url->link(\'csvprice_pro/app_about\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n		}\n      ]]></add>\n    </operation>\n  </file>\n  <file path="admin/view/template/common/menu.tpl">\n    <operation>\n      <search><![CDATA[<li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li>]]></search>\n      <add position="after"><![CDATA[\n		<?php if(isset($text_csvprice_pro)) { ?>\n			<li><a class="parent"><?php echo $text_csvprice_pro; ?></a>\n				<ul>\n				<li id="app_product"><a href="<?php echo $csvprice_pro_products[\'url\'];?>"><?php echo $csvprice_pro_products[\'text\'];?></a></li>\n				<li id="app_category"><a href="<?php echo $csvprice_pro_categories[\'url\'];?>"><?php echo $csvprice_pro_categories[\'text\'];?></a></li>\n				<li id="app_manufacturer"><a href="<?php echo $csvprice_pro_manufacturers[\'url\'];?>"><?php echo $csvprice_pro_manufacturers[\'text\'];?></a></li>\n				<li id="app_crontab"><a href="<?php echo $csvprice_pro_crontab[\'url\'];?>"><?php echo $csvprice_pro_crontab[\'text\'];?></a></li>\n				<li id="app_customer"><a href="<?php echo $csvprice_pro_customers[\'url\'];?>"><?php echo $csvprice_pro_customers[\'text\'];?></a></li>\n				<li id="app_order"><a href="<?php echo $csvprice_pro_orders[\'url\'];?>"><?php echo $csvprice_pro_orders[\'text\'];?></a></li>\n				<li id="app_about"><a href="<?php echo $csvprice_pro_about[\'url\'];?>"><?php echo $csvprice_pro_about[\'text\'];?></a></li>\n				</ul>\n			</li>\n		<?php } ?>\n	  ]]></add>\n    </operation>\n  </file>\n\n</modification>', 1, '2016-11-08 22:52:42'),
(14, 'Modification watermark module for opencart', 'default', 'Webengineer', '1.0', 'http://www.opencart.com', '<?xml version="1.0" encoding="utf-8"?>\n<modification>\n  <name>Modification watermark module for opencart</name>\n  <code>default</code>\n  <version>1.0</version>\n  <author>Webengineer</author>\n  <link>http://www.opencart.com</link>\n\n  <file path="catalog/controller/module/featured.php">\n	  <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($product_info[\'image\'], $setting[\'width\'], $setting[\'height\']);]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($product_info[\'image\'], $setting[\'width\'], $setting[\'height\'],\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/module/bestseller.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\']);]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'],\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/module/ebay_listing.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_openbay_ebay_product->resize($product[\'pictures\'][0], $this->config->get(\'ebay_listing_width\'), $this->config->get(\'ebay_listing_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_openbay_ebay_product->resize($product[\'pictures\'][0], $this->config->get(\'ebay_listing_width\'), $this->config->get(\'ebay_listing_height\'),\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/module/carousel.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[\'image\' => $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'])]]>\n      </search>\n      <add position="replace">\n        <![CDATA[\'image\' => $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'], \'product_in_cart\')]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/module/latest.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\']);]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'],\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/module/special.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\']);]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'],\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/module/slideshow.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[\'image\' => $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'])]]>\n      </search>\n      <add position="replace">\n        <![CDATA[\'image\' => $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'], \'category_image\')]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/module/banner.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[\'image\' => $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'])]]>\n      </search>\n      <add position="replace">\n        <![CDATA[\'image\' => $this->model_tool_image->resize($result[\'image\'], $setting[\'width\'], $setting[\'height\'], \'category_image\')]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/feed/google_sitemap.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$output .= \'<image:loc>\' . $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_popup_width\'), $this->config->get(\'config_image_popup_height\')) . \'</image:loc>\';]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$output .= \'<image:loc>\' . $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_popup_width\'), $this->config->get(\'config_image_popup_height\'), \'product_popup\') . \'</image:loc>\';]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/feed/google_base.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$output .= \'<g:image_link>\' . $this->model_tool_image->resize($product[\'image\'], 500, 500) . \'</g:image_link>\';]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$output .= \'<g:image_link>\' . $this->model_tool_image->resize($product[\'image\'], 500, 500, \'product_popup\') . \'</g:image_link>\';]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/product/product.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$data[\'popup\'] = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_popup_width\'), $this->config->get(\'config_image_popup_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$data[\'popup\'] = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_popup_width\'), $this->config->get(\'config_image_popup_height\'), \'product_popup\');]]>\n      </add>\n    </operation>\n    <operation>\n      <search regex="false">\n        <![CDATA[$data[\'thumb\'] = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_thumb_width\'), $this->config->get(\'config_image_thumb_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$data[\'thumb\'] = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_thumb_width\'), $this->config->get(\'config_image_thumb_height\'), \'product_thumb\');]]>\n      </add>\n    </operation>\n    <operation>\n      <search regex="false">\n        <![CDATA[\'popup\' => $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_popup_width\'), $this->config->get(\'config_image_popup_height\')),]]>\n      </search>\n      <add position="replace">\n        <![CDATA[\'popup\' => $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_popup_width\'), $this->config->get(\'config_image_popup_height\'), \'product_popup\'),]]>\n      </add>\n    </operation>\n    <operation>\n      <search regex="false">\n        <![CDATA[\'thumb\' => $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_additional_width\'), $this->config->get(\'config_image_additional_height\'))]]>\n      </search>\n      <add position="replace">\n        <![CDATA[\'thumb\' => $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_additional_width\'), $this->config->get(\'config_image_additional_height\'), \'product_popup\')]]>\n      </add>\n    </operation>\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_related_width\'), $this->config->get(\'config_image_related_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_related_width\'), $this->config->get(\'config_image_related_height\'), \'product_related\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/product/special.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'),\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/product/manufacturer.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'),\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/product/search.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'),\'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/product/compare.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_compare_width\'), $this->config->get(\'config_image_compare_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_compare_width\'), $this->config->get(\'config_image_compare_height\'), \'product_in_compare\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/product/category.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$data[\'thumb\'] = $this->model_tool_image->resize($category_info[\'image\'], $this->config->get(\'config_image_category_width\'), $this->config->get(\'config_image_category_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$data[\'thumb\'] = $this->model_tool_image->resize($category_info[\'image\'], $this->config->get(\'config_image_category_width\'), $this->config->get(\'config_image_category_height\'), \'category_image\');]]>\n      </add>\n    </operation>\n\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($result[\'image\'], $this->config->get(\'config_image_product_width\'), $this->config->get(\'config_image_product_height\'), \'product_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/common/cart.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_cart_width\'), $this->config->get(\'config_image_cart_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_cart_width\'), $this->config->get(\'config_image_cart_height\'), \'product_in_cart\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/account/wishlist.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_wishlist_width\'), $this->config->get(\'config_image_wishlist_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($product_info[\'image\'], $this->config->get(\'config_image_wishlist_width\'), $this->config->get(\'config_image_wishlist_height\'), \'product_in_wish_list\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/payment/pp_express.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_cart_width\'), $this->config->get(\'config_image_cart_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_cart_width\'), $this->config->get(\'config_image_cart_height\'), \'product_in_cart\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/controller/checkout/cart.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[$image = $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_cart_width\'), $this->config->get(\'config_image_cart_height\'));]]>\n      </search>\n      <add position="replace">\n        <![CDATA[$image = $this->model_tool_image->resize($product[\'image\'], $this->config->get(\'config_image_cart_width\'), $this->config->get(\'config_image_cart_height\'), \'product_in_cart\');]]>\n      </add>\n    </operation>\n  </file>\n\n  <file path="catalog/model/tool/image.php">\n    <operation>\n      <search regex="false">\n        <![CDATA[class ModelToolImage extends Model {]]>\n      </search>\n      <add position="before">\n        <![CDATA[\n\nrequire_once DIR_APPLICATION . \'model/module/watermark/image.watermark.php\';\n\n/*\nclass ModelToolImage extends Model {\n        ]]>\n      </add>\n    </operation>\n    <operation>\n      <search regex="false" index="12">\n        <![CDATA[}]]>\n      </search>\n      <add position="after">\n        <![CDATA[\n        */\n        ]]>\n      </add>\n    </operation>\n  </file>\n\n\n</modification>\n', 1, '2017-01-16 11:25:13'),
(17, 'Quick Login / Sign up', 'Quick Login / Sign up', 'Webmedialdh', '2x', '', '<?xml version="1.0" encoding="UTF-8"?>\n<modification>\n	<name>Quick Login / Sign up</name>\n	<version>2x</version>\n	<code>Quick Login / Sign up</code>\n	<author>Webmedialdh</author>\n	<!--CATALOG STARTS-->\n	<file path="catalog/view/theme/*/template/common/header.tpl">\n		<operation>\n			<search><![CDATA[<?php echo $telephone; ?></span></li>]]></search>\n			<add position="after"><![CDATA[\n		<?php if ($logged) { ?>\n			]]></add>\n		</operation>\n		<operation>\n			<search><![CDATA[<li><a href="<?php echo $wishlist; ?>"]]></search>\n			<add position="before"><![CDATA[<?php }else { ?>\n		<li class="quick-login"><a class="quick_login"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_login; ?></span></a></li>\n		<li class="quick-register"><a class="quick_register"><i class="fa fa-user-plus"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_login; ?></span></a></li>\n		<?php } ?>\n		]]></add>\n		</operation>\n		<operation>\n			<search><![CDATA[</header>]]></search>\n			<add position="after"><![CDATA[\n<?php echo $quicksignup; ?>]]></add>\n		</operation>\n	</file>\n	<file path="catalog/controller/common/header.php">\n		<operation>\n			<search><![CDATA[if (file_exists(DIR_TEMPLATE . $this->config->get(\'config_template\') . \'/template/common/header.tpl\')) {]]></search>\n			<add position="before"><![CDATA[\n		//XML\n		$data[\'quicksignup\'] = $this->load->controller(\'common/quicksignup\');\n		$data[\'text_register\'] = $this->language->get(\'text_register\');\n		$data[\'text_login\'] = $this->language->get(\'text_login\');\n		]]></add>\n		</operation>\n	</file>\n	<file path="catalog/language/*/common/header.php">\n		<operation>\n			<search><![CDATA[// Text]]></search>\n			<add position="before"><![CDATA[//XML\n$_[\'signin_or_register\']           = \'Sign In\\Register\';\n]]>			</add>\n		</operation>\n	</file>\n	<!--CATALOG ENDS-->\n</modification>', 0, '2017-01-16 11:56:59');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{"name":"Category","banner_id":"6","width":"182","height":"182","status":"1"}'),
(29, 'Home Page', 'carousel', '{"name":"Home Page","banner_id":"8","width":"100","height":"65","status":"1"}'),
(28, 'Home Page', 'featured', '{"name":"Home Page","product":["43","40","42","30"],"limit":"4","width":"200","height":"200","status":"1"}'),
(27, 'Home Page', 'slideshow', '{"name":"Home Page","banner_id":"7","width":"1170","height":"525","status":"1"}'),
(31, 'Banner 1', 'banner', '{"name":"Banner 1","banner_id":"6","width":"182","height":"182","status":"1"}'),
(32, 'Блок новостей', 'news', '{"name":"\\u0411\\u043b\\u043e\\u043a \\u043d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0435\\u0439","news_limit":"5","ncategory_id":"all","status":"1"}'),
(33, 'Новинки', 'latest', '{"name":"\\u041d\\u043e\\u0432\\u0438\\u043d\\u043a\\u0438","limit":"10","width":"500","height":"500","status":"1"}'),
(34, 'про нас ', 'html', '{"name":"\\u043f\\u0440\\u043e \\u043d\\u0430\\u0441 ","module_description":{"1":{"title":"","description":"&lt;div class=&quot;main_about_as&quot;&gt;\\r\\n            &lt;div class=&quot;title&quot;&gt;\\r\\n                &lt;h3&gt;&lt;a href=&quot;\\/about_company&quot;&gt;\\u041f\\u0440\\u043e Nikola&lt;\\/a&gt;&lt;\\/h3&gt;\\r\\n            &lt;\\/div&gt;\\r\\n            &lt;div class=&quot;wrapp_baner&quot;&gt;\\r\\n                &lt;img src=&quot;\\/image\\/catalog\\/about_us.jpg&quot; alt=&quot;about_us&quot;&gt;\\r\\n            &lt;\\/div&gt;\\r\\n            &lt;ul&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;15 \\u043b\\u0435\\u0442&lt;br&gt;\\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f&lt;br&gt;\\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u043a\\u0440\\u0435\\u0434\\u0438\\u0442&lt;br&gt;\\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;100%&lt;br&gt;\\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u044b&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u0431\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439&lt;br&gt;\\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n            &lt;\\/ul&gt;\\r\\n            &lt;p&gt;\\u0418\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442-\\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u0448\\u0430\\u043f\\u043e\\u0447\\u0435\\u043a \\u043d\\u0430 \\u043b\\u044e\\u0431\\u043e\\u0439 \\u0432\\u043a\\u0443\\u0441. \\u041c\\u044b \\u043e\\u0442\\u0441\\u043b\\u0435\\u0436\\u0438\\u0432\\u0430\\u0435\\u043c \\u043c\\u043e\\u0434\\u043d\\u044b\\u0435 \\u0442\\u0435\\u043d\\u0434\\u0435\\u043d\\u0446\\u0438\\u0438&lt;br&gt;\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u043c \\u0434\\u043b\\u044f \\u0432\\u0430\\u0441 \\u0443\\u0436\\u0435 \\u0431\\u043e\\u043b\\u0435\\u0435 15 \\u043b\\u0435\\u0442.&lt;\\/p&gt;\\r\\n            &lt;br&gt;\\r\\n            &lt;p&gt;\\u041c\\u044b \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u0443\\u0436\\u0435 15 \\u043b\\u0435\\u0442. \\u041f\\u043e\\u043c\\u0438\\u043c\\u043e \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u0430 \\u043b\\u0438\\u0447\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0430 \\u043c\\u044b \\u0442\\u0430\\u043a\\u0436\\u0435 \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u043c \\u0448\\u0438\\u0440\\u043e\\u043a\\u0438\\u0439 \\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442 \\u043f\\u043e\\u043b\\u044c\\u0441\\u043a\\u0438\\u0445 \\u0448\\u0430\\u043f\\u043e\\u043a \\u0438,&lt;br&gt;\\u0441\\u0440\\u0435\\u0434\\u0438 \\u043d\\u0435\\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0431\\u0440\\u0435\\u043d\\u0434\\u043e\\u0432, \\u043c\\u044b \\u044f\\u0432\\u043b\\u044f\\u0435\\u043c\\u0441\\u044f \\u043f\\u0435\\u0440\\u0432\\u044b\\u043c\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438 \\u0432 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435. \\u041f\\u043e\\u043c\\u0438\\u043c\\u043e \\u0437\\u0430\\u043f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u043e\\u0439 \\u043e\\u043d\\u043b\\u0430\\u0439\\u043d  \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0438 \\u0443 \\u043d\\u0430\\u0441 \\u0442\\u0430\\u043a\\u0436\\u0435&lt;br&gt;\\u0435\\u0441\\u0442\\u044c \\u0441\\u0435\\u0442\\u044c \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u043e\\u0432, \\u043a\\u0443\\u0434\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u043f\\u0440\\u0438\\u0439\\u0442\\u0438 \\u0438 \\u043e\\u0441\\u043c\\u043e\\u0442\\u0440\\u0435\\u0442\\u044c \\u0442\\u043e\\u0432\\u0430\\u0440, \\u043f\\u043e\\u0449\\u0443\\u043f\\u0430\\u0442\\u044c, \\u043f\\u0440\\u0438\\u043c\\u0435\\u0440\\u0438\\u0442\\u044c.&lt;\\/p&gt;\\r\\n        &lt;\\/div&gt;"},"3":{"title":"","description":"&lt;div class=&quot;main_about_as&quot;&gt;\\r\\n            &lt;div class=&quot;title&quot;&gt;\\r\\n                &lt;h3&gt;&lt;a href=&quot;\\/about_company&quot;&gt;\\u041f\\u0440\\u043e Nikola&lt;\\/a&gt;&lt;\\/h3&gt;\\r\\n            &lt;\\/div&gt;\\r\\n            &lt;div class=&quot;wrapp_baner&quot;&gt;\\r\\n                &lt;img src=&quot;\\/image\\/catalog\\/about_us.jpg&quot; alt=&quot;about_us&quot;&gt;\\r\\n            &lt;\\/div&gt;\\r\\n            &lt;ul&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;15 \\u043b\\u0435\\u0442&lt;br&gt;\\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f&lt;br&gt;\\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u043a\\u0440\\u0435\\u0434\\u0438\\u0442&lt;br&gt;\\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;100%&lt;br&gt;\\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u044b&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u0431\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439&lt;br&gt;\\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n            &lt;\\/ul&gt;\\r\\n            &lt;p&gt;\\u0418\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442-\\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u0448\\u0430\\u043f\\u043e\\u0447\\u0435\\u043a \\u043d\\u0430 \\u043b\\u044e\\u0431\\u043e\\u0439 \\u0432\\u043a\\u0443\\u0441. \\u041c\\u044b \\u043e\\u0442\\u0441\\u043b\\u0435\\u0436\\u0438\\u0432\\u0430\\u0435\\u043c \\u043c\\u043e\\u0434\\u043d\\u044b\\u0435 \\u0442\\u0435\\u043d\\u0434\\u0435\\u043d\\u0446\\u0438\\u0438&lt;br&gt;\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u043c \\u0434\\u043b\\u044f \\u0432\\u0430\\u0441 \\u0443\\u0436\\u0435 \\u0431\\u043e\\u043b\\u0435\\u0435 15 \\u043b\\u0435\\u0442.&lt;\\/p&gt;\\r\\n            &lt;br&gt;\\r\\n            &lt;p&gt;\\u041c\\u044b \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u0443\\u0436\\u0435 15 \\u043b\\u0435\\u0442. \\u041f\\u043e\\u043c\\u0438\\u043c\\u043e \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u0430 \\u043b\\u0438\\u0447\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0430 \\u043c\\u044b \\u0442\\u0430\\u043a\\u0436\\u0435 \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u043c \\u0448\\u0438\\u0440\\u043e\\u043a\\u0438\\u0439 \\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442 \\u043f\\u043e\\u043b\\u044c\\u0441\\u043a\\u0438\\u0445 \\u0448\\u0430\\u043f\\u043e\\u043a \\u0438,&lt;br&gt;\\u0441\\u0440\\u0435\\u0434\\u0438 \\u043d\\u0435\\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0431\\u0440\\u0435\\u043d\\u0434\\u043e\\u0432, \\u043c\\u044b \\u044f\\u0432\\u043b\\u044f\\u0435\\u043c\\u0441\\u044f \\u043f\\u0435\\u0440\\u0432\\u044b\\u043c\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438 \\u0432 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435. \\u041f\\u043e\\u043c\\u0438\\u043c\\u043e \\u0437\\u0430\\u043f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u043e\\u0439 \\u043e\\u043d\\u043b\\u0430\\u0439\\u043d  \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0438 \\u0443 \\u043d\\u0430\\u0441 \\u0442\\u0430\\u043a\\u0436\\u0435&lt;br&gt;\\u0435\\u0441\\u0442\\u044c \\u0441\\u0435\\u0442\\u044c \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u043e\\u0432, \\u043a\\u0443\\u0434\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u043f\\u0440\\u0438\\u0439\\u0442\\u0438 \\u0438 \\u043e\\u0441\\u043c\\u043e\\u0442\\u0440\\u0435\\u0442\\u044c \\u0442\\u043e\\u0432\\u0430\\u0440, \\u043f\\u043e\\u0449\\u0443\\u043f\\u0430\\u0442\\u044c, \\u043f\\u0440\\u0438\\u043c\\u0435\\u0440\\u0438\\u0442\\u044c.&lt;\\/p&gt;\\r\\n        &lt;\\/div&gt;"},"2":{"title":"","description":"&lt;div class=&quot;main_about_as&quot;&gt;\\r\\n            &lt;div class=&quot;title&quot;&gt;\\r\\n                &lt;h3&gt;&lt;a href=&quot;\\/about_company&quot;&gt;\\u041f\\u0440\\u043e Nikola&lt;\\/a&gt;&lt;\\/h3&gt;\\r\\n            &lt;\\/div&gt;\\r\\n            &lt;div class=&quot;wrapp_baner&quot;&gt;\\r\\n                &lt;img src=&quot;\\/image\\/catalog\\/about_us.jpg&quot; alt=&quot;about_us&quot;&gt;\\r\\n            &lt;\\/div&gt;\\r\\n            &lt;ul&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;15 \\u043b\\u0435\\u0442&lt;br&gt;\\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f&lt;br&gt;\\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u043a\\u0440\\u0435\\u0434\\u0438\\u0442&lt;br&gt;\\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;100%&lt;br&gt;\\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u044b&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n                &lt;li&gt;\\r\\n                    &lt;p&gt;\\u0431\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439&lt;br&gt;\\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442&lt;\\/p&gt;\\r\\n                &lt;\\/li&gt;\\r\\n            &lt;\\/ul&gt;\\r\\n            &lt;p&gt;\\u0418\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442-\\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u0448\\u0430\\u043f\\u043e\\u0447\\u0435\\u043a \\u043d\\u0430 \\u043b\\u044e\\u0431\\u043e\\u0439 \\u0432\\u043a\\u0443\\u0441. \\u041c\\u044b \\u043e\\u0442\\u0441\\u043b\\u0435\\u0436\\u0438\\u0432\\u0430\\u0435\\u043c \\u043c\\u043e\\u0434\\u043d\\u044b\\u0435 \\u0442\\u0435\\u043d\\u0434\\u0435\\u043d\\u0446\\u0438\\u0438&lt;br&gt;\\u0438 \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u043c \\u0434\\u043b\\u044f \\u0432\\u0430\\u0441 \\u0443\\u0436\\u0435 \\u0431\\u043e\\u043b\\u0435\\u0435 15 \\u043b\\u0435\\u0442.&lt;\\/p&gt;\\r\\n            &lt;br&gt;\\r\\n            &lt;p&gt;\\u041c\\u044b \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u0443\\u0436\\u0435 15 \\u043b\\u0435\\u0442. \\u041f\\u043e\\u043c\\u0438\\u043c\\u043e \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u0430 \\u043b\\u0438\\u0447\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0430 \\u043c\\u044b \\u0442\\u0430\\u043a\\u0436\\u0435 \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u043c \\u0448\\u0438\\u0440\\u043e\\u043a\\u0438\\u0439 \\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442 \\u043f\\u043e\\u043b\\u044c\\u0441\\u043a\\u0438\\u0445 \\u0448\\u0430\\u043f\\u043e\\u043a \\u0438,&lt;br&gt;\\u0441\\u0440\\u0435\\u0434\\u0438 \\u043d\\u0435\\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0431\\u0440\\u0435\\u043d\\u0434\\u043e\\u0432, \\u043c\\u044b \\u044f\\u0432\\u043b\\u044f\\u0435\\u043c\\u0441\\u044f \\u043f\\u0435\\u0440\\u0432\\u044b\\u043c\\u0438 \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c\\u0438 \\u0432 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435. \\u041f\\u043e\\u043c\\u0438\\u043c\\u043e \\u0437\\u0430\\u043f\\u043b\\u0430\\u043d\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u043e\\u0439 \\u043e\\u043d\\u043b\\u0430\\u0439\\u043d  \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0438 \\u0443 \\u043d\\u0430\\u0441 \\u0442\\u0430\\u043a\\u0436\\u0435&lt;br&gt;\\u0435\\u0441\\u0442\\u044c \\u0441\\u0435\\u0442\\u044c \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u043e\\u0432, \\u043a\\u0443\\u0434\\u0430 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u043f\\u0440\\u0438\\u0439\\u0442\\u0438 \\u0438 \\u043e\\u0441\\u043c\\u043e\\u0442\\u0440\\u0435\\u0442\\u044c \\u0442\\u043e\\u0432\\u0430\\u0440, \\u043f\\u043e\\u0449\\u0443\\u043f\\u0430\\u0442\\u044c, \\u043f\\u0440\\u0438\\u043c\\u0435\\u0440\\u0438\\u0442\\u044c.&lt;\\/p&gt;\\r\\n        &lt;\\/div&gt;"}},"status":"1"}'),
(35, 'social_login', 'ulogin', '{"name":"social_login","uloginid":"c5572c89","type":"offline","status":"1"}');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_nauthor`
--

CREATE TABLE `oc_nauthor` (
  `nauthor_id` int(11) NOT NULL,
  `adminid` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_nauthor_description`
--

CREATE TABLE `oc_nauthor_description` (
  `nauthor_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `ctitle` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ncategory`
--

CREATE TABLE `oc_ncategory` (
  `ncategory_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_ncategory`
--

INSERT INTO `oc_ncategory` (`ncategory_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(59, '', 0, 0, 4, 0, 1, '2016-11-06 23:32:16', '2017-08-22 12:09:32');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ncategory_description`
--

CREATE TABLE `oc_ncategory_description` (
  `ncategory_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_ncategory_description`
--

INSERT INTO `oc_ncategory_description` (`ncategory_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`) VALUES
(59, 3, 'Галерея', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', ''),
(59, 2, 'Gallery', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', ''),
(59, 4, 'Galeria', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', ''),
(59, 1, 'Галерея', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ncategory_to_layout`
--

CREATE TABLE `oc_ncategory_to_layout` (
  `ncategory_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_ncategory_to_layout`
--

INSERT INTO `oc_ncategory_to_layout` (`ncategory_id`, `store_id`, `layout_id`) VALUES
(59, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ncategory_to_store`
--

CREATE TABLE `oc_ncategory_to_store` (
  `ncategory_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_ncategory_to_store`
--

INSERT INTO `oc_ncategory_to_store` (`ncategory_id`, `store_id`) VALUES
(59, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ncomments`
--

CREATE TABLE `oc_ncomments` (
  `ncomment_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `language_id` int(2) NOT NULL,
  `reply_id` int(11) NOT NULL DEFAULT '0',
  `author` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `text` text COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news`
--

CREATE TABLE `oc_news` (
  `news_id` int(11) NOT NULL,
  `nauthor_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `acom` int(1) NOT NULL DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `image2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `gal_thumb_w` int(5) NOT NULL,
  `gal_thumb_h` int(5) NOT NULL,
  `gal_popup_w` int(5) NOT NULL,
  `gal_popup_h` int(5) NOT NULL,
  `gal_slider_h` int(4) NOT NULL,
  `gal_slider_t` int(1) NOT NULL,
  `date_pub` datetime DEFAULT NULL,
  `gal_slider_w` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_news`
--

INSERT INTO `oc_news` (`news_id`, `nauthor_id`, `status`, `image`, `acom`, `date_added`, `date_updated`, `image2`, `sort_order`, `gal_thumb_w`, `gal_thumb_h`, `gal_popup_w`, `gal_popup_h`, `gal_slider_h`, `gal_slider_t`, `date_pub`, `gal_slider_w`) VALUES
(12, 0, 1, '', 0, '2017-01-16 12:36:38', '2017-01-16 12:36:38', '', 1, 150, 150, 700, 700, 400, 1, '2017-01-15 12:36:38', 980),
(13, 0, 1, 'catalog/gallery/gal1.jpg', 0, '2017-08-10 14:22:12', '2017-08-22 09:09:55', '', 1, 270, 270, 700, 700, 400, 1, '2017-08-09 14:22:12', 980),
(14, 0, 1, 'catalog/gallery/gal3.jpg', 0, '2017-08-10 14:22:12', '2017-08-10 14:24:18', '', 1, 150, 150, 700, 700, 400, 1, '2017-08-09 14:22:12', 980),
(15, 0, 1, 'catalog/gallery/gal4.jpg', 0, '2017-08-10 14:22:12', '2017-08-10 14:25:14', '', 1, 150, 150, 700, 700, 400, 1, '2017-08-09 14:22:12', 980),
(16, 0, 1, 'catalog/gallery/gal5.jpg', 0, '2017-08-10 14:22:12', '2017-08-10 14:25:25', '', 1, 150, 150, 700, 700, 400, 1, '2017-08-09 14:22:12', 980),
(18, 0, 1, 'catalog/gallery/gal2.jpg', 0, '2017-08-10 14:18:35', '2017-08-10 14:18:35', '', 1, 150, 150, 700, 700, 400, 1, '2017-08-09 14:18:35', 980);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsletter`
--

CREATE TABLE `oc_newsletter` (
  `news_id` int(11) NOT NULL,
  `news_email` varchar(255) NOT NULL,
  `news_name` varchar(255) NOT NULL,
  `token_key` varchar(64) NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsletter`
--

INSERT INTO `oc_newsletter` (`news_id`, `news_email`, `news_name`, `token_key`, `status`) VALUES
(2, 'mail@mail.ru', 'rrr', '0', NULL),
(3, '1111@mail.ru', 'asdasd', 'd9c4f8682e', 0),
(4, 'gggg@mail.ru', 'eqwe', '3dc1195317', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_description`
--

CREATE TABLE `oc_news_description` (
  `news_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ctitle` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description2` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_desc` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `ntags` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cfield1` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cfield2` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cfield3` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cfield4` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_news_description`
--

INSERT INTO `oc_news_description` (`news_id`, `language_id`, `title`, `ctitle`, `description`, `description2`, `meta_desc`, `meta_key`, `ntags`, `cfield1`, `cfield2`, `cfield3`, `cfield4`) VALUES
(12, 1, 'Lorem ipsum', '', '&lt;p&gt;Lorem ipsum&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(12, 3, 'Lorem ipsum', '', '&lt;p&gt;Lorem ipsum&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(12, 2, 'Lorem ipsum', '', '&lt;p&gt;Lorem ipsum&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(13, 3, 'Новая коллекция зимних шапочек  Nikola', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(13, 2, 'Новая коллекция зимних шапочек  Nikola', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(13, 1, 'Новая коллекция зимних шапочек  Nikola', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(14, 2, 'Шапочки для малышей', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(14, 3, 'Шапочки для малышей', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(14, 1, 'Шапочки для малышей', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(15, 2, 'Новая коллекция зимних шапочек  Nikola copy', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(15, 1, 'Новая коллекция зимних шапочек  Nikola', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(15, 3, 'Новая коллекция зимних шапочек  Nikola copy', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(16, 2, 'Осень - Весна 2017-18', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(16, 3, 'Осень - Весна 2017-18', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(16, 1, 'Осень - Весна 2017-18', '', '&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(18, 1, 'Осень - Весна 2017-18', '', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(18, 3, 'Осень - Весна 2017-18', '', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(18, 2, 'Осень - Весна 2017-18', '', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', ''),
(13, 4, 'Nowa kolekcja zimowych kapeluszy Nikola', '', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_gallery`
--

CREATE TABLE `oc_news_gallery` (
  `news_image_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `text` text NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_news_gallery`
--

INSERT INTO `oc_news_gallery` (`news_image_id`, `news_id`, `image`, `text`, `sort_order`) VALUES
(30, 13, 'catalog/gallery2/06-07_12.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(29, 13, 'catalog/gallery2/06-07_11.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(28, 13, 'catalog/gallery2/06-07_4.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(27, 13, 'catalog/gallery2/06-07_5.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(26, 13, 'catalog/gallery2/06-07_6.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(25, 13, 'catalog/gallery2/06-07_7.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(24, 13, 'catalog/gallery2/06-07_8.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(23, 13, 'catalog/gallery2/06-07_9.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(31, 13, 'catalog/gallery2/06-07_3.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(32, 13, 'catalog/gallery2/06-07_10.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0),
(33, 13, 'catalog/gallery2/06-07.jpg', 'a:4:{i:1;s:21:"Шапочка Nikola";i:3;s:21:"Шапочка Nikola";i:2;s:21:"Шапочка Nikola";i:4;s:21:"Шапочка Nikola";}', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_related`
--

CREATE TABLE `oc_news_related` (
  `news_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_to_layout`
--

CREATE TABLE `oc_news_to_layout` (
  `news_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_news_to_layout`
--

INSERT INTO `oc_news_to_layout` (`news_id`, `store_id`, `layout_id`) VALUES
(8, 0, 0),
(9, 0, 0),
(10, 0, 0),
(11, 0, 0),
(12, 0, 0),
(13, 0, 0),
(14, 0, 0),
(15, 0, 0),
(16, 0, 0),
(17, 0, 0),
(18, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_to_ncategory`
--

CREATE TABLE `oc_news_to_ncategory` (
  `news_id` int(11) NOT NULL,
  `ncategory_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_news_to_ncategory`
--

INSERT INTO `oc_news_to_ncategory` (`news_id`, `ncategory_id`) VALUES
(13, 59),
(14, 59),
(15, 59),
(16, 59),
(18, 59);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_to_store`
--

CREATE TABLE `oc_news_to_store` (
  `news_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `oc_news_to_store`
--

INSERT INTO `oc_news_to_store` (`news_id`, `store_id`) VALUES
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(18, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_video`
--

CREATE TABLE `oc_news_video` (
  `news_video_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `video` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 0),
(2, 'radio', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Розмір'),
(2, 1, 'Колір');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(1, 1, '', 0),
(2, 2, '', 0),
(3, 2, '', 0),
(4, 2, '', 0),
(5, 2, '', 0),
(6, 2, '', 0),
(7, 2, '', 0),
(8, 1, '', 0),
(9, 2, '', 0),
(10, 2, '', 0),
(11, 2, '', 0),
(12, 2, '', 0),
(13, 2, '', 0),
(14, 2, '', 0),
(15, 2, '', 0),
(16, 2, '', 0),
(17, 2, '', 0),
(18, 2, '', 0),
(19, 2, '', 0),
(20, 2, '', 0),
(21, 2, '', 0),
(22, 2, '', 0),
(23, 2, '', 0),
(24, 2, '', 0),
(25, 2, '', 0),
(26, 2, '', 0),
(27, 2, '', 0),
(28, 2, '', 0),
(29, 2, '', 0),
(30, 2, '', 0),
(31, 2, '', 0),
(32, 1, '', 0),
(33, 1, '', 0),
(34, 1, '', 0),
(35, 2, '', 0),
(36, 2, '', 0),
(37, 2, '', 0),
(38, 2, '', 0),
(39, 2, '', 0),
(40, 2, '', 0),
(41, 2, '', 0),
(42, 2, '', 0),
(43, 2, '', 0),
(44, 2, '', 0),
(45, 2, '', 0),
(46, 2, '', 0),
(47, 1, '', 0),
(48, 2, '', 0),
(49, 2, '', 0),
(50, 2, '', 0),
(51, 2, '', 0),
(52, 2, '', 0),
(53, 2, '', 0),
(54, 2, '', 0),
(55, 1, '', 0),
(56, 2, '', 0),
(57, 1, '', 0),
(58, 2, '', 0),
(59, 2, '', 0),
(60, 2, '', 0),
(61, 2, '', 0),
(62, 2, '', 0),
(63, 2, '', 0),
(64, 2, '', 0),
(65, 2, '', 0),
(66, 2, '', 0),
(67, 2, '', 0),
(68, 2, '', 0),
(69, 2, '', 0),
(70, 2, '', 0),
(71, 2, '', 0),
(72, 2, '', 0),
(73, 2, '', 0),
(74, 2, '', 0),
(75, 2, '', 0),
(76, 2, '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(1, 1, 1, '48-52'),
(2, 1, 2, 'Сірий'),
(3, 1, 2, 'Білий'),
(4, 1, 2, 'Блідо-рожевий'),
(5, 1, 2, 'Молочний'),
(6, 1, 2, 'Персиковий'),
(7, 1, 2, 'Синій'),
(8, 1, 1, 'універсал'),
(9, 1, 2, '14422'),
(10, 1, 2, '14403'),
(11, 1, 2, '14401'),
(12, 1, 2, '14414'),
(13, 1, 2, '14408'),
(14, 1, 2, 'Бургундський'),
(15, 1, 2, 'Темно-рожевий'),
(16, 1, 2, 'Темно-сірий'),
(17, 1, 2, 'Темно-синій'),
(18, 1, 2, 'Зелений'),
(19, 1, 2, 'Асвальтовий'),
(20, 1, 2, 'Пісочний'),
(21, 1, 2, 'Ліловий'),
(22, 1, 2, 'Мишка'),
(23, 1, 2, 'Чорний'),
(24, 1, 2, 'Буро-сірий'),
(25, 1, 2, 'Махагон'),
(26, 1, 2, 'Темно-коричневий'),
(27, 1, 2, 'Рижий'),
(28, 1, 2, 'Лампоне'),
(29, 1, 2, 'Баклажан'),
(30, 1, 2, 'Рубін'),
(31, 1, 2, 'Капучіно'),
(32, 1, 1, '42-44'),
(33, 1, 1, '44-46'),
(34, 1, 1, '46-48'),
(35, 1, 2, 'Тортора'),
(36, 1, 2, 'Світло-фіолетовий'),
(37, 1, 2, 'Рожевий'),
(38, 1, 2, 'Яскраво-рожевий'),
(39, 1, 2, 'Світло-рожевий'),
(40, 1, 2, 'Фіолетовий'),
(41, 1, 2, 'Фуксія'),
(42, 1, 2, 'Світло-бежевий'),
(43, 1, 2, 'Світло-сірий'),
(44, 1, 2, 'Салатовий'),
(45, 1, 2, 'Темно-малиновий'),
(46, 1, 2, 'Яскраво-малиновий'),
(47, 1, 1, '48-50'),
(48, 1, 2, 'Бірюза'),
(49, 1, 2, 'Бордовий'),
(50, 1, 2, 'Жовтий'),
(51, 1, 2, 'Коричневий'),
(52, 1, 2, 'Малиновий'),
(53, 1, 2, 'Голубий'),
(54, 1, 2, 'Графіт'),
(55, 1, 1, '50-52'),
(56, 1, 2, 'Червоний'),
(57, 1, 1, '52-54'),
(58, 1, 2, 'Темно-бежевий'),
(59, 1, 2, '14417'),
(60, 1, 2, 'Джинсовий'),
(61, 1, 2, 'Бежевий'),
(62, 1, 2, 'Темно-фіолетовий'),
(63, 1, 2, 'Сливовий'),
(64, 1, 2, 'Cиній'),
(65, 1, 2, '4558'),
(66, 1, 2, '4576'),
(67, 1, 2, '4577'),
(68, 1, 2, '4553'),
(69, 1, 2, '4566'),
(70, 1, 2, '4551'),
(71, 1, 2, '4564'),
(72, 1, 2, '4557'),
(73, 1, 2, '4559'),
(74, 1, 2, '4555'),
(75, 1, 2, '4572'),
(76, 1, 2, '4573');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(40) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(1, 0, 'INV-2016-00', 0, 'Мой Магазин', 'http://nikola/', 0, 1, 'TEST', 'TEST', 'melnykserhiy1@gmail.com', '+38 (999) 999 99 99', '', '[]', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '336622', 'Украина', 220, 'Луцк', 3502, '', '[]', 'Оплата при получении', 'cod', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '336622', 'Украина', 220, 'Луцк', 3502, '', '[]', 'Бесплатная Доставка', 'free.free', '', '1199.0000', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4', '2017-08-08 15:39:55', '2017-08-08 15:39:58'),
(2, 0, 'INV-2016-00', 0, 'Мой Магазин', 'http://nikola/', 1, 1, 'TEST', 'TEST', 'melnykserhiy1@gmail.com', '+9999999999', '', '[]', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Армения', 11, 'Лори', 185, '', '[]', 'Оплата при получении', 'cod', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Армения', 11, 'Лори', 185, '', '[]', 'Бесплатная Доставка', 'free.free', '', '1199.0000', 1, 0, '0.0000', 0, '', 1, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4', '2017-08-08 15:41:27', '2017-08-08 15:41:34'),
(3, 0, 'INV-2016-00', 0, 'Мой Магазин', 'http://nikola/', 1, 1, 'TEST', 'TEST', 'melnykserhiy1@gmail.com', '+38 (999) 999 99 99', '', '[]', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Армения', 11, 'Лори', 185, '', '[]', 'Оплата при отриманні', 'cod', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Армения', 11, 'Лори', 185, '', '[]', 'Безкоштовна Доставка', 'free.free', '', '1199.0000', 0, 0, '0.0000', 0, '', 3, 1, 'RUB', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4', '2017-08-08 15:51:55', '2017-08-08 15:51:55'),
(4, 0, 'INV-2016-00', 0, 'Мой Магазин', 'http://nikola/', 0, 1, 'TEST', 'TEST', 'melnykserhiy1@gmail.com', '+38 (999) 999 99 99', '', '[]', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Украина', 220, 'Донецк', 3485, '', '[]', 'Оплата при получении', 'cod', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Украина', 220, 'Донецк', 3485, '', '[]', 'Бесплатная Доставка', 'free.free', '', '420.0000', 1, 0, '0.0000', 0, '', 1, 2, 'USD', '0.01720000', '127.0.0.1', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4', '2017-08-31 18:33:35', '2017-08-31 18:33:37'),
(5, 0, 'INV-2016-00', 0, 'Мой Магазин', 'http://nikola/', 0, 1, 'TEST', 'TEST', 'melnykserhiy1@gmail.com', '+38 (999) 999 99 99', '', '[]', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Украина', 220, 'Донецк', 3485, '', '[]', 'Оплата при получении', 'cod', 'TEST', 'TEST', '', 'adres 1', '', 'TEST', '', 'Украина', 220, 'Донецк', 3485, '', '[]', 'Бесплатная Доставка', 'free.free', '', '360.0000', 1, 0, '0.0000', 0, '', 1, 2, 'USD', '0.01720000', '127.0.0.1', '', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4', '2017-08-31 18:34:28', '2017-08-31 18:34:32'),
(6, 0, 'INV-2016-00', 0, 'Мой Магазин', 'http://nikola/', 0, 1, '12312', '', 'asd@asd', '+38 (333) 333 33 33', '', '', '12312', '', '', '', '', '', '', '', 0, '', 0, '', '', '--', 'free_checkout', '12312', '', '', '', '', '', '', '', 0, '', 0, '', '', '', '', '', '530.0000', 1, 0, '0.0000', 0, '', 1, 2, 'USD', '0.01720000', '127.0.0.1', '', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36', 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4', '2017-08-31 19:40:47', '2017-08-31 19:40:47');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_custom_field`
--

CREATE TABLE `oc_order_custom_field` (
  `order_custom_field_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `location` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_history`
--

INSERT INTO `oc_order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(1, 1, 1, 0, '', '2017-08-08 15:39:58'),
(2, 2, 1, 0, '', '2017-08-08 15:41:34'),
(3, 4, 1, 0, '', '2017-08-31 18:33:37'),
(4, 5, 1, 0, '', '2017-08-31 18:34:32'),
(5, 6, 1, 0, '', '2017-08-31 19:40:47');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_option`
--

INSERT INTO `oc_order_option` (`order_option_id`, `order_id`, `order_product_id`, `product_option_id`, `product_option_value_id`, `name`, `value`, `type`) VALUES
(1, 4, 14, 39, 143, 'Розмір', '46-48', 'radio'),
(2, 4, 14, 40, 148, 'Колір', 'Світло-сірий', 'radio'),
(3, 5, 15, 7, 22, 'Розмір', '48-52', 'radio'),
(4, 5, 15, 8, 25, 'Колір', 'Сірий', 'radio');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(1, 1, 54, 'Napapijri - шапка', 'model1', 1, '1199.0000', '1199.0000', '0.0000', 0),
(2, 2, 54, 'Napapijri - шапка', 'model1', 1, '1199.0000', '1199.0000', '0.0000', 0),
(13, 3, 56, 'Napapijri - шапка Fabralyn', 'model1', 1, '1199.0000', '1199.0000', '0.0000', 0),
(14, 4, 21, '15 Z 03 Australia', 'не задана', 1, '420.0000', '420.0000', '0.0000', 0),
(15, 5, 4, '15 Z 04 SOFFIO (изософтова подкл.)', 'не задана', 1, '360.0000', '360.0000', '0.0000', 0),
(16, 6, 112, '15 Z-59K SOFFIO', 'не задана', 1, '530.0000', '530.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_simple_fields`
--

CREATE TABLE `oc_order_simple_fields` (
  `order_id` int(11) NOT NULL,
  `metadata` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Ожидание'),
(2, 1, 'В обработке'),
(3, 1, 'Доставлено'),
(7, 1, 'Отменено'),
(5, 1, 'Сделка завершена'),
(8, 1, 'Возврат'),
(9, 1, 'Отмена и аннулирование'),
(10, 1, 'Неудавшийся'),
(11, 1, 'Возмещенный'),
(12, 1, 'Полностью измененный'),
(13, 1, 'Полный возврат'),
(1, 2, 'Pending'),
(2, 2, 'Processing'),
(3, 2, 'Shipped'),
(7, 2, 'Canceled'),
(5, 2, 'Complete'),
(8, 2, 'Denied'),
(9, 2, 'Canceled Reversal'),
(10, 2, 'Failed'),
(11, 2, 'Refunded'),
(12, 2, 'Reversed'),
(13, 2, 'Chargeback'),
(1, 3, 'Ожидание'),
(2, 3, 'В обработке'),
(3, 3, 'Доставлено'),
(7, 3, 'Отменено'),
(5, 3, 'Сделка завершена'),
(8, 3, 'Возврат'),
(9, 3, 'Отмена и аннулирование'),
(10, 3, 'Неудавшийся'),
(11, 3, 'Возмещенный'),
(12, 3, 'Полностью измененный'),
(13, 3, 'Полный возврат'),
(1, 4, 'Ожидание'),
(2, 4, 'В обработке'),
(3, 4, 'Доставлено'),
(7, 4, 'Отменено'),
(5, 4, 'Сделка завершена'),
(8, 4, 'Возврат'),
(9, 4, 'Отмена и аннулирование'),
(10, 4, 'Неудавшийся'),
(11, 4, 'Возмещенный'),
(12, 4, 'Полностью измененный'),
(13, 4, 'Полный возврат');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(1, 1, 'sub_total', 'Предварительная стоимость', '1199.0000', 1),
(2, 1, 'total', 'Итого', '1199.0000', 9),
(3, 2, 'sub_total', 'Предварительная стоимость', '1199.0000', 1),
(4, 2, 'total', 'Итого', '1199.0000', 9),
(26, 3, 'total', 'Всього', '1199.0000', 9),
(25, 3, 'sub_total', 'Попередня вартість', '1199.0000', 1),
(27, 4, 'sub_total', 'Предварительная стоимость', '420.0000', 1),
(28, 4, 'total', 'Итого', '420.0000', 9),
(29, 5, 'sub_total', 'Предварительная стоимость', '360.0000', 1),
(30, 5, 'total', 'Итого', '360.0000', 9),
(31, 6, 'sub_total', 'Предварительная стоимость', '530.0000', 1),
(32, 6, 'total', 'Итого', '530.0000', 9);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,2) NOT NULL DEFAULT '0.00',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,2) NOT NULL DEFAULT '0.00',
  `width` decimal(15,2) NOT NULL DEFAULT '0.00',
  `height` decimal(15,2) NOT NULL DEFAULT '0.00',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(1, 'не задана', '', '', '', '', '', '', '', 336, 0, 'import_files/14/1484f158467511e5803300ac06be3045_2294d7833fd5453e8246022cf4ccd7f9.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(2, 'не задана', '', '', '', '', '', '', '', 221, 0, 'import_files/14/1484f15d467511e5803300ac06be3045_3c425ce1b1d8404b83ad98b780c2dab7.jpg', 0, 1, '800.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(3, 'не задана', '', '', '', '', '', '', '', 76, 0, 'import_files/14/1484f15f467511e5803300ac06be3045_ee7af2361f3a43a7ada5691c8e3d5076.jpg', 0, 1, '360.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(4, 'не задана', '', '', '', '', '', '', '', 74, 0, 'import_files/14/1484f167467511e5803300ac06be3045_198e8a82d4c34a87a1853fc7cf2827ac.jpg', 0, 1, '360.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 1, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(5, 'не задана', '', '', '', '', '', '', '', 291, 0, 'import_files/14/1484f16d467511e5803300ac06be3045_a92fb26153f240d4b93bd994048f0b85.jpg', 0, 1, '375.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(6, 'не задана', '', '', '', '', '', '', '', 156, 0, 'import_files/14/1484f174467511e5803300ac06be3045_3d68796d54164f3d9151d732a6193c9a.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(7, 'не задана', '', '', '', '', '', '', '', 174, 0, 'import_files/14/1484f17b467511e5803300ac06be3045_79d4a81301ad45ed8a0fc8d7b6613d2e.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(8, 'не задана', '', '', '', '', '', '', '', 156, 0, 'import_files/14/1484f180467511e5803300ac06be3045_e61761fac6e54877b1df5d324040ed78.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(9, 'не задана', '', '', '', '', '', '', '', 199, 0, 'import_files/14/1484f186467511e5803300ac06be3045_a9411a2096c9429d9438a8256d4c55e0.jpg', 0, 1, '450.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(10, 'не задана', '', '', '', '', '', '', '', 334, 0, 'import_files/34/34c734d0471c11e5803300ac06be3045_d0633c855dda48da856a96a5a3403ec1.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(11, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 0, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(12, 'не задана', '', '', '', '', '', '', '', 383, 0, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_992d9ce886cf4a25bd3e4472a6a36e87.jpg', 0, 1, '525.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(13, 'не задана', '', '', '', '', '', '', '', 206, 0, 'import_files/cb/cbac2b254ae711e5803300ac06be3045_d9aaaad41bff4396b26c1a6b32e38484.jpg', 0, 1, '240.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(14, 'не задана', '', '', '', '', '', '', '', 54, 0, 'import_files/cb/cbac2b2c4ae711e5803300ac06be3045_1407429c2c814572bd6ea6a82fb71a4f.jpg', 0, 1, '420.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(15, 'не задана', '', '', '', '', '', '', '', 185, 0, 'import_files/cb/cbac2b364ae711e5803300ac06be3045_1921268a67e849509049953d83b08ea5.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(16, 'не задана', '', '', '', '', '', '', '', 348, 0, 'import_files/cb/cbac2b3b4ae711e5803300ac06be3045_a5012d57c7b8469f829fcf91f5846417.jpg', 0, 1, '325.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(17, 'не задана', '', '', '', '', '', '', '', 1, 0, 'import_files/f7/f7d2d3b652bf11e5803300ac06be3045_728e3952633c41bababaf215cb74fd97.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(18, 'не задана', '', '', '', '', '', '', '', 279, 0, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_162e5356408c486aafca0dfccc7cc8d7.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(19, 'не задана', '', '', '', '', '', '', '', 81, 0, 'import_files/f7/f7d2d3cc52bf11e5803300ac06be3045_8bdcd265ad7c4409bd4244c52225b5a1.jpg', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(20, 'не задана', '', '', '', '', '', '', '', 102, 0, 'import_files/f7/f7d2d3dc52bf11e5803300ac06be3045_c225ec76189a4eb18da0421b84f3647e.jpg', 0, 1, '420.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(21, 'не задана', '', '', '', '', '', '', '', 242, 0, 'import_files/f7/f7d2d3e952bf11e5803300ac06be3045_456673c9048741229be5efb941f86566.jpg', 0, 1, '420.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 17, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(22, 'не задана', '', '', '', '', '', '', '', 130, 0, 'import_files/fa/fad705d152dc11e5803300ac06be3045_3ddb5b47110d4557beaea7b8defef515.jpg', 0, 1, '420.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(23, 'не задана', '', '', '', '', '', '', '', 71, 0, 'import_files/fa/fad705de52dc11e5803300ac06be3045_dd43e7d5d60847958e96b52e125fb97c.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(24, 'не задана', '', '', '', '', '', '', '', 199, 0, 'import_files/fa/fad705e552dc11e5803300ac06be3045_126740152f304c7d87c8cb7c8d04511a.jpg', 0, 1, '360.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(25, 'не задана', '', '', '', '', '', '', '', 77, 0, 'import_files/fa/fad705ec52dc11e5803300ac06be3045_64cbe0ab2b604c0594eb02f1551a16bb.jpg', 0, 1, '300.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(26, 'не задана', '', '', '', '', '', '', '', 294, 0, 'import_files/fa/fad705f352dc11e5803300ac06be3045_b465d88ea92e4a0caf81720b2032a62c.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(27, 'не задана', '', '', '', '', '', '', '', 48, 0, 'import_files/fa/fad705fb52dc11e5803300ac06be3045_a22d791397cd4ecd82b23e80eaf78c53.jpg', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(28, 'не задана', '', '', '', '', '', '', '', 109, 0, 'import_files/fa/fad7060b52dc11e5803300ac06be3045_9ef6f4f558204a9ebc8e3a663b687519.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(29, 'не задана', '', '', '', '', '', '', '', 145, 0, 'import_files/94/94c0004852e711e5803300ac06be3045_fed234fd56e1402487e8e68ad126a443.jpg', 0, 1, '390.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(30, 'не задана', '', '', '', '', '', '', '', 186, 0, 'import_files/94/94c0005852e711e5803300ac06be3045_844340be80b94f03bea6bc3350e83ea7.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(31, 'не задана', '', '', '', '', '', '', '', 82, 0, 'import_files/94/94c0006852e711e5803300ac06be3045_7c766ea6c418420f8273f22336f236ff.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(32, 'не задана', '', '', '', '', '', '', '', 141, 0, 'import_files/94/94c0007852e711e5803300ac06be3045_a325b7e44bf94e7f8362444cebcffb69.jpg', 0, 1, '360.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(33, 'не задана', '', '', '', '', '', '', '', 44, 0, 'import_files/b0/b051ced552f011e5803300ac06be3045_b5cd2c97d6a4437cae07115557f160ae.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(34, 'не задана', '', '', '', '', '', '', '', 493, 0, 'import_files/b0/b051cee552f011e5803300ac06be3045_bca24fc7a2f34d7580378ee7358e3784.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(35, 'не задана', '', '', '', '', '', '', '', 234, 0, 'import_files/9a/9ac78bf3530b11e5803300ac06be3045_104c80cf7cfa42bdb5462878aed13506.jpg', 0, 1, '375.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(36, 'не задана', '', '', '', '', '', '', '', 260, 0, 'import_files/9a/9ac78bfa530b11e5803300ac06be3045_b91236b3299e4c8e8afc5763adc65efe.jpg', 0, 1, '325.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(37, 'не задана', '', '', '', '', '', '', '', 256, 0, 'import_files/9a/9ac78c00530b11e5803300ac06be3045_5c2b559e69c74dd1a9145ad0b67de2c9.jpg', 0, 1, '300.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(38, 'не задана', '', '', '', '', '', '', '', 136, 0, 'import_files/9a/9ac78c06530b11e5803300ac06be3045_aeaf5a146b0c4f19a218d1eeea50ed99.jpg', 0, 1, '375.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:41'),
(39, 'не задана', '', '', '', '', '', '', '', 409, 0, 'import_files/9a/9a662f24538811e5803300ac06be3045_a0afe79a808d4f72b10fed41f691c9a6.jpg', 0, 1, '325.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(40, 'не задана', '', '', '', '', '', '', '', 208, 0, 'import_files/9a/9a662f2f538811e5803300ac06be3045_6b7a20ca720540f2bb2b605ace0f60d0.jpg', 0, 1, '275.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(41, 'не задана', '', '', '', '', '', '', '', 207, 0, 'import_files/9a/9a662f3a538811e5803300ac06be3045_adf0633ed5ef4e158c5b1e6213eb7350.jpg', 0, 1, '420.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(42, 'не задана', '', '', '', '', '', '', '', 160, 0, 'import_files/9a/9a662f4a538811e5803300ac06be3045_ccf24454e1f644ba941476cdac0ca3b8.jpg', 0, 1, '380.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(43, 'не задана', '', '', '', '', '', '', '', 95, 0, 'import_files/9a/9a662f50538811e5803300ac06be3045_d95a7998fff54d34bbdc04a5d14d038d.jpg', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(44, 'не задана', '', '', '', '', '', '', '', 157, 0, 'import_files/9a/9a662f56538811e5803300ac06be3045_a2d40610d86244ca8a1f0c64dac3a542.jpg', 0, 1, '390.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(45, 'не задана', '', '', '', '', '', '', '', 102, 0, 'import_files/9a/9a662f5c538811e5803300ac06be3045_b81f174de5444a878087753926291ee6.jpg', 0, 1, '360.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(46, 'не задана', '', '', '', '', '', '', '', 143, 0, '', 0, 1, '380.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(47, 'не задана', '', '', '', '', '', '', '', 85, 0, 'import_files/6b/6b849be8539011e5803300ac06be3045_bb4144c112fe4e40953fc3049821eb8b.jpg', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(48, 'не задана', '', '', '', '', '', '', '', 193, 0, 'import_files/6b/6b849bef539011e5803300ac06be3045_203b03ee6da64d4bb79cff84ebfb4bcd.jpg', 0, 1, '360.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(49, 'не задана', '', '', '', '', '', '', '', 106, 0, '', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(50, 'не задана', '', '', '', '', '', '', '', 561, 0, 'import_files/6b/6b849c05539011e5803300ac06be3045_c9508326ff9543799118f0cb8106bd10.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(51, 'не задана', '', '', '', '', '', '', '', 70, 0, 'import_files/6b/6b849c0a539011e5803300ac06be3045_703bcc9ef1164191baf358bc448bbf1f.jpg', 0, 1, '380.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(52, 'не задана', '', '', '', '', '', '', '', 111, 0, 'import_files/6b/6b849c10539011e5803300ac06be3045_1a660876cfd34ef28eb61c9b0829cc93.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(53, 'не задана', '', '', '', '', '', '', '', 193, 0, 'import_files/b1/b153d6a2539811e5803300ac06be3045_e55df045be6a441f825848ddd2a24f65.jpg', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(54, 'не задана', '', '', '', '', '', '', '', 501, 0, 'import_files/b1/b153d6a7539811e5803300ac06be3045_137c9549990d4a3dbfd1b5e8be8cceaf.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(55, 'не задана', '', '', '', '', '', '', '', 498, 0, 'import_files/b1/b153d6af539811e5803300ac06be3045_2256b449383c462b9816904c6bbb2872.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(56, 'не задана', '', '', '', '', '', '', '', 505, 0, 'import_files/b1/b153d6b7539811e5803300ac06be3045_9ed1125d8c514a2cb84df3b11580538b.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(57, 'не задана', '', '', '', '', '', '', '', 118, 0, 'import_files/76/760aeee053b911e5803300ac06be3045_521d00fdc6b54fd8b2bacab03ed30859.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(58, 'не задана', '', '', '', '', '', '', '', 230, 0, 'import_files/76/760aeee753b911e5803300ac06be3045_a6508de8f70d417fb50287fd5bd06a5c.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(59, 'не задана', '', '', '', '', '', '', '', 184, 0, 'import_files/76/760aeeec53b911e5803300ac06be3045_5a7aa241acd741f19a58352eaec9f737.jpg', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(60, 'не задана', '', '', '', '', '', '', '', 93, 0, 'import_files/76/760aeef553b911e5803300ac06be3045_bc37f02311044b20be4784b496a1f7d5.jpg', 0, 1, '550.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(61, 'не задана', '', '', '', '', '', '', '', 172, 0, 'import_files/76/760aeefa53b911e5803300ac06be3045_fb068bfa774543158379c61d39ec4a71.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(62, 'не задана', '', '', '', '', '', '', '', 78, 0, 'import_files/76/760aef0753b911e5803300ac06be3045_55ccaea61fe44ed9b7ca52b9d8170b71.jpg', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(63, 'не задана', '', '', '', '', '', '', '', 223, 0, 'import_files/76/760aef0c53b911e5803300ac06be3045_888adebbe1c94d5792a27508bc720a2f.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(64, 'не задана', '', '', '', '', '', '', '', 93, 0, 'import_files/76/760aef1c53b911e5803300ac06be3045_96ab2fc2e0524c4d824de01c0d072570.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(65, 'не задана', '', '', '', '', '', '', '', 188, 0, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_50c5906a82854f08b783960ff753b9e4.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(66, 'не задана', '', '', '', '', '', '', '', 167, 0, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_e200e804a9bc4d478c0c7b452d1b6835.jpg', 0, 1, '450.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(67, 'не задана', '', '', '', '', '', '', '', 96, 0, 'import_files/9b/9b2fab233fe811e58a4f00ac06be3045_ce7fbce671734cd282a6b075b134cadb.jpg', 0, 1, '300.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(68, 'не задана', '', '', '', '', '', '', '', 119, 0, 'import_files/7c/7c830c47401011e58a4f00ac06be3045_e58ecffcf0fc46d4822a1df23ca63d26.jpg', 0, 1, '350.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(69, 'не задана', '', '', '', '', '', '', '', 21, 0, 'import_files/7c/7c830c4d401011e58a4f00ac06be3045_e2c91b7f74dc497aafad49e815fb0b37.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(70, 'не задана', '', '', '', '', '', '', '', 74, 0, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_7df2f0a750404b85b99168854578ada7.jpg', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(71, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(72, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(73, 'не задана', '', '', '', '', '', '', '', 27, 0, 'import_files/64/64053bac5c7c11e5970800ac06be3045_67c6acf1526d42188093b31246115a0e.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(74, 'не задана', '', '', '', '', '', '', '', 265, 0, 'import_files/64/64053bbc5c7c11e5970800ac06be3045_1faa6ac29b2a4135826bdcbc0772f73c.jpg', 0, 1, '315.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(75, 'не задана', '', '', '', '', '', '', '', 116, 0, 'import_files/d9/d90ff3b25d0111e5970800ac06be3045_25b1f42ad51b4196b53d114ee5527e2e.jpg', 0, 1, '380.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(76, 'не задана', '', '', '', '', '', '', '', 274, 0, 'import_files/1a/1aa5692d5de211e5970800ac06be3045_5db583244d2c4355838434103d2aacd5.jpg', 0, 1, '300.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(77, 'не задана', '', '', '', '', '', '', '', 461, 0, 'import_files/32/32118fc75f8311e5970800ac06be3045_c16f815cb3834afab7250f9f233b5221.jpg', 0, 1, '170.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(78, 'не задана', '', '', '', '', '', '', '', 256, 0, 'import_files/d3/d3ba0c84611411e5970800ac06be3045_8ac3d975f1564c74860de9a07f5fc581.jpg', 0, 1, '315.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(79, 'не задана', '', '', '', '', '', '', '', 248, 0, 'import_files/d3/d3ba0c8a611411e5970800ac06be3045_22259eebfbfa43e0b74c2818dbf9a5da.jpg', 0, 1, '275.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(80, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(81, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(82, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(83, 'не задана', '', '', '', '', '', '', '', 352, 0, 'import_files/5d/5dcecf5b8cf511e59c3500ac06be3045_c7ec9d59fd3b4c14b292b2c11c4c1b12.jpg', 0, 1, '200.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(84, 'не задана', '', '', '', '', '', '', '', 90, 0, '', 0, 1, '640.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(85, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(86, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(87, 'не задана', '', '', '', '', '', '', '', 165, 0, '', 0, 1, '240.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(88, 'не задана', '', '', '', '', '', '', '', 413, 0, 'import_files/fd/fd154ae07ca411e5aacb00ac06be3045_8389272313de455481771c8734d03624.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(89, 'не задана', '', '', '', '', '', '', '', 242, 0, '', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(90, 'не задана', '', '', '', '', '', '', '', 102, 0, '', 0, 1, '240.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(91, 'не задана', '', '', '', '', '', '', '', 253, 0, '', 0, 1, '240.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(92, 'не задана', '', '', '', '', '', '', '', 321, 0, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_0d6d24986a794321ac80311f188f2d15.jpg', 0, 1, '300.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(93, 'не задана', '', '', '', '', '', '', '', 431, 0, '', 0, 1, '240.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(94, 'не задана', '', '', '', '', '', '', '', 180, 0, 'import_files/b3/b39873f6820311e5aacb00ac06be3045_3a149e3cc6e446f08b9747d87a8d0283.jpg', 0, 1, '300.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(95, 'не задана', '', '', '', '', '', '', '', 731, 0, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_1e6a41aee4904268beac748946281a35.jpg', 0, 1, '250.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(96, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(97, 'не задана', '', '', '', '', '', '', '', 188, 0, '', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(98, 'не задана', '', '', '', '', '', '', '', 137, 0, 'import_files/c5/c5dd127d87b911e5aacb00ac06be3045_98a804898e934917af15086d7e21c017.jpg', 0, 1, '740.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(99, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(100, 'не задана', '', '', '', '', '', '', '', 0, 0, '', 0, 1, '0.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '0000-00-00 00:00:00'),
(101, 'не задана', '', '', '', '', '', '', '', 178, 0, 'import_files/b5/b5fd305d6beb11e5b37400ac06be3045_268d123561a241e08151b12e11521dc6.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(102, 'не задана', '', '', '', '', '', '', '', 127, 0, '', 0, 1, '390.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(103, 'не задана', '', '', '', '', '', '', '', 122, 0, 'import_files/b5/b5fd30716beb11e5b37400ac06be3045_08819390e35a4906b952dacfc9a6aae6.jpg', 0, 1, '390.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(104, 'не задана', '', '', '', '', '', '', '', 109, 0, 'import_files/2b/2b72975d70b211e5b37400ac06be3045_a0edcc81cbef45159f48aa0d8df8661b.jpg', 0, 1, '1050.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(105, 'не задана', '', '', '', '', '', '', '', 233, 0, '', 0, 1, '280.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(106, 'не задана', '', '', '', '', '', '', '', 169, 0, 'import_files/48/482241d7629811e5b48a00ac06be3045_0cad997298a3410687e0f90e8c4de564.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(107, 'не задана', '', '', '', '', '', '', '', 24, 0, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_e3b1074ac914460fb29eff99d221896c.jpg', 0, 1, '200.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(108, 'не задана', '', '', '', '', '', '', '', 385, 0, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_6f0ef34cc8ef4701b6b4f4bf77cbfc62.jpg', 0, 1, '400.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(109, 'не задана', '', '', '', '', '', '', '', 288, 0, 'import_files/71/7115084840f511e5bd3400ac06be3045_d6ca554416f7462288de221d57a5bbd9.jpg', 0, 1, '300.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(110, 'не задана', '', '', '', '', '', '', '', 210, 0, 'import_files/71/7115084f40f511e5bd3400ac06be3045_bd073db39ead45ffbcafd206b328ad10.jpg', 0, 1, '250.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(111, 'не задана', '', '', '', '', '', '', '', 185, 0, '', 0, 1, '530.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(112, 'не задана', '', '', '', '', '', '', '', 270, 0, '', 0, 1, '530.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 19, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(113, 'не задана', '', '', '', '', '', '', '', 49, 0, 'import_files/cd/cd4ce157675b11e5be9200ac06be3045_569e9c337d1c488f9372679c37880093.jpg', 0, 1, '320.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(114, 'не задана', '', '', '', '', '', '', '', 180, 0, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_e8fb305184f743f6bc5498d486899d05.jpg', 0, 1, '240.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42'),
(115, 'не задана', '', '', '', '', '', '', '', 196, 0, 'import_files/38/38006f81681811e5be9200ac06be3045_44389cb83491476786fceaeaa8199435.jpg', 0, 1, '315.0000', 0, 0, '2017-08-28', '0.00', 1, '0.00', '0.00', '0.00', 1, 1, 1, 1, 1, 0, '2017-08-29 16:19:41', '2017-08-29 16:19:42');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_attribute`
--

INSERT INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(1, 19, 1, '70 % ПАН, 30 % вовна'),
(2, 19, 1, '50% WOOL, 50% ACRIL'),
(3, 19, 1, '70 % ПАН, 30 % вовна'),
(4, 19, 1, '70 % ПАН, 30 % вовна'),
(5, 19, 1, '70 % ПАН, 30 % вовна'),
(6, 19, 1, '70 % ПАН, 30 % вовна'),
(7, 19, 1, '70 % ПАН, 30 % вовна'),
(8, 19, 1, '70 % ПАН, 30 % вовна'),
(9, 19, 1, '50% шерсть, 50%ПАН'),
(10, 19, 1, '70 % ПАН, 30 % вовна'),
(12, 19, 1, '50% шерсть, 50%ПАН'),
(13, 19, 1, '50% шерсть, 50%ПАН'),
(14, 19, 1, '70 %ACRIL , 30 % WOOl'),
(15, 19, 1, '50% шерсть, 50%ПАН'),
(16, 19, 1, '70 % ПАН, 30 % вовна'),
(17, 19, 1, '70 % ПАН, 30 % вовна'),
(18, 19, 1, '50% шерсть, 50%ПАН'),
(19, 19, 1, '50% WOOL, 50% ACRIL'),
(20, 19, 1, '70 % ПАН, 30 % вовна'),
(21, 19, 1, '50% шерсть, 50%ПАН'),
(22, 19, 1, '70 % ПАН, 30 % вовна'),
(23, 19, 1, '70 % ПАН, 30 % вовна'),
(24, 19, 1, '50% WOOL, 50% ACRIL'),
(25, 19, 1, '70 % ПАН, 30 % вовна'),
(26, 19, 1, '70 % ПАН, 30 % вовна'),
(27, 19, 1, '70 % ПАН, 30 % вовна'),
(28, 19, 1, '70 % ПАН, 30 % вовна'),
(29, 19, 1, '70 % ПАН, 30 % вовна'),
(30, 19, 1, '70 % ПАН, 30 % вовна'),
(31, 19, 1, '70 % ПАН, 30 % вовна'),
(32, 19, 1, '70 % ПАН, 30 % вовна'),
(33, 19, 1, '70 % ПАН, 30 % вовна'),
(34, 19, 1, '50% шерсть, 50%ПАН'),
(35, 19, 1, '70 % ПАН, 30 % вовна'),
(36, 19, 1, '70 % ПАН, 30 % вовна'),
(37, 19, 1, '70 % ПАН, 30 % вовна'),
(38, 19, 1, '70 % ПАН, 30 % вовна'),
(39, 19, 1, '70 % ПАН, 30 % вовна'),
(40, 19, 1, '70 % ПАН, 30 % вовна'),
(41, 19, 1, '70 % ПАН, 30 % вовна'),
(42, 19, 1, '70 % ПАН, 30 % вовна'),
(43, 19, 1, '70 % ПАН, 30 % вовна'),
(44, 19, 1, '70 % ПАН, 30 % вовна'),
(45, 19, 1, '70 % ПАН, 30 % вовна'),
(46, 19, 1, '70 % ПАН, 30 % вовна'),
(47, 19, 1, '70 % ПАН, 30 % вовна'),
(48, 19, 1, '70 % ПАН, 30 % вовна'),
(49, 19, 1, '70 % ПАН, 30 % вовна'),
(50, 19, 1, '50% шерсть, 50%ПАН'),
(51, 19, 1, '70 % ПАН, 30 % вовна'),
(52, 19, 1, '70 % ПАН, 30 % вовна'),
(53, 19, 1, '70 % ПАН, 30 % вовна'),
(54, 19, 1, '50% шерсть, 50%ПАН'),
(55, 19, 1, '50% шерсть, 50%ПАН'),
(56, 19, 1, '50% шерсть, 50%ПАН'),
(57, 19, 1, '70 % ПАН, 30 % вовна'),
(58, 19, 1, '70 % ПАН, 30 % вовна'),
(59, 19, 1, '70 % ПАН, 30 % вовна'),
(60, 19, 1, '50% шерсть, 50%ПАН'),
(61, 19, 1, '70 % ПАН, 30 % вовна'),
(62, 19, 1, '70 % ПАН, 30 % вовна'),
(63, 19, 1, '70 % ПАН, 30 % вовна'),
(64, 19, 1, '70 % ПАН, 30 % вовна'),
(65, 19, 1, '70 % ПАН, 30 % вовна'),
(66, 19, 1, '70 % ПАН, 30 % вовна'),
(67, 19, 1, '70 % ПАН, 30 % вовна'),
(68, 19, 1, '70 % ПАН, 30 % вовна'),
(69, 19, 1, '70 % ПАН, 30 % вовна'),
(70, 19, 1, '50% шерсть, 50%ПАН'),
(71, 19, 1, '50% ACRIL,50% WOOL'),
(72, 19, 1, '70% WOOL, 30% ACRIL'),
(73, 19, 1, '70 % ПАН, 30 % вовна'),
(74, 19, 1, '50% шерсть, 50%ПАН'),
(75, 19, 1, '70 % ПАН, 30 % вовна'),
(76, 19, 1, '70 % ПАН, 30 % вовна'),
(77, 19, 1, '70 % ПАН, 30 % вовна'),
(78, 19, 1, '70 % ПАН, 30 % вовна'),
(79, 19, 1, '70 % ПАН, 30 % вовна'),
(80, 19, 1, '70 % ПАН, 30 % вовна'),
(81, 19, 1, '50% ACRIL,50% WOOL'),
(82, 19, 1, '50% WOOL, 50% ACRIL'),
(83, 19, 1, '50% WOOL, 50% ACRIL'),
(84, 19, 1, '50% WOOL, 50% ACRIL'),
(85, 19, 1, '50% WOOL, 50% ACRIL'),
(86, 19, 1, '50% WOOL, 50% ACRIL'),
(87, 19, 1, '50% WOOL, 50% ACRIL'),
(88, 19, 1, '70 %ACRIL , 30 % WOOl'),
(89, 19, 1, '70 %ACRIL , 30 % WOOl'),
(90, 19, 1, '50% WOOL, 50% ACRIL'),
(91, 19, 1, '50% WOOL, 50% ACRIL'),
(92, 19, 1, '50% WOOL, 50% ACRIL'),
(93, 19, 1, '50% WOOL, 50% ACRIL'),
(94, 19, 1, '50% WOOL, 50% ACRIL'),
(95, 19, 1, '50% шерсть, 50%ПАН'),
(96, 19, 1, '70 %ACRIL , 30 % WOOl'),
(97, 19, 1, '70 % ПАН, 30 % вовна'),
(98, 19, 1, '70 % ПАН, 30 % вовна'),
(99, 19, 1, '70 %ACRIL , 30 % WOOl'),
(100, 19, 1, '70 %ACRIL , 30 % WOOl'),
(101, 19, 1, '70 %ACRIL , 30 % WOOl'),
(102, 19, 1, '70 %ACRIL , 30 % WOOl'),
(103, 19, 1, '70 %ACRIL , 30 % WOOl'),
(104, 19, 1, '50% WOOL, 50% ACRIL'),
(105, 19, 1, '50% WOOL, 50% ACRIL'),
(106, 19, 1, '70 % ПАН, 30 % вовна'),
(107, 19, 1, '70 %ACRIL , 30 % WOOl'),
(108, 19, 1, '50% WOOL, 50% ACRIL'),
(109, 19, 1, '70 % ПАН, 30 % вовна'),
(110, 19, 1, '70 % ПАН, 30 % вовна'),
(111, 19, 1, '70 %ACRIL , 30 % WOOl'),
(112, 19, 1, '70 %ACRIL , 30 % WOOl'),
(113, 19, 1, '70 %ACRIL , 30 % WOOl'),
(114, 19, 1, '50% WOOL, 50% ACRIL'),
(115, 19, 1, '50% WOOL, 50% ACRIL');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES
(1, 1, '15 Z 54 SOFFIO', '', '', '', '', '', ''),
(2, 1, '15 Z 108 Cofil', '', '', '', '', '', ''),
(3, 1, '15 Z 206 SOFFIO', '', '', '', '', '', ''),
(4, 1, '15 Z 04 SOFFIO (изософтова подкл.)', '', '', '', '', '', ''),
(5, 1, '15 Z 52 SOFFIO', '', '', '', '', '', ''),
(6, 1, '15 Z 51 SOFFIO', '', '', '', '', '', ''),
(7, 1, '15 Z 217 SOFFIO', '', '', '', '', '', ''),
(8, 1, '15 Z 57 SOFFIO', '', '', '', '', '', ''),
(9, 1, '15 Z 220 Gemini', '', '', '', '', '', ''),
(10, 1, '15 Z 56 SOFFIO', '', '', '', '', '', ''),
(11, 1, '15 Z 56 SOFFIO', '', '', '', '', '', ''),
(12, 1, '15 Z 106 Fulham', '', '', '', '', '', ''),
(13, 1, '15 Z 103 Fulham', '', '', '', '', '', ''),
(14, 1, '15 Z 01 SOFFIO', '', '', '', '', '', ''),
(15, 1, '15 Z 218 Fulham', '', '', '', '', '', ''),
(16, 1, '15 Z 212 SOFFIO', '', '', '', '', '', ''),
(17, 1, '15 Z-05 SOFFIO', '', '', '', '', '', ''),
(18, 1, '15 Z 105 Fulham', '', '', '', '', '', ''),
(19, 1, '15 Z 101 Fulham', '', '', '', '', '', ''),
(20, 1, '15 Z 06 SOFFIO', '', '', '', '', '', ''),
(21, 1, '15 Z 03 Australia', '', '', '', '', '', ''),
(22, 1, '15 Z-255 SOFFIO', '', '', '', '', '', ''),
(23, 1, '15 Z-237 RX', '', '', '', '', '', ''),
(24, 1, '15 Z-235 SOFFIO', '', '', '', '', '', ''),
(25, 1, '15 Z-233 SOFFIO', '', '', '', '', '', ''),
(26, 1, '15 Z-253 SOFFIO', '', '', '', '', '', ''),
(27, 1, '15 Z-248 SOFFIO', '', '', '', '', '', ''),
(28, 1, '15 Z-250 SOFFIO', '', '', '', '', '', ''),
(29, 1, '15 Z-246 SOFFIO', '', '', '', '', '', ''),
(30, 1, '15 Z-247 SOFFIO', '', '', '', '', '', ''),
(31, 1, '15 Z-243 SOFFIO', '', '', '', '', '', ''),
(32, 1, '15 Z-239 SOFFIO', '', '', '', '', '', ''),
(33, 1, '15 Z-245 SOFFIO', '', '', '', '', '', ''),
(34, 1, '15 Z-225 Fulham', '', '', '', '', '', ''),
(35, 1, '15 Z-201 SOFFIO', '', '', '', '', '', ''),
(36, 1, '15 Z-204 SOFFIO', '', '', '', '', '', ''),
(37, 1, '15 Z-205 SOFFIO', '', '', '', '', '', ''),
(38, 1, '15 Z-209 SOFFIO', '', '', '', '', '', ''),
(39, 1, '15 Z-215 SOFFIO', '', '', '', '', '', ''),
(40, 1, '15 Z-214 SOFFIO', '', '', '', '', '', ''),
(41, 1, '15 Z-242 SOFFIO', '', '', '', '', '', ''),
(42, 1, '15 Z-234 SOFFIO', '', '', '', '', '', ''),
(43, 1, '15 Z-238 SOFFIO', '', '', '', '', '', ''),
(44, 1, '15 Z-11 SOFFIO', '', '', '', '', '', ''),
(45, 1, '15 Z-240 SOFFIO', '', '', '', '', '', ''),
(46, 1, '15 Z-230 SOFFIO', '', '', '', '', '', ''),
(47, 1, '15 Z-257 SOFFIO', '', '', '', '', '', ''),
(48, 1, '15 Z-241 SOFFIO', '', '', '', '', '', ''),
(49, 1, '15 Z-236 RX', '', '', '', '', '', ''),
(50, 1, '15 Z-104 Fulham', '', '', '', '', '', ''),
(51, 1, '15 Z-231 SOFFIO', '', '', '', '', '', ''),
(52, 1, '15 Z-244 SOFFIO', '', '', '', '', '', ''),
(53, 1, '15 Z-254 SOFFIO', '', '', '', '', '', ''),
(54, 1, '15 Z-109 Fulham', '', '', '', '', '', ''),
(55, 1, '15 Z-110 Fulham', '', '', '', '', '', ''),
(56, 1, '15 Z-111 Fulham', '', '', '', '', '', ''),
(57, 1, '15 Z-207 SOFFIO', '', '', '', '', '', ''),
(58, 1, '15 Z-256 SOFFIO', '', '', '', '', '', ''),
(59, 1, '15 Z-252 SOFFIO', '', '', '', '', '', ''),
(60, 1, '15 Z-58 Fulham', '', '', '', '', '', ''),
(61, 1, '15 Z-07 SOFFIO', '', '', '', '', '', ''),
(62, 1, '15 Z-53 SOFFIO', '', '', '', '', '', ''),
(63, 1, '15 Z-09 SOFFIO', '', '', '', '', '', ''),
(64, 1, '15 Z-10 SOFFIO', '', '', '', '', '', ''),
(65, 1, '15 Z 49 SOFFIO', '', '', '', '', '', ''),
(66, 1, '15 Z 59 SOFFIO', '', '', '', '', '', ''),
(67, 1, '15 Z 222 SOFFIO', '', '', '', '', '', ''),
(68, 1, '15 Z 55 SOFFIO', '', '', '', '', '', ''),
(69, 1, '15 Z 50 SOFFIO', '', '', '', '', '', ''),
(70, 1, '15 Z 102 Fulham', '', '', '', '', '', ''),
(71, 1, '15 Z 62', '', '', '', '', '', ''),
(72, 1, '15 Z 63', '', '', '', '', '', ''),
(73, 1, '15 Z-249 SOFFIO', '', '', '', '', '', ''),
(74, 1, '15 Z-210 Fulham', '', '', '', '', '', ''),
(75, 1, '15 Z-12 SOFFIO', '', '', '', '', '', ''),
(76, 1, '15 Z-203 SOFFIO', '', '', '', '', '', ''),
(77, 1, '15 Z-258 SOFFIO', '', '', '', '', '', ''),
(78, 1, '15 Z-223 SOFFIO', '', '', '', '', '', ''),
(79, 1, '15 Z-216 SOFFIO', '', '', '', '', '', ''),
(80, 1, '15 Z-12/1 SOFFIO', '', '', '', '', '', ''),
(81, 1, '15 Z 03 /1', '', '', '', '', '', ''),
(82, 1, '15 Z-113K Gemini', '', '', '', '', '', ''),
(83, 1, '15 Z-270 Marilyn', '', '', '', '', '', ''),
(84, 1, '15 Z-114K WILD', '', '', '', '', '', ''),
(85, 1, '15 Z 61', '', '', '', '', '', ''),
(86, 1, '15 Z 301', '', '', '', '', '', ''),
(87, 1, '15 Z-112 COFIL', '', '', '', '', '', ''),
(88, 1, '15 Z-260 SOFFIO', '', '', '', '', '', ''),
(89, 1, '15 Z-261 SOFFIO', '', '', '', '', '', ''),
(90, 1, '15 Z-262 Fulham', '', '', '', '', '', ''),
(91, 1, '15 Z-263 Fulham', '', '', '', '', '', ''),
(92, 1, '15 Z-264 Fulham', '', '', '', '', '', ''),
(93, 1, '15 Z-265 Fulham', '', '', '', '', '', ''),
(94, 1, '15 Z-113 Gemini', '', '', '', '', '', ''),
(95, 1, '15 Z 266 Gemini', '', '', '', '', '', ''),
(96, 1, '15 Z 269 ', '', '', '', '', '', ''),
(97, 1, '15 Z 267K SOFFIO', '', '', '', '', '', ''),
(98, 1, '15 Z 217K SOFFIO', '', '', '', '', '', ''),
(99, 1, '15 Z-13/1 SOFFIO', '', '', '', '', '', ''),
(100, 1, '15 Z-14/1 SOFFIO', '', '', '', '', '', ''),
(101, 1, '15 Z-02 SOFFIO', '', '', '', '', '', ''),
(102, 1, '15 Z-13 SOFFIO', '', '', '', '', '', ''),
(103, 1, '15 Z-14 SOFFIO', '', '', '', '', '', ''),
(104, 1, '15 Z-226 PL', '', '', '', '', '', ''),
(105, 1, '15 Z-259 Fulham', '', '', '', '', '', ''),
(106, 1, '15 Z-251 SOFFIO', '', '', '', '', '', ''),
(107, 1, '15 Z 60', '', '', '', '', '', ''),
(108, 1, '15 Z-107 COFIL', '', '', '', '', '', ''),
(109, 1, '15 Z 208 SOFFIO', '', '', '', '', '', ''),
(110, 1, '15 Z 202 SOFFIO', '', '', '', '', '', ''),
(111, 1, '15 Z-206K SOFFIO', '', '', '', '', '', ''),
(112, 1, '15 Z-59K SOFFIO', '', '', '', '', '', ''),
(113, 1, '15 Z-08 SOFFIO', '', '', '', '', '', ''),
(114, 1, '15 Z-114 WILD', '', '', '', '', '', ''),
(115, 1, '15 Z-211 Fulham', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_discount`
--

INSERT INTO `oc_product_discount` (`product_discount_id`, `product_id`, `customer_group_id`, `quantity`, `priority`, `price`, `date_start`, `date_end`) VALUES
(1, 1, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(2, 1, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(3, 2, 2, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(4, 2, 1, 0, 0, '800.0000', '0000-00-00', '0000-00-00'),
(5, 3, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(6, 3, 1, 0, 0, '360.0000', '0000-00-00', '0000-00-00'),
(7, 4, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(8, 4, 1, 0, 0, '360.0000', '0000-00-00', '0000-00-00'),
(9, 5, 1, 0, 0, '375.0000', '0000-00-00', '0000-00-00'),
(10, 5, 2, 0, 0, '110.0000', '0000-00-00', '0000-00-00'),
(11, 6, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(12, 6, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(13, 7, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(14, 7, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(15, 8, 2, 0, 0, '110.0000', '0000-00-00', '0000-00-00'),
(16, 8, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(17, 9, 1, 0, 0, '450.0000', '0000-00-00', '0000-00-00'),
(18, 9, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(19, 10, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(20, 10, 2, 0, 0, '110.0000', '0000-00-00', '0000-00-00'),
(21, 12, 1, 0, 0, '525.0000', '0000-00-00', '0000-00-00'),
(22, 12, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(23, 13, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(24, 13, 1, 0, 0, '240.0000', '0000-00-00', '0000-00-00'),
(25, 14, 1, 0, 0, '420.0000', '0000-00-00', '0000-00-00'),
(26, 14, 2, 0, 0, '210.0000', '0000-00-00', '0000-00-00'),
(27, 15, 2, 0, 0, '220.0000', '0000-00-00', '0000-00-00'),
(28, 15, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(29, 16, 1, 0, 0, '325.0000', '0000-00-00', '0000-00-00'),
(30, 16, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(31, 17, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(32, 17, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(33, 18, 2, 0, 0, '110.0000', '0000-00-00', '0000-00-00'),
(34, 18, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(35, 19, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(36, 20, 1, 0, 0, '420.0000', '0000-00-00', '0000-00-00'),
(37, 20, 2, 0, 0, '210.0000', '0000-00-00', '0000-00-00'),
(38, 21, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(39, 21, 1, 0, 0, '420.0000', '0000-00-00', '0000-00-00'),
(40, 22, 1, 0, 0, '420.0000', '0000-00-00', '0000-00-00'),
(41, 22, 2, 0, 0, '210.0000', '0000-00-00', '0000-00-00'),
(42, 23, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(43, 23, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(44, 24, 1, 0, 0, '360.0000', '0000-00-00', '0000-00-00'),
(45, 24, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(46, 25, 1, 0, 0, '300.0000', '0000-00-00', '0000-00-00'),
(47, 25, 2, 0, 0, '110.0000', '0000-00-00', '0000-00-00'),
(48, 26, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(49, 26, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(50, 27, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(51, 27, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(52, 28, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(53, 28, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(54, 29, 2, 0, 0, '195.0000', '0000-00-00', '0000-00-00'),
(55, 29, 1, 0, 0, '390.0000', '0000-00-00', '0000-00-00'),
(56, 30, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(57, 30, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(58, 31, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(59, 31, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(60, 32, 1, 0, 0, '360.0000', '0000-00-00', '0000-00-00'),
(61, 32, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(62, 33, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(63, 33, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(64, 34, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(65, 34, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(66, 35, 2, 0, 0, '110.0000', '0000-00-00', '0000-00-00'),
(67, 35, 1, 0, 0, '375.0000', '0000-00-00', '0000-00-00'),
(68, 36, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(69, 36, 1, 0, 0, '325.0000', '0000-00-00', '0000-00-00'),
(70, 37, 1, 0, 0, '300.0000', '0000-00-00', '0000-00-00'),
(71, 37, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(72, 38, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(73, 38, 1, 0, 0, '375.0000', '0000-00-00', '0000-00-00'),
(74, 39, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(75, 39, 1, 0, 0, '325.0000', '0000-00-00', '0000-00-00'),
(76, 40, 1, 0, 0, '275.0000', '0000-00-00', '0000-00-00'),
(77, 40, 2, 0, 0, '85.0000', '0000-00-00', '0000-00-00'),
(78, 41, 1, 0, 0, '420.0000', '0000-00-00', '0000-00-00'),
(79, 41, 2, 0, 0, '210.0000', '0000-00-00', '0000-00-00'),
(80, 42, 2, 0, 0, '190.0000', '0000-00-00', '0000-00-00'),
(81, 42, 1, 0, 0, '380.0000', '0000-00-00', '0000-00-00'),
(82, 43, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(83, 43, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(84, 44, 1, 0, 0, '390.0000', '0000-00-00', '0000-00-00'),
(85, 44, 2, 0, 0, '195.0000', '0000-00-00', '0000-00-00'),
(86, 45, 1, 0, 0, '360.0000', '0000-00-00', '0000-00-00'),
(87, 45, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(88, 46, 1, 0, 0, '380.0000', '0000-00-00', '0000-00-00'),
(89, 46, 2, 0, 0, '190.0000', '0000-00-00', '0000-00-00'),
(90, 47, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(91, 47, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(92, 48, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(93, 48, 1, 0, 0, '360.0000', '0000-00-00', '0000-00-00'),
(94, 49, 2, 0, 0, '78.0000', '0000-00-00', '0000-00-00'),
(95, 49, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(96, 50, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(97, 50, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(98, 51, 1, 0, 0, '380.0000', '0000-00-00', '0000-00-00'),
(99, 51, 2, 0, 0, '190.0000', '0000-00-00', '0000-00-00'),
(100, 52, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(101, 52, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(102, 53, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(103, 53, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(104, 54, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(105, 54, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(106, 55, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(107, 55, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(108, 56, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(109, 56, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(110, 57, 2, 0, 0, '130.0000', '0000-00-00', '0000-00-00'),
(111, 57, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(112, 58, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(113, 58, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(114, 59, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(115, 59, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(116, 60, 2, 0, 0, '180.0000', '0000-00-00', '0000-00-00'),
(117, 60, 1, 0, 0, '550.0000', '0000-00-00', '0000-00-00'),
(118, 61, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(119, 61, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(120, 62, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(121, 62, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(122, 63, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(123, 63, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(124, 64, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(125, 64, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(126, 65, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(127, 65, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(128, 66, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(129, 66, 1, 0, 0, '450.0000', '0000-00-00', '0000-00-00'),
(130, 67, 1, 0, 0, '300.0000', '0000-00-00', '0000-00-00'),
(131, 67, 2, 0, 0, '150.0000', '0000-00-00', '0000-00-00'),
(132, 68, 1, 0, 0, '350.0000', '0000-00-00', '0000-00-00'),
(133, 68, 2, 0, 0, '110.0000', '0000-00-00', '0000-00-00'),
(134, 69, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(135, 69, 2, 0, 0, '150.0000', '0000-00-00', '0000-00-00'),
(136, 70, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(137, 70, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(138, 73, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(139, 73, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(140, 74, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(141, 74, 1, 0, 0, '315.0000', '0000-00-00', '0000-00-00'),
(142, 75, 1, 0, 0, '380.0000', '0000-00-00', '0000-00-00'),
(143, 75, 2, 0, 0, '190.0000', '0000-00-00', '0000-00-00'),
(144, 76, 1, 0, 0, '300.0000', '0000-00-00', '0000-00-00'),
(145, 76, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(146, 77, 1, 0, 0, '170.0000', '0000-00-00', '0000-00-00'),
(147, 77, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(148, 78, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(149, 78, 1, 0, 0, '315.0000', '0000-00-00', '0000-00-00'),
(150, 79, 1, 0, 0, '275.0000', '0000-00-00', '0000-00-00'),
(151, 79, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(152, 83, 1, 0, 0, '200.0000', '0000-00-00', '0000-00-00'),
(153, 83, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(154, 84, 2, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(155, 84, 1, 0, 0, '640.0000', '0000-00-00', '0000-00-00'),
(156, 87, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(157, 87, 1, 0, 0, '240.0000', '0000-00-00', '0000-00-00'),
(158, 88, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(159, 88, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(160, 89, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(161, 89, 2, 0, 0, '150.0000', '0000-00-00', '0000-00-00'),
(162, 90, 1, 0, 0, '240.0000', '0000-00-00', '0000-00-00'),
(163, 90, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(164, 91, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(165, 91, 1, 0, 0, '240.0000', '0000-00-00', '0000-00-00'),
(166, 92, 1, 0, 0, '300.0000', '0000-00-00', '0000-00-00'),
(167, 92, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(168, 93, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(169, 93, 1, 0, 0, '240.0000', '0000-00-00', '0000-00-00'),
(170, 94, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(171, 94, 1, 0, 0, '300.0000', '0000-00-00', '0000-00-00'),
(172, 95, 1, 0, 0, '250.0000', '0000-00-00', '0000-00-00'),
(173, 95, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(174, 97, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(175, 97, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(176, 98, 2, 0, 0, '295.0000', '0000-00-00', '0000-00-00'),
(177, 98, 1, 0, 0, '740.0000', '0000-00-00', '0000-00-00'),
(178, 101, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(179, 101, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(180, 102, 2, 0, 0, '195.0000', '0000-00-00', '0000-00-00'),
(181, 102, 1, 0, 0, '390.0000', '0000-00-00', '0000-00-00'),
(182, 103, 1, 0, 0, '390.0000', '0000-00-00', '0000-00-00'),
(183, 103, 2, 0, 0, '195.0000', '0000-00-00', '0000-00-00'),
(184, 104, 1, 0, 0, '1050.0000', '0000-00-00', '0000-00-00'),
(185, 104, 2, 0, 0, '420.0000', '0000-00-00', '0000-00-00'),
(186, 105, 1, 0, 0, '280.0000', '0000-00-00', '0000-00-00'),
(187, 105, 2, 0, 0, '140.0000', '0000-00-00', '0000-00-00'),
(188, 106, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(189, 106, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(190, 107, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(191, 107, 1, 0, 0, '200.0000', '0000-00-00', '0000-00-00'),
(192, 108, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(193, 108, 1, 0, 0, '400.0000', '0000-00-00', '0000-00-00'),
(194, 109, 1, 0, 0, '300.0000', '0000-00-00', '0000-00-00'),
(195, 109, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00'),
(196, 110, 1, 0, 0, '250.0000', '0000-00-00', '0000-00-00'),
(197, 110, 2, 0, 0, '125.0000', '0000-00-00', '0000-00-00'),
(198, 111, 2, 0, 0, '265.0000', '0000-00-00', '0000-00-00'),
(199, 111, 1, 0, 0, '530.0000', '0000-00-00', '0000-00-00'),
(200, 112, 1, 0, 0, '530.0000', '0000-00-00', '0000-00-00'),
(201, 112, 2, 0, 0, '265.0000', '0000-00-00', '0000-00-00'),
(202, 113, 2, 0, 0, '160.0000', '0000-00-00', '0000-00-00'),
(203, 113, 1, 0, 0, '320.0000', '0000-00-00', '0000-00-00'),
(204, 114, 2, 0, 0, '120.0000', '0000-00-00', '0000-00-00'),
(205, 114, 1, 0, 0, '240.0000', '0000-00-00', '0000-00-00'),
(206, 115, 1, 0, 0, '315.0000', '0000-00-00', '0000-00-00'),
(207, 115, 2, 0, 0, '95.0000', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(424, 1, 'import_files/14/1484f158467511e5803300ac06be3045_88abac31e4f246dbbdfa0101a0f1e452.jpg', 0),
(423, 1, 'import_files/14/1484f158467511e5803300ac06be3045_91aafa6c6f2749d3bceb706844e935ac.jpg', 0),
(422, 1, 'import_files/14/1484f158467511e5803300ac06be3045_bf162cad976443529fb1c163691f5d58.jpg', 0),
(421, 1, 'import_files/14/1484f158467511e5803300ac06be3045_5545e29bf9dc45d799d70fd1d143910a.jpg', 0),
(420, 1, 'import_files/14/1484f158467511e5803300ac06be3045_f17f993d77aa48448c093ddf2b625392.jpg', 0),
(428, 2, 'import_files/14/1484f15d467511e5803300ac06be3045_eb954055187d4a27ae19aa297c2e6c05.jpg', 0),
(427, 2, 'import_files/14/1484f15d467511e5803300ac06be3045_e5fac8eb2cff439b9f89a4201d504acd.jpg', 0),
(426, 2, 'import_files/14/1484f15d467511e5803300ac06be3045_df830676d43441ce990e37225878df3b.jpg', 0),
(425, 2, 'import_files/14/1484f15d467511e5803300ac06be3045_233ba85ffb874d5e96f271f49987d003.jpg', 0),
(434, 3, 'import_files/14/1484f15f467511e5803300ac06be3045_4da97c216cd14f9487bca59323c46615.jpg', 0),
(433, 3, 'import_files/14/1484f15f467511e5803300ac06be3045_50f4d5e6e1664c008f72e7593fea38f4.jpg', 0),
(432, 3, 'import_files/14/1484f15f467511e5803300ac06be3045_711ef6c4bf8f4b7898bd13dcd8001452.jpg', 0),
(431, 3, 'import_files/14/1484f15f467511e5803300ac06be3045_6a292cbebbeb4638b7063b56627062d2.jpg', 0),
(430, 3, 'import_files/14/1484f15f467511e5803300ac06be3045_faed4127f23346ebb75eefa229658ce2.jpg', 0),
(429, 3, 'import_files/14/1484f15f467511e5803300ac06be3045_fc75e752b17d40e6bdd86c6e212e2745.jpg', 0),
(443, 4, 'import_files/14/1484f167467511e5803300ac06be3045_f297e10a9867485991fe2974853482d5.jpg', 0),
(442, 4, 'import_files/14/1484f167467511e5803300ac06be3045_17a063b1da434ff899e860d0d9c5968e.jpg', 0),
(441, 4, 'import_files/14/1484f167467511e5803300ac06be3045_4992e0a60634475e9aab361dacb79e38.jpg', 0),
(440, 4, 'import_files/14/1484f167467511e5803300ac06be3045_957edd44ff1c4a9ca644fa31d6676805.jpg', 0),
(439, 4, 'import_files/14/1484f167467511e5803300ac06be3045_aed6cc1e88b24b33af764538a28a7322.jpg', 0),
(438, 4, 'import_files/14/1484f167467511e5803300ac06be3045_572cc44f153149d9b1c745395e3f9d4a.jpg', 0),
(437, 4, 'import_files/14/1484f167467511e5803300ac06be3045_36a19e721c374358ba7c9e933b57bf01.jpg', 0),
(436, 4, 'import_files/14/1484f167467511e5803300ac06be3045_39e5de579b214e17bac1d94b9c1f6d99.jpg', 0),
(435, 4, 'import_files/14/1484f167467511e5803300ac06be3045_3a465bac2ed94251bc8054e94cd6fd7a.jpg', 0),
(449, 5, 'import_files/14/1484f16d467511e5803300ac06be3045_1297ae70daa3455688a54f7925570b0d.jpg', 0),
(448, 5, 'import_files/14/1484f16d467511e5803300ac06be3045_5af57990dd6e48c888f602586426697a.jpg', 0),
(447, 5, 'import_files/14/1484f16d467511e5803300ac06be3045_230e59b5f22a4a758b828874f440c510.jpg', 0),
(446, 5, 'import_files/14/1484f16d467511e5803300ac06be3045_eca88d0a290f4c0994df47ef8ee4d6dc.jpg', 0),
(445, 5, 'import_files/14/1484f16d467511e5803300ac06be3045_f0a3e24f68b946519bf1f0871bb33b94.jpg', 0),
(444, 5, 'import_files/14/1484f16d467511e5803300ac06be3045_9d6ab801fdbb4732a1a7e24a3234d51d.jpg', 0),
(453, 6, 'import_files/14/1484f174467511e5803300ac06be3045_c280150b21854b7fb34500daf44b5b42.jpg', 0),
(452, 6, 'import_files/14/1484f174467511e5803300ac06be3045_bb8d7032e2c043edb1fbea0e60f0a4da.jpg', 0),
(451, 6, 'import_files/14/1484f174467511e5803300ac06be3045_a9fa0bbb67384c67a625a6c52ed5a7c3.jpg', 0),
(450, 6, 'import_files/14/1484f174467511e5803300ac06be3045_45e5e77c505c45fba33dd4a05bf1cf94.jpg', 0),
(456, 7, 'import_files/14/1484f17b467511e5803300ac06be3045_a433116ee15b4c7fb7f93188aaefbd76.jpg', 0),
(455, 7, 'import_files/14/1484f17b467511e5803300ac06be3045_f2830c175a154319b526d71c84a51dfc.jpg', 0),
(454, 7, 'import_files/14/1484f17b467511e5803300ac06be3045_0962f5071c0149f4b4fbfde3cc47ab77.jpg', 0),
(459, 8, 'import_files/14/1484f180467511e5803300ac06be3045_6812feef5ad14607801bb0bc37562c0c.jpg', 0),
(458, 8, 'import_files/14/1484f180467511e5803300ac06be3045_de32a9cc2d2f48219d60195961b81822.jpg', 0),
(457, 8, 'import_files/14/1484f180467511e5803300ac06be3045_138a579587be4436a581bc0b8bffbaf5.jpg', 0),
(464, 9, 'import_files/14/1484f186467511e5803300ac06be3045_33d5d8a752734baeb231770d8fa4c131.jpg', 0),
(463, 9, 'import_files/14/1484f186467511e5803300ac06be3045_7d529dbbac8649bbaef119e5e853d3a7.jpg', 0),
(462, 9, 'import_files/14/1484f186467511e5803300ac06be3045_a66439e47c6a4dd5ad246bba88e6a5ae.jpg', 0),
(461, 9, 'import_files/14/1484f186467511e5803300ac06be3045_498a8463de0740339eccad3bf013d0ae.jpg', 0),
(460, 9, 'import_files/14/1484f186467511e5803300ac06be3045_7b94b1cb7322445f950f1a468c8c2dd2.jpg', 0),
(467, 10, 'import_files/34/34c734d0471c11e5803300ac06be3045_5e6f8df87ef34d62b6ac1f1157cd0029.jpg', 0),
(466, 10, 'import_files/34/34c734d0471c11e5803300ac06be3045_c120b9ebdfae4feaaee93a00621539b6.jpg', 0),
(465, 10, 'import_files/34/34c734d0471c11e5803300ac06be3045_83d9b1354c99496898a1edf0ce82e0d0.jpg', 0),
(475, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_37c7fb0a1b69460280d425da1b8bf63d.jpg', 0),
(474, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_9c6b62a53490423187990a2a22c9a5fb.jpg', 0),
(473, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_a79c59209c77415e9a89663d3ce9b2cf.jpg', 0),
(472, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_0a30b3517c6844a39acdac2a79fa94f8.jpg', 0),
(471, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_8f5db5c0cf2449989b9cfc0d4be43ca9.jpg', 0),
(470, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_1bd9b46a147740b99e296d14446165af.jpg', 0),
(469, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_0e3e96547d8c4d30a9aeec2a844b1e52.jpg', 0),
(468, 12, 'import_files/cb/cbac2b1a4ae711e5803300ac06be3045_8bd90b9ebab84f85a9c537c20dd8ff51.jpg', 0),
(479, 13, 'import_files/cb/cbac2b254ae711e5803300ac06be3045_56a3974ead88416486f1e955155bc509.jpg', 0),
(478, 13, 'import_files/cb/cbac2b254ae711e5803300ac06be3045_dd930702e90e40649a3463f7146bbc03.jpg', 0),
(477, 13, 'import_files/cb/cbac2b254ae711e5803300ac06be3045_6eb935dcbf9c46419e396fdb33c26066.jpg', 0),
(476, 13, 'import_files/cb/cbac2b254ae711e5803300ac06be3045_d2a24e3f4acd40d7a2877d66a4d6f688.jpg', 0),
(484, 14, 'import_files/cb/cbac2b2c4ae711e5803300ac06be3045_6c3674d1af2e4368a9b8d1f25c5c2d37.jpg', 0),
(483, 14, 'import_files/cb/cbac2b2c4ae711e5803300ac06be3045_4768786f42dd48a8a73641037f156449.jpg', 0),
(482, 14, 'import_files/cb/cbac2b2c4ae711e5803300ac06be3045_2ad2bbd73ada4f529176e8a56a7a4a6a.jpg', 0),
(481, 14, 'import_files/cb/cbac2b2c4ae711e5803300ac06be3045_1fd5c1bd0f964ef690674c62f576dafd.jpg', 0),
(480, 14, 'import_files/cb/cbac2b2c4ae711e5803300ac06be3045_0a725b31bd5d4c968fda86986481f473.jpg', 0),
(487, 15, 'import_files/cb/cbac2b364ae711e5803300ac06be3045_94d054d7ec1540c5a2710ddb9f191d20.jpg', 0),
(486, 15, 'import_files/cb/cbac2b364ae711e5803300ac06be3045_1f3c3ab67cc94f39a5073b5103525b8d.jpg', 0),
(485, 15, 'import_files/cb/cbac2b364ae711e5803300ac06be3045_25e6ae98073a40cdacb26d2ff1037db9.jpg', 0),
(492, 16, 'import_files/cb/cbac2b3b4ae711e5803300ac06be3045_ecfe23f98d6844ceb9fa47591e101aad.jpg', 0),
(491, 16, 'import_files/cb/cbac2b3b4ae711e5803300ac06be3045_1279a8a9042a4fb8b77ccf32b78bb97e.jpg', 0),
(490, 16, 'import_files/cb/cbac2b3b4ae711e5803300ac06be3045_6b0e224c86ff42b6a7a8dfbb3a6f22db.jpg', 0),
(489, 16, 'import_files/cb/cbac2b3b4ae711e5803300ac06be3045_760298eb1c0c44ff9a0c8aea393dd23c.jpg', 0),
(488, 16, 'import_files/cb/cbac2b3b4ae711e5803300ac06be3045_292dd8684f404d43970c7beca895c005.jpg', 0),
(495, 17, 'import_files/f7/f7d2d3b652bf11e5803300ac06be3045_7336c354df7d4f35a333c7102cd9a237.jpg', 0),
(494, 17, 'import_files/f7/f7d2d3b652bf11e5803300ac06be3045_6f3ec2f6d2414245b339c7efd8d398ba.jpg', 0),
(493, 17, 'import_files/f7/f7d2d3b652bf11e5803300ac06be3045_ca81e56c2eeb45deb47ce6d1ab742ce4.jpg', 0),
(504, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_b1722fcf8de54e458582d79dc7128f72.jpg', 0),
(503, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_0cdc42e4feb249798c8ef32d5220de6a.jpg', 0),
(502, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_939d792b13b841b597fb2258013a0297.jpg', 0),
(501, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_e7920bddf55a44869904caeb3eb33ead.jpg', 0),
(500, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_8c4ff158831641ac9b01c448f2f15cbb.jpg', 0),
(499, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_f1de469786ca49619e46c1edb5f378a5.jpg', 0),
(498, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_35fcc53510b6433f9ff30b2a329a426b.jpg', 0),
(497, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_0057e8dcf6ce493cb0934062c0769c7a.jpg', 0),
(496, 18, 'import_files/f7/f7d2d3c352bf11e5803300ac06be3045_c56c8cc6f3714018bf2ac6806dd16b1c.jpg', 0),
(509, 19, 'import_files/f7/f7d2d3cc52bf11e5803300ac06be3045_3f83c408255640d08f435c37f80756dc.jpg', 0),
(508, 19, 'import_files/f7/f7d2d3cc52bf11e5803300ac06be3045_5b9e333bafc94c81aacbcdfdfe612a4f.jpg', 0),
(507, 19, 'import_files/f7/f7d2d3cc52bf11e5803300ac06be3045_98ba0ca519ad4ca1aed3e66ba4ccf2e7.jpg', 0),
(506, 19, 'import_files/f7/f7d2d3cc52bf11e5803300ac06be3045_ef35aa939c6d46d6b3b79dabc20c86cd.jpg', 0),
(505, 19, 'import_files/f7/f7d2d3cc52bf11e5803300ac06be3045_8cb69cadf9bf4725bcb1f420ed6e8025.jpg', 0),
(512, 20, 'import_files/f7/f7d2d3dc52bf11e5803300ac06be3045_a167006457934291bcb59476430b4a2e.jpg', 0),
(511, 20, 'import_files/f7/f7d2d3dc52bf11e5803300ac06be3045_b9f5345eec274fe7aefcdd8688d5182d.jpg', 0),
(510, 20, 'import_files/f7/f7d2d3dc52bf11e5803300ac06be3045_10881fb9fab8497097595df8ad2127eb.jpg', 0),
(513, 21, 'import_files/f7/f7d2d3e952bf11e5803300ac06be3045_3b9bbad2de9a4fdbadce8341a24dbe33.jpg', 0),
(515, 22, 'import_files/fa/fad705d152dc11e5803300ac06be3045_a9833f18576a4ea1979caaf1781d3295.jpg', 0),
(514, 22, 'import_files/fa/fad705d152dc11e5803300ac06be3045_064eb1744b8745f5a7d521c04e433208.jpg', 0),
(517, 23, 'import_files/fa/fad705de52dc11e5803300ac06be3045_18b03df87a4a443bb49c12ed5294a6c4.jpg', 0),
(516, 23, 'import_files/fa/fad705de52dc11e5803300ac06be3045_9fd4ef53c70b42f598fd62b4d06b9b09.jpg', 0),
(521, 24, 'import_files/fa/fad705e552dc11e5803300ac06be3045_c6487a5929ef4870bb124fd18ee1cb6d.jpg', 0),
(520, 24, 'import_files/fa/fad705e552dc11e5803300ac06be3045_c5421ed83ed9451cae5bd8c6b6d4a3ac.jpg', 0),
(519, 24, 'import_files/fa/fad705e552dc11e5803300ac06be3045_b99ea5bdedd4440da780200cbb7fe2f5.jpg', 0),
(518, 24, 'import_files/fa/fad705e552dc11e5803300ac06be3045_00f5ac278f984daaa160eb5185571c8f.jpg', 0),
(523, 25, 'import_files/fa/fad705ec52dc11e5803300ac06be3045_68b500ae7f994f1aa66b17764c90dc6d.jpg', 0),
(522, 25, 'import_files/fa/fad705ec52dc11e5803300ac06be3045_fbdd6fe31c3048559b658ecc230b5b3f.jpg', 0),
(528, 26, 'import_files/fa/fad705f352dc11e5803300ac06be3045_7a2abc6e9dc448218989080f70858fd5.jpg', 0),
(527, 26, 'import_files/fa/fad705f352dc11e5803300ac06be3045_acd06dc94f9a46cf8a4fdc97236faa11.jpg', 0),
(526, 26, 'import_files/fa/fad705f352dc11e5803300ac06be3045_c2ddc437eb99435b8bb588445721fbd2.jpg', 0),
(525, 26, 'import_files/fa/fad705f352dc11e5803300ac06be3045_d35dd14f8740436aaa17bb045b363da3.jpg', 0),
(524, 26, 'import_files/fa/fad705f352dc11e5803300ac06be3045_bbce745243284b16aab080df31f21761.jpg', 0),
(532, 27, 'import_files/fa/fad705fb52dc11e5803300ac06be3045_3ee18d010acf4a67bd63291eac5be543.jpg', 0),
(531, 27, 'import_files/fa/fad705fb52dc11e5803300ac06be3045_34d48a77fe664197b2d0acc2e3ec94bd.jpg', 0),
(530, 27, 'import_files/fa/fad705fb52dc11e5803300ac06be3045_573952b17c944663902d418f37c3ecd4.jpg', 0),
(529, 27, 'import_files/fa/fad705fb52dc11e5803300ac06be3045_0f2c158eaf154f5a82fbc1f364e5868f.jpg', 0),
(535, 28, 'import_files/fa/fad7060b52dc11e5803300ac06be3045_2c239901c28e441a83f664293d36d42d.jpg', 0),
(534, 28, 'import_files/fa/fad7060b52dc11e5803300ac06be3045_f6ddd9ba0d264ef78e38095b5e7fcf2e.jpg', 0),
(533, 28, 'import_files/fa/fad7060b52dc11e5803300ac06be3045_a1f429e11bfe42d4b3111f820d93890b.jpg', 0),
(539, 29, 'import_files/94/94c0004852e711e5803300ac06be3045_4e909607274645ebb9718f2e9a592a4a.jpg', 0),
(538, 29, 'import_files/94/94c0004852e711e5803300ac06be3045_c3a5fe26691246eaaf9467734ce60af8.jpg', 0),
(537, 29, 'import_files/94/94c0004852e711e5803300ac06be3045_e410fdd2219d423fa3ec8ed8a3c7ee3c.jpg', 0),
(536, 29, 'import_files/94/94c0004852e711e5803300ac06be3045_5b89c45014e24c28a25a7d7863f13a8b.jpg', 0),
(543, 30, 'import_files/94/94c0005852e711e5803300ac06be3045_ff920a1ef10740d9a59245659af61e39.jpg', 0),
(542, 30, 'import_files/94/94c0005852e711e5803300ac06be3045_20100a655c344c7ea8584d1c27ab6380.jpg', 0),
(541, 30, 'import_files/94/94c0005852e711e5803300ac06be3045_fc9998ef39d242e6b0c284090bb484db.jpg', 0),
(540, 30, 'import_files/94/94c0005852e711e5803300ac06be3045_ec1aae76dd6745c6b22b23bf919a1b6f.jpg', 0),
(547, 31, 'import_files/94/94c0006852e711e5803300ac06be3045_89e69ce8a57c40f8aff75e9421dcc76d.jpg', 0),
(546, 31, 'import_files/94/94c0006852e711e5803300ac06be3045_a7197a56130b41659e003d41069ae587.jpg', 0),
(545, 31, 'import_files/94/94c0006852e711e5803300ac06be3045_f2cbf1d7dd5c4acc9c1baf422671b8d5.jpg', 0),
(544, 31, 'import_files/94/94c0006852e711e5803300ac06be3045_b94d3ad8383744dd8775ec70f7a9dbe9.jpg', 0),
(551, 32, 'import_files/94/94c0007852e711e5803300ac06be3045_826a99055a2d4df8bb933768d4eb9457.jpg', 0),
(550, 32, 'import_files/94/94c0007852e711e5803300ac06be3045_6ba3d798afdb4368b7b93b45fac62de8.jpg', 0),
(549, 32, 'import_files/94/94c0007852e711e5803300ac06be3045_1728f87f5d3a4eb39fc2fb9fd000eb08.jpg', 0),
(548, 32, 'import_files/94/94c0007852e711e5803300ac06be3045_6d2ab827f58044fb98f1005b36926068.jpg', 0),
(555, 33, 'import_files/b0/b051ced552f011e5803300ac06be3045_478fc23fdd8141cc84f2ad05d29b7f87.jpg', 0),
(554, 33, 'import_files/b0/b051ced552f011e5803300ac06be3045_76d6134623cd4a6086207917ce499cb1.jpg', 0),
(553, 33, 'import_files/b0/b051ced552f011e5803300ac06be3045_20098a46b67549d59b9d1d74d2f60049.jpg', 0),
(552, 33, 'import_files/b0/b051ced552f011e5803300ac06be3045_76e760c13f234652a69ca5ad5b7d262e.jpg', 0),
(561, 34, 'import_files/b0/b051cee552f011e5803300ac06be3045_61d4e19157344549bc1662ca7454961b.jpg', 0),
(560, 34, 'import_files/b0/b051cee552f011e5803300ac06be3045_b956fe35660d42219b320fbac083261c.jpg', 0),
(559, 34, 'import_files/b0/b051cee552f011e5803300ac06be3045_d6859bab4b694e888fc95a93c51c3890.jpg', 0),
(558, 34, 'import_files/b0/b051cee552f011e5803300ac06be3045_cb14326c982a4d6a8d7cd6f6a57c3a62.jpg', 0),
(557, 34, 'import_files/b0/b051cee552f011e5803300ac06be3045_1cfef8f0f2544d318c8a3d5b06f785ec.jpg', 0),
(556, 34, 'import_files/b0/b051cee552f011e5803300ac06be3045_4857bbc52def49528b206612ebcb5eb1.jpg', 0),
(566, 35, 'import_files/9a/9ac78bf3530b11e5803300ac06be3045_86dc34f63abd4eb582153fba2148710d.jpg', 0),
(565, 35, 'import_files/9a/9ac78bf3530b11e5803300ac06be3045_df61371d87d94c64840b93d7f83750ca.jpg', 0),
(564, 35, 'import_files/9a/9ac78bf3530b11e5803300ac06be3045_14195b81bd82439689ebf6498519ab5a.jpg', 0),
(563, 35, 'import_files/9a/9ac78bf3530b11e5803300ac06be3045_d5145991cf0547f59bc6fd7b2eb5a983.jpg', 0),
(562, 35, 'import_files/9a/9ac78bf3530b11e5803300ac06be3045_a6a1bd851a4e45cfaa08e1297480d4ad.jpg', 0),
(571, 36, 'import_files/9a/9ac78bfa530b11e5803300ac06be3045_c5d1ba839a334baebf45bd1f9a48c202.jpg', 0),
(570, 36, 'import_files/9a/9ac78bfa530b11e5803300ac06be3045_8acf36a3a1854290b0bf2a2c95ecdf24.jpg', 0),
(569, 36, 'import_files/9a/9ac78bfa530b11e5803300ac06be3045_4080b3619bae4b50aea89b5369d229ae.jpg', 0),
(568, 36, 'import_files/9a/9ac78bfa530b11e5803300ac06be3045_8476a66ce2c74ca4aaa918f4c179c0e2.jpg', 0),
(567, 36, 'import_files/9a/9ac78bfa530b11e5803300ac06be3045_ca1765657e3f448c986146ec26bfaea6.jpg', 0),
(576, 37, 'import_files/9a/9ac78c00530b11e5803300ac06be3045_b1c3443b05844ef88ee455e9709379e8.jpg', 0),
(575, 37, 'import_files/9a/9ac78c00530b11e5803300ac06be3045_e1ad34db93084688902d57d80cd48724.jpg', 0),
(574, 37, 'import_files/9a/9ac78c00530b11e5803300ac06be3045_456210f1c72e44189b87bbd9ec8d15aa.jpg', 0),
(573, 37, 'import_files/9a/9ac78c00530b11e5803300ac06be3045_3b1e237a43c44dea9cfb8431d118d4c8.jpg', 0),
(572, 37, 'import_files/9a/9ac78c00530b11e5803300ac06be3045_58281afb8bc740b7a3c17d5790f50ee1.jpg', 0),
(581, 38, 'import_files/9a/9ac78c06530b11e5803300ac06be3045_e0c7c2652eed45658d97a331397d7a87.jpg', 0),
(580, 38, 'import_files/9a/9ac78c06530b11e5803300ac06be3045_64eafc37233b406b8f87c3b237e91abf.jpg', 0),
(579, 38, 'import_files/9a/9ac78c06530b11e5803300ac06be3045_5d469467792943b4970fb79bd5179bac.jpg', 0),
(578, 38, 'import_files/9a/9ac78c06530b11e5803300ac06be3045_168d5c0d01214f799765212b1b3964a7.jpg', 0),
(577, 38, 'import_files/9a/9ac78c06530b11e5803300ac06be3045_36a6af987a754ab99a8511f99c27c513.jpg', 0),
(590, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_26395254fb574e6182251c1f77e76c34.jpg', 0),
(589, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_d9ac99004d474ef6831822fc74a51922.jpg', 0),
(588, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_da3e8f3393f64f20832f6623bec66ad2.jpg', 0),
(587, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_129693ff8f1c43668b2c7476e06c8e25.jpg', 0),
(586, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_2a53f789e28b430e8f7ea56d092e92b9.jpg', 0),
(585, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_9bbee2567b0649a29356a118384c83cf.jpg', 0),
(584, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_35ff4d0f143644da99451e685708e462.jpg', 0),
(583, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_de8b4bde12314f239ee63a91b2272452.jpg', 0),
(582, 39, 'import_files/9a/9a662f24538811e5803300ac06be3045_33eec06506b848ccae949499a427476f.jpg', 0),
(600, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_f3b97f0834ea44428ae45397eb5766c9.jpg', 0),
(599, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_77373987d7d94e9a930934fe0e310810.jpg', 0),
(598, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_fc27634d3b894cf896980ca3aa11eb7c.jpg', 0),
(597, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_b9b51a4fe0bf4a769f040616463b2233.jpg', 0),
(596, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_ad68fc19882a49a8a49c7eda6bd290f0.jpg', 0),
(595, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_e00bb9a3f8514351aa0aa170d0a5a128.jpg', 0),
(594, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_e14218dfe1164227ab59803a18b22fbe.jpg', 0),
(593, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_c39bf555b78849d9af56cee14be33be2.jpg', 0),
(592, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_cd55e81507184cadb8c12d13341723fa.jpg', 0),
(591, 40, 'import_files/9a/9a662f2f538811e5803300ac06be3045_c482af8369de4b32bda87234b3d14ba6.jpg', 0),
(604, 41, 'import_files/9a/9a662f3a538811e5803300ac06be3045_4e3e5ad2183a4962bd4b15d851f9f583.jpg', 0),
(603, 41, 'import_files/9a/9a662f3a538811e5803300ac06be3045_a93dddbc6f3e4c86b371b2dd9882b2eb.jpg', 0),
(602, 41, 'import_files/9a/9a662f3a538811e5803300ac06be3045_1a6589d458344fd8a375ecda1176cd8a.jpg', 0),
(601, 41, 'import_files/9a/9a662f3a538811e5803300ac06be3045_0341507374d14d7e8e56a5076c243aae.jpg', 0),
(607, 43, 'import_files/9a/9a662f50538811e5803300ac06be3045_976f123b062341458597f230674d4dbc.jpg', 0),
(606, 43, 'import_files/9a/9a662f50538811e5803300ac06be3045_beeb5b35e01249db8dc14bfb1d832d76.jpg', 0),
(605, 43, 'import_files/9a/9a662f50538811e5803300ac06be3045_4e6ce519be40418998a9088c48ef7393.jpg', 0),
(610, 44, 'import_files/9a/9a662f56538811e5803300ac06be3045_5fa51bd513214592af368ba947ee8168.jpg', 0),
(609, 44, 'import_files/9a/9a662f56538811e5803300ac06be3045_3f330f8d176a419eaa7e566c536f9fe7.jpg', 0),
(608, 44, 'import_files/9a/9a662f56538811e5803300ac06be3045_e0417eb117564de2a65f4938c9864ef9.jpg', 0),
(614, 45, 'import_files/9a/9a662f5c538811e5803300ac06be3045_f78f7137e2fa43fbbf10b1f1c40c69ac.jpg', 0),
(613, 45, 'import_files/9a/9a662f5c538811e5803300ac06be3045_b7dfb3f511314d80afdf99a94754f58d.jpg', 0),
(612, 45, 'import_files/9a/9a662f5c538811e5803300ac06be3045_fe6cb7531a214d9197c10f2a522b9f71.jpg', 0),
(611, 45, 'import_files/9a/9a662f5c538811e5803300ac06be3045_d6412f751d9f42258d6df4a8c9442dba.jpg', 0),
(616, 47, 'import_files/6b/6b849be8539011e5803300ac06be3045_4a001f0255b44b62bdd5ef506a336bf9.jpg', 0),
(615, 47, 'import_files/6b/6b849be8539011e5803300ac06be3045_4136a537a39a452c96c0384f7b76b053.jpg', 0),
(620, 48, 'import_files/6b/6b849bef539011e5803300ac06be3045_10c6c9d457f74a1b8a3818d80105ed52.jpg', 0),
(619, 48, 'import_files/6b/6b849bef539011e5803300ac06be3045_d31247bf2e814674940d4879bdb1730f.jpg', 0),
(618, 48, 'import_files/6b/6b849bef539011e5803300ac06be3045_580382c01af44e31ad8137fbf606c8da.jpg', 0),
(617, 48, 'import_files/6b/6b849bef539011e5803300ac06be3045_8b7a935931514dfcb1821f5210c51bea.jpg', 0),
(626, 50, 'import_files/6b/6b849c05539011e5803300ac06be3045_affc0e51e0384dfab4a066ce2ceea4b4.jpg', 0),
(625, 50, 'import_files/6b/6b849c05539011e5803300ac06be3045_57b520c932344678af7d769b7ff35b18.jpg', 0),
(624, 50, 'import_files/6b/6b849c05539011e5803300ac06be3045_68be8bd44e634e2baf553cd625a006ab.jpg', 0),
(623, 50, 'import_files/6b/6b849c05539011e5803300ac06be3045_f5ef75ab041f4915aef61689568bcc0d.jpg', 0),
(622, 50, 'import_files/6b/6b849c05539011e5803300ac06be3045_20fe099bccd643faac68cab55fcc72c7.jpg', 0),
(621, 50, 'import_files/6b/6b849c05539011e5803300ac06be3045_0ee866c88c5c47f7a4d29f552df8cd80.jpg', 0),
(629, 51, 'import_files/6b/6b849c0a539011e5803300ac06be3045_d51e27beb5324895817d6974f990575e.jpg', 0),
(628, 51, 'import_files/6b/6b849c0a539011e5803300ac06be3045_7fbdc2e4e758442c8aa270637e87e1e7.jpg', 0),
(627, 51, 'import_files/6b/6b849c0a539011e5803300ac06be3045_42908691fc22488f9dcdc76f0ffbefc1.jpg', 0),
(633, 52, 'import_files/6b/6b849c10539011e5803300ac06be3045_5b49811d677043cabdb29c7f3cd11fe5.jpg', 0),
(632, 52, 'import_files/6b/6b849c10539011e5803300ac06be3045_57f34723bf074284afc7e13e710dffce.jpg', 0),
(631, 52, 'import_files/6b/6b849c10539011e5803300ac06be3045_ac0b247e8d614d37a78567fe3b931655.jpg', 0),
(630, 52, 'import_files/6b/6b849c10539011e5803300ac06be3045_50ba4943ddf649c1974e89b885efd6ce.jpg', 0),
(636, 53, 'import_files/b1/b153d6a2539811e5803300ac06be3045_e7fc401695e149b1b8210bc7bfa38c6c.jpg', 0),
(635, 53, 'import_files/b1/b153d6a2539811e5803300ac06be3045_d7972136c22a46259e191659abc297d9.jpg', 0),
(634, 53, 'import_files/b1/b153d6a2539811e5803300ac06be3045_0fe64de3ec4646968ec90a1856c4abcd.jpg', 0),
(641, 54, 'import_files/b1/b153d6a7539811e5803300ac06be3045_668048d834724994832d2e1267e6f8b2.jpg', 0),
(640, 54, 'import_files/b1/b153d6a7539811e5803300ac06be3045_bef841509c304181932ee6b975b789d1.jpg', 0),
(639, 54, 'import_files/b1/b153d6a7539811e5803300ac06be3045_8799602c6a38415e97e28d967dee482a.jpg', 0),
(638, 54, 'import_files/b1/b153d6a7539811e5803300ac06be3045_8bcfb4a0c223499ea80c617b12fcb5a9.jpg', 0),
(637, 54, 'import_files/b1/b153d6a7539811e5803300ac06be3045_2d7a4e606608432fa9f64c0ca469571a.jpg', 0),
(646, 55, 'import_files/b1/b153d6af539811e5803300ac06be3045_48803f8bdc9e46f4b82c017de4e52647.jpg', 0),
(645, 55, 'import_files/b1/b153d6af539811e5803300ac06be3045_964fa739c3d04a5ea679e8843a63f1c5.jpg', 0),
(644, 55, 'import_files/b1/b153d6af539811e5803300ac06be3045_f9c38cec85164ebfa2054cffdd43ad14.jpg', 0),
(643, 55, 'import_files/b1/b153d6af539811e5803300ac06be3045_acf2bafe41eb4f8aa1d1c8b8fc3d5f65.jpg', 0),
(642, 55, 'import_files/b1/b153d6af539811e5803300ac06be3045_9dd5b34d914640fe9bb4bdc02ca67919.jpg', 0),
(651, 56, 'import_files/b1/b153d6b7539811e5803300ac06be3045_db6515959b4440ee805692e1ca31c122.jpg', 0),
(650, 56, 'import_files/b1/b153d6b7539811e5803300ac06be3045_85ad97116f5e451c883771daefeca3ff.jpg', 0),
(649, 56, 'import_files/b1/b153d6b7539811e5803300ac06be3045_c0d89c60e5fc46ee8d765ec593912eea.jpg', 0),
(648, 56, 'import_files/b1/b153d6b7539811e5803300ac06be3045_4e0c7a37ba604a679c9cc79fbf5f5ce4.jpg', 0),
(647, 56, 'import_files/b1/b153d6b7539811e5803300ac06be3045_40b18047f5f043c3ab4512ee9a2544ec.jpg', 0),
(656, 57, 'import_files/76/760aeee053b911e5803300ac06be3045_5d0b266cf10b43ef8d4f9bae34e08ccb.jpg', 0),
(655, 57, 'import_files/76/760aeee053b911e5803300ac06be3045_b342152c6db747978f319364b6a0356a.jpg', 0),
(654, 57, 'import_files/76/760aeee053b911e5803300ac06be3045_77188f53eecb4dfb92bab98108ad97dd.jpg', 0),
(653, 57, 'import_files/76/760aeee053b911e5803300ac06be3045_e856e21e9f9e4b2b9c361ff3d1652710.jpg', 0),
(652, 57, 'import_files/76/760aeee053b911e5803300ac06be3045_cb626e443be44e89b236d63d6b373c43.jpg', 0),
(658, 58, 'import_files/76/760aeee753b911e5803300ac06be3045_83dfc5e692b14321bd78df189a810ceb.jpg', 0),
(657, 58, 'import_files/76/760aeee753b911e5803300ac06be3045_9dae0c08182a49bbb69a045d784d173d.jpg', 0),
(660, 59, 'import_files/76/760aeeec53b911e5803300ac06be3045_6246efe8eb0f42179dd5fa677dccbf24.jpg', 0),
(659, 59, 'import_files/76/760aeeec53b911e5803300ac06be3045_50bc861cb02b49f69ceb089f3c391194.jpg', 0),
(663, 60, 'import_files/76/760aeef553b911e5803300ac06be3045_441c2347237e41c78629586f03b112a6.jpg', 0),
(662, 60, 'import_files/76/760aeef553b911e5803300ac06be3045_22ef974ba7e84ee29ad9db33364e4ed0.jpg', 0),
(661, 60, 'import_files/76/760aeef553b911e5803300ac06be3045_60c70c2ed55c41fbb4357fc81cf95acc.jpg', 0),
(666, 61, 'import_files/76/760aeefa53b911e5803300ac06be3045_c2b95c35a58744bfaf89560792f7daaa.jpg', 0),
(665, 61, 'import_files/76/760aeefa53b911e5803300ac06be3045_747cb8341b794eb3a0d1fc04ca331dfd.jpg', 0),
(664, 61, 'import_files/76/760aeefa53b911e5803300ac06be3045_11cd5482f55c4b4c9f3426c21da5d906.jpg', 0),
(671, 62, 'import_files/76/760aef0753b911e5803300ac06be3045_65d476378d6243f68fda92fcb689de8b.jpg', 0),
(670, 62, 'import_files/76/760aef0753b911e5803300ac06be3045_7b2fa3e86c5047ca93f5062e4959d28a.jpg', 0),
(669, 62, 'import_files/76/760aef0753b911e5803300ac06be3045_f1e2251a363548bca7ac3f53e3bf921e.jpg', 0),
(668, 62, 'import_files/76/760aef0753b911e5803300ac06be3045_b489934be13d4eb7aa12da3c1985846b.jpg', 0),
(667, 62, 'import_files/76/760aef0753b911e5803300ac06be3045_84cb8d2d508a42a7b3cfd236b787aafa.jpg', 0),
(675, 63, 'import_files/76/760aef0c53b911e5803300ac06be3045_e4efb50a3f7e4407befede8143e0b379.jpg', 0),
(674, 63, 'import_files/76/760aef0c53b911e5803300ac06be3045_6ef4b5a6e30f428eba8db0950dd24d66.jpg', 0),
(673, 63, 'import_files/76/760aef0c53b911e5803300ac06be3045_562fb048ac8e4b24b1f7d32637bed6b4.jpg', 0),
(672, 63, 'import_files/76/760aef0c53b911e5803300ac06be3045_048c9e98982f4054a283f0a2d3d74c36.jpg', 0),
(678, 64, 'import_files/76/760aef1c53b911e5803300ac06be3045_3a36673ab62e4ac6b0e80dfdd2c6ea7b.jpg', 0),
(677, 64, 'import_files/76/760aef1c53b911e5803300ac06be3045_f8e47ade3ee44e90ac6691e2d07222f7.jpg', 0),
(676, 64, 'import_files/76/760aef1c53b911e5803300ac06be3045_60afb59fe86d47ed87cb8b431caf1c35.jpg', 0),
(685, 65, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_1faed25174eb4ec68e6421f5ddbadc02.jpg', 0),
(684, 65, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_f3dd35d1e3634642990af44945c9342c.jpg', 0),
(683, 65, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_6301af7690f64885a45b29ffe78de6e2.jpg', 0),
(682, 65, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_d252d84d5563405bae4395c7378cdcef.jpg', 0),
(681, 65, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_189999ee9d884a46b02e4de6df095b7d.jpg', 0),
(680, 65, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_961d915fd9024ed0b05aa08943241bd3.jpg', 0),
(679, 65, 'import_files/a6/a64672dc3c1611e58a4f00ac06be3045_c9beb3cf525a46f4b38e6154b47c6ed3.jpg', 0),
(692, 66, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_2d3904141ab94555b5c3ecf5ea34242c.jpg', 0),
(691, 66, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_f93fb1b01ae44483b26e7700abe665d4.jpg', 0),
(690, 66, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_688575152d984bd0b21968eb6dcfba26.jpg', 0),
(689, 66, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_7786a99ae4e54f7d9b840b49fe1af217.jpg', 0),
(688, 66, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_b51e43c272e54f42955d794153bd0e57.jpg', 0),
(687, 66, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_a135248f400244c38f1b169b22c96189.jpg', 0),
(686, 66, 'import_files/47/476442c83f6a11e58a4f00ac06be3045_83a5f53466b640518ec50cffb12f49c2.jpg', 0),
(698, 67, 'import_files/9b/9b2fab233fe811e58a4f00ac06be3045_0f58726f059046e28909d3f79db71705.jpg', 0),
(697, 67, 'import_files/9b/9b2fab233fe811e58a4f00ac06be3045_5c91a485f8aa42a38980a40de0fef367.jpg', 0),
(696, 67, 'import_files/9b/9b2fab233fe811e58a4f00ac06be3045_7033c8818deb41f98c5548c50162dc78.jpg', 0),
(695, 67, 'import_files/9b/9b2fab233fe811e58a4f00ac06be3045_5089e52b127640d79c8bda55856fa6f8.jpg', 0),
(694, 67, 'import_files/9b/9b2fab233fe811e58a4f00ac06be3045_da01a355bec74264b3070ce5e2e04308.jpg', 0),
(693, 67, 'import_files/9b/9b2fab233fe811e58a4f00ac06be3045_563df944c91c4596b6baf6835e6b647a.jpg', 0),
(702, 68, 'import_files/7c/7c830c47401011e58a4f00ac06be3045_b7863ab0e6d443b9bde69220dad69c2f.jpg', 0),
(701, 68, 'import_files/7c/7c830c47401011e58a4f00ac06be3045_d93287927b424dd795aa584c6510d569.jpg', 0),
(700, 68, 'import_files/7c/7c830c47401011e58a4f00ac06be3045_5f4e23171c3e4d668bc1b593451b2bc4.jpg', 0),
(699, 68, 'import_files/7c/7c830c47401011e58a4f00ac06be3045_d4e7160d633a491e884af78633de5c21.jpg', 0),
(707, 69, 'import_files/7c/7c830c4d401011e58a4f00ac06be3045_b21bab61b4c146fd9a92310ae99f7f8f.jpg', 0),
(706, 69, 'import_files/7c/7c830c4d401011e58a4f00ac06be3045_8f56d21684a34dbb9fe0c7c09719eac2.jpg', 0),
(705, 69, 'import_files/7c/7c830c4d401011e58a4f00ac06be3045_f4c74fcda8bc4ca2a109bcd03199e20c.jpg', 0),
(704, 69, 'import_files/7c/7c830c4d401011e58a4f00ac06be3045_5ab118cdc6c04f0daba93b1c38662a16.jpg', 0),
(703, 69, 'import_files/7c/7c830c4d401011e58a4f00ac06be3045_e66e4997f6e74c5baf29e1e2fca380f8.jpg', 0),
(714, 70, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_d6fb676316254bbc86f3e8acce1994b0.jpg', 0),
(713, 70, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_5bf902903d204088886d268fde3c1e42.jpg', 0),
(712, 70, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_bbd8b6ac6ab345109481afbee8e32a0f.jpg', 0),
(711, 70, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_10d6c4dffc6c424b96eace3d865f5ab7.jpg', 0),
(710, 70, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_b5960757001f4c66a7a56a4fd79add06.jpg', 0),
(709, 70, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_9868bb2d126a4693a7ad7a1402d54ed4.jpg', 0),
(708, 70, 'import_files/7c/7c830c56401011e58a4f00ac06be3045_6d8ff2ec89ad473fb846a6bbc22e4e27.jpg', 0),
(718, 73, 'import_files/64/64053bac5c7c11e5970800ac06be3045_8c5415fc683f4190a2644b50337c0eb6.jpg', 0),
(717, 73, 'import_files/64/64053bac5c7c11e5970800ac06be3045_dc0da7931fb54fbe9f9b79f47217c165.jpg', 0),
(716, 73, 'import_files/64/64053bac5c7c11e5970800ac06be3045_eca27fa1e75048649adfa212b788275b.jpg', 0),
(715, 73, 'import_files/64/64053bac5c7c11e5970800ac06be3045_4c335901e2024a2399cb5ee22cc3b38e.jpg', 0),
(721, 74, 'import_files/64/64053bbc5c7c11e5970800ac06be3045_0b1504fa90d64bf09ab9befbeee12eb7.jpg', 0),
(720, 74, 'import_files/64/64053bbc5c7c11e5970800ac06be3045_3080ec5aeb3042068f7981f22a7e7623.jpg', 0),
(719, 74, 'import_files/64/64053bbc5c7c11e5970800ac06be3045_20d49c8116a24cd68494f46ce37a7317.jpg', 0),
(725, 75, 'import_files/d9/d90ff3b25d0111e5970800ac06be3045_22d8c1928baa448b8898b3aa48fee125.jpg', 0),
(724, 75, 'import_files/d9/d90ff3b25d0111e5970800ac06be3045_00184cd7774a4bac8ddca07d8a592d19.jpg', 0),
(723, 75, 'import_files/d9/d90ff3b25d0111e5970800ac06be3045_865d1901ad4e4d5c918a5cf706273452.jpg', 0),
(722, 75, 'import_files/d9/d90ff3b25d0111e5970800ac06be3045_79faee7081f04f6d9803ca7967803a2f.jpg', 0),
(730, 76, 'import_files/1a/1aa5692d5de211e5970800ac06be3045_139a09a4fcf040b0b966f1d40a79f59d.jpg', 0),
(729, 76, 'import_files/1a/1aa5692d5de211e5970800ac06be3045_7d75a1db7a854273b7dbcf66f79c6686.jpg', 0),
(728, 76, 'import_files/1a/1aa5692d5de211e5970800ac06be3045_6a3f2a1f553343e2abcbb2e054199ce7.jpg', 0),
(727, 76, 'import_files/1a/1aa5692d5de211e5970800ac06be3045_dfceb9e880de42fe9a2a5210ec6c9b64.jpg', 0),
(726, 76, 'import_files/1a/1aa5692d5de211e5970800ac06be3045_ebf045d08022445999cde2f642ac7979.jpg', 0),
(735, 77, 'import_files/32/32118fc75f8311e5970800ac06be3045_d3e1ad8f70c049299916c205dfa52635.jpg', 0),
(734, 77, 'import_files/32/32118fc75f8311e5970800ac06be3045_6104253f8b0e420b9b64022f9775d9be.jpg', 0),
(733, 77, 'import_files/32/32118fc75f8311e5970800ac06be3045_1d076f99e8c546b29cac3671a5052fe2.jpg', 0),
(732, 77, 'import_files/32/32118fc75f8311e5970800ac06be3045_ce23767ca94f4556aa6223965a5e4084.jpg', 0),
(731, 77, 'import_files/32/32118fc75f8311e5970800ac06be3045_85f92f7f15c145d4b0ee1b6be762762b.jpg', 0),
(739, 78, 'import_files/d3/d3ba0c84611411e5970800ac06be3045_ca01444a1ac54cc2b69e2531e6ffd20e.jpg', 0),
(738, 78, 'import_files/d3/d3ba0c84611411e5970800ac06be3045_d2a4368e90874908abcbc8807f491d5b.jpg', 0),
(737, 78, 'import_files/d3/d3ba0c84611411e5970800ac06be3045_9206e487d8084c05a1447e9f52401387.jpg', 0),
(736, 78, 'import_files/d3/d3ba0c84611411e5970800ac06be3045_a6ff663fb2c440c894975c0a0eff56cf.jpg', 0),
(744, 79, 'import_files/d3/d3ba0c8a611411e5970800ac06be3045_d1a820050b4d41d384480e7e81fccf60.jpg', 0),
(743, 79, 'import_files/d3/d3ba0c8a611411e5970800ac06be3045_7eeaacf241714da09554cb0398294e36.jpg', 0),
(742, 79, 'import_files/d3/d3ba0c8a611411e5970800ac06be3045_45538e982e4a4851a14284a6e4836ec6.jpg', 0),
(741, 79, 'import_files/d3/d3ba0c8a611411e5970800ac06be3045_31313ee0cdea4f52af8b6321dc194917.jpg', 0),
(740, 79, 'import_files/d3/d3ba0c8a611411e5970800ac06be3045_fe174665f950400eb726925e124ffe1d.jpg', 0),
(750, 83, 'import_files/5d/5dcecf5b8cf511e59c3500ac06be3045_f77b9f940c4e4243884f7b6d810653e1.jpg', 0),
(749, 83, 'import_files/5d/5dcecf5b8cf511e59c3500ac06be3045_80373a4787cb4c4d8b2bd9710c7d8ce1.jpg', 0),
(748, 83, 'import_files/5d/5dcecf5b8cf511e59c3500ac06be3045_02b0293cdd534e07a29ea4ef52280af8.jpg', 0),
(747, 83, 'import_files/5d/5dcecf5b8cf511e59c3500ac06be3045_82a288068fe442c5a65d61d93d687879.jpg', 0),
(746, 83, 'import_files/5d/5dcecf5b8cf511e59c3500ac06be3045_167f882ba64e4b03afcec372f1872dd4.jpg', 0),
(745, 83, 'import_files/5d/5dcecf5b8cf511e59c3500ac06be3045_b59bea73c16a442bb05d22f661add0f1.jpg', 0),
(754, 88, 'import_files/fd/fd154ae07ca411e5aacb00ac06be3045_c4a529ab0c1c44c1bc11c64fc4ea3118.jpg', 0),
(753, 88, 'import_files/fd/fd154ae07ca411e5aacb00ac06be3045_055a55684d5047a391bfee3f8ef05826.jpg', 0),
(752, 88, 'import_files/fd/fd154ae07ca411e5aacb00ac06be3045_dc5c469570ea412e91bb86d6836784d1.jpg', 0),
(751, 88, 'import_files/fd/fd154ae07ca411e5aacb00ac06be3045_611b38c3edef4c6f8791c59349fd4b0e.jpg', 0),
(761, 92, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_b2ca6a1953174a8a80bd15e1c01151ae.jpg', 0),
(760, 92, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_22618b31d1fa44498dbd8293ce1486b9.jpg', 0),
(759, 92, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_99fc5c3277254b609aa138a370c4dcc0.jpg', 0),
(758, 92, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_5dc19681ff97447ca53ebfdd2de87a9c.jpg', 0),
(757, 92, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_9774f0c624b94d03a700f306201f50df.jpg', 0),
(756, 92, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_91818bfdb2e54482a70790c49cfbcabe.jpg', 0),
(755, 92, 'import_files/bd/bdcf7b4d7ed111e5aacb00ac06be3045_a2256476dad44ba6a999df7992fc2633.jpg', 0),
(766, 94, 'import_files/b3/b39873f6820311e5aacb00ac06be3045_0d5da929bd984964b1e78863f6119f8f.jpg', 0),
(765, 94, 'import_files/b3/b39873f6820311e5aacb00ac06be3045_8b63a65237344531a6d5567be34d083b.jpg', 0),
(764, 94, 'import_files/b3/b39873f6820311e5aacb00ac06be3045_f6716c20681b468ea44bfc394a6f1568.jpg', 0),
(763, 94, 'import_files/b3/b39873f6820311e5aacb00ac06be3045_68791cd9d3c94ed9a2940ed827557651.jpg', 0),
(762, 94, 'import_files/b3/b39873f6820311e5aacb00ac06be3045_9844fafee80a4e468fdb4872d8445b34.jpg', 0),
(778, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_354b63d688154e4283a28d248b1b9350.jpg', 0),
(777, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_4f36f61a8e9c44709992bd199d24ec3f.jpg', 0),
(776, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_20982737f0d940ac9c0d9cab795e3b24.jpg', 0),
(775, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_2193829e2f2749e89cc222a7df784e85.jpg', 0),
(774, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_6a6fa639ed24484ca779e8cd9ea18e2d.jpg', 0),
(773, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_7a175a55689c42f8a948a5af4c991b22.jpg', 0),
(772, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_80b5b6353d654bd1ae374f47f16dc437.jpg', 0),
(771, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_4c79d158eb7b4768aee6fe5870c11342.jpg', 0),
(770, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_958d500842954ba2b71bbf1204c22305.jpg', 0),
(769, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_680f943b2c9447feb7d65623e28dd953.jpg', 0),
(768, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_de6e2028df9947f2b926751b25e85b0d.jpg', 0),
(767, 95, 'import_files/bf/bf11ae72847511e5aacb00ac06be3045_897df7138e1c48fdbde8f4aa90cfc197.jpg', 0),
(782, 98, 'import_files/c5/c5dd127d87b911e5aacb00ac06be3045_fa8551a74b0a48308cc44781161d7aac.jpg', 0),
(781, 98, 'import_files/c5/c5dd127d87b911e5aacb00ac06be3045_f3993bab59fb4cea8e495f7d05f92565.jpg', 0),
(780, 98, 'import_files/c5/c5dd127d87b911e5aacb00ac06be3045_f4764f3305564b5da31b8343a950cd36.jpg', 0),
(779, 98, 'import_files/c5/c5dd127d87b911e5aacb00ac06be3045_affe236b55964866b33059219171d1b9.jpg', 0),
(785, 101, 'import_files/b5/b5fd305d6beb11e5b37400ac06be3045_e8341c7f84e9473ab44836a8105d29a4.jpg', 0),
(784, 101, 'import_files/b5/b5fd305d6beb11e5b37400ac06be3045_abd52cbd0871413099dc6bd6543ed15b.jpg', 0),
(783, 101, 'import_files/b5/b5fd305d6beb11e5b37400ac06be3045_3feb918ab2714f7085cdb8ffb5fd2d41.jpg', 0),
(790, 103, 'import_files/b5/b5fd30716beb11e5b37400ac06be3045_70474f0cf1c24dc3a3c185bce13f6786.jpg', 0),
(789, 103, 'import_files/b5/b5fd30716beb11e5b37400ac06be3045_4baca532747241d0a542c65f1fdf86e0.jpg', 0),
(788, 103, 'import_files/b5/b5fd30716beb11e5b37400ac06be3045_d4ccb22a3df243d3af606f7191e3b0f0.jpg', 0),
(787, 103, 'import_files/b5/b5fd30716beb11e5b37400ac06be3045_0c0368618e7e4846b4aadd5e3335eb1c.jpg', 0),
(786, 103, 'import_files/b5/b5fd30716beb11e5b37400ac06be3045_51ff37cbfc7c4b87b54cda178b0c3dcf.jpg', 0),
(793, 104, 'import_files/2b/2b72975d70b211e5b37400ac06be3045_9e6efe058d364e63b3c65408db9c08cb.jpg', 0),
(792, 104, 'import_files/2b/2b72975d70b211e5b37400ac06be3045_9807b9b11ddf4811af679dba5ebab70d.jpg', 0),
(791, 104, 'import_files/2b/2b72975d70b211e5b37400ac06be3045_6c251b89243045bcadb015dd1c857dfc.jpg', 0),
(795, 106, 'import_files/48/482241d7629811e5b48a00ac06be3045_bbbc5bbc79fa46cdb97ce109e0dab59c.jpg', 0),
(794, 106, 'import_files/48/482241d7629811e5b48a00ac06be3045_a9f81041f9394cbc9ae7843f7ee0646e.jpg', 0),
(803, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_115e294eed16484385f0ff04bc208ceb.jpg', 0),
(802, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_b0a94108977f46c2986203dbb17db19d.jpg', 0),
(801, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_a1a0e5e86a7e47939d5d8d879e57aea7.jpg', 0),
(800, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_9251230f2a2148a2a04ab2ad80734f06.jpg', 0),
(799, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_57b16f69b91845a0a1ef257d7de96492.jpg', 0),
(798, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_7fa48b2204884d60a2ff9c250f731ae5.jpg', 0),
(797, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_12e92b8931e540c4a4b0908ee2be5b75.jpg', 0),
(796, 107, 'import_files/da/da4ae9e7348011e6b6f500ac06be3045_f05af2e401354bb3adeb5ecc5a9171a5.jpg', 0),
(814, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_8c3c4cead0d0445880529990eb73f90a.jpg', 0),
(813, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_fc1554d3d07d4abf830588d497ecbf57.jpg', 0),
(812, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_9f71e461debd4e09845160f1c29573ab.jpg', 0),
(811, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_9385d35d248143608d14709204a7b2e7.jpg', 0),
(810, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_e1bf054c26014ff595c89168574a7c71.jpg', 0),
(809, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_265b321cf66a4367a118dda481ecd642.jpg', 0),
(808, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_53b7b8afa0054d55a1f66c1c14a65654.jpg', 0),
(807, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_6fab1717214746d6a38316562db42fe1.jpg', 0),
(806, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_5d234546dbf541cea7caaeb044889ca5.jpg', 0),
(805, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_95a1a919512f4cb4b0a4715870c7282d.jpg', 0),
(804, 108, 'import_files/ff/ff64052c681a11e5b7e900ac06be3045_2110828b8fa04e5fb53d372c1c4c9b37.jpg', 0),
(820, 109, 'import_files/71/7115084840f511e5bd3400ac06be3045_5e2e6e88f3fc4ef1b88fe8308b4f078a.jpg', 0),
(819, 109, 'import_files/71/7115084840f511e5bd3400ac06be3045_819a43cf22554340a5d5e6b3caa5e67c.jpg', 0),
(818, 109, 'import_files/71/7115084840f511e5bd3400ac06be3045_7f1e9a9d6f3946a0a473d8df555f9431.jpg', 0),
(817, 109, 'import_files/71/7115084840f511e5bd3400ac06be3045_e3819e35fbf84aff9fe91a823405d3e8.jpg', 0),
(816, 109, 'import_files/71/7115084840f511e5bd3400ac06be3045_98b622dffea9491a970d345918ed26a3.jpg', 0),
(815, 109, 'import_files/71/7115084840f511e5bd3400ac06be3045_853cfcf152ae4f798fd3d1d75d37b81f.jpg', 0),
(825, 110, 'import_files/71/7115084f40f511e5bd3400ac06be3045_6a0b8d7c2bcd41fd8313c0b55f83d3af.jpg', 0),
(824, 110, 'import_files/71/7115084f40f511e5bd3400ac06be3045_49fd885852e2455485b09745a77a3b1a.jpg', 0),
(823, 110, 'import_files/71/7115084f40f511e5bd3400ac06be3045_328b7fee16704880884607cc723d9b8a.jpg', 0),
(822, 110, 'import_files/71/7115084f40f511e5bd3400ac06be3045_f0365b0790ab4cb98895103cf81124ac.jpg', 0),
(821, 110, 'import_files/71/7115084f40f511e5bd3400ac06be3045_40deddb094a04ca1a90cb9829006f8e5.jpg', 0),
(827, 113, 'import_files/cd/cd4ce157675b11e5be9200ac06be3045_1232da72005d4c88bf6df18903eb1c2f.jpg', 0),
(826, 113, 'import_files/cd/cd4ce157675b11e5be9200ac06be3045_d01c9356fa6d469fb341c089b1741333.jpg', 0),
(835, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_8271d9de304a432c8c4222b02d8c2b5d.jpg', 0),
(834, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_7de1ac1b703a492f94d4678e644d1bb7.jpg', 0),
(833, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_4937b7b674c64cf2953b16b3f3c621e4.jpg', 0),
(832, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_23a528c943254390a5be14825ce10d67.jpg', 0),
(831, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_3469d40831b9479db1e2d1de9ea40ee8.jpg', 0),
(830, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_6b4e37deaacf4a85b22f04147c6a84e0.jpg', 0),
(829, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_1e80545a3ba54c0bb60e82dda4f38b5a.jpg', 0),
(828, 114, 'import_files/5f/5ff07d4f680b11e5be9200ac06be3045_fcc59688446149b2b9bd3c0b5626476d.jpg', 0),
(838, 115, 'import_files/38/38006f81681811e5be9200ac06be3045_a25009ef114d4bc9af8e9d889d2da604.jpg', 0),
(837, 115, 'import_files/38/38006f81681811e5be9200ac06be3045_415d12d6419b40bb98982bccf3400a85.jpg', 0),
(836, 115, 'import_files/38/38006f81681811e5be9200ac06be3045_fb2941898b4949e889ac8252d0ec1cff.jpg', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_option`
--

INSERT INTO `oc_product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(1, 1, 1, '', 1),
(2, 1, 2, '', 1),
(3, 2, 1, '', 1),
(4, 2, 2, '', 1),
(5, 3, 1, '', 1),
(6, 3, 2, '', 1),
(7, 4, 1, '', 1),
(8, 4, 2, '', 1),
(9, 5, 1, '', 1),
(10, 5, 2, '', 1),
(11, 6, 1, '', 1),
(12, 6, 2, '', 1),
(13, 7, 1, '', 1),
(14, 7, 2, '', 1),
(15, 8, 1, '', 1),
(16, 8, 2, '', 1),
(17, 9, 1, '', 1),
(18, 9, 2, '', 1),
(19, 10, 1, '', 1),
(20, 10, 2, '', 1),
(21, 12, 1, '', 1),
(22, 12, 2, '', 1),
(23, 13, 1, '', 1),
(24, 13, 2, '', 1),
(25, 14, 1, '', 1),
(26, 14, 2, '', 1),
(27, 15, 1, '', 1),
(28, 15, 2, '', 1),
(29, 16, 1, '', 1),
(30, 16, 2, '', 1),
(31, 17, 1, '', 1),
(32, 17, 2, '', 1),
(33, 18, 1, '', 1),
(34, 18, 2, '', 1),
(35, 19, 1, '', 1),
(36, 19, 2, '', 1),
(37, 20, 1, '', 1),
(38, 20, 2, '', 1),
(39, 21, 1, '', 1),
(40, 21, 2, '', 1),
(41, 22, 1, '', 1),
(42, 22, 2, '', 1),
(43, 23, 1, '', 1),
(44, 23, 2, '', 1),
(45, 24, 1, '', 1),
(46, 24, 2, '', 1),
(47, 25, 1, '', 1),
(48, 25, 2, '', 1),
(49, 26, 1, '', 1),
(50, 26, 2, '', 1),
(51, 27, 1, '', 1),
(52, 27, 2, '', 1),
(53, 28, 1, '', 1),
(54, 28, 2, '', 1),
(55, 29, 1, '', 1),
(56, 29, 2, '', 1),
(57, 30, 1, '', 1),
(58, 30, 2, '', 1),
(59, 31, 1, '', 1),
(60, 31, 2, '', 1),
(61, 32, 1, '', 1),
(62, 32, 2, '', 1),
(63, 33, 1, '', 1),
(64, 33, 2, '', 1),
(65, 34, 1, '', 1),
(66, 34, 2, '', 1),
(67, 35, 1, '', 1),
(68, 35, 2, '', 1),
(69, 36, 1, '', 1),
(70, 36, 2, '', 1),
(71, 37, 1, '', 1),
(72, 37, 2, '', 1),
(73, 38, 1, '', 1),
(74, 38, 2, '', 1),
(75, 39, 1, '', 1),
(76, 39, 2, '', 1),
(77, 40, 1, '', 1),
(78, 40, 2, '', 1),
(79, 41, 1, '', 1),
(80, 41, 2, '', 1),
(81, 42, 1, '', 1),
(82, 42, 2, '', 1),
(83, 43, 1, '', 1),
(84, 43, 2, '', 1),
(85, 44, 1, '', 1),
(86, 44, 2, '', 1),
(87, 45, 1, '', 1),
(88, 45, 2, '', 1),
(89, 46, 1, '', 1),
(90, 46, 2, '', 1),
(91, 47, 1, '', 1),
(92, 47, 2, '', 1),
(93, 48, 1, '', 1),
(94, 48, 2, '', 1),
(95, 49, 1, '', 1),
(96, 49, 2, '', 1),
(97, 50, 1, '', 1),
(98, 50, 2, '', 1),
(99, 51, 1, '', 1),
(100, 51, 2, '', 1),
(101, 52, 1, '', 1),
(102, 52, 2, '', 1),
(103, 53, 1, '', 1),
(104, 53, 2, '', 1),
(105, 54, 1, '', 1),
(106, 54, 2, '', 1),
(107, 55, 1, '', 1),
(108, 55, 2, '', 1),
(109, 56, 1, '', 1),
(110, 56, 2, '', 1),
(111, 57, 1, '', 1),
(112, 57, 2, '', 1),
(113, 58, 1, '', 1),
(114, 58, 2, '', 1),
(115, 59, 1, '', 1),
(116, 59, 2, '', 1),
(117, 60, 1, '', 1),
(118, 60, 2, '', 1),
(119, 61, 1, '', 1),
(120, 61, 2, '', 1),
(121, 62, 1, '', 1),
(122, 62, 2, '', 1),
(123, 63, 1, '', 1),
(124, 63, 2, '', 1),
(125, 64, 1, '', 1),
(126, 64, 2, '', 1),
(127, 65, 1, '', 1),
(128, 65, 2, '', 1),
(129, 66, 1, '', 1),
(130, 66, 2, '', 1),
(131, 67, 1, '', 1),
(132, 67, 2, '', 1),
(133, 68, 1, '', 1),
(134, 68, 2, '', 1),
(135, 69, 1, '', 1),
(136, 69, 2, '', 1),
(137, 70, 1, '', 1),
(138, 70, 2, '', 1),
(139, 73, 1, '', 1),
(140, 73, 2, '', 1),
(141, 74, 1, '', 1),
(142, 74, 2, '', 1),
(143, 75, 1, '', 1),
(144, 75, 2, '', 1),
(145, 76, 1, '', 1),
(146, 76, 2, '', 1),
(147, 77, 1, '', 1),
(148, 77, 2, '', 1),
(149, 78, 1, '', 1),
(150, 78, 2, '', 1),
(151, 79, 1, '', 1),
(152, 79, 2, '', 1),
(153, 83, 1, '', 1),
(154, 83, 2, '', 1),
(155, 84, 1, '', 1),
(156, 84, 2, '', 1),
(157, 87, 1, '', 1),
(158, 87, 2, '', 1),
(159, 88, 1, '', 1),
(160, 88, 2, '', 1),
(161, 89, 1, '', 1),
(162, 89, 2, '', 1),
(163, 90, 1, '', 1),
(164, 90, 2, '', 1),
(165, 91, 1, '', 1),
(166, 91, 2, '', 1),
(167, 92, 1, '', 1),
(168, 92, 2, '', 1),
(169, 93, 1, '', 1),
(170, 93, 2, '', 1),
(171, 94, 1, '', 1),
(172, 94, 2, '', 1),
(173, 95, 1, '', 1),
(174, 95, 2, '', 1),
(175, 97, 1, '', 1),
(176, 97, 2, '', 1),
(177, 98, 1, '', 1),
(178, 98, 2, '', 1),
(179, 101, 1, '', 1),
(180, 101, 2, '', 1),
(181, 102, 1, '', 1),
(182, 102, 2, '', 1),
(183, 103, 1, '', 1),
(184, 103, 2, '', 1),
(185, 104, 1, '', 1),
(186, 104, 2, '', 1),
(187, 105, 1, '', 1),
(188, 105, 2, '', 1),
(189, 106, 1, '', 1),
(190, 106, 2, '', 1),
(191, 107, 1, '', 1),
(192, 107, 2, '', 1),
(193, 108, 1, '', 1),
(194, 108, 2, '', 1),
(195, 109, 1, '', 1),
(196, 109, 2, '', 1),
(197, 110, 1, '', 1),
(198, 110, 2, '', 1),
(199, 111, 1, '', 1),
(200, 111, 2, '', 1),
(201, 112, 1, '', 1),
(202, 112, 2, '', 1),
(203, 113, 1, '', 1),
(204, 113, 2, '', 1),
(205, 114, 1, '', 1),
(206, 114, 2, '', 1),
(207, 115, 1, '', 1),
(208, 115, 2, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,2) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_option_value`
--

INSERT INTO `oc_product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(1, 1, 1, 1, 1, 336, 1, '0.0000', '', 0, '', '0.00', ''),
(2, 2, 1, 2, 2, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(3, 2, 1, 2, 3, 58, 1, '0.0000', '', 0, '', '0.00', ''),
(4, 2, 1, 2, 4, 69, 1, '0.0000', '', 0, '', '0.00', ''),
(5, 2, 1, 2, 5, 66, 1, '0.0000', '', 0, '', '0.00', ''),
(6, 2, 1, 2, 6, 63, 1, '0.0000', '', 0, '', '0.00', ''),
(7, 2, 1, 2, 7, 62, 1, '0.0000', '', 0, '', '0.00', ''),
(8, 3, 2, 1, 8, 221, 1, '0.0000', '', 0, '', '0.00', ''),
(9, 4, 2, 2, 9, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(10, 4, 2, 2, 10, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(11, 4, 2, 2, 11, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(12, 4, 2, 2, 12, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(13, 4, 2, 2, 13, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(14, 5, 3, 1, 1, 76, 1, '0.0000', '', 0, '', '0.00', ''),
(15, 6, 3, 2, 14, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(16, 6, 3, 2, 15, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(17, 6, 3, 2, 16, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(18, 6, 3, 2, 17, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(19, 6, 3, 2, 18, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(20, 6, 3, 2, 6, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(21, 6, 3, 2, 5, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(22, 7, 4, 1, 1, 74, 1, '0.0000', '', 0, '', '0.00', ''),
(23, 8, 4, 2, 3, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(24, 8, 4, 2, 5, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(25, 8, 4, 2, 2, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(26, 8, 4, 2, 4, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(27, 8, 4, 2, 15, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(28, 9, 5, 1, 1, 291, 1, '0.0000', '', 0, '', '0.00', ''),
(29, 10, 5, 2, 6, 51, 1, '0.0000', '', 0, '', '0.00', ''),
(30, 10, 5, 2, 15, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(31, 10, 5, 2, 3, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(32, 10, 5, 2, 5, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(33, 10, 5, 2, 2, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(34, 10, 5, 2, 7, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(35, 10, 5, 2, 4, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(36, 11, 6, 1, 1, 156, 1, '0.0000', '', 0, '', '0.00', ''),
(37, 12, 6, 2, 3, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(38, 12, 6, 2, 5, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(39, 12, 6, 2, 19, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(40, 12, 6, 2, 4, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(41, 12, 6, 2, 2, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(42, 12, 6, 2, 6, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(43, 13, 7, 1, 1, 174, 1, '0.0000', '', 0, '', '0.00', ''),
(44, 14, 7, 2, 3, 57, 1, '0.0000', '', 0, '', '0.00', ''),
(45, 14, 7, 2, 20, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(46, 14, 7, 2, 14, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(47, 14, 7, 2, 2, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(48, 15, 8, 1, 1, 156, 1, '0.0000', '', 0, '', '0.00', ''),
(49, 16, 8, 2, 3, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(50, 16, 8, 2, 14, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(51, 16, 8, 2, 21, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(52, 16, 8, 2, 22, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(53, 16, 8, 2, 17, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(54, 17, 9, 1, 1, 199, 1, '0.0000', '', 0, '', '0.00', ''),
(55, 18, 9, 2, 3, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(56, 18, 9, 2, 5, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(57, 18, 9, 2, 23, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(58, 18, 9, 2, 24, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(59, 18, 9, 2, 2, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(60, 18, 9, 2, 16, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(61, 19, 10, 1, 1, 334, 1, '0.0000', '', 0, '', '0.00', ''),
(62, 20, 10, 2, 2, 80, 1, '0.0000', '', 0, '', '0.00', ''),
(63, 20, 10, 2, 19, 85, 1, '0.0000', '', 0, '', '0.00', ''),
(64, 20, 10, 2, 5, 86, 1, '0.0000', '', 0, '', '0.00', ''),
(65, 20, 10, 2, 25, 83, 1, '0.0000', '', 0, '', '0.00', ''),
(66, 21, 12, 1, 1, 383, 1, '0.0000', '', 0, '', '0.00', ''),
(67, 22, 12, 2, 26, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(68, 22, 12, 2, 27, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(69, 22, 12, 2, 23, 168, 1, '0.0000', '', 0, '', '0.00', ''),
(70, 22, 12, 2, 3, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(71, 22, 12, 2, 16, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(72, 22, 12, 2, 2, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(73, 22, 12, 2, 5, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(74, 22, 12, 2, 28, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(75, 22, 12, 2, 29, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(76, 22, 12, 2, 30, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(77, 23, 13, 1, 1, 206, 1, '0.0000', '', 0, '', '0.00', ''),
(78, 24, 13, 2, 3, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(79, 24, 13, 2, 5, 27, 1, '0.0000', '', 0, '', '0.00', ''),
(80, 24, 13, 2, 23, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(81, 24, 13, 2, 16, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(82, 24, 13, 2, 2, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(83, 24, 13, 2, 31, 27, 1, '0.0000', '', 0, '', '0.00', ''),
(84, 25, 14, 1, 32, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(85, 25, 14, 1, 33, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(86, 25, 14, 1, 34, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(87, 26, 14, 2, 15, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(88, 26, 14, 2, 2, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(89, 26, 14, 2, 5, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(90, 26, 14, 2, 4, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(91, 26, 14, 2, 3, 6, 1, '0.0000', '', 0, '', '0.00', ''),
(92, 27, 15, 1, 1, 185, 1, '0.0000', '', 0, '', '0.00', ''),
(93, 28, 15, 2, 2, 53, 1, '0.0000', '', 0, '', '0.00', ''),
(94, 28, 15, 2, 5, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(95, 28, 15, 2, 23, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(96, 28, 15, 2, 35, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(97, 29, 16, 1, 1, 348, 1, '0.0000', '', 0, '', '0.00', ''),
(98, 30, 16, 2, 22, 58, 1, '0.0000', '', 0, '', '0.00', ''),
(99, 30, 16, 2, 15, 59, 1, '0.0000', '', 0, '', '0.00', ''),
(100, 30, 16, 2, 36, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(101, 30, 16, 2, 14, 59, 1, '0.0000', '', 0, '', '0.00', ''),
(102, 30, 16, 2, 3, 59, 1, '0.0000', '', 0, '', '0.00', ''),
(103, 30, 16, 2, 17, 53, 1, '0.0000', '', 0, '', '0.00', ''),
(104, 31, 17, 1, 32, 0, 1, '0.0000', '', 0, '', '0.00', ''),
(105, 31, 17, 1, 33, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(106, 31, 17, 1, 34, 0, 1, '0.0000', '', 0, '', '0.00', ''),
(107, 32, 17, 2, 5, 0, 1, '0.0000', '', 0, '', '0.00', ''),
(108, 32, 17, 2, 3, 0, 1, '0.0000', '', 0, '', '0.00', ''),
(109, 32, 17, 2, 37, 0, 1, '0.0000', '', 0, '', '0.00', ''),
(110, 32, 17, 2, 38, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(111, 33, 18, 1, 8, 279, 1, '0.0000', '', 0, '', '0.00', ''),
(112, 34, 18, 2, 23, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(113, 34, 18, 2, 3, 5, 1, '0.0000', '', 0, '', '0.00', ''),
(114, 34, 18, 2, 5, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(115, 34, 18, 2, 16, 59, 1, '0.0000', '', 0, '', '0.00', ''),
(116, 34, 18, 2, 2, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(117, 34, 18, 2, 31, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(118, 34, 18, 2, 39, 6, 1, '0.0000', '', 0, '', '0.00', ''),
(119, 34, 18, 2, 15, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(120, 34, 18, 2, 40, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(121, 34, 18, 2, 35, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(122, 35, 19, 1, 32, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(123, 35, 19, 1, 8, 80, 1, '0.0000', '', 0, '', '0.00', ''),
(124, 36, 19, 2, 15, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(125, 36, 19, 2, 28, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(126, 36, 19, 2, 16, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(127, 36, 19, 2, 26, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(128, 36, 19, 2, 40, 5, 1, '0.0000', '', 0, '', '0.00', ''),
(129, 36, 19, 2, 31, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(130, 36, 19, 2, 2, 5, 1, '0.0000', '', 0, '', '0.00', ''),
(131, 36, 19, 2, 23, 3, 1, '0.0000', '', 0, '', '0.00', ''),
(132, 36, 19, 2, 3, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(133, 36, 19, 2, 5, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(134, 37, 20, 1, 32, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(135, 37, 20, 1, 33, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(136, 37, 20, 1, 34, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(137, 38, 20, 2, 3, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(138, 38, 20, 2, 5, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(139, 38, 20, 2, 37, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(140, 38, 20, 2, 38, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(141, 39, 21, 1, 32, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(142, 39, 21, 1, 33, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(143, 39, 21, 1, 34, 214, 1, '0.0000', '', 0, '', '0.00', ''),
(144, 40, 21, 2, 39, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(145, 40, 21, 2, 5, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(146, 40, 21, 2, 41, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(147, 40, 21, 2, 42, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(148, 40, 21, 2, 43, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(149, 40, 21, 2, 15, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(150, 40, 21, 2, 44, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(151, 40, 21, 2, 3, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(152, 41, 22, 1, 32, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(153, 41, 22, 1, 33, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(154, 41, 22, 1, 34, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(155, 42, 22, 2, 3, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(156, 42, 22, 2, 5, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(157, 42, 22, 2, 38, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(158, 42, 22, 2, 37, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(159, 43, 23, 1, 8, 71, 1, '0.0000', '', 0, '', '0.00', ''),
(160, 44, 23, 2, 3, 13, 1, '0.0000', '', 0, '', '0.00', ''),
(161, 44, 23, 2, 5, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(162, 44, 23, 2, 39, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(163, 44, 23, 2, 2, 13, 1, '0.0000', '', 0, '', '0.00', ''),
(164, 44, 23, 2, 45, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(165, 45, 24, 1, 8, 199, 1, '0.0000', '', 0, '', '0.00', ''),
(166, 46, 24, 2, 3, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(167, 46, 24, 2, 5, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(168, 46, 24, 2, 37, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(169, 46, 24, 2, 2, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(170, 46, 24, 2, 15, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(171, 46, 24, 2, 6, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(172, 47, 25, 1, 32, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(173, 47, 25, 1, 33, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(174, 48, 25, 2, 3, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(175, 48, 25, 2, 5, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(176, 48, 25, 2, 4, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(177, 49, 26, 1, 8, 294, 1, '0.0000', '', 0, '', '0.00', ''),
(178, 50, 26, 2, 3, 67, 1, '0.0000', '', 0, '', '0.00', ''),
(179, 50, 26, 2, 5, 63, 1, '0.0000', '', 0, '', '0.00', ''),
(180, 50, 26, 2, 37, 63, 1, '0.0000', '', 0, '', '0.00', ''),
(181, 50, 26, 2, 46, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(182, 50, 26, 2, 2, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(183, 50, 26, 2, 6, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(184, 50, 26, 2, 21, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(185, 51, 27, 1, 33, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(186, 51, 27, 1, 34, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(187, 51, 27, 1, 47, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(188, 52, 27, 2, 3, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(189, 52, 27, 2, 5, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(190, 52, 27, 2, 37, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(191, 52, 27, 2, 2, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(192, 52, 27, 2, 15, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(193, 53, 28, 1, 32, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(194, 53, 28, 1, 33, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(195, 53, 28, 1, 34, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(196, 54, 28, 2, 3, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(197, 54, 28, 2, 5, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(198, 54, 28, 2, 37, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(199, 54, 28, 2, 2, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(200, 54, 28, 2, 15, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(201, 55, 29, 1, 32, 50, 1, '0.0000', '', 0, '', '0.00', ''),
(202, 55, 29, 1, 33, 51, 1, '0.0000', '', 0, '', '0.00', ''),
(203, 55, 29, 1, 34, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(204, 56, 29, 2, 3, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(205, 56, 29, 2, 5, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(206, 56, 29, 2, 37, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(207, 56, 29, 2, 2, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(208, 56, 29, 2, 15, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(209, 57, 30, 1, 33, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(210, 57, 30, 1, 34, 71, 1, '0.0000', '', 0, '', '0.00', ''),
(211, 57, 30, 1, 47, 63, 1, '0.0000', '', 0, '', '0.00', ''),
(212, 58, 30, 2, 3, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(213, 58, 30, 2, 5, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(214, 58, 30, 2, 37, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(215, 58, 30, 2, 2, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(216, 58, 30, 2, 15, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(217, 59, 31, 1, 33, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(218, 59, 31, 1, 34, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(219, 59, 31, 1, 32, 4, 1, '0.0000', '', 0, '', '0.00', ''),
(220, 59, 31, 1, 47, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(221, 60, 31, 2, 3, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(222, 60, 31, 2, 5, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(223, 60, 31, 2, 37, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(224, 60, 31, 2, 2, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(225, 60, 31, 2, 15, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(226, 61, 32, 1, 33, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(227, 61, 32, 1, 34, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(228, 61, 32, 1, 47, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(229, 62, 32, 2, 3, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(230, 62, 32, 2, 5, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(231, 62, 32, 2, 37, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(232, 62, 32, 2, 2, 4, 1, '0.0000', '', 0, '', '0.00', ''),
(233, 62, 32, 2, 15, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(234, 63, 33, 1, 33, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(235, 63, 33, 1, 34, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(236, 63, 33, 1, 47, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(237, 64, 33, 2, 3, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(238, 64, 33, 2, 5, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(239, 64, 33, 2, 37, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(240, 64, 33, 2, 2, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(241, 64, 33, 2, 15, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(242, 65, 34, 1, 8, 493, 1, '0.0000', '', 0, '', '0.00', ''),
(243, 66, 34, 2, 23, 288, 1, '0.0000', '', 0, '', '0.00', ''),
(244, 66, 34, 2, 48, 68, 1, '0.0000', '', 0, '', '0.00', ''),
(245, 66, 34, 2, 27, 68, 1, '0.0000', '', 0, '', '0.00', ''),
(246, 66, 34, 2, 35, 69, 1, '0.0000', '', 0, '', '0.00', ''),
(247, 67, 35, 1, 8, 234, 1, '0.0000', '', 0, '', '0.00', ''),
(248, 68, 35, 2, 49, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(249, 68, 35, 2, 15, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(250, 68, 35, 2, 50, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(251, 68, 35, 2, 5, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(252, 68, 35, 2, 7, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(253, 68, 35, 2, 51, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(254, 69, 36, 1, 8, 260, 1, '0.0000', '', 0, '', '0.00', ''),
(255, 70, 36, 2, 17, 81, 1, '0.0000', '', 0, '', '0.00', ''),
(256, 70, 36, 2, 52, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(257, 70, 36, 2, 2, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(258, 70, 36, 2, 48, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(259, 70, 36, 2, 36, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(260, 71, 37, 1, 8, 256, 1, '0.0000', '', 0, '', '0.00', ''),
(261, 72, 37, 2, 17, 66, 1, '0.0000', '', 0, '', '0.00', ''),
(262, 72, 37, 2, 52, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(263, 72, 37, 2, 2, 51, 1, '0.0000', '', 0, '', '0.00', ''),
(264, 72, 37, 2, 48, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(265, 72, 37, 2, 36, 50, 1, '0.0000', '', 0, '', '0.00', ''),
(266, 73, 38, 1, 8, 136, 1, '0.0000', '', 0, '', '0.00', ''),
(267, 74, 38, 2, 17, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(268, 74, 38, 2, 14, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(269, 74, 38, 2, 18, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(270, 74, 38, 2, 15, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(271, 74, 38, 2, 48, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(272, 74, 38, 2, 3, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(273, 75, 39, 1, 8, 409, 1, '0.0000', '', 0, '', '0.00', ''),
(274, 76, 39, 2, 3, 67, 1, '0.0000', '', 0, '', '0.00', ''),
(275, 76, 39, 2, 17, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(276, 76, 39, 2, 51, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(277, 76, 39, 2, 52, 55, 1, '0.0000', '', 0, '', '0.00', ''),
(278, 76, 39, 2, 18, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(279, 76, 39, 2, 4, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(280, 76, 39, 2, 20, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(281, 76, 39, 2, 26, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(282, 76, 39, 2, 41, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(283, 76, 39, 2, 40, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(284, 77, 40, 1, 8, 208, 1, '0.0000', '', 0, '', '0.00', ''),
(285, 78, 40, 2, 3, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(286, 78, 40, 2, 17, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(287, 78, 40, 2, 51, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(288, 78, 40, 2, 52, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(289, 78, 40, 2, 18, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(290, 78, 40, 2, 4, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(291, 78, 40, 2, 20, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(292, 78, 40, 2, 26, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(293, 78, 40, 2, 41, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(294, 78, 40, 2, 40, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(295, 79, 41, 1, 33, 76, 1, '0.0000', '', 0, '', '0.00', ''),
(296, 79, 41, 1, 34, 69, 1, '0.0000', '', 0, '', '0.00', ''),
(297, 79, 41, 1, 47, 62, 1, '0.0000', '', 0, '', '0.00', ''),
(298, 80, 41, 2, 3, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(299, 80, 41, 2, 5, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(300, 80, 41, 2, 37, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(301, 80, 41, 2, 2, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(302, 80, 41, 2, 15, 55, 1, '0.0000', '', 0, '', '0.00', ''),
(303, 81, 42, 1, 8, 160, 1, '0.0000', '', 0, '', '0.00', ''),
(304, 82, 42, 2, 3, 39, 1, '0.0000', '', 0, '', '0.00', ''),
(305, 82, 42, 2, 5, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(306, 82, 42, 2, 2, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(307, 82, 42, 2, 41, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(308, 83, 43, 1, 8, 95, 1, '0.0000', '', 0, '', '0.00', ''),
(309, 84, 43, 2, 17, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(310, 84, 43, 2, 53, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(311, 84, 43, 2, 36, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(312, 84, 43, 2, 2, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(313, 84, 43, 2, 52, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(314, 85, 44, 1, 8, 157, 1, '0.0000', '', 0, '', '0.00', ''),
(315, 86, 44, 2, 3, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(316, 86, 44, 2, 5, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(317, 86, 44, 2, 2, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(318, 86, 44, 2, 38, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(319, 86, 44, 2, 37, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(320, 87, 45, 1, 33, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(321, 87, 45, 1, 34, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(322, 87, 45, 1, 47, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(323, 88, 45, 2, 3, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(324, 88, 45, 2, 5, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(325, 88, 45, 2, 37, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(326, 88, 45, 2, 2, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(327, 88, 45, 2, 15, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(328, 89, 46, 1, 8, 143, 1, '0.0000', '', 0, '', '0.00', ''),
(329, 90, 46, 2, 3, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(330, 90, 46, 2, 5, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(331, 90, 46, 2, 37, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(332, 90, 46, 2, 2, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(333, 90, 46, 2, 15, 27, 1, '0.0000', '', 0, '', '0.00', ''),
(334, 90, 46, 2, 21, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(335, 91, 47, 1, 32, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(336, 91, 47, 1, 33, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(337, 92, 47, 2, 5, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(338, 92, 47, 2, 3, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(339, 92, 47, 2, 38, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(340, 93, 48, 1, 33, 65, 1, '0.0000', '', 0, '', '0.00', ''),
(341, 93, 48, 1, 34, 66, 1, '0.0000', '', 0, '', '0.00', ''),
(342, 93, 48, 1, 47, 62, 1, '0.0000', '', 0, '', '0.00', ''),
(343, 94, 48, 2, 3, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(344, 94, 48, 2, 5, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(345, 94, 48, 2, 37, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(346, 94, 48, 2, 2, 39, 1, '0.0000', '', 0, '', '0.00', ''),
(347, 94, 48, 2, 15, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(348, 95, 49, 1, 8, 106, 1, '0.0000', '', 0, '', '0.00', ''),
(349, 96, 49, 2, 3, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(350, 96, 49, 2, 5, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(351, 96, 49, 2, 2, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(352, 96, 49, 2, 52, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(353, 96, 49, 2, 39, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(354, 97, 50, 1, 8, 561, 1, '0.0000', '', 0, '', '0.00', ''),
(355, 98, 50, 2, 23, 318, 1, '0.0000', '', 0, '', '0.00', ''),
(356, 98, 50, 2, 48, 83, 1, '0.0000', '', 0, '', '0.00', ''),
(357, 98, 50, 2, 27, 82, 1, '0.0000', '', 0, '', '0.00', ''),
(358, 98, 50, 2, 35, 78, 1, '0.0000', '', 0, '', '0.00', ''),
(359, 99, 51, 1, 8, 70, 1, '0.0000', '', 0, '', '0.00', ''),
(360, 100, 51, 2, 3, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(361, 100, 51, 2, 5, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(362, 100, 51, 2, 37, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(363, 100, 51, 2, 2, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(364, 100, 51, 2, 15, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(365, 101, 52, 1, 33, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(366, 101, 52, 1, 34, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(367, 101, 52, 1, 47, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(368, 102, 52, 2, 3, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(369, 102, 52, 2, 5, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(370, 102, 52, 2, 37, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(371, 102, 52, 2, 2, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(372, 102, 52, 2, 15, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(373, 103, 53, 1, 8, 193, 1, '0.0000', '', 0, '', '0.00', ''),
(374, 104, 53, 2, 7, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(375, 104, 53, 2, 54, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(376, 104, 53, 2, 2, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(377, 104, 53, 2, 5, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(378, 105, 54, 1, 8, 501, 1, '0.0000', '', 0, '', '0.00', ''),
(379, 106, 54, 2, 23, 155, 1, '0.0000', '', 0, '', '0.00', ''),
(380, 106, 54, 2, 3, 73, 1, '0.0000', '', 0, '', '0.00', ''),
(381, 106, 54, 2, 5, 81, 1, '0.0000', '', 0, '', '0.00', ''),
(382, 106, 54, 2, 2, 27, 1, '0.0000', '', 0, '', '0.00', ''),
(383, 106, 54, 2, 16, 72, 1, '0.0000', '', 0, '', '0.00', ''),
(384, 106, 54, 2, 35, 86, 1, '0.0000', '', 0, '', '0.00', ''),
(385, 106, 54, 2, 26, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(386, 107, 55, 1, 8, 498, 1, '0.0000', '', 0, '', '0.00', ''),
(387, 108, 55, 2, 23, 144, 1, '0.0000', '', 0, '', '0.00', ''),
(388, 108, 55, 2, 3, 78, 1, '0.0000', '', 0, '', '0.00', ''),
(389, 108, 55, 2, 5, 82, 1, '0.0000', '', 0, '', '0.00', ''),
(390, 108, 55, 2, 2, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(391, 108, 55, 2, 16, 65, 1, '0.0000', '', 0, '', '0.00', ''),
(392, 108, 55, 2, 35, 84, 1, '0.0000', '', 0, '', '0.00', ''),
(393, 108, 55, 2, 26, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(394, 109, 56, 1, 8, 505, 1, '0.0000', '', 0, '', '0.00', ''),
(395, 110, 56, 2, 23, 155, 1, '0.0000', '', 0, '', '0.00', ''),
(396, 110, 56, 2, 3, 79, 1, '0.0000', '', 0, '', '0.00', ''),
(397, 110, 56, 2, 5, 77, 1, '0.0000', '', 0, '', '0.00', ''),
(398, 110, 56, 2, 2, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(399, 110, 56, 2, 16, 65, 1, '0.0000', '', 0, '', '0.00', ''),
(400, 110, 56, 2, 35, 77, 1, '0.0000', '', 0, '', '0.00', ''),
(401, 110, 56, 2, 26, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(402, 111, 57, 1, 8, 118, 1, '0.0000', '', 0, '', '0.00', ''),
(403, 112, 57, 2, 3, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(404, 112, 57, 2, 5, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(405, 112, 57, 2, 38, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(406, 112, 57, 2, 21, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(407, 112, 57, 2, 2, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(408, 112, 57, 2, 15, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(409, 113, 58, 1, 8, 230, 1, '0.0000', '', 0, '', '0.00', ''),
(410, 114, 58, 2, 7, 53, 1, '0.0000', '', 0, '', '0.00', ''),
(411, 114, 58, 2, 54, 57, 1, '0.0000', '', 0, '', '0.00', ''),
(412, 114, 58, 2, 5, 62, 1, '0.0000', '', 0, '', '0.00', ''),
(413, 114, 58, 2, 2, 58, 1, '0.0000', '', 0, '', '0.00', ''),
(414, 115, 59, 1, 33, 87, 1, '0.0000', '', 0, '', '0.00', ''),
(415, 115, 59, 1, 34, 97, 1, '0.0000', '', 0, '', '0.00', ''),
(416, 116, 59, 2, 2, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(417, 116, 59, 2, 51, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(418, 116, 59, 2, 22, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(419, 116, 59, 2, 26, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(420, 117, 60, 1, 8, 93, 1, '0.0000', '', 0, '', '0.00', ''),
(421, 118, 60, 2, 23, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(422, 118, 60, 2, 35, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(423, 118, 60, 2, 16, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(424, 118, 60, 2, 2, 27, 1, '0.0000', '', 0, '', '0.00', ''),
(425, 119, 61, 1, 32, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(426, 119, 61, 1, 33, 65, 1, '0.0000', '', 0, '', '0.00', ''),
(427, 119, 61, 1, 34, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(428, 120, 61, 2, 16, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(429, 120, 61, 2, 5, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(430, 120, 61, 2, 7, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(431, 120, 61, 2, 20, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(432, 121, 62, 1, 8, 78, 1, '0.0000', '', 0, '', '0.00', ''),
(433, 122, 62, 2, 3, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(434, 122, 62, 2, 5, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(435, 122, 62, 2, 54, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(436, 122, 62, 2, 15, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(437, 122, 62, 2, 37, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(438, 122, 62, 2, 2, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(439, 123, 63, 1, 32, 81, 1, '0.0000', '', 0, '', '0.00', ''),
(440, 123, 63, 1, 33, 74, 1, '0.0000', '', 0, '', '0.00', ''),
(441, 123, 63, 1, 34, 68, 1, '0.0000', '', 0, '', '0.00', ''),
(442, 124, 63, 2, 38, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(443, 124, 63, 2, 4, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(444, 124, 63, 2, 5, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(445, 124, 63, 2, 2, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(446, 124, 63, 2, 3, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(447, 125, 64, 1, 32, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(448, 125, 64, 1, 33, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(449, 125, 64, 1, 34, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(450, 126, 64, 2, 2, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(451, 126, 64, 2, 7, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(452, 126, 64, 2, 5, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(453, 126, 64, 2, 51, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(454, 127, 65, 1, 34, 188, 1, '0.0000', '', 0, '', '0.00', ''),
(455, 128, 65, 2, 18, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(456, 128, 65, 2, 23, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(457, 128, 65, 2, 17, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(458, 128, 65, 2, 3, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(459, 128, 65, 2, 21, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(460, 128, 65, 2, 14, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(461, 128, 65, 2, 19, 27, 1, '0.0000', '', 0, '', '0.00', ''),
(462, 128, 65, 2, 51, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(463, 129, 66, 1, 34, 167, 1, '0.0000', '', 0, '', '0.00', ''),
(464, 130, 66, 2, 5, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(465, 130, 66, 2, 18, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(466, 130, 66, 2, 14, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(467, 130, 66, 2, 16, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(468, 130, 66, 2, 15, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(469, 130, 66, 2, 6, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(470, 130, 66, 2, 17, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(471, 130, 66, 2, 20, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(472, 131, 67, 1, 34, 96, 1, '0.0000', '', 0, '', '0.00', ''),
(473, 132, 67, 2, 21, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(474, 132, 67, 2, 18, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(475, 132, 67, 2, 3, 13, 1, '0.0000', '', 0, '', '0.00', ''),
(476, 132, 67, 2, 2, 13, 1, '0.0000', '', 0, '', '0.00', ''),
(477, 132, 67, 2, 36, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(478, 132, 67, 2, 5, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(479, 132, 67, 2, 38, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(480, 132, 67, 2, 52, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(481, 133, 68, 1, 47, 119, 1, '0.0000', '', 0, '', '0.00', ''),
(482, 134, 68, 2, 18, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(483, 134, 68, 2, 14, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(484, 134, 68, 2, 3, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(485, 134, 68, 2, 2, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(486, 134, 68, 2, 17, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(487, 135, 69, 1, 55, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(488, 136, 69, 2, 18, 5, 1, '0.0000', '', 0, '', '0.00', ''),
(489, 136, 69, 2, 4, 3, 1, '0.0000', '', 0, '', '0.00', ''),
(490, 136, 69, 2, 5, 2, 1, '0.0000', '', 0, '', '0.00', ''),
(491, 136, 69, 2, 17, 2, 1, '0.0000', '', 0, '', '0.00', ''),
(492, 136, 69, 2, 15, 3, 1, '0.0000', '', 0, '', '0.00', ''),
(493, 136, 69, 2, 56, 4, 1, '0.0000', '', 0, '', '0.00', ''),
(494, 136, 69, 2, 2, 2, 1, '0.0000', '', 0, '', '0.00', ''),
(495, 137, 70, 1, 57, 74, 1, '0.0000', '', 0, '', '0.00', ''),
(496, 138, 70, 2, 48, 6, 1, '0.0000', '', 0, '', '0.00', ''),
(497, 138, 70, 2, 26, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(498, 138, 70, 2, 31, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(499, 138, 70, 2, 3, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(500, 138, 70, 2, 5, 4, 1, '0.0000', '', 0, '', '0.00', ''),
(501, 138, 70, 2, 40, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(502, 138, 70, 2, 2, 6, 1, '0.0000', '', 0, '', '0.00', ''),
(503, 138, 70, 2, 23, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(504, 138, 70, 2, 16, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(505, 139, 73, 1, 33, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(506, 139, 73, 1, 34, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(507, 139, 73, 1, 47, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(508, 140, 73, 2, 3, 2, 1, '0.0000', '', 0, '', '0.00', ''),
(509, 140, 73, 2, 5, 6, 1, '0.0000', '', 0, '', '0.00', ''),
(510, 140, 73, 2, 15, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(511, 140, 73, 2, 2, 7, 1, '0.0000', '', 0, '', '0.00', ''),
(512, 140, 73, 2, 37, 3, 1, '0.0000', '', 0, '', '0.00', ''),
(513, 141, 74, 1, 8, 265, 1, '0.0000', '', 0, '', '0.00', ''),
(514, 142, 74, 2, 5, 66, 1, '0.0000', '', 0, '', '0.00', ''),
(515, 142, 74, 2, 28, 67, 1, '0.0000', '', 0, '', '0.00', ''),
(516, 142, 74, 2, 26, 72, 1, '0.0000', '', 0, '', '0.00', ''),
(517, 142, 74, 2, 2, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(518, 143, 75, 1, 8, 116, 1, '0.0000', '', 0, '', '0.00', ''),
(519, 144, 75, 2, 3, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(520, 144, 75, 2, 5, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(521, 144, 75, 2, 37, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(522, 144, 75, 2, 15, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(523, 144, 75, 2, 2, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(524, 145, 76, 1, 8, 274, 1, '0.0000', '', 0, '', '0.00', ''),
(525, 146, 76, 2, 51, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(526, 146, 76, 2, 38, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(527, 146, 76, 2, 18, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(528, 146, 76, 2, 5, 39, 1, '0.0000', '', 0, '', '0.00', ''),
(529, 146, 76, 2, 7, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(530, 146, 76, 2, 49, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(531, 147, 77, 1, 8, 461, 1, '0.0000', '', 0, '', '0.00', ''),
(532, 148, 77, 2, 37, 68, 1, '0.0000', '', 0, '', '0.00', ''),
(533, 148, 77, 2, 3, 108, 1, '0.0000', '', 0, '', '0.00', ''),
(534, 148, 77, 2, 2, 67, 1, '0.0000', '', 0, '', '0.00', ''),
(535, 148, 77, 2, 5, 102, 1, '0.0000', '', 0, '', '0.00', ''),
(536, 148, 77, 2, 15, 67, 1, '0.0000', '', 0, '', '0.00', ''),
(537, 148, 77, 2, 21, 49, 1, '0.0000', '', 0, '', '0.00', ''),
(538, 149, 78, 1, 8, 256, 1, '0.0000', '', 0, '', '0.00', ''),
(539, 150, 78, 2, 3, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(540, 150, 78, 2, 5, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(541, 150, 78, 2, 2, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(542, 150, 78, 2, 37, 51, 1, '0.0000', '', 0, '', '0.00', ''),
(543, 150, 78, 2, 15, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(544, 151, 79, 1, 8, 248, 1, '0.0000', '', 0, '', '0.00', ''),
(545, 152, 79, 2, 23, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(546, 152, 79, 2, 17, 44, 1, '0.0000', '', 0, '', '0.00', ''),
(547, 152, 79, 2, 22, 25, 1, '0.0000', '', 0, '', '0.00', ''),
(548, 152, 79, 2, 14, 64, 1, '0.0000', '', 0, '', '0.00', ''),
(549, 152, 79, 2, 3, 64, 1, '0.0000', '', 0, '', '0.00', ''),
(550, 152, 79, 2, 16, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(551, 153, 83, 1, 8, 352, 1, '0.0000', '', 0, '', '0.00', ''),
(552, 154, 83, 2, 27, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(553, 154, 83, 2, 43, 65, 1, '0.0000', '', 0, '', '0.00', ''),
(554, 154, 83, 2, 3, 50, 1, '0.0000', '', 0, '', '0.00', ''),
(555, 154, 83, 2, 16, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(556, 154, 83, 2, 42, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(557, 154, 83, 2, 58, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(558, 154, 83, 2, 23, 68, 1, '0.0000', '', 0, '', '0.00', ''),
(559, 155, 84, 1, 8, 90, 1, '0.0000', '', 0, '', '0.00', ''),
(560, 156, 84, 2, 3, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(561, 156, 84, 2, 54, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(562, 156, 84, 2, 31, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(563, 156, 84, 2, 51, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(564, 156, 84, 2, 5, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(565, 156, 84, 2, 2, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(566, 156, 84, 2, 26, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(567, 156, 84, 2, 16, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(568, 156, 84, 2, 23, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(569, 157, 87, 1, 8, 165, 1, '0.0000', '', 0, '', '0.00', ''),
(570, 158, 87, 2, 11, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(571, 158, 87, 2, 59, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(572, 158, 87, 2, 13, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(573, 158, 87, 2, 10, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(574, 158, 87, 2, 12, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(575, 158, 87, 2, 9, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(576, 159, 88, 1, 8, 413, 1, '0.0000', '', 0, '', '0.00', ''),
(577, 160, 88, 2, 3, 61, 1, '0.0000', '', 0, '', '0.00', ''),
(578, 160, 88, 2, 5, 58, 1, '0.0000', '', 0, '', '0.00', ''),
(579, 160, 88, 2, 37, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(580, 160, 88, 2, 15, 61, 1, '0.0000', '', 0, '', '0.00', ''),
(581, 160, 88, 2, 2, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(582, 160, 88, 2, 6, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(583, 160, 88, 2, 21, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(584, 160, 88, 2, 42, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(585, 161, 89, 1, 8, 242, 1, '0.0000', '', 0, '', '0.00', ''),
(586, 162, 89, 2, 3, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(587, 162, 89, 2, 5, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(588, 162, 89, 2, 37, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(589, 162, 89, 2, 15, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(590, 162, 89, 2, 2, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(591, 162, 89, 2, 42, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(592, 163, 90, 1, 8, 102, 1, '0.0000', '', 0, '', '0.00', ''),
(593, 164, 90, 2, 23, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(594, 164, 90, 2, 2, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(595, 164, 90, 2, 16, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(596, 164, 90, 2, 3, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(597, 164, 90, 2, 5, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(598, 165, 91, 1, 8, 253, 1, '0.0000', '', 0, '', '0.00', ''),
(599, 166, 91, 2, 23, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(600, 166, 91, 2, 2, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(601, 166, 91, 2, 16, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(602, 166, 91, 2, 3, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(603, 166, 91, 2, 5, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(604, 166, 91, 2, 27, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(605, 166, 91, 2, 35, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(606, 166, 91, 2, 51, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(607, 166, 91, 2, 17, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(608, 166, 91, 2, 60, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(609, 167, 92, 1, 8, 321, 1, '0.0000', '', 0, '', '0.00', ''),
(610, 168, 92, 2, 23, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(611, 168, 92, 2, 2, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(612, 168, 92, 2, 16, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(613, 168, 92, 2, 3, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(614, 168, 92, 2, 5, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(615, 168, 92, 2, 27, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(616, 168, 92, 2, 35, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(617, 168, 92, 2, 51, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(618, 168, 92, 2, 17, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(619, 168, 92, 2, 60, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(620, 168, 92, 2, 24, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(621, 169, 93, 1, 8, 431, 1, '0.0000', '', 0, '', '0.00', ''),
(622, 170, 93, 2, 23, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(623, 170, 93, 2, 2, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(624, 170, 93, 2, 16, 53, 1, '0.0000', '', 0, '', '0.00', ''),
(625, 170, 93, 2, 3, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(626, 170, 93, 2, 5, 52, 1, '0.0000', '', 0, '', '0.00', ''),
(627, 170, 93, 2, 27, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(628, 170, 93, 2, 35, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(629, 170, 93, 2, 51, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(630, 170, 93, 2, 17, 40, 1, '0.0000', '', 0, '', '0.00', ''),
(631, 170, 93, 2, 60, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(632, 171, 94, 1, 8, 180, 1, '0.0000', '', 0, '', '0.00', ''),
(633, 172, 94, 2, 3, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(634, 172, 94, 2, 61, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(635, 172, 94, 2, 16, 28, 1, '0.0000', '', 0, '', '0.00', ''),
(636, 172, 94, 2, 2, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(637, 172, 94, 2, 23, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(638, 172, 94, 2, 27, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(639, 173, 95, 1, 8, 731, 1, '0.0000', '', 0, '', '0.00', ''),
(640, 174, 95, 2, 37, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(641, 174, 95, 2, 16, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(642, 174, 95, 2, 2, 70, 1, '0.0000', '', 0, '', '0.00', ''),
(643, 174, 95, 2, 24, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(644, 174, 95, 2, 43, 39, 1, '0.0000', '', 0, '', '0.00', ''),
(645, 174, 95, 2, 3, 58, 1, '0.0000', '', 0, '', '0.00', ''),
(646, 174, 95, 2, 20, 53, 1, '0.0000', '', 0, '', '0.00', ''),
(647, 174, 95, 2, 44, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(648, 174, 95, 2, 53, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(649, 174, 95, 2, 4, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(650, 174, 95, 2, 23, 54, 1, '0.0000', '', 0, '', '0.00', ''),
(651, 174, 95, 2, 62, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(652, 174, 95, 2, 35, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(653, 174, 95, 2, 63, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(654, 174, 95, 2, 27, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(655, 175, 97, 1, 55, 67, 1, '0.0000', '', 0, '', '0.00', ''),
(656, 175, 97, 1, 47, 121, 1, '0.0000', '', 0, '', '0.00', ''),
(657, 176, 97, 2, 16, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(658, 176, 97, 2, 7, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(659, 176, 97, 2, 43, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(660, 176, 97, 2, 5, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(661, 176, 97, 2, 51, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(662, 176, 97, 2, 20, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(663, 177, 98, 1, 1, 137, 1, '0.0000', '', 0, '', '0.00', ''),
(664, 178, 98, 2, 3, 66, 1, '0.0000', '', 0, '', '0.00', ''),
(665, 178, 98, 2, 14, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(666, 178, 98, 2, 2, 36, 1, '0.0000', '', 0, '', '0.00', ''),
(667, 179, 101, 1, 32, 57, 1, '0.0000', '', 0, '', '0.00', ''),
(668, 179, 101, 1, 33, 61, 1, '0.0000', '', 0, '', '0.00', ''),
(669, 179, 101, 1, 34, 60, 1, '0.0000', '', 0, '', '0.00', ''),
(670, 180, 101, 2, 5, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(671, 180, 101, 2, 64, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(672, 180, 101, 2, 51, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(673, 180, 101, 2, 2, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(674, 181, 102, 1, 8, 127, 1, '0.0000', '', 0, '', '0.00', ''),
(675, 182, 102, 2, 3, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(676, 182, 102, 2, 5, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(677, 182, 102, 2, 2, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(678, 182, 102, 2, 37, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(679, 182, 102, 2, 15, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(680, 182, 102, 2, 21, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(681, 183, 103, 1, 8, 122, 1, '0.0000', '', 0, '', '0.00', ''),
(682, 184, 103, 2, 3, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(683, 184, 103, 2, 5, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(684, 184, 103, 2, 2, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(685, 184, 103, 2, 37, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(686, 184, 103, 2, 15, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(687, 184, 103, 2, 21, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(688, 185, 104, 1, 8, 109, 1, '0.0000', '', 0, '', '0.00', ''),
(689, 186, 104, 2, 5, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(690, 186, 104, 2, 2, 14, 1, '0.0000', '', 0, '', '0.00', ''),
(691, 186, 104, 2, 16, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(692, 186, 104, 2, 23, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(693, 187, 105, 1, 8, 233, 1, '0.0000', '', 0, '', '0.00', ''),
(694, 188, 105, 2, 35, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(695, 188, 105, 2, 3, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(696, 188, 105, 2, 5, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(697, 188, 105, 2, 23, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(698, 188, 105, 2, 2, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(699, 188, 105, 2, 16, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(700, 188, 105, 2, 26, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(701, 188, 105, 2, 64, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(702, 189, 106, 1, 32, 61, 1, '0.0000', '', 0, '', '0.00', ''),
(703, 189, 106, 1, 33, 58, 1, '0.0000', '', 0, '', '0.00', ''),
(704, 189, 106, 1, 34, 50, 1, '0.0000', '', 0, '', '0.00', ''),
(705, 190, 106, 2, 5, 42, 1, '0.0000', '', 0, '', '0.00', ''),
(706, 190, 106, 2, 3, 38, 1, '0.0000', '', 0, '', '0.00', ''),
(707, 190, 106, 2, 20, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(708, 190, 106, 2, 37, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(709, 191, 107, 1, 8, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(710, 192, 107, 2, 48, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(711, 192, 107, 2, 5, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(712, 192, 107, 2, 39, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(713, 192, 107, 2, 37, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(714, 192, 107, 2, 23, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(715, 192, 107, 2, 40, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(716, 192, 107, 2, 36, 1, 1, '0.0000', '', 0, '', '0.00', ''),
(717, 192, 107, 2, 28, 8, 1, '0.0000', '', 0, '', '0.00', ''),
(718, 193, 108, 1, 8, 385, 1, '0.0000', '', 0, '', '0.00', ''),
(719, 194, 108, 2, 65, 37, 1, '0.0000', '', 0, '', '0.00', ''),
(720, 194, 108, 2, 66, 32, 1, '0.0000', '', 0, '', '0.00', ''),
(721, 194, 108, 2, 67, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(722, 194, 108, 2, 68, 35, 1, '0.0000', '', 0, '', '0.00', ''),
(723, 194, 108, 2, 69, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(724, 194, 108, 2, 70, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(725, 194, 108, 2, 71, 34, 1, '0.0000', '', 0, '', '0.00', ''),
(726, 194, 108, 2, 72, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(727, 194, 108, 2, 73, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(728, 194, 108, 2, 74, 29, 1, '0.0000', '', 0, '', '0.00', ''),
(729, 194, 108, 2, 75, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(730, 194, 108, 2, 76, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(731, 195, 109, 1, 47, 288, 1, '0.0000', '', 0, '', '0.00', ''),
(732, 196, 109, 2, 3, 55, 1, '0.0000', '', 0, '', '0.00', ''),
(733, 196, 109, 2, 5, 9, 1, '0.0000', '', 0, '', '0.00', ''),
(734, 196, 109, 2, 38, 65, 1, '0.0000', '', 0, '', '0.00', ''),
(735, 196, 109, 2, 21, 64, 1, '0.0000', '', 0, '', '0.00', ''),
(736, 196, 109, 2, 17, 64, 1, '0.0000', '', 0, '', '0.00', ''),
(737, 196, 109, 2, 16, 15, 1, '0.0000', '', 0, '', '0.00', ''),
(738, 196, 109, 2, 51, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(739, 197, 110, 1, 47, 210, 1, '0.0000', '', 0, '', '0.00', ''),
(740, 198, 110, 2, 3, 26, 1, '0.0000', '', 0, '', '0.00', ''),
(741, 198, 110, 2, 5, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(742, 198, 110, 2, 38, 43, 1, '0.0000', '', 0, '', '0.00', ''),
(743, 198, 110, 2, 21, 47, 1, '0.0000', '', 0, '', '0.00', ''),
(744, 198, 110, 2, 16, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(745, 198, 110, 2, 17, 41, 1, '0.0000', '', 0, '', '0.00', ''),
(746, 198, 110, 2, 51, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(747, 199, 111, 1, 8, 185, 1, '0.0000', '', 0, '', '0.00', ''),
(748, 200, 111, 2, 14, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(749, 200, 111, 2, 18, 33, 1, '0.0000', '', 0, '', '0.00', ''),
(750, 200, 111, 2, 5, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(751, 200, 111, 2, 6, 30, 1, '0.0000', '', 0, '', '0.00', ''),
(752, 200, 111, 2, 15, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(753, 200, 111, 2, 17, 31, 1, '0.0000', '', 0, '', '0.00', ''),
(754, 201, 112, 1, 8, 271, 1, '0.0000', '', 0, '', '0.00', ''),
(755, 202, 112, 2, 14, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(756, 202, 112, 2, 18, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(757, 202, 112, 2, 5, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(758, 202, 112, 2, 6, 46, 1, '0.0000', '', 0, '', '0.00', ''),
(759, 202, 112, 2, 15, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(760, 202, 112, 2, 17, 45, 1, '0.0000', '', 0, '', '0.00', ''),
(761, 203, 113, 1, 33, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(762, 203, 113, 1, 32, 24, 1, '0.0000', '', 0, '', '0.00', ''),
(763, 203, 113, 1, 34, 3, 1, '0.0000', '', 0, '', '0.00', ''),
(764, 204, 113, 2, 2, 12, 1, '0.0000', '', 0, '', '0.00', ''),
(765, 204, 113, 2, 7, 11, 1, '0.0000', '', 0, '', '0.00', ''),
(766, 204, 113, 2, 5, 10, 1, '0.0000', '', 0, '', '0.00', ''),
(767, 204, 113, 2, 51, 16, 1, '0.0000', '', 0, '', '0.00', ''),
(768, 205, 114, 1, 8, 180, 1, '0.0000', '', 0, '', '0.00', ''),
(769, 206, 114, 2, 23, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(770, 206, 114, 2, 3, 18, 1, '0.0000', '', 0, '', '0.00', ''),
(771, 206, 114, 2, 54, 22, 1, '0.0000', '', 0, '', '0.00', ''),
(772, 206, 114, 2, 31, 21, 1, '0.0000', '', 0, '', '0.00', ''),
(773, 206, 114, 2, 26, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(774, 206, 114, 2, 5, 23, 1, '0.0000', '', 0, '', '0.00', ''),
(775, 206, 114, 2, 16, 20, 1, '0.0000', '', 0, '', '0.00', ''),
(776, 206, 114, 2, 2, 19, 1, '0.0000', '', 0, '', '0.00', ''),
(777, 206, 114, 2, 51, 17, 1, '0.0000', '', 0, '', '0.00', ''),
(778, 207, 115, 1, 8, 196, 1, '0.0000', '', 0, '', '0.00', ''),
(779, 208, 115, 2, 29, 48, 1, '0.0000', '', 0, '', '0.00', ''),
(780, 208, 115, 2, 30, 50, 1, '0.0000', '', 0, '', '0.00', ''),
(781, 208, 115, 2, 35, 50, 1, '0.0000', '', 0, '', '0.00', ''),
(782, 208, 115, 2, 16, 48, 1, '0.0000', '', 0, '', '0.00', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_1c`
--

CREATE TABLE `oc_product_to_1c` (
  `product_id` int(11) NOT NULL,
  `1c_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_to_1c`
--

INSERT INTO `oc_product_to_1c` (`product_id`, `1c_id`) VALUES
(1, '1484f158-4675-11e5-8033-00ac06be3045'),
(2, '1484f15d-4675-11e5-8033-00ac06be3045'),
(3, '1484f15f-4675-11e5-8033-00ac06be3045'),
(4, '1484f167-4675-11e5-8033-00ac06be3045'),
(5, '1484f16d-4675-11e5-8033-00ac06be3045'),
(6, '1484f174-4675-11e5-8033-00ac06be3045'),
(7, '1484f17b-4675-11e5-8033-00ac06be3045'),
(8, '1484f180-4675-11e5-8033-00ac06be3045'),
(9, '1484f186-4675-11e5-8033-00ac06be3045'),
(10, '34c734d0-471c-11e5-8033-00ac06be3045'),
(11, 'c593d9bb-48a6-11e5-8033-00ac06be3045'),
(12, 'cbac2b1a-4ae7-11e5-8033-00ac06be3045'),
(13, 'cbac2b25-4ae7-11e5-8033-00ac06be3045'),
(14, 'cbac2b2c-4ae7-11e5-8033-00ac06be3045'),
(15, 'cbac2b36-4ae7-11e5-8033-00ac06be3045'),
(16, 'cbac2b3b-4ae7-11e5-8033-00ac06be3045'),
(17, 'f7d2d3b6-52bf-11e5-8033-00ac06be3045'),
(18, 'f7d2d3c3-52bf-11e5-8033-00ac06be3045'),
(19, 'f7d2d3cc-52bf-11e5-8033-00ac06be3045'),
(20, 'f7d2d3dc-52bf-11e5-8033-00ac06be3045'),
(21, 'f7d2d3e9-52bf-11e5-8033-00ac06be3045'),
(22, 'fad705d1-52dc-11e5-8033-00ac06be3045'),
(23, 'fad705de-52dc-11e5-8033-00ac06be3045'),
(24, 'fad705e5-52dc-11e5-8033-00ac06be3045'),
(25, 'fad705ec-52dc-11e5-8033-00ac06be3045'),
(26, 'fad705f3-52dc-11e5-8033-00ac06be3045'),
(27, 'fad705fb-52dc-11e5-8033-00ac06be3045'),
(28, 'fad7060b-52dc-11e5-8033-00ac06be3045'),
(29, '94c00048-52e7-11e5-8033-00ac06be3045'),
(30, '94c00058-52e7-11e5-8033-00ac06be3045'),
(31, '94c00068-52e7-11e5-8033-00ac06be3045'),
(32, '94c00078-52e7-11e5-8033-00ac06be3045'),
(33, 'b051ced5-52f0-11e5-8033-00ac06be3045'),
(34, 'b051cee5-52f0-11e5-8033-00ac06be3045'),
(35, '9ac78bf3-530b-11e5-8033-00ac06be3045'),
(36, '9ac78bfa-530b-11e5-8033-00ac06be3045'),
(37, '9ac78c00-530b-11e5-8033-00ac06be3045'),
(38, '9ac78c06-530b-11e5-8033-00ac06be3045'),
(39, '9a662f24-5388-11e5-8033-00ac06be3045'),
(40, '9a662f2f-5388-11e5-8033-00ac06be3045'),
(41, '9a662f3a-5388-11e5-8033-00ac06be3045'),
(42, '9a662f4a-5388-11e5-8033-00ac06be3045'),
(43, '9a662f50-5388-11e5-8033-00ac06be3045'),
(44, '9a662f56-5388-11e5-8033-00ac06be3045'),
(45, '9a662f5c-5388-11e5-8033-00ac06be3045'),
(46, '6b849be1-5390-11e5-8033-00ac06be3045'),
(47, '6b849be8-5390-11e5-8033-00ac06be3045'),
(48, '6b849bef-5390-11e5-8033-00ac06be3045'),
(49, '6b849bff-5390-11e5-8033-00ac06be3045'),
(50, '6b849c05-5390-11e5-8033-00ac06be3045'),
(51, '6b849c0a-5390-11e5-8033-00ac06be3045'),
(52, '6b849c10-5390-11e5-8033-00ac06be3045'),
(53, 'b153d6a2-5398-11e5-8033-00ac06be3045'),
(54, 'b153d6a7-5398-11e5-8033-00ac06be3045'),
(55, 'b153d6af-5398-11e5-8033-00ac06be3045'),
(56, 'b153d6b7-5398-11e5-8033-00ac06be3045'),
(57, '760aeee0-53b9-11e5-8033-00ac06be3045'),
(58, '760aeee7-53b9-11e5-8033-00ac06be3045'),
(59, '760aeeec-53b9-11e5-8033-00ac06be3045'),
(60, '760aeef5-53b9-11e5-8033-00ac06be3045'),
(61, '760aeefa-53b9-11e5-8033-00ac06be3045'),
(62, '760aef07-53b9-11e5-8033-00ac06be3045'),
(63, '760aef0c-53b9-11e5-8033-00ac06be3045'),
(64, '760aef1c-53b9-11e5-8033-00ac06be3045'),
(65, 'a64672dc-3c16-11e5-8a4f-00ac06be3045'),
(66, '476442c8-3f6a-11e5-8a4f-00ac06be3045'),
(67, '9b2fab23-3fe8-11e5-8a4f-00ac06be3045'),
(68, '7c830c47-4010-11e5-8a4f-00ac06be3045'),
(69, '7c830c4d-4010-11e5-8a4f-00ac06be3045'),
(70, '7c830c56-4010-11e5-8a4f-00ac06be3045'),
(71, '2aa10086-3db7-11e6-8c3d-00ac06be3045'),
(72, '979ad400-4435-11e6-8c3d-00ac06be3045'),
(73, '64053bac-5c7c-11e5-9708-00ac06be3045'),
(74, '64053bbc-5c7c-11e5-9708-00ac06be3045'),
(75, 'd90ff3b2-5d01-11e5-9708-00ac06be3045'),
(76, '1aa5692d-5de2-11e5-9708-00ac06be3045'),
(77, '32118fc7-5f83-11e5-9708-00ac06be3045'),
(78, 'd3ba0c84-6114-11e5-9708-00ac06be3045'),
(79, 'd3ba0c8a-6114-11e5-9708-00ac06be3045'),
(80, '77ec71f1-fa68-11e5-9761-00ac06be3045'),
(81, '07962bd5-bc2c-11e5-97b8-00ac06be3045'),
(82, 'a19b17b3-8c77-11e5-9c35-00ac06be3045'),
(83, '5dcecf5b-8cf5-11e5-9c35-00ac06be3045'),
(84, 'fd0f9d4d-8d14-11e5-9c35-00ac06be3045'),
(85, '9c889c98-3c5d-11e6-9cdb-00ac06be3045'),
(86, '36134c7b-8a1b-11e6-aa30-00ac06be3045'),
(87, '8fa576b1-7411-11e5-aacb-00ac06be3045'),
(88, 'fd154ae0-7ca4-11e5-aacb-00ac06be3045'),
(89, 'fd154ae9-7ca4-11e5-aacb-00ac06be3045'),
(90, '9d93e128-7e57-11e5-aacb-00ac06be3045'),
(91, '9d93e133-7e57-11e5-aacb-00ac06be3045'),
(92, 'bdcf7b4d-7ed1-11e5-aacb-00ac06be3045'),
(93, 'bdcf7b58-7ed1-11e5-aacb-00ac06be3045'),
(94, 'b39873f6-8203-11e5-aacb-00ac06be3045'),
(95, 'bf11ae72-8475-11e5-aacb-00ac06be3045'),
(96, 'bf11ae74-8475-11e5-aacb-00ac06be3045'),
(97, 'bf11ae75-8475-11e5-aacb-00ac06be3045'),
(98, 'c5dd127d-87b9-11e5-aacb-00ac06be3045'),
(99, '9d3d7a2e-b913-11e5-b304-00ac06be3045'),
(100, '9d3d7a35-b913-11e5-b304-00ac06be3045'),
(101, 'b5fd305d-6beb-11e5-b374-00ac06be3045'),
(102, 'b5fd306a-6beb-11e5-b374-00ac06be3045'),
(103, 'b5fd3071-6beb-11e5-b374-00ac06be3045'),
(104, '2b72975d-70b2-11e5-b374-00ac06be3045'),
(105, '2b729762-70b2-11e5-b374-00ac06be3045'),
(106, '482241d7-6298-11e5-b48a-00ac06be3045'),
(107, 'da4ae9e7-3480-11e6-b6f5-00ac06be3045'),
(108, 'ff64052c-681a-11e5-b7e9-00ac06be3045'),
(109, '71150848-40f5-11e5-bd34-00ac06be3045'),
(110, '7115084f-40f5-11e5-bd34-00ac06be3045'),
(111, 'fa1b4de8-7316-11e5-bd5c-00ac06be3045'),
(112, 'fa1b4def-7316-11e5-bd5c-00ac06be3045'),
(113, 'cd4ce157-675b-11e5-be92-00ac06be3045'),
(114, '5ff07d4f-680b-11e5-be92-00ac06be3045'),
(115, '38006f81-6818-11e5-be92-00ac06be3045');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `main_category` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`, `main_category`) VALUES
(1, 59, 1),
(2, 59, 1),
(3, 59, 1),
(4, 59, 1),
(5, 59, 1),
(6, 59, 1),
(7, 59, 1),
(8, 59, 1),
(9, 59, 1),
(10, 59, 1),
(11, 67, 1),
(12, 59, 1),
(13, 59, 1),
(14, 59, 1),
(15, 59, 1),
(16, 59, 1),
(17, 59, 1),
(18, 59, 1),
(19, 59, 1),
(20, 59, 1),
(21, 59, 1),
(22, 59, 1),
(23, 59, 1),
(24, 59, 1),
(25, 59, 1),
(26, 59, 1),
(27, 59, 1),
(28, 59, 1),
(29, 59, 1),
(30, 59, 1),
(31, 59, 1),
(32, 59, 1),
(33, 59, 1),
(34, 59, 1),
(35, 59, 1),
(36, 59, 1),
(37, 59, 1),
(38, 59, 1),
(39, 59, 1),
(40, 59, 1),
(41, 59, 1),
(42, 59, 1),
(43, 59, 1),
(44, 59, 1),
(45, 59, 1),
(46, 59, 1),
(47, 59, 1),
(48, 59, 1),
(49, 59, 1),
(50, 59, 1),
(51, 59, 1),
(52, 59, 1),
(53, 59, 1),
(54, 59, 1),
(55, 59, 1),
(56, 59, 1),
(57, 59, 1),
(58, 59, 1),
(59, 59, 1),
(60, 59, 1),
(61, 59, 1),
(62, 59, 1),
(63, 59, 1),
(64, 59, 1),
(65, 59, 1),
(66, 59, 1),
(67, 59, 1),
(68, 59, 1),
(69, 59, 1),
(70, 59, 1),
(71, 67, 1),
(72, 67, 1),
(73, 59, 1),
(74, 59, 1),
(75, 59, 1),
(76, 59, 1),
(77, 59, 1),
(78, 59, 1),
(79, 59, 1),
(80, 67, 1),
(81, 67, 1),
(82, 67, 1),
(83, 59, 1),
(84, 59, 1),
(85, 67, 1),
(86, 67, 1),
(87, 59, 1),
(88, 59, 1),
(89, 59, 1),
(90, 59, 1),
(91, 59, 1),
(92, 59, 1),
(93, 59, 1),
(94, 59, 1),
(95, 59, 1),
(96, 67, 1),
(97, 59, 1),
(98, 59, 1),
(99, 67, 1),
(100, 67, 1),
(101, 59, 1),
(102, 59, 1),
(103, 59, 1),
(104, 59, 1),
(105, 59, 1),
(106, 59, 1),
(107, 59, 1),
(108, 59, 1),
(109, 59, 1),
(110, 59, 1),
(111, 59, 1),
(112, 59, 1),
(113, 59, 1),
(114, 59, 1),
(115, 59, 1),
(1, 67, 0),
(2, 67, 0),
(3, 67, 0),
(4, 67, 0),
(5, 67, 0),
(6, 67, 0),
(7, 67, 0),
(8, 67, 0),
(9, 67, 0),
(10, 67, 0),
(11, 59, 0),
(12, 67, 0),
(13, 67, 0),
(14, 67, 0),
(15, 67, 0),
(16, 67, 0),
(17, 67, 0),
(18, 67, 0),
(19, 67, 0),
(20, 67, 0),
(21, 67, 0),
(22, 67, 0),
(23, 67, 0),
(24, 67, 0),
(25, 67, 0),
(26, 67, 0),
(27, 67, 0),
(28, 67, 0),
(29, 67, 0),
(30, 67, 0),
(31, 67, 0),
(32, 67, 0),
(33, 67, 0),
(34, 67, 0),
(35, 67, 0),
(36, 67, 0),
(37, 67, 0),
(38, 67, 0),
(39, 67, 0),
(40, 67, 0),
(41, 67, 0),
(42, 67, 0),
(43, 67, 0),
(44, 67, 0),
(45, 67, 0),
(46, 67, 0),
(47, 67, 0),
(48, 67, 0),
(49, 67, 0),
(50, 67, 0),
(51, 67, 0),
(52, 67, 0),
(53, 67, 0),
(54, 67, 0),
(55, 67, 0),
(56, 67, 0),
(57, 67, 0),
(58, 67, 0),
(59, 67, 0),
(60, 67, 0),
(61, 67, 0),
(62, 67, 0),
(63, 67, 0),
(64, 67, 0),
(65, 67, 0),
(66, 67, 0),
(67, 67, 0),
(68, 67, 0),
(69, 67, 0),
(70, 67, 0),
(71, 59, 0),
(72, 59, 0),
(73, 67, 0),
(74, 67, 0),
(75, 67, 0),
(76, 67, 0),
(77, 67, 0),
(78, 67, 0),
(79, 67, 0),
(80, 59, 0),
(81, 59, 0),
(82, 59, 0),
(83, 67, 0),
(84, 67, 0),
(85, 59, 0),
(86, 59, 0),
(87, 67, 0),
(88, 67, 0),
(89, 67, 0),
(90, 67, 0),
(91, 67, 0),
(92, 67, 0),
(93, 67, 0),
(94, 67, 0),
(95, 67, 0),
(96, 59, 0),
(97, 67, 0),
(98, 67, 0),
(99, 59, 0),
(100, 59, 0),
(101, 67, 0),
(102, 67, 0),
(103, 67, 0),
(104, 67, 0),
(105, 67, 0),
(106, 67, 0),
(107, 67, 0),
(108, 67, 0),
(109, 67, 0),
(110, 67, 0),
(111, 67, 0),
(112, 67, 0),
(113, 67, 0),
(114, 67, 0),
(115, 67, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0),
(98, 0),
(99, 0),
(100, 0),
(101, 0),
(102, 0),
(103, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 0),
(108, 0),
(109, 0),
(110, 0),
(111, 0),
(112, 0),
(113, 0),
(114, 0),
(115, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions`
--

CREATE TABLE `oc_relatedoptions` (
  `relatedoptions_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `location` varchar(128) NOT NULL,
  `defaultselect` tinyint(11) NOT NULL,
  `defaultselectpriority` int(11) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `stock_status_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_relatedoptions`
--

INSERT INTO `oc_relatedoptions` (`relatedoptions_id`, `product_id`, `quantity`, `price`, `model`, `sku`, `upc`, `location`, `defaultselect`, `defaultselectpriority`, `weight`, `weight_prefix`, `price_prefix`, `ean`, `stock_status_id`) VALUES
(1, 1, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(2, 1, 58, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(3, 1, 69, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(4, 1, 66, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(5, 1, 63, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(6, 1, 62, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(7, 2, 42, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(8, 2, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(9, 2, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(10, 2, 46, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(11, 2, 42, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(12, 3, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(13, 3, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(14, 3, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(15, 3, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(16, 3, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(17, 3, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(18, 3, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(19, 4, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(20, 4, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(21, 4, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(22, 4, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(23, 4, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(24, 5, 51, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(25, 5, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(26, 5, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(27, 5, 47, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(28, 5, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(29, 5, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(30, 5, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(31, 6, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(32, 6, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(33, 6, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(34, 6, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(35, 6, 29, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(36, 6, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(37, 7, 57, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(38, 7, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(39, 7, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(40, 7, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(41, 8, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(42, 8, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(43, 8, 34, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(44, 8, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(45, 8, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(46, 9, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(47, 9, 29, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(48, 9, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(49, 9, 28, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(50, 9, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(51, 9, 44, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(52, 10, 80, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(53, 10, 85, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(54, 10, 86, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(55, 10, 83, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(56, 12, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(57, 12, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(58, 12, 168, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(59, 12, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(60, 12, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(61, 12, 28, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(62, 12, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(63, 12, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(64, 12, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(65, 12, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(66, 13, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(67, 13, 27, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(68, 13, 60, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(69, 13, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(70, 13, 34, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(71, 13, 27, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(72, 14, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(73, 14, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(74, 14, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(75, 14, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(76, 14, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(77, 14, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(78, 14, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(79, 14, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(80, 14, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(81, 14, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(82, 14, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(83, 14, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(84, 14, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(85, 14, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(86, 14, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(87, 15, 53, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(88, 15, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(89, 15, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(90, 15, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(91, 16, 58, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(92, 16, 59, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(93, 16, 60, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(94, 16, 59, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(95, 16, 59, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(96, 16, 53, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(97, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(98, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(99, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(100, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(101, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(102, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(103, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(104, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(105, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(106, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(107, 17, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(108, 17, 0, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(109, 18, 44, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(110, 18, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(111, 18, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(112, 18, 59, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(113, 18, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(114, 18, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(115, 18, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(116, 18, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(117, 18, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(118, 18, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(119, 19, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(120, 19, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(121, 19, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(122, 19, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(123, 19, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(124, 19, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(125, 19, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(126, 19, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(127, 19, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(128, 19, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(129, 20, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(130, 20, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(131, 20, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(132, 20, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(133, 20, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(134, 20, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(135, 20, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(136, 20, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(137, 20, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(138, 20, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(139, 20, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(140, 20, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(141, 21, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(142, 21, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(143, 21, 35, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(144, 21, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(145, 21, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(146, 21, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(147, 21, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(148, 21, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(149, 21, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(150, 21, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(151, 21, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(152, 21, 35, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(153, 21, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(154, 21, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(155, 21, 36, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(156, 21, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(157, 22, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(158, 22, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(159, 22, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(160, 22, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(161, 22, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(162, 22, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(163, 22, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(164, 22, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(165, 22, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(166, 22, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(167, 22, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(168, 22, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(169, 23, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(170, 23, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(171, 23, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(172, 23, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(173, 23, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(174, 24, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(175, 24, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(176, 24, 36, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(177, 24, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(178, 24, 34, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(179, 24, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(180, 25, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(181, 25, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(182, 25, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(183, 25, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(184, 25, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(185, 25, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(186, 26, 67, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(187, 26, 63, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(188, 26, 63, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(189, 26, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(190, 26, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(191, 26, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(192, 26, 37, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(193, 27, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(194, 27, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(195, 27, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(196, 27, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(197, 27, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(198, 27, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(199, 27, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(200, 27, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(201, 27, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(202, 27, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(203, 27, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(204, 27, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(205, 27, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(206, 27, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(207, 27, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(208, 28, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(209, 28, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(210, 28, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(211, 28, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(212, 28, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(213, 28, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(214, 28, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(215, 28, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(216, 28, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(217, 28, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(218, 28, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(219, 28, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(220, 28, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(221, 28, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(222, 29, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(223, 29, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(224, 29, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(225, 29, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(226, 29, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(227, 29, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(228, 29, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(229, 29, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(230, 29, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(231, 29, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(232, 29, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(233, 29, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(234, 29, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(235, 29, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(236, 29, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(237, 30, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(238, 30, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(239, 30, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(240, 30, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(241, 30, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(242, 30, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(243, 30, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(244, 30, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(245, 30, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(246, 30, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(247, 30, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(248, 30, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(249, 30, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(250, 30, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(251, 30, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(252, 31, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(253, 31, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(254, 31, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(255, 31, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(256, 31, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(257, 31, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(258, 31, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(259, 31, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(260, 31, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(261, 31, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(262, 31, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(263, 31, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(264, 31, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(265, 31, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(266, 31, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(267, 31, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(268, 31, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(269, 32, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(270, 32, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(271, 32, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(272, 32, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(273, 32, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(274, 32, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(275, 32, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(276, 32, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(277, 32, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(278, 32, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(279, 32, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(280, 32, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(281, 32, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(282, 32, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(283, 33, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(284, 33, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(285, 33, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(286, 33, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(287, 33, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(288, 33, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(289, 33, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(290, 33, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(291, 33, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(292, 33, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(293, 33, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(294, 33, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(295, 33, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(296, 33, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(297, 33, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(298, 34, 288, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(299, 34, 68, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(300, 34, 68, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(301, 34, 69, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(302, 35, 37, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(303, 35, 44, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(304, 35, 42, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(305, 35, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(306, 35, 38, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(307, 35, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(308, 36, 81, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(309, 36, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(310, 36, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(311, 36, 46, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(312, 36, 49, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(313, 37, 66, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(314, 37, 47, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(315, 37, 51, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(316, 37, 42, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(317, 37, 50, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(318, 38, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(319, 38, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(320, 38, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(321, 38, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(322, 38, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(323, 38, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(324, 39, 67, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(325, 39, 46, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(326, 39, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(327, 39, 55, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(328, 39, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(329, 39, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(330, 39, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(331, 39, 42, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(332, 39, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(333, 39, 46, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(334, 40, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(335, 40, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(336, 40, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(337, 40, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(338, 40, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(339, 40, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(340, 40, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(341, 40, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(342, 40, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(343, 40, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(344, 41, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(345, 41, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(346, 41, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(347, 41, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(348, 41, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(349, 41, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(350, 41, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(351, 41, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(352, 41, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(353, 41, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(354, 41, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(355, 41, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(356, 41, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(357, 41, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(358, 41, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(359, 42, 39, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(360, 42, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(361, 42, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(362, 42, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(363, 43, 28, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(364, 43, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(365, 43, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(366, 43, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(367, 43, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(368, 44, 36, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(369, 44, 29, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(370, 44, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(371, 44, 34, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(372, 44, 28, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(373, 45, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(374, 45, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(375, 45, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(376, 45, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(377, 45, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(378, 45, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(379, 45, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(380, 45, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(381, 45, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(382, 45, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(383, 45, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(384, 45, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(385, 45, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(386, 45, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(387, 45, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(388, 46, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(389, 46, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(390, 46, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(391, 46, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(392, 46, 27, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(393, 46, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(394, 47, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(395, 47, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(396, 47, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(397, 47, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(398, 47, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(399, 48, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(400, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(401, 48, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(402, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(403, 48, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(404, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(405, 48, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(406, 48, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(407, 48, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(408, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(409, 48, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(410, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(411, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(412, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(413, 48, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(414, 49, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(415, 49, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(416, 49, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(417, 49, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(418, 49, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(419, 50, 318, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(420, 50, 83, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(421, 50, 82, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(422, 50, 78, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(423, 51, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(424, 51, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(425, 51, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(426, 51, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(427, 51, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(428, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(429, 52, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(430, 52, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(431, 52, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(432, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(433, 52, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(434, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(435, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(436, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(437, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(438, 52, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(439, 52, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(440, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(441, 52, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(442, 52, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(443, 53, 49, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(444, 53, 47, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(445, 53, 49, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(446, 53, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(447, 54, 155, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(448, 54, 73, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(449, 54, 81, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(450, 54, 27, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(451, 54, 72, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(452, 54, 86, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(453, 54, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(454, 55, 144, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(455, 55, 78, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(456, 55, 82, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(457, 55, 37, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(458, 55, 65, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(459, 55, 84, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(460, 55, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(461, 56, 155, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(462, 56, 79, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(463, 56, 77, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(464, 56, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(465, 56, 65, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(466, 56, 77, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(467, 56, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(468, 57, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(469, 57, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(470, 57, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(471, 57, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(472, 57, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(473, 57, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(474, 58, 53, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(475, 58, 57, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(476, 58, 62, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(477, 58, 58, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(478, 59, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(479, 59, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(480, 59, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(481, 59, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(482, 59, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(483, 59, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(484, 59, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(485, 59, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(486, 60, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(487, 60, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(488, 60, 37, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(489, 60, 27, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(490, 61, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(491, 61, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(492, 61, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(493, 61, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(494, 61, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(495, 61, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(496, 61, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(497, 61, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(498, 61, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(499, 61, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(500, 61, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(501, 61, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(502, 62, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(503, 62, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(504, 62, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(505, 62, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(506, 62, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(507, 62, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(508, 63, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(509, 63, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(510, 63, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(511, 63, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(512, 63, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(513, 63, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(514, 63, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(515, 63, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(516, 63, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(517, 63, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(518, 63, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(519, 63, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(520, 63, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(521, 63, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(522, 63, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(523, 64, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(524, 64, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(525, 64, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(526, 64, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(527, 64, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(528, 64, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(529, 64, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(530, 64, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(531, 64, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(532, 64, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(533, 64, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(534, 64, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(535, 65, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(536, 65, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(537, 65, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(538, 65, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(539, 65, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(540, 65, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(541, 65, 27, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(542, 65, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(543, 66, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(544, 66, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(545, 66, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(546, 66, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(547, 66, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(548, 66, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(549, 66, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(550, 66, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(551, 67, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(552, 67, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(553, 67, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(554, 67, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(555, 67, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(556, 67, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(557, 67, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(558, 67, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(559, 68, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(560, 68, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(561, 68, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(562, 68, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(563, 68, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(564, 69, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(565, 69, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(566, 69, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(567, 69, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(568, 69, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(569, 69, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(570, 69, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(571, 70, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(572, 70, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(573, 70, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(574, 70, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(575, 70, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(576, 70, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(577, 70, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(578, 70, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(579, 70, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(580, 73, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(581, 73, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(582, 73, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(583, 73, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(584, 73, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(585, 73, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(586, 73, 4, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(587, 73, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(588, 73, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(589, 73, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(590, 73, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(591, 73, 2, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(592, 73, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(593, 74, 66, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(594, 74, 67, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(595, 74, 72, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(596, 74, 60, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(597, 75, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(598, 75, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(599, 75, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(600, 75, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(601, 75, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(602, 76, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(603, 76, 47, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(604, 76, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(605, 76, 39, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(606, 76, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(607, 76, 49, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(608, 77, 68, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(609, 77, 108, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(610, 77, 67, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(611, 77, 102, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(612, 77, 67, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(613, 77, 49, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(614, 78, 47, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(615, 78, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(616, 78, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(617, 78, 51, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(618, 78, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(619, 79, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(620, 79, 44, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(621, 79, 25, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(622, 79, 64, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(623, 79, 64, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(624, 79, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(625, 83, 60, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(626, 83, 65, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(627, 83, 50, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(628, 83, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(629, 83, 34, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(630, 83, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(631, 83, 68, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(632, 84, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(633, 84, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(634, 84, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(635, 84, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(636, 84, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(637, 84, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(638, 84, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(639, 84, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(640, 84, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(641, 87, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(642, 87, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(643, 87, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(644, 87, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(645, 87, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(646, 87, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(647, 88, 61, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(648, 88, 58, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(649, 88, 60, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(650, 88, 61, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(651, 88, 60, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(652, 88, 36, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(653, 88, 35, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(654, 88, 42, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(655, 89, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(656, 89, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(657, 89, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(658, 89, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(659, 89, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(660, 89, 42, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(661, 90, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(662, 90, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(663, 90, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(664, 90, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(665, 90, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(666, 91, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(667, 91, 38, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(668, 91, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0);
INSERT INTO `oc_relatedoptions` (`relatedoptions_id`, `product_id`, `quantity`, `price`, `model`, `sku`, `upc`, `location`, `defaultselect`, `defaultselectpriority`, `weight`, `weight_prefix`, `price_prefix`, `ean`, `stock_status_id`) VALUES
(669, 91, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(670, 91, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(671, 91, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(672, 91, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(673, 91, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(674, 91, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(675, 91, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(676, 92, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(677, 92, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(678, 92, 45, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(679, 92, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(680, 92, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(681, 92, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(682, 92, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(683, 92, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(684, 92, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(685, 92, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(686, 92, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(687, 93, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(688, 93, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(689, 93, 53, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(690, 93, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(691, 93, 52, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(692, 93, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(693, 93, 28, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(694, 93, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(695, 93, 40, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(696, 93, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(697, 94, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(698, 94, 35, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(699, 94, 28, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(700, 94, 29, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(701, 94, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(702, 94, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(703, 95, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(704, 95, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(705, 95, 70, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(706, 95, 60, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(707, 95, 39, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(708, 95, 58, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(709, 95, 53, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(710, 95, 38, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(711, 95, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(712, 95, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(713, 95, 54, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(714, 95, 37, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(715, 95, 35, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(716, 95, 37, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(717, 95, 34, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(718, 97, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(719, 97, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(720, 97, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(721, 97, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(722, 97, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(723, 97, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(724, 97, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(725, 97, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(726, 97, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(727, 97, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(728, 97, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(729, 97, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(730, 98, 66, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(731, 98, 35, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(732, 98, 36, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(733, 101, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(734, 101, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(735, 101, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(736, 101, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(737, 101, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(738, 101, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(739, 101, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(740, 101, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(741, 101, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(742, 101, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(743, 101, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(744, 101, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(745, 102, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(746, 102, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(747, 102, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(748, 102, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(749, 102, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(750, 102, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(751, 103, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(752, 103, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(753, 103, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(754, 103, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(755, 103, 24, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(756, 103, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(757, 104, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(758, 104, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(759, 104, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(760, 104, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(761, 105, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(762, 105, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(763, 105, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(764, 105, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(765, 105, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(766, 105, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(767, 105, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(768, 105, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(769, 106, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(770, 106, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(771, 106, 12, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(772, 106, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(773, 106, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(774, 106, 11, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(775, 106, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(776, 106, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(777, 106, 13, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(778, 106, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(779, 106, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(780, 106, 14, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(781, 107, 10, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(782, 107, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(783, 107, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(784, 107, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(785, 107, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(786, 107, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(787, 107, 1, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(788, 107, 8, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(789, 108, 37, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(790, 108, 32, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(791, 108, 29, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(792, 108, 35, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(793, 108, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(794, 108, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(795, 108, 34, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(796, 108, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(797, 108, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(798, 108, 29, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(799, 108, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(800, 108, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(801, 109, 55, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(802, 109, 9, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(803, 109, 65, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(804, 109, 64, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(805, 109, 64, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(806, 109, 15, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(807, 109, 16, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(808, 110, 26, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(809, 110, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(810, 110, 43, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(811, 110, 47, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(812, 110, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(813, 110, 41, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(814, 110, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(815, 111, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(816, 111, 33, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(817, 111, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(818, 111, 30, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(819, 111, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(820, 111, 31, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(821, 112, 45, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(822, 112, 45, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(823, 112, 45, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(824, 112, 46, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(825, 112, 45, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(826, 112, 45, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(827, 113, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(828, 113, 5, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(829, 113, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(830, 113, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(831, 113, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(832, 113, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(833, 113, 6, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(834, 113, 7, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(835, 113, 3, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(836, 114, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(837, 114, 18, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(838, 114, 22, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(839, 114, 21, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(840, 114, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(841, 114, 23, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(842, 114, 20, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(843, 114, 19, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(844, 114, 17, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(845, 115, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(846, 115, 50, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(847, 115, 50, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0),
(848, 115, 48, '0.0000', '', '', '', '', 0, 0, '0.00000000', '', '=', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions_discount`
--

CREATE TABLE `oc_relatedoptions_discount` (
  `relatedoptions_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL,
  `priority` int(5) NOT NULL,
  `price` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions_option`
--

CREATE TABLE `oc_relatedoptions_option` (
  `relatedoptions_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_relatedoptions_option`
--

INSERT INTO `oc_relatedoptions_option` (`relatedoptions_id`, `product_id`, `option_id`, `option_value_id`) VALUES
(1, 1, 1, 1),
(1, 1, 2, 2),
(2, 1, 1, 1),
(2, 1, 2, 3),
(3, 1, 1, 1),
(3, 1, 2, 4),
(4, 1, 1, 1),
(4, 1, 2, 5),
(5, 1, 1, 1),
(5, 1, 2, 6),
(6, 1, 1, 1),
(6, 1, 2, 7),
(7, 2, 1, 8),
(7, 2, 2, 9),
(8, 2, 1, 8),
(8, 2, 2, 10),
(9, 2, 1, 8),
(9, 2, 2, 11),
(10, 2, 1, 8),
(10, 2, 2, 12),
(11, 2, 1, 8),
(11, 2, 2, 13),
(12, 3, 1, 1),
(12, 3, 2, 14),
(13, 3, 1, 1),
(13, 3, 2, 15),
(14, 3, 1, 1),
(14, 3, 2, 16),
(15, 3, 1, 1),
(15, 3, 2, 17),
(16, 3, 1, 1),
(16, 3, 2, 18),
(17, 3, 1, 1),
(17, 3, 2, 6),
(18, 3, 1, 1),
(18, 3, 2, 5),
(19, 4, 1, 1),
(19, 4, 2, 3),
(20, 4, 1, 1),
(20, 4, 2, 5),
(21, 4, 1, 1),
(21, 4, 2, 2),
(22, 4, 1, 1),
(22, 4, 2, 4),
(23, 4, 1, 1),
(23, 4, 2, 15),
(24, 5, 1, 1),
(24, 5, 2, 6),
(25, 5, 1, 1),
(25, 5, 2, 15),
(26, 5, 1, 1),
(26, 5, 2, 3),
(27, 5, 1, 1),
(27, 5, 2, 5),
(28, 5, 1, 1),
(28, 5, 2, 2),
(29, 5, 1, 1),
(29, 5, 2, 7),
(30, 5, 1, 1),
(30, 5, 2, 4),
(31, 6, 1, 1),
(31, 6, 2, 3),
(32, 6, 1, 1),
(32, 6, 2, 5),
(33, 6, 1, 1),
(33, 6, 2, 19),
(34, 6, 1, 1),
(34, 6, 2, 4),
(35, 6, 1, 1),
(35, 6, 2, 2),
(36, 6, 1, 1),
(36, 6, 2, 6),
(37, 7, 1, 1),
(37, 7, 2, 3),
(38, 7, 1, 1),
(38, 7, 2, 20),
(39, 7, 1, 1),
(39, 7, 2, 14),
(40, 7, 1, 1),
(40, 7, 2, 2),
(41, 8, 1, 1),
(41, 8, 2, 3),
(42, 8, 1, 1),
(42, 8, 2, 14),
(43, 8, 1, 1),
(43, 8, 2, 21),
(44, 8, 1, 1),
(44, 8, 2, 22),
(45, 8, 1, 1),
(45, 8, 2, 17),
(46, 9, 1, 1),
(46, 9, 2, 3),
(47, 9, 1, 1),
(47, 9, 2, 5),
(48, 9, 1, 1),
(48, 9, 2, 23),
(49, 9, 1, 1),
(49, 9, 2, 24),
(50, 9, 1, 1),
(50, 9, 2, 2),
(51, 9, 1, 1),
(51, 9, 2, 16),
(52, 10, 1, 1),
(52, 10, 2, 2),
(53, 10, 1, 1),
(53, 10, 2, 19),
(54, 10, 1, 1),
(54, 10, 2, 5),
(55, 10, 1, 1),
(55, 10, 2, 25),
(56, 12, 1, 1),
(56, 12, 2, 26),
(57, 12, 1, 1),
(57, 12, 2, 27),
(58, 12, 1, 1),
(58, 12, 2, 23),
(59, 12, 1, 1),
(59, 12, 2, 3),
(60, 12, 1, 1),
(60, 12, 2, 16),
(61, 12, 1, 1),
(61, 12, 2, 2),
(62, 12, 1, 1),
(62, 12, 2, 5),
(63, 12, 1, 1),
(63, 12, 2, 28),
(64, 12, 1, 1),
(64, 12, 2, 29),
(65, 12, 1, 1),
(65, 12, 2, 30),
(66, 13, 1, 1),
(66, 13, 2, 3),
(67, 13, 1, 1),
(67, 13, 2, 5),
(68, 13, 1, 1),
(68, 13, 2, 23),
(69, 13, 1, 1),
(69, 13, 2, 16),
(70, 13, 1, 1),
(70, 13, 2, 2),
(71, 13, 1, 1),
(71, 13, 2, 31),
(72, 14, 1, 32),
(72, 14, 2, 15),
(73, 14, 1, 33),
(73, 14, 2, 15),
(74, 14, 1, 34),
(74, 14, 2, 15),
(75, 14, 1, 32),
(75, 14, 2, 2),
(76, 14, 1, 33),
(76, 14, 2, 2),
(77, 14, 1, 34),
(77, 14, 2, 2),
(78, 14, 1, 32),
(78, 14, 2, 5),
(79, 14, 1, 33),
(79, 14, 2, 5),
(80, 14, 1, 34),
(80, 14, 2, 5),
(81, 14, 1, 32),
(81, 14, 2, 4),
(82, 14, 1, 33),
(82, 14, 2, 4),
(83, 14, 1, 34),
(83, 14, 2, 4),
(84, 14, 1, 32),
(84, 14, 2, 3),
(85, 14, 1, 33),
(85, 14, 2, 3),
(86, 14, 1, 34),
(86, 14, 2, 3),
(87, 15, 1, 1),
(87, 15, 2, 2),
(88, 15, 1, 1),
(88, 15, 2, 5),
(89, 15, 1, 1),
(89, 15, 2, 23),
(90, 15, 1, 1),
(90, 15, 2, 35),
(91, 16, 1, 1),
(91, 16, 2, 22),
(92, 16, 1, 1),
(92, 16, 2, 15),
(93, 16, 1, 1),
(93, 16, 2, 36),
(94, 16, 1, 1),
(94, 16, 2, 14),
(95, 16, 1, 1),
(95, 16, 2, 3),
(96, 16, 1, 1),
(96, 16, 2, 17),
(97, 17, 1, 32),
(97, 17, 2, 5),
(98, 17, 1, 33),
(98, 17, 2, 5),
(99, 17, 1, 34),
(99, 17, 2, 5),
(100, 17, 1, 32),
(100, 17, 2, 3),
(101, 17, 1, 33),
(101, 17, 2, 3),
(102, 17, 1, 34),
(102, 17, 2, 3),
(103, 17, 1, 32),
(103, 17, 2, 37),
(104, 17, 1, 33),
(104, 17, 2, 37),
(105, 17, 1, 34),
(105, 17, 2, 37),
(106, 17, 1, 32),
(106, 17, 2, 38),
(107, 17, 1, 33),
(107, 17, 2, 38),
(108, 17, 1, 34),
(108, 17, 2, 38),
(109, 18, 1, 8),
(109, 18, 2, 23),
(110, 18, 1, 8),
(110, 18, 2, 3),
(111, 18, 1, 8),
(111, 18, 2, 5),
(112, 18, 1, 8),
(112, 18, 2, 16),
(113, 18, 1, 8),
(113, 18, 2, 2),
(114, 18, 1, 8),
(114, 18, 2, 31),
(115, 18, 1, 8),
(115, 18, 2, 39),
(116, 18, 1, 8),
(116, 18, 2, 15),
(117, 18, 1, 8),
(117, 18, 2, 40),
(118, 18, 1, 8),
(118, 18, 2, 35),
(119, 19, 1, 32),
(119, 19, 2, 15),
(120, 19, 1, 8),
(120, 19, 2, 28),
(121, 19, 1, 8),
(121, 19, 2, 16),
(122, 19, 1, 8),
(122, 19, 2, 26),
(123, 19, 1, 8),
(123, 19, 2, 40),
(124, 19, 1, 8),
(124, 19, 2, 31),
(125, 19, 1, 8),
(125, 19, 2, 2),
(126, 19, 1, 8),
(126, 19, 2, 23),
(127, 19, 1, 8),
(127, 19, 2, 3),
(128, 19, 1, 8),
(128, 19, 2, 5),
(129, 20, 1, 32),
(129, 20, 2, 3),
(130, 20, 1, 33),
(130, 20, 2, 3),
(131, 20, 1, 34),
(131, 20, 2, 3),
(132, 20, 1, 32),
(132, 20, 2, 5),
(133, 20, 1, 33),
(133, 20, 2, 5),
(134, 20, 1, 34),
(134, 20, 2, 5),
(135, 20, 1, 32),
(135, 20, 2, 37),
(136, 20, 1, 33),
(136, 20, 2, 37),
(137, 20, 1, 34),
(137, 20, 2, 37),
(138, 20, 1, 32),
(138, 20, 2, 38),
(139, 20, 1, 33),
(139, 20, 2, 38),
(140, 20, 1, 34),
(140, 20, 2, 38),
(141, 21, 1, 32),
(141, 21, 2, 39),
(142, 21, 1, 33),
(142, 21, 2, 39),
(143, 21, 1, 34),
(143, 21, 2, 39),
(144, 21, 1, 32),
(144, 21, 2, 5),
(145, 21, 1, 33),
(145, 21, 2, 5),
(146, 21, 1, 34),
(146, 21, 2, 5),
(147, 21, 1, 32),
(147, 21, 2, 41),
(148, 21, 1, 33),
(148, 21, 2, 41),
(149, 21, 1, 34),
(149, 21, 2, 41),
(150, 21, 1, 34),
(150, 21, 2, 42),
(151, 21, 1, 33),
(151, 21, 2, 43),
(152, 21, 1, 34),
(152, 21, 2, 43),
(153, 21, 1, 33),
(153, 21, 2, 15),
(154, 21, 1, 34),
(154, 21, 2, 15),
(155, 21, 1, 34),
(155, 21, 2, 44),
(156, 21, 1, 34),
(156, 21, 2, 3),
(157, 22, 1, 32),
(157, 22, 2, 3),
(158, 22, 1, 33),
(158, 22, 2, 3),
(159, 22, 1, 34),
(159, 22, 2, 3),
(160, 22, 1, 32),
(160, 22, 2, 5),
(161, 22, 1, 33),
(161, 22, 2, 5),
(162, 22, 1, 34),
(162, 22, 2, 5),
(163, 22, 1, 32),
(163, 22, 2, 38),
(164, 22, 1, 33),
(164, 22, 2, 38),
(165, 22, 1, 34),
(165, 22, 2, 38),
(166, 22, 1, 32),
(166, 22, 2, 37),
(167, 22, 1, 33),
(167, 22, 2, 37),
(168, 22, 1, 34),
(168, 22, 2, 37),
(169, 23, 1, 8),
(169, 23, 2, 3),
(170, 23, 1, 8),
(170, 23, 2, 5),
(171, 23, 1, 8),
(171, 23, 2, 39),
(172, 23, 1, 8),
(172, 23, 2, 2),
(173, 23, 1, 8),
(173, 23, 2, 45),
(174, 24, 1, 8),
(174, 24, 2, 3),
(175, 24, 1, 8),
(175, 24, 2, 5),
(176, 24, 1, 8),
(176, 24, 2, 37),
(177, 24, 1, 8),
(177, 24, 2, 2),
(178, 24, 1, 8),
(178, 24, 2, 15),
(179, 24, 1, 8),
(179, 24, 2, 6),
(180, 25, 1, 32),
(180, 25, 2, 3),
(181, 25, 1, 33),
(181, 25, 2, 3),
(182, 25, 1, 32),
(182, 25, 2, 5),
(183, 25, 1, 33),
(183, 25, 2, 5),
(184, 25, 1, 32),
(184, 25, 2, 4),
(185, 25, 1, 33),
(185, 25, 2, 4),
(186, 26, 1, 8),
(186, 26, 2, 3),
(187, 26, 1, 8),
(187, 26, 2, 5),
(188, 26, 1, 8),
(188, 26, 2, 37),
(189, 26, 1, 8),
(189, 26, 2, 46),
(190, 26, 1, 8),
(190, 26, 2, 2),
(191, 26, 1, 8),
(191, 26, 2, 6),
(192, 26, 1, 8),
(192, 26, 2, 21),
(193, 27, 1, 33),
(193, 27, 2, 3),
(194, 27, 1, 34),
(194, 27, 2, 3),
(195, 27, 1, 47),
(195, 27, 2, 3),
(196, 27, 1, 33),
(196, 27, 2, 5),
(197, 27, 1, 34),
(197, 27, 2, 5),
(198, 27, 1, 47),
(198, 27, 2, 5),
(199, 27, 1, 33),
(199, 27, 2, 37),
(200, 27, 1, 34),
(200, 27, 2, 37),
(201, 27, 1, 47),
(201, 27, 2, 37),
(202, 27, 1, 33),
(202, 27, 2, 2),
(203, 27, 1, 34),
(203, 27, 2, 2),
(204, 27, 1, 47),
(204, 27, 2, 2),
(205, 27, 1, 33),
(205, 27, 2, 15),
(206, 27, 1, 34),
(206, 27, 2, 15),
(207, 27, 1, 47),
(207, 27, 2, 15),
(208, 28, 1, 32),
(208, 28, 2, 3),
(209, 28, 1, 33),
(209, 28, 2, 3),
(210, 28, 1, 34),
(210, 28, 2, 3),
(211, 28, 1, 32),
(211, 28, 2, 5),
(212, 28, 1, 33),
(212, 28, 2, 5),
(213, 28, 1, 34),
(213, 28, 2, 5),
(214, 28, 1, 32),
(214, 28, 2, 37),
(215, 28, 1, 33),
(215, 28, 2, 37),
(216, 28, 1, 34),
(216, 28, 2, 37),
(217, 28, 1, 32),
(217, 28, 2, 2),
(218, 28, 1, 33),
(218, 28, 2, 2),
(219, 28, 1, 32),
(219, 28, 2, 15),
(220, 28, 1, 33),
(220, 28, 2, 15),
(221, 28, 1, 34),
(221, 28, 2, 15),
(222, 29, 1, 32),
(222, 29, 2, 3),
(223, 29, 1, 33),
(223, 29, 2, 3),
(224, 29, 1, 34),
(224, 29, 2, 3),
(225, 29, 1, 32),
(225, 29, 2, 5),
(226, 29, 1, 33),
(226, 29, 2, 5),
(227, 29, 1, 34),
(227, 29, 2, 5),
(228, 29, 1, 32),
(228, 29, 2, 37),
(229, 29, 1, 33),
(229, 29, 2, 37),
(230, 29, 1, 34),
(230, 29, 2, 37),
(231, 29, 1, 32),
(231, 29, 2, 2),
(232, 29, 1, 33),
(232, 29, 2, 2),
(233, 29, 1, 34),
(233, 29, 2, 2),
(234, 29, 1, 32),
(234, 29, 2, 15),
(235, 29, 1, 33),
(235, 29, 2, 15),
(236, 29, 1, 34),
(236, 29, 2, 15),
(237, 30, 1, 33),
(237, 30, 2, 3),
(238, 30, 1, 34),
(238, 30, 2, 3),
(239, 30, 1, 47),
(239, 30, 2, 3),
(240, 30, 1, 33),
(240, 30, 2, 5),
(241, 30, 1, 34),
(241, 30, 2, 5),
(242, 30, 1, 47),
(242, 30, 2, 5),
(243, 30, 1, 33),
(243, 30, 2, 37),
(244, 30, 1, 34),
(244, 30, 2, 37),
(245, 30, 1, 47),
(245, 30, 2, 37),
(246, 30, 1, 33),
(246, 30, 2, 2),
(247, 30, 1, 34),
(247, 30, 2, 2),
(248, 30, 1, 47),
(248, 30, 2, 2),
(249, 30, 1, 33),
(249, 30, 2, 15),
(250, 30, 1, 34),
(250, 30, 2, 15),
(251, 30, 1, 47),
(251, 30, 2, 15),
(252, 31, 1, 33),
(252, 31, 2, 3),
(253, 31, 1, 34),
(253, 31, 2, 3),
(254, 31, 1, 32),
(254, 31, 2, 5),
(255, 31, 1, 33),
(255, 31, 2, 5),
(256, 31, 1, 34),
(256, 31, 2, 5),
(257, 31, 1, 33),
(257, 31, 2, 37),
(258, 31, 1, 34),
(258, 31, 2, 37),
(259, 31, 1, 32),
(259, 31, 2, 2),
(260, 31, 1, 33),
(260, 31, 2, 2),
(261, 31, 1, 34),
(261, 31, 2, 2),
(262, 31, 1, 33),
(262, 31, 2, 15),
(263, 31, 1, 34),
(263, 31, 2, 15),
(264, 31, 1, 47),
(264, 31, 2, 3),
(265, 31, 1, 47),
(265, 31, 2, 5),
(266, 31, 1, 47),
(266, 31, 2, 37),
(267, 31, 1, 47),
(267, 31, 2, 15),
(268, 31, 1, 47),
(268, 31, 2, 2),
(269, 32, 1, 33),
(269, 32, 2, 3),
(270, 32, 1, 34),
(270, 32, 2, 3),
(271, 32, 1, 47),
(271, 32, 2, 3),
(272, 32, 1, 33),
(272, 32, 2, 5),
(273, 32, 1, 34),
(273, 32, 2, 5),
(274, 32, 1, 47),
(274, 32, 2, 5),
(275, 32, 1, 33),
(275, 32, 2, 37),
(276, 32, 1, 34),
(276, 32, 2, 37),
(277, 32, 1, 47),
(277, 32, 2, 37),
(278, 32, 1, 33),
(278, 32, 2, 2),
(279, 32, 1, 47),
(279, 32, 2, 2),
(280, 32, 1, 33),
(280, 32, 2, 15),
(281, 32, 1, 34),
(281, 32, 2, 15),
(282, 32, 1, 47),
(282, 32, 2, 15),
(283, 33, 1, 33),
(283, 33, 2, 3),
(284, 33, 1, 34),
(284, 33, 2, 3),
(285, 33, 1, 33),
(285, 33, 2, 5),
(286, 33, 1, 34),
(286, 33, 2, 5),
(287, 33, 1, 33),
(287, 33, 2, 37),
(288, 33, 1, 34),
(288, 33, 2, 37),
(289, 33, 1, 33),
(289, 33, 2, 2),
(290, 33, 1, 34),
(290, 33, 2, 2),
(291, 33, 1, 33),
(291, 33, 2, 15),
(292, 33, 1, 34),
(292, 33, 2, 15),
(293, 33, 1, 47),
(293, 33, 2, 3),
(294, 33, 1, 47),
(294, 33, 2, 5),
(295, 33, 1, 47),
(295, 33, 2, 37),
(296, 33, 1, 47),
(296, 33, 2, 2),
(297, 33, 1, 47),
(297, 33, 2, 15),
(298, 34, 1, 8),
(298, 34, 2, 23),
(299, 34, 1, 8),
(299, 34, 2, 48),
(300, 34, 1, 8),
(300, 34, 2, 27),
(301, 34, 1, 8),
(301, 34, 2, 35),
(302, 35, 1, 8),
(302, 35, 2, 49),
(303, 35, 1, 8),
(303, 35, 2, 15),
(304, 35, 1, 8),
(304, 35, 2, 50),
(305, 35, 1, 8),
(305, 35, 2, 5),
(306, 35, 1, 8),
(306, 35, 2, 7),
(307, 35, 1, 8),
(307, 35, 2, 51),
(308, 36, 1, 8),
(308, 36, 2, 17),
(309, 36, 1, 8),
(309, 36, 2, 52),
(310, 36, 1, 8),
(310, 36, 2, 2),
(311, 36, 1, 8),
(311, 36, 2, 48),
(312, 36, 1, 8),
(312, 36, 2, 36),
(313, 37, 1, 8),
(313, 37, 2, 17),
(314, 37, 1, 8),
(314, 37, 2, 52),
(315, 37, 1, 8),
(315, 37, 2, 2),
(316, 37, 1, 8),
(316, 37, 2, 48),
(317, 37, 1, 8),
(317, 37, 2, 36),
(318, 38, 1, 8),
(318, 38, 2, 17),
(319, 38, 1, 8),
(319, 38, 2, 14),
(320, 38, 1, 8),
(320, 38, 2, 18),
(321, 38, 1, 8),
(321, 38, 2, 15),
(322, 38, 1, 8),
(322, 38, 2, 48),
(323, 38, 1, 8),
(323, 38, 2, 3),
(324, 39, 1, 8),
(324, 39, 2, 3),
(325, 39, 1, 8),
(325, 39, 2, 17),
(326, 39, 1, 8),
(326, 39, 2, 51),
(327, 39, 1, 8),
(327, 39, 2, 52),
(328, 39, 1, 8),
(328, 39, 2, 18),
(329, 39, 1, 8),
(329, 39, 2, 4),
(330, 39, 1, 8),
(330, 39, 2, 20),
(331, 39, 1, 8),
(331, 39, 2, 26),
(332, 39, 1, 8),
(332, 39, 2, 41),
(333, 39, 1, 8),
(333, 39, 2, 40),
(334, 40, 1, 8),
(334, 40, 2, 3),
(335, 40, 1, 8),
(335, 40, 2, 17),
(336, 40, 1, 8),
(336, 40, 2, 51),
(337, 40, 1, 8),
(337, 40, 2, 52),
(338, 40, 1, 8),
(338, 40, 2, 18),
(339, 40, 1, 8),
(339, 40, 2, 4),
(340, 40, 1, 8),
(340, 40, 2, 20),
(341, 40, 1, 8),
(341, 40, 2, 26),
(342, 40, 1, 8),
(342, 40, 2, 41),
(343, 40, 1, 8),
(343, 40, 2, 40),
(344, 41, 1, 33),
(344, 41, 2, 3),
(345, 41, 1, 34),
(345, 41, 2, 3),
(346, 41, 1, 47),
(346, 41, 2, 3),
(347, 41, 1, 33),
(347, 41, 2, 5),
(348, 41, 1, 34),
(348, 41, 2, 5),
(349, 41, 1, 47),
(349, 41, 2, 5),
(350, 41, 1, 33),
(350, 41, 2, 37),
(351, 41, 1, 34),
(351, 41, 2, 37),
(352, 41, 1, 47),
(352, 41, 2, 37),
(353, 41, 1, 33),
(353, 41, 2, 2),
(354, 41, 1, 34),
(354, 41, 2, 2),
(355, 41, 1, 47),
(355, 41, 2, 2),
(356, 41, 1, 33),
(356, 41, 2, 15),
(357, 41, 1, 34),
(357, 41, 2, 15),
(358, 41, 1, 47),
(358, 41, 2, 15),
(359, 42, 1, 8),
(359, 42, 2, 3),
(360, 42, 1, 8),
(360, 42, 2, 5),
(361, 42, 1, 8),
(361, 42, 2, 2),
(362, 42, 1, 8),
(362, 42, 2, 41),
(363, 43, 1, 8),
(363, 43, 2, 17),
(364, 43, 1, 8),
(364, 43, 2, 53),
(365, 43, 1, 8),
(365, 43, 2, 36),
(366, 43, 1, 8),
(366, 43, 2, 2),
(367, 43, 1, 8),
(367, 43, 2, 52),
(368, 44, 1, 8),
(368, 44, 2, 3),
(369, 44, 1, 8),
(369, 44, 2, 5),
(370, 44, 1, 8),
(370, 44, 2, 2),
(371, 44, 1, 8),
(371, 44, 2, 38),
(372, 44, 1, 8),
(372, 44, 2, 37),
(373, 45, 1, 33),
(373, 45, 2, 3),
(374, 45, 1, 34),
(374, 45, 2, 3),
(375, 45, 1, 33),
(375, 45, 2, 5),
(376, 45, 1, 34),
(376, 45, 2, 5),
(377, 45, 1, 33),
(377, 45, 2, 37),
(378, 45, 1, 34),
(378, 45, 2, 37),
(379, 45, 1, 33),
(379, 45, 2, 2),
(380, 45, 1, 34),
(380, 45, 2, 2),
(381, 45, 1, 33),
(381, 45, 2, 15),
(382, 45, 1, 34),
(382, 45, 2, 15),
(383, 45, 1, 47),
(383, 45, 2, 3),
(384, 45, 1, 47),
(384, 45, 2, 5),
(385, 45, 1, 47),
(385, 45, 2, 37),
(386, 45, 1, 47),
(386, 45, 2, 15),
(387, 45, 1, 47),
(387, 45, 2, 2),
(388, 46, 1, 8),
(388, 46, 2, 3),
(389, 46, 1, 8),
(389, 46, 2, 5),
(390, 46, 1, 8),
(390, 46, 2, 37),
(391, 46, 1, 8),
(391, 46, 2, 2),
(392, 46, 1, 8),
(392, 46, 2, 15),
(393, 46, 1, 8),
(393, 46, 2, 21),
(394, 47, 1, 32),
(394, 47, 2, 5),
(395, 47, 1, 33),
(395, 47, 2, 5),
(396, 47, 1, 32),
(396, 47, 2, 3),
(397, 47, 1, 33),
(397, 47, 2, 3),
(398, 47, 1, 32),
(398, 47, 2, 38),
(399, 48, 1, 33),
(399, 48, 2, 3),
(400, 48, 1, 34),
(400, 48, 2, 3),
(401, 48, 1, 33),
(401, 48, 2, 5),
(402, 48, 1, 34),
(402, 48, 2, 5),
(403, 48, 1, 33),
(403, 48, 2, 37),
(404, 48, 1, 34),
(404, 48, 2, 37),
(405, 48, 1, 33),
(405, 48, 2, 2),
(406, 48, 1, 34),
(406, 48, 2, 2),
(407, 48, 1, 33),
(407, 48, 2, 15),
(408, 48, 1, 34),
(408, 48, 2, 15),
(409, 48, 1, 47),
(409, 48, 2, 2),
(410, 48, 1, 47),
(410, 48, 2, 3),
(411, 48, 1, 47),
(411, 48, 2, 5),
(412, 48, 1, 47),
(412, 48, 2, 15),
(413, 48, 1, 47),
(413, 48, 2, 37),
(414, 49, 1, 8),
(414, 49, 2, 3),
(415, 49, 1, 8),
(415, 49, 2, 5),
(416, 49, 1, 8),
(416, 49, 2, 2),
(417, 49, 1, 8),
(417, 49, 2, 52),
(418, 49, 1, 8),
(418, 49, 2, 39),
(419, 50, 1, 8),
(419, 50, 2, 23),
(420, 50, 1, 8),
(420, 50, 2, 48),
(421, 50, 1, 8),
(421, 50, 2, 27),
(422, 50, 1, 8),
(422, 50, 2, 35),
(423, 51, 1, 8),
(423, 51, 2, 3),
(424, 51, 1, 8),
(424, 51, 2, 5),
(425, 51, 1, 8),
(425, 51, 2, 37),
(426, 51, 1, 8),
(426, 51, 2, 2),
(427, 51, 1, 8),
(427, 51, 2, 15),
(428, 52, 1, 33),
(428, 52, 2, 3),
(429, 52, 1, 34),
(429, 52, 2, 3),
(430, 52, 1, 33),
(430, 52, 2, 5),
(431, 52, 1, 34),
(431, 52, 2, 5),
(432, 52, 1, 33),
(432, 52, 2, 37),
(433, 52, 1, 34),
(433, 52, 2, 37),
(434, 52, 1, 33),
(434, 52, 2, 2),
(435, 52, 1, 34),
(435, 52, 2, 2),
(436, 52, 1, 33),
(436, 52, 2, 15),
(437, 52, 1, 34),
(437, 52, 2, 15),
(438, 52, 1, 47),
(438, 52, 2, 3),
(439, 52, 1, 47),
(439, 52, 2, 5),
(440, 52, 1, 47),
(440, 52, 2, 15),
(441, 52, 1, 47),
(441, 52, 2, 2),
(442, 52, 1, 47),
(442, 52, 2, 37),
(443, 53, 1, 8),
(443, 53, 2, 7),
(444, 53, 1, 8),
(444, 53, 2, 54),
(445, 53, 1, 8),
(445, 53, 2, 2),
(446, 53, 1, 8),
(446, 53, 2, 5),
(447, 54, 1, 8),
(447, 54, 2, 23),
(448, 54, 1, 8),
(448, 54, 2, 3),
(449, 54, 1, 8),
(449, 54, 2, 5),
(450, 54, 1, 8),
(450, 54, 2, 2),
(451, 54, 1, 8),
(451, 54, 2, 16),
(452, 54, 1, 8),
(452, 54, 2, 35),
(453, 54, 1, 8),
(453, 54, 2, 26),
(454, 55, 1, 8),
(454, 55, 2, 23),
(455, 55, 1, 8),
(455, 55, 2, 3),
(456, 55, 1, 8),
(456, 55, 2, 5),
(457, 55, 1, 8),
(457, 55, 2, 2),
(458, 55, 1, 8),
(458, 55, 2, 16),
(459, 55, 1, 8),
(459, 55, 2, 35),
(460, 55, 1, 8),
(460, 55, 2, 26),
(461, 56, 1, 8),
(461, 56, 2, 23),
(462, 56, 1, 8),
(462, 56, 2, 3),
(463, 56, 1, 8),
(463, 56, 2, 5),
(464, 56, 1, 8),
(464, 56, 2, 2),
(465, 56, 1, 8),
(465, 56, 2, 16),
(466, 56, 1, 8),
(466, 56, 2, 35),
(467, 56, 1, 8),
(467, 56, 2, 26),
(468, 57, 1, 8),
(468, 57, 2, 3),
(469, 57, 1, 8),
(469, 57, 2, 5),
(470, 57, 1, 8),
(470, 57, 2, 38),
(471, 57, 1, 8),
(471, 57, 2, 21),
(472, 57, 1, 8),
(472, 57, 2, 2),
(473, 57, 1, 8),
(473, 57, 2, 15),
(474, 58, 1, 8),
(474, 58, 2, 7),
(475, 58, 1, 8),
(475, 58, 2, 54),
(476, 58, 1, 8),
(476, 58, 2, 5),
(477, 58, 1, 8),
(477, 58, 2, 2),
(478, 59, 1, 33),
(478, 59, 2, 2),
(479, 59, 1, 34),
(479, 59, 2, 2),
(480, 59, 1, 33),
(480, 59, 2, 51),
(481, 59, 1, 34),
(481, 59, 2, 51),
(482, 59, 1, 33),
(482, 59, 2, 22),
(483, 59, 1, 34),
(483, 59, 2, 22),
(484, 59, 1, 33),
(484, 59, 2, 26),
(485, 59, 1, 34),
(485, 59, 2, 26),
(486, 60, 1, 8),
(486, 60, 2, 23),
(487, 60, 1, 8),
(487, 60, 2, 35),
(488, 60, 1, 8),
(488, 60, 2, 16),
(489, 60, 1, 8),
(489, 60, 2, 2),
(490, 61, 1, 32),
(490, 61, 2, 16),
(491, 61, 1, 33),
(491, 61, 2, 16),
(492, 61, 1, 34),
(492, 61, 2, 16),
(493, 61, 1, 32),
(493, 61, 2, 5),
(494, 61, 1, 33),
(494, 61, 2, 5),
(495, 61, 1, 34),
(495, 61, 2, 5),
(496, 61, 1, 32),
(496, 61, 2, 7),
(497, 61, 1, 33),
(497, 61, 2, 7),
(498, 61, 1, 34),
(498, 61, 2, 7),
(499, 61, 1, 32),
(499, 61, 2, 20),
(500, 61, 1, 33),
(500, 61, 2, 20),
(501, 61, 1, 34),
(501, 61, 2, 20),
(502, 62, 1, 8),
(502, 62, 2, 3),
(503, 62, 1, 8),
(503, 62, 2, 5),
(504, 62, 1, 8),
(504, 62, 2, 54),
(505, 62, 1, 8),
(505, 62, 2, 15),
(506, 62, 1, 8),
(506, 62, 2, 37),
(507, 62, 1, 8),
(507, 62, 2, 2),
(508, 63, 1, 32),
(508, 63, 2, 38),
(509, 63, 1, 33),
(509, 63, 2, 38),
(510, 63, 1, 34),
(510, 63, 2, 38),
(511, 63, 1, 32),
(511, 63, 2, 4),
(512, 63, 1, 33),
(512, 63, 2, 4),
(513, 63, 1, 34),
(513, 63, 2, 4),
(514, 63, 1, 32),
(514, 63, 2, 5),
(515, 63, 1, 33),
(515, 63, 2, 5),
(516, 63, 1, 34),
(516, 63, 2, 5),
(517, 63, 1, 32),
(517, 63, 2, 2),
(518, 63, 1, 33),
(518, 63, 2, 2),
(519, 63, 1, 34),
(519, 63, 2, 2),
(520, 63, 1, 32),
(520, 63, 2, 3),
(521, 63, 1, 33),
(521, 63, 2, 3),
(522, 63, 1, 34),
(522, 63, 2, 3),
(523, 64, 1, 32),
(523, 64, 2, 2),
(524, 64, 1, 33),
(524, 64, 2, 2),
(525, 64, 1, 34),
(525, 64, 2, 2),
(526, 64, 1, 32),
(526, 64, 2, 7),
(527, 64, 1, 33),
(527, 64, 2, 7),
(528, 64, 1, 34),
(528, 64, 2, 7),
(529, 64, 1, 32),
(529, 64, 2, 5),
(530, 64, 1, 33),
(530, 64, 2, 5),
(531, 64, 1, 34),
(531, 64, 2, 5),
(532, 64, 1, 32),
(532, 64, 2, 51),
(533, 64, 1, 33),
(533, 64, 2, 51),
(534, 64, 1, 34),
(534, 64, 2, 51),
(535, 65, 1, 34),
(535, 65, 2, 18),
(536, 65, 1, 34),
(536, 65, 2, 23),
(537, 65, 1, 34),
(537, 65, 2, 17),
(538, 65, 1, 34),
(538, 65, 2, 3),
(539, 65, 1, 34),
(539, 65, 2, 21),
(540, 65, 1, 34),
(540, 65, 2, 14),
(541, 65, 1, 34),
(541, 65, 2, 19),
(542, 65, 1, 34),
(542, 65, 2, 51),
(543, 66, 1, 34),
(543, 66, 2, 5),
(544, 66, 1, 34),
(544, 66, 2, 18),
(545, 66, 1, 34),
(545, 66, 2, 14),
(546, 66, 1, 34),
(546, 66, 2, 16),
(547, 66, 1, 34),
(547, 66, 2, 15),
(548, 66, 1, 34),
(548, 66, 2, 6),
(549, 66, 1, 34),
(549, 66, 2, 17),
(550, 66, 1, 34),
(550, 66, 2, 20),
(551, 67, 1, 34),
(551, 67, 2, 21),
(552, 67, 1, 34),
(552, 67, 2, 18),
(553, 67, 1, 34),
(553, 67, 2, 3),
(554, 67, 1, 34),
(554, 67, 2, 2),
(555, 67, 1, 34),
(555, 67, 2, 36),
(556, 67, 1, 34),
(556, 67, 2, 5),
(557, 67, 1, 34),
(557, 67, 2, 38),
(558, 67, 1, 34),
(558, 67, 2, 52),
(559, 68, 1, 47),
(559, 68, 2, 18),
(560, 68, 1, 47),
(560, 68, 2, 14),
(561, 68, 1, 47),
(561, 68, 2, 3),
(562, 68, 1, 47),
(562, 68, 2, 2),
(563, 68, 1, 47),
(563, 68, 2, 17),
(564, 69, 1, 55),
(564, 69, 2, 18),
(565, 69, 1, 55),
(565, 69, 2, 4),
(566, 69, 1, 55),
(566, 69, 2, 5),
(567, 69, 1, 55),
(567, 69, 2, 17),
(568, 69, 1, 55),
(568, 69, 2, 15),
(569, 69, 1, 55),
(569, 69, 2, 56),
(570, 69, 1, 55),
(570, 69, 2, 2),
(571, 70, 1, 57),
(571, 70, 2, 48),
(572, 70, 1, 57),
(572, 70, 2, 26),
(573, 70, 1, 57),
(573, 70, 2, 31),
(574, 70, 1, 57),
(574, 70, 2, 3),
(575, 70, 1, 57),
(575, 70, 2, 5),
(576, 70, 1, 57),
(576, 70, 2, 40),
(577, 70, 1, 57),
(577, 70, 2, 2),
(578, 70, 1, 57),
(578, 70, 2, 23),
(579, 70, 1, 57),
(579, 70, 2, 16),
(580, 73, 1, 33),
(580, 73, 2, 3),
(581, 73, 1, 34),
(581, 73, 2, 3),
(582, 73, 1, 33),
(582, 73, 2, 5),
(583, 73, 1, 34),
(583, 73, 2, 5),
(584, 73, 1, 47),
(584, 73, 2, 5),
(585, 73, 1, 33),
(585, 73, 2, 15),
(586, 73, 1, 34),
(586, 73, 2, 15),
(587, 73, 1, 47),
(587, 73, 2, 15),
(588, 73, 1, 33),
(588, 73, 2, 2),
(589, 73, 1, 34),
(589, 73, 2, 2),
(590, 73, 1, 47),
(590, 73, 2, 2),
(591, 73, 1, 33),
(591, 73, 2, 37),
(592, 73, 1, 34),
(592, 73, 2, 37),
(593, 74, 1, 8),
(593, 74, 2, 5),
(594, 74, 1, 8),
(594, 74, 2, 28),
(595, 74, 1, 8),
(595, 74, 2, 26),
(596, 74, 1, 8),
(596, 74, 2, 2),
(597, 75, 1, 8),
(597, 75, 2, 3),
(598, 75, 1, 8),
(598, 75, 2, 5),
(599, 75, 1, 8),
(599, 75, 2, 37),
(600, 75, 1, 8),
(600, 75, 2, 15),
(601, 75, 1, 8),
(601, 75, 2, 2),
(602, 76, 1, 8),
(602, 76, 2, 51),
(603, 76, 1, 8),
(603, 76, 2, 38),
(604, 76, 1, 8),
(604, 76, 2, 18),
(605, 76, 1, 8),
(605, 76, 2, 5),
(606, 76, 1, 8),
(606, 76, 2, 7),
(607, 76, 1, 8),
(607, 76, 2, 49),
(608, 77, 1, 8),
(608, 77, 2, 37),
(609, 77, 1, 8),
(609, 77, 2, 3),
(610, 77, 1, 8),
(610, 77, 2, 2),
(611, 77, 1, 8),
(611, 77, 2, 5),
(612, 77, 1, 8),
(612, 77, 2, 15),
(613, 77, 1, 8),
(613, 77, 2, 21),
(614, 78, 1, 8),
(614, 78, 2, 3),
(615, 78, 1, 8),
(615, 78, 2, 5),
(616, 78, 1, 8),
(616, 78, 2, 2),
(617, 78, 1, 8),
(617, 78, 2, 37),
(618, 78, 1, 8),
(618, 78, 2, 15),
(619, 79, 1, 8),
(619, 79, 2, 23),
(620, 79, 1, 8),
(620, 79, 2, 17),
(621, 79, 1, 8),
(621, 79, 2, 22),
(622, 79, 1, 8),
(622, 79, 2, 14),
(623, 79, 1, 8),
(623, 79, 2, 3),
(624, 79, 1, 8),
(624, 79, 2, 16),
(625, 83, 1, 8),
(625, 83, 2, 27),
(626, 83, 1, 8),
(626, 83, 2, 43),
(627, 83, 1, 8),
(627, 83, 2, 3),
(628, 83, 1, 8),
(628, 83, 2, 16),
(629, 83, 1, 8),
(629, 83, 2, 42),
(630, 83, 1, 8),
(630, 83, 2, 58),
(631, 83, 1, 8),
(631, 83, 2, 23),
(632, 84, 1, 8),
(632, 84, 2, 3),
(633, 84, 1, 8),
(633, 84, 2, 54),
(634, 84, 1, 8),
(634, 84, 2, 31),
(635, 84, 1, 8),
(635, 84, 2, 51),
(636, 84, 1, 8),
(636, 84, 2, 5),
(637, 84, 1, 8),
(637, 84, 2, 2),
(638, 84, 1, 8),
(638, 84, 2, 26),
(639, 84, 1, 8),
(639, 84, 2, 16),
(640, 84, 1, 8),
(640, 84, 2, 23),
(641, 87, 1, 8),
(641, 87, 2, 11),
(642, 87, 1, 8),
(642, 87, 2, 59),
(643, 87, 1, 8),
(643, 87, 2, 13),
(644, 87, 1, 8),
(644, 87, 2, 10),
(645, 87, 1, 8),
(645, 87, 2, 12),
(646, 87, 1, 8),
(646, 87, 2, 9),
(647, 88, 1, 8),
(647, 88, 2, 3),
(648, 88, 1, 8),
(648, 88, 2, 5),
(649, 88, 1, 8),
(649, 88, 2, 37),
(650, 88, 1, 8),
(650, 88, 2, 15),
(651, 88, 1, 8),
(651, 88, 2, 2),
(652, 88, 1, 8),
(652, 88, 2, 6),
(653, 88, 1, 8),
(653, 88, 2, 21),
(654, 88, 1, 8),
(654, 88, 2, 42),
(655, 89, 1, 8),
(655, 89, 2, 3),
(656, 89, 1, 8),
(656, 89, 2, 5),
(657, 89, 1, 8),
(657, 89, 2, 37),
(658, 89, 1, 8),
(658, 89, 2, 15),
(659, 89, 1, 8),
(659, 89, 2, 2),
(660, 89, 1, 8),
(660, 89, 2, 42),
(661, 90, 1, 8),
(661, 90, 2, 23),
(662, 90, 1, 8),
(662, 90, 2, 2),
(663, 90, 1, 8),
(663, 90, 2, 16),
(664, 90, 1, 8),
(664, 90, 2, 3),
(665, 90, 1, 8),
(665, 90, 2, 5),
(666, 91, 1, 8),
(666, 91, 2, 23),
(667, 91, 1, 8),
(667, 91, 2, 2),
(668, 91, 1, 8),
(668, 91, 2, 16),
(669, 91, 1, 8),
(669, 91, 2, 3),
(670, 91, 1, 8),
(670, 91, 2, 5),
(671, 91, 1, 8),
(671, 91, 2, 27),
(672, 91, 1, 8),
(672, 91, 2, 35),
(673, 91, 1, 8),
(673, 91, 2, 51),
(674, 91, 1, 8),
(674, 91, 2, 17),
(675, 91, 1, 8),
(675, 91, 2, 60),
(676, 92, 1, 8),
(676, 92, 2, 23),
(677, 92, 1, 8),
(677, 92, 2, 2),
(678, 92, 1, 8),
(678, 92, 2, 16),
(679, 92, 1, 8),
(679, 92, 2, 3),
(680, 92, 1, 8),
(680, 92, 2, 5),
(681, 92, 1, 8),
(681, 92, 2, 27),
(682, 92, 1, 8),
(682, 92, 2, 35),
(683, 92, 1, 8),
(683, 92, 2, 51),
(684, 92, 1, 8),
(684, 92, 2, 17),
(685, 92, 1, 8),
(685, 92, 2, 60),
(686, 92, 1, 8),
(686, 92, 2, 24),
(687, 93, 1, 8),
(687, 93, 2, 23),
(688, 93, 1, 8),
(688, 93, 2, 2),
(689, 93, 1, 8),
(689, 93, 2, 16),
(690, 93, 1, 8),
(690, 93, 2, 3),
(691, 93, 1, 8),
(691, 93, 2, 5),
(692, 93, 1, 8),
(692, 93, 2, 27),
(693, 93, 1, 8),
(693, 93, 2, 35),
(694, 93, 1, 8),
(694, 93, 2, 51),
(695, 93, 1, 8),
(695, 93, 2, 17),
(696, 93, 1, 8),
(696, 93, 2, 60),
(697, 94, 1, 8),
(697, 94, 2, 3),
(698, 94, 1, 8),
(698, 94, 2, 61),
(699, 94, 1, 8),
(699, 94, 2, 16),
(700, 94, 1, 8),
(700, 94, 2, 2),
(701, 94, 1, 8),
(701, 94, 2, 23),
(702, 94, 1, 8),
(702, 94, 2, 27),
(703, 95, 1, 8),
(703, 95, 2, 37),
(704, 95, 1, 8),
(704, 95, 2, 16),
(705, 95, 1, 8),
(705, 95, 2, 2),
(706, 95, 1, 8),
(706, 95, 2, 24),
(707, 95, 1, 8),
(707, 95, 2, 43),
(708, 95, 1, 8),
(708, 95, 2, 3),
(709, 95, 1, 8),
(709, 95, 2, 20),
(710, 95, 1, 8),
(710, 95, 2, 44),
(711, 95, 1, 8),
(711, 95, 2, 53),
(712, 95, 1, 8),
(712, 95, 2, 4),
(713, 95, 1, 8),
(713, 95, 2, 23),
(714, 95, 1, 8),
(714, 95, 2, 62),
(715, 95, 1, 8),
(715, 95, 2, 35),
(716, 95, 1, 8),
(716, 95, 2, 63),
(717, 95, 1, 8),
(717, 95, 2, 27),
(718, 97, 1, 55),
(718, 97, 2, 16),
(719, 97, 1, 55),
(719, 97, 2, 7),
(720, 97, 1, 55),
(720, 97, 2, 43),
(721, 97, 1, 55),
(721, 97, 2, 5),
(722, 97, 1, 55),
(722, 97, 2, 51),
(723, 97, 1, 55),
(723, 97, 2, 20),
(724, 97, 1, 47),
(724, 97, 2, 51),
(725, 97, 1, 47),
(725, 97, 2, 5),
(726, 97, 1, 47),
(726, 97, 2, 20),
(727, 97, 1, 47),
(727, 97, 2, 43),
(728, 97, 1, 47),
(728, 97, 2, 7),
(729, 97, 1, 47),
(729, 97, 2, 16),
(730, 98, 1, 1),
(730, 98, 2, 3),
(731, 98, 1, 1),
(731, 98, 2, 14),
(732, 98, 1, 1),
(732, 98, 2, 2),
(733, 101, 1, 32),
(733, 101, 2, 5),
(734, 101, 1, 33),
(734, 101, 2, 5),
(735, 101, 1, 34),
(735, 101, 2, 5),
(736, 101, 1, 32),
(736, 101, 2, 64),
(737, 101, 1, 33),
(737, 101, 2, 64),
(738, 101, 1, 34),
(738, 101, 2, 64),
(739, 101, 1, 32),
(739, 101, 2, 51),
(740, 101, 1, 33),
(740, 101, 2, 51),
(741, 101, 1, 34),
(741, 101, 2, 51),
(742, 101, 1, 32),
(742, 101, 2, 2),
(743, 101, 1, 33),
(743, 101, 2, 2),
(744, 101, 1, 34),
(744, 101, 2, 2),
(745, 102, 1, 8),
(745, 102, 2, 3),
(746, 102, 1, 8),
(746, 102, 2, 5),
(747, 102, 1, 8),
(747, 102, 2, 2),
(748, 102, 1, 8),
(748, 102, 2, 37),
(749, 102, 1, 8),
(749, 102, 2, 15),
(750, 102, 1, 8),
(750, 102, 2, 21),
(751, 103, 1, 8),
(751, 103, 2, 3),
(752, 103, 1, 8),
(752, 103, 2, 5),
(753, 103, 1, 8),
(753, 103, 2, 2),
(754, 103, 1, 8),
(754, 103, 2, 37),
(755, 103, 1, 8),
(755, 103, 2, 15),
(756, 103, 1, 8),
(756, 103, 2, 21),
(757, 104, 1, 8),
(757, 104, 2, 5),
(758, 104, 1, 8),
(758, 104, 2, 2),
(759, 104, 1, 8),
(759, 104, 2, 16),
(760, 104, 1, 8),
(760, 104, 2, 23),
(761, 105, 1, 8),
(761, 105, 2, 35),
(762, 105, 1, 8),
(762, 105, 2, 3),
(763, 105, 1, 8),
(763, 105, 2, 5),
(764, 105, 1, 8),
(764, 105, 2, 23),
(765, 105, 1, 8),
(765, 105, 2, 2),
(766, 105, 1, 8),
(766, 105, 2, 16),
(767, 105, 1, 8),
(767, 105, 2, 26),
(768, 105, 1, 8),
(768, 105, 2, 64),
(769, 106, 1, 32),
(769, 106, 2, 5),
(770, 106, 1, 33),
(770, 106, 2, 5),
(771, 106, 1, 34),
(771, 106, 2, 5),
(772, 106, 1, 32),
(772, 106, 2, 3),
(773, 106, 1, 33),
(773, 106, 2, 3),
(774, 106, 1, 34),
(774, 106, 2, 3),
(775, 106, 1, 32),
(775, 106, 2, 20),
(776, 106, 1, 33),
(776, 106, 2, 20),
(777, 106, 1, 34),
(777, 106, 2, 20),
(778, 106, 1, 32),
(778, 106, 2, 37),
(779, 106, 1, 33),
(779, 106, 2, 37),
(780, 106, 1, 34),
(780, 106, 2, 37),
(781, 107, 1, 8),
(781, 107, 2, 48),
(782, 107, 1, 8),
(782, 107, 2, 5),
(783, 107, 1, 8),
(783, 107, 2, 39),
(784, 107, 1, 8),
(784, 107, 2, 37),
(785, 107, 1, 8),
(785, 107, 2, 23),
(786, 107, 1, 8),
(786, 107, 2, 40),
(787, 107, 1, 8),
(787, 107, 2, 36),
(788, 107, 1, 8),
(788, 107, 2, 28),
(789, 108, 1, 8),
(789, 108, 2, 65),
(790, 108, 1, 8),
(790, 108, 2, 66),
(791, 108, 1, 8),
(791, 108, 2, 67),
(792, 108, 1, 8),
(792, 108, 2, 68),
(793, 108, 1, 8),
(793, 108, 2, 69),
(794, 108, 1, 8),
(794, 108, 2, 70),
(795, 108, 1, 8),
(795, 108, 2, 71),
(796, 108, 1, 8),
(796, 108, 2, 72),
(797, 108, 1, 8),
(797, 108, 2, 73),
(798, 108, 1, 8),
(798, 108, 2, 74),
(799, 108, 1, 8),
(799, 108, 2, 75),
(800, 108, 1, 8),
(800, 108, 2, 76),
(801, 109, 1, 47),
(801, 109, 2, 3),
(802, 109, 1, 47),
(802, 109, 2, 5),
(803, 109, 1, 47),
(803, 109, 2, 38),
(804, 109, 1, 47),
(804, 109, 2, 21),
(805, 109, 1, 47),
(805, 109, 2, 17),
(806, 109, 1, 47),
(806, 109, 2, 16),
(807, 109, 1, 47),
(807, 109, 2, 51),
(808, 110, 1, 47),
(808, 110, 2, 3),
(809, 110, 1, 47),
(809, 110, 2, 5),
(810, 110, 1, 47),
(810, 110, 2, 38),
(811, 110, 1, 47),
(811, 110, 2, 21),
(812, 110, 1, 47),
(812, 110, 2, 16),
(813, 110, 1, 47),
(813, 110, 2, 17),
(814, 110, 1, 47),
(814, 110, 2, 51),
(815, 111, 1, 8),
(815, 111, 2, 14),
(816, 111, 1, 8),
(816, 111, 2, 18),
(817, 111, 1, 8),
(817, 111, 2, 5),
(818, 111, 1, 8),
(818, 111, 2, 6),
(819, 111, 1, 8),
(819, 111, 2, 15),
(820, 111, 1, 8),
(820, 111, 2, 17),
(821, 112, 1, 8),
(821, 112, 2, 14),
(822, 112, 1, 8),
(822, 112, 2, 18),
(823, 112, 1, 8),
(823, 112, 2, 5),
(824, 112, 1, 8),
(824, 112, 2, 6),
(825, 112, 1, 8),
(825, 112, 2, 15),
(826, 112, 1, 8),
(826, 112, 2, 17),
(827, 113, 1, 33),
(827, 113, 2, 2),
(828, 113, 1, 32),
(828, 113, 2, 7),
(829, 113, 1, 32),
(829, 113, 2, 2),
(830, 113, 1, 33),
(830, 113, 2, 7),
(831, 113, 1, 32),
(831, 113, 2, 5),
(832, 113, 1, 33),
(832, 113, 2, 5),
(833, 113, 1, 32),
(833, 113, 2, 51),
(834, 113, 1, 33),
(834, 113, 2, 51),
(835, 113, 1, 34),
(835, 113, 2, 51),
(836, 114, 1, 8),
(836, 114, 2, 23),
(837, 114, 1, 8),
(837, 114, 2, 3),
(838, 114, 1, 8),
(838, 114, 2, 54),
(839, 114, 1, 8),
(839, 114, 2, 31),
(840, 114, 1, 8),
(840, 114, 2, 26),
(841, 114, 1, 8),
(841, 114, 2, 5),
(842, 114, 1, 8),
(842, 114, 2, 16),
(843, 114, 1, 8),
(843, 114, 2, 2),
(844, 114, 1, 8),
(844, 114, 2, 51),
(845, 115, 1, 8),
(845, 115, 2, 29),
(846, 115, 1, 8),
(846, 115, 2, 30),
(847, 115, 1, 8),
(847, 115, 2, 35),
(848, 115, 1, 8),
(848, 115, 2, 16);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions_special`
--

CREATE TABLE `oc_relatedoptions_special` (
  `relatedoptions_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL,
  `price` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions_to_char`
--

CREATE TABLE `oc_relatedoptions_to_char` (
  `relatedoptions_id` int(11) NOT NULL,
  `char_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_relatedoptions_to_char`
--

INSERT INTO `oc_relatedoptions_to_char` (`relatedoptions_id`, `char_id`) VALUES
(1, '1484f159-4675-11e5-8033-00ac06be3045'),
(2, '1484f15a-4675-11e5-8033-00ac06be3045'),
(3, '1484f15b-4675-11e5-8033-00ac06be3045'),
(4, '1484f15c-4675-11e5-8033-00ac06be3045'),
(5, '09023fc8-6ce5-11e5-b374-00ac06be3045'),
(6, '09023fc9-6ce5-11e5-b374-00ac06be3045'),
(7, '8fa576be-7411-11e5-aacb-00ac06be3045'),
(8, '8fa576bf-7411-11e5-aacb-00ac06be3045'),
(9, '8fa576c0-7411-11e5-aacb-00ac06be3045'),
(10, '8fa576c1-7411-11e5-aacb-00ac06be3045'),
(11, '8fa576c2-7411-11e5-aacb-00ac06be3045'),
(12, '1484f160-4675-11e5-8033-00ac06be3045'),
(13, '1484f161-4675-11e5-8033-00ac06be3045'),
(14, '1484f162-4675-11e5-8033-00ac06be3045'),
(15, '1484f163-4675-11e5-8033-00ac06be3045'),
(16, '1484f164-4675-11e5-8033-00ac06be3045'),
(17, '1484f165-4675-11e5-8033-00ac06be3045'),
(18, '1484f166-4675-11e5-8033-00ac06be3045'),
(19, '1484f168-4675-11e5-8033-00ac06be3045'),
(20, '1484f169-4675-11e5-8033-00ac06be3045'),
(21, '1484f16a-4675-11e5-8033-00ac06be3045'),
(22, '1484f16b-4675-11e5-8033-00ac06be3045'),
(23, '1484f16c-4675-11e5-8033-00ac06be3045'),
(24, '1484f16e-4675-11e5-8033-00ac06be3045'),
(25, '1484f16f-4675-11e5-8033-00ac06be3045'),
(26, '1484f170-4675-11e5-8033-00ac06be3045'),
(27, '1484f171-4675-11e5-8033-00ac06be3045'),
(28, '1484f172-4675-11e5-8033-00ac06be3045'),
(29, '1484f173-4675-11e5-8033-00ac06be3045'),
(30, '09023fc7-6ce5-11e5-b374-00ac06be3045'),
(31, '1484f175-4675-11e5-8033-00ac06be3045'),
(32, '1484f176-4675-11e5-8033-00ac06be3045'),
(33, '1484f177-4675-11e5-8033-00ac06be3045'),
(34, '1484f178-4675-11e5-8033-00ac06be3045'),
(35, '1484f179-4675-11e5-8033-00ac06be3045'),
(36, '09023fca-6ce5-11e5-b374-00ac06be3045'),
(37, '1484f17c-4675-11e5-8033-00ac06be3045'),
(38, '1484f17d-4675-11e5-8033-00ac06be3045'),
(39, '1484f17e-4675-11e5-8033-00ac06be3045'),
(40, '1484f17f-4675-11e5-8033-00ac06be3045'),
(41, '1484f181-4675-11e5-8033-00ac06be3045'),
(42, '1484f182-4675-11e5-8033-00ac06be3045'),
(43, '1484f183-4675-11e5-8033-00ac06be3045'),
(44, '1484f184-4675-11e5-8033-00ac06be3045'),
(45, '1484f185-4675-11e5-8033-00ac06be3045'),
(46, '1484f187-4675-11e5-8033-00ac06be3045'),
(47, '1484f188-4675-11e5-8033-00ac06be3045'),
(48, '1484f189-4675-11e5-8033-00ac06be3045'),
(49, '1484f18a-4675-11e5-8033-00ac06be3045'),
(50, '1484f18b-4675-11e5-8033-00ac06be3045'),
(51, '1484f18c-4675-11e5-8033-00ac06be3045'),
(52, 'c593d9bc-48a6-11e5-8033-00ac06be3045'),
(53, 'c593d9bd-48a6-11e5-8033-00ac06be3045'),
(54, 'cbac2b18-4ae7-11e5-8033-00ac06be3045'),
(55, 'cbac2b19-4ae7-11e5-8033-00ac06be3045'),
(56, 'cbac2b1b-4ae7-11e5-8033-00ac06be3045'),
(57, 'cbac2b1c-4ae7-11e5-8033-00ac06be3045'),
(58, 'cbac2b1d-4ae7-11e5-8033-00ac06be3045'),
(59, 'cbac2b1e-4ae7-11e5-8033-00ac06be3045'),
(60, 'cbac2b1f-4ae7-11e5-8033-00ac06be3045'),
(61, 'cbac2b20-4ae7-11e5-8033-00ac06be3045'),
(62, 'cbac2b21-4ae7-11e5-8033-00ac06be3045'),
(63, 'cbac2b22-4ae7-11e5-8033-00ac06be3045'),
(64, 'cbac2b23-4ae7-11e5-8033-00ac06be3045'),
(65, 'cbac2b24-4ae7-11e5-8033-00ac06be3045'),
(66, 'cbac2b26-4ae7-11e5-8033-00ac06be3045'),
(67, 'cbac2b27-4ae7-11e5-8033-00ac06be3045'),
(68, 'cbac2b28-4ae7-11e5-8033-00ac06be3045'),
(69, 'cbac2b29-4ae7-11e5-8033-00ac06be3045'),
(70, 'cbac2b2a-4ae7-11e5-8033-00ac06be3045'),
(71, 'cbac2b2b-4ae7-11e5-8033-00ac06be3045'),
(72, 'cd4ce148-675b-11e5-be92-00ac06be3045'),
(73, 'cd4ce149-675b-11e5-be92-00ac06be3045'),
(74, 'cd4ce14a-675b-11e5-be92-00ac06be3045'),
(75, 'cd4ce14b-675b-11e5-be92-00ac06be3045'),
(76, 'cd4ce14c-675b-11e5-be92-00ac06be3045'),
(77, 'cd4ce14d-675b-11e5-be92-00ac06be3045'),
(78, 'cd4ce14e-675b-11e5-be92-00ac06be3045'),
(79, 'cd4ce14f-675b-11e5-be92-00ac06be3045'),
(80, 'cd4ce150-675b-11e5-be92-00ac06be3045'),
(81, 'cd4ce151-675b-11e5-be92-00ac06be3045'),
(82, 'cd4ce152-675b-11e5-be92-00ac06be3045'),
(83, 'cd4ce153-675b-11e5-be92-00ac06be3045'),
(84, 'cd4ce154-675b-11e5-be92-00ac06be3045'),
(85, 'cd4ce155-675b-11e5-be92-00ac06be3045'),
(86, 'cd4ce156-675b-11e5-be92-00ac06be3045'),
(87, 'cbac2b37-4ae7-11e5-8033-00ac06be3045'),
(88, 'cbac2b38-4ae7-11e5-8033-00ac06be3045'),
(89, 'cbac2b39-4ae7-11e5-8033-00ac06be3045'),
(90, 'cbac2b3a-4ae7-11e5-8033-00ac06be3045'),
(91, 'cbac2b3c-4ae7-11e5-8033-00ac06be3045'),
(92, 'cbac2b3d-4ae7-11e5-8033-00ac06be3045'),
(93, 'cbac2b3e-4ae7-11e5-8033-00ac06be3045'),
(94, 'cbac2b3f-4ae7-11e5-8033-00ac06be3045'),
(95, 'cbac2b40-4ae7-11e5-8033-00ac06be3045'),
(96, 'cbac2b41-4ae7-11e5-8033-00ac06be3045'),
(97, 'f7d2d3b7-52bf-11e5-8033-00ac06be3045'),
(98, 'f7d2d3b8-52bf-11e5-8033-00ac06be3045'),
(99, 'f7d2d3b9-52bf-11e5-8033-00ac06be3045'),
(100, 'f7d2d3ba-52bf-11e5-8033-00ac06be3045'),
(101, 'f7d2d3bb-52bf-11e5-8033-00ac06be3045'),
(102, 'f7d2d3bc-52bf-11e5-8033-00ac06be3045'),
(103, 'f7d2d3bd-52bf-11e5-8033-00ac06be3045'),
(104, 'f7d2d3be-52bf-11e5-8033-00ac06be3045'),
(105, 'f7d2d3bf-52bf-11e5-8033-00ac06be3045'),
(106, 'f7d2d3c0-52bf-11e5-8033-00ac06be3045'),
(107, 'f7d2d3c1-52bf-11e5-8033-00ac06be3045'),
(108, 'f7d2d3c2-52bf-11e5-8033-00ac06be3045'),
(109, 'f7d2d3c4-52bf-11e5-8033-00ac06be3045'),
(110, 'f7d2d3c5-52bf-11e5-8033-00ac06be3045'),
(111, 'f7d2d3c6-52bf-11e5-8033-00ac06be3045'),
(112, 'f7d2d3c7-52bf-11e5-8033-00ac06be3045'),
(113, 'f7d2d3c8-52bf-11e5-8033-00ac06be3045'),
(114, 'f7d2d3c9-52bf-11e5-8033-00ac06be3045'),
(115, 'f7d2d3ca-52bf-11e5-8033-00ac06be3045'),
(116, 'f7d2d3cb-52bf-11e5-8033-00ac06be3045'),
(117, '09023fc5-6ce5-11e5-b374-00ac06be3045'),
(118, '09023fc6-6ce5-11e5-b374-00ac06be3045'),
(119, 'f7d2d3d3-52bf-11e5-8033-00ac06be3045'),
(120, 'cd4ce13f-675b-11e5-be92-00ac06be3045'),
(121, 'cd4ce140-675b-11e5-be92-00ac06be3045'),
(122, 'cd4ce141-675b-11e5-be92-00ac06be3045'),
(123, 'cd4ce142-675b-11e5-be92-00ac06be3045'),
(124, 'cd4ce143-675b-11e5-be92-00ac06be3045'),
(125, 'cd4ce144-675b-11e5-be92-00ac06be3045'),
(126, 'cd4ce145-675b-11e5-be92-00ac06be3045'),
(127, 'cd4ce146-675b-11e5-be92-00ac06be3045'),
(128, 'cd4ce147-675b-11e5-be92-00ac06be3045'),
(129, 'f7d2d3dd-52bf-11e5-8033-00ac06be3045'),
(130, 'f7d2d3de-52bf-11e5-8033-00ac06be3045'),
(131, 'f7d2d3df-52bf-11e5-8033-00ac06be3045'),
(132, 'f7d2d3e0-52bf-11e5-8033-00ac06be3045'),
(133, 'f7d2d3e1-52bf-11e5-8033-00ac06be3045'),
(134, 'f7d2d3e2-52bf-11e5-8033-00ac06be3045'),
(135, 'f7d2d3e3-52bf-11e5-8033-00ac06be3045'),
(136, 'f7d2d3e4-52bf-11e5-8033-00ac06be3045'),
(137, 'f7d2d3e5-52bf-11e5-8033-00ac06be3045'),
(138, 'f7d2d3e6-52bf-11e5-8033-00ac06be3045'),
(139, 'f7d2d3e7-52bf-11e5-8033-00ac06be3045'),
(140, 'f7d2d3e8-52bf-11e5-8033-00ac06be3045'),
(141, 'f7d2d3ea-52bf-11e5-8033-00ac06be3045'),
(142, 'f7d2d3eb-52bf-11e5-8033-00ac06be3045'),
(143, 'f7d2d3ec-52bf-11e5-8033-00ac06be3045'),
(144, 'f7d2d3ed-52bf-11e5-8033-00ac06be3045'),
(145, 'f7d2d3ee-52bf-11e5-8033-00ac06be3045'),
(146, 'f7d2d3ef-52bf-11e5-8033-00ac06be3045'),
(147, 'f7d2d3f0-52bf-11e5-8033-00ac06be3045'),
(148, 'f7d2d3f1-52bf-11e5-8033-00ac06be3045'),
(149, 'f7d2d3f2-52bf-11e5-8033-00ac06be3045'),
(150, '2b729754-70b2-11e5-b374-00ac06be3045'),
(151, '2b729755-70b2-11e5-b374-00ac06be3045'),
(152, '2b729756-70b2-11e5-b374-00ac06be3045'),
(153, '2b729757-70b2-11e5-b374-00ac06be3045'),
(154, '2b729758-70b2-11e5-b374-00ac06be3045'),
(155, '2b72975a-70b2-11e5-b374-00ac06be3045'),
(156, '2b72975c-70b2-11e5-b374-00ac06be3045'),
(157, 'fad705d2-52dc-11e5-8033-00ac06be3045'),
(158, 'fad705d3-52dc-11e5-8033-00ac06be3045'),
(159, 'fad705d4-52dc-11e5-8033-00ac06be3045'),
(160, 'fad705d5-52dc-11e5-8033-00ac06be3045'),
(161, 'fad705d6-52dc-11e5-8033-00ac06be3045'),
(162, 'fad705d7-52dc-11e5-8033-00ac06be3045'),
(163, 'fad705d8-52dc-11e5-8033-00ac06be3045'),
(164, 'fad705d9-52dc-11e5-8033-00ac06be3045'),
(165, 'fad705da-52dc-11e5-8033-00ac06be3045'),
(166, 'fad705db-52dc-11e5-8033-00ac06be3045'),
(167, 'fad705dc-52dc-11e5-8033-00ac06be3045'),
(168, 'fad705dd-52dc-11e5-8033-00ac06be3045'),
(169, 'fad705df-52dc-11e5-8033-00ac06be3045'),
(170, 'fad705e0-52dc-11e5-8033-00ac06be3045'),
(171, 'fad705e1-52dc-11e5-8033-00ac06be3045'),
(172, 'fad705e2-52dc-11e5-8033-00ac06be3045'),
(173, 'fad705e4-52dc-11e5-8033-00ac06be3045'),
(174, 'fad705e6-52dc-11e5-8033-00ac06be3045'),
(175, 'fad705e7-52dc-11e5-8033-00ac06be3045'),
(176, 'fad705e8-52dc-11e5-8033-00ac06be3045'),
(177, 'fad705e9-52dc-11e5-8033-00ac06be3045'),
(178, 'fad705ea-52dc-11e5-8033-00ac06be3045'),
(179, 'fad705eb-52dc-11e5-8033-00ac06be3045'),
(180, 'fad705ed-52dc-11e5-8033-00ac06be3045'),
(181, 'fad705ee-52dc-11e5-8033-00ac06be3045'),
(182, 'fad705ef-52dc-11e5-8033-00ac06be3045'),
(183, 'fad705f0-52dc-11e5-8033-00ac06be3045'),
(184, 'fad705f1-52dc-11e5-8033-00ac06be3045'),
(185, 'fad705f2-52dc-11e5-8033-00ac06be3045'),
(186, 'fad705f4-52dc-11e5-8033-00ac06be3045'),
(187, 'fad705f5-52dc-11e5-8033-00ac06be3045'),
(188, 'fad705f6-52dc-11e5-8033-00ac06be3045'),
(189, 'fad705f7-52dc-11e5-8033-00ac06be3045'),
(190, 'fad705f8-52dc-11e5-8033-00ac06be3045'),
(191, 'fad705f9-52dc-11e5-8033-00ac06be3045'),
(192, 'fad705fa-52dc-11e5-8033-00ac06be3045'),
(193, 'fad705fc-52dc-11e5-8033-00ac06be3045'),
(194, 'fad705fd-52dc-11e5-8033-00ac06be3045'),
(195, 'fad705fe-52dc-11e5-8033-00ac06be3045'),
(196, 'fad705ff-52dc-11e5-8033-00ac06be3045'),
(197, 'fad70600-52dc-11e5-8033-00ac06be3045'),
(198, 'fad70601-52dc-11e5-8033-00ac06be3045'),
(199, 'fad70602-52dc-11e5-8033-00ac06be3045'),
(200, 'fad70603-52dc-11e5-8033-00ac06be3045'),
(201, 'fad70604-52dc-11e5-8033-00ac06be3045'),
(202, 'fad70605-52dc-11e5-8033-00ac06be3045'),
(203, 'fad70606-52dc-11e5-8033-00ac06be3045'),
(204, 'fad70607-52dc-11e5-8033-00ac06be3045'),
(205, 'fad70608-52dc-11e5-8033-00ac06be3045'),
(206, 'fad70609-52dc-11e5-8033-00ac06be3045'),
(207, 'fad7060a-52dc-11e5-8033-00ac06be3045'),
(208, 'fad7060c-52dc-11e5-8033-00ac06be3045'),
(209, 'fad7060d-52dc-11e5-8033-00ac06be3045'),
(210, 'fad7060e-52dc-11e5-8033-00ac06be3045'),
(211, 'fad7060f-52dc-11e5-8033-00ac06be3045'),
(212, 'fad70610-52dc-11e5-8033-00ac06be3045'),
(213, '94c0003e-52e7-11e5-8033-00ac06be3045'),
(214, '94c0003f-52e7-11e5-8033-00ac06be3045'),
(215, '94c00040-52e7-11e5-8033-00ac06be3045'),
(216, '94c00041-52e7-11e5-8033-00ac06be3045'),
(217, '94c00042-52e7-11e5-8033-00ac06be3045'),
(218, '94c00043-52e7-11e5-8033-00ac06be3045'),
(219, '94c00045-52e7-11e5-8033-00ac06be3045'),
(220, '94c00046-52e7-11e5-8033-00ac06be3045'),
(221, '94c00047-52e7-11e5-8033-00ac06be3045'),
(222, '94c00049-52e7-11e5-8033-00ac06be3045'),
(223, '94c0004a-52e7-11e5-8033-00ac06be3045'),
(224, '94c0004b-52e7-11e5-8033-00ac06be3045'),
(225, '94c0004c-52e7-11e5-8033-00ac06be3045'),
(226, '94c0004d-52e7-11e5-8033-00ac06be3045'),
(227, '94c0004e-52e7-11e5-8033-00ac06be3045'),
(228, '94c0004f-52e7-11e5-8033-00ac06be3045'),
(229, '94c00050-52e7-11e5-8033-00ac06be3045'),
(230, '94c00051-52e7-11e5-8033-00ac06be3045'),
(231, '94c00052-52e7-11e5-8033-00ac06be3045'),
(232, '94c00053-52e7-11e5-8033-00ac06be3045'),
(233, '94c00054-52e7-11e5-8033-00ac06be3045'),
(234, '94c00055-52e7-11e5-8033-00ac06be3045'),
(235, '94c00056-52e7-11e5-8033-00ac06be3045'),
(236, '94c00057-52e7-11e5-8033-00ac06be3045'),
(237, '94c00059-52e7-11e5-8033-00ac06be3045'),
(238, '94c0005a-52e7-11e5-8033-00ac06be3045'),
(239, '94c0005b-52e7-11e5-8033-00ac06be3045'),
(240, '94c0005c-52e7-11e5-8033-00ac06be3045'),
(241, '94c0005d-52e7-11e5-8033-00ac06be3045'),
(242, '94c0005e-52e7-11e5-8033-00ac06be3045'),
(243, '94c0005f-52e7-11e5-8033-00ac06be3045'),
(244, '94c00060-52e7-11e5-8033-00ac06be3045'),
(245, '94c00061-52e7-11e5-8033-00ac06be3045'),
(246, '94c00062-52e7-11e5-8033-00ac06be3045'),
(247, '94c00063-52e7-11e5-8033-00ac06be3045'),
(248, '94c00064-52e7-11e5-8033-00ac06be3045'),
(249, '94c00065-52e7-11e5-8033-00ac06be3045'),
(250, '94c00066-52e7-11e5-8033-00ac06be3045'),
(251, '94c00067-52e7-11e5-8033-00ac06be3045'),
(252, '94c0006a-52e7-11e5-8033-00ac06be3045'),
(253, '94c0006b-52e7-11e5-8033-00ac06be3045'),
(254, '94c0006c-52e7-11e5-8033-00ac06be3045'),
(255, '94c0006d-52e7-11e5-8033-00ac06be3045'),
(256, '94c0006e-52e7-11e5-8033-00ac06be3045'),
(257, '94c00070-52e7-11e5-8033-00ac06be3045'),
(258, '94c00071-52e7-11e5-8033-00ac06be3045'),
(259, '94c00072-52e7-11e5-8033-00ac06be3045'),
(260, '94c00073-52e7-11e5-8033-00ac06be3045'),
(261, '94c00074-52e7-11e5-8033-00ac06be3045'),
(262, '94c00076-52e7-11e5-8033-00ac06be3045'),
(263, '94c00077-52e7-11e5-8033-00ac06be3045'),
(264, '05b5ec58-634f-11e5-b48a-00ac06be3045'),
(265, '05b5ec59-634f-11e5-b48a-00ac06be3045'),
(266, '05b5ec5a-634f-11e5-b48a-00ac06be3045'),
(267, '05b5ec5b-634f-11e5-b48a-00ac06be3045'),
(268, '05b5ec5c-634f-11e5-b48a-00ac06be3045'),
(269, '94c00079-52e7-11e5-8033-00ac06be3045'),
(270, '94c0007a-52e7-11e5-8033-00ac06be3045'),
(271, '94c0007b-52e7-11e5-8033-00ac06be3045'),
(272, '94c0007c-52e7-11e5-8033-00ac06be3045'),
(273, '94c0007d-52e7-11e5-8033-00ac06be3045'),
(274, 'b051cecb-52f0-11e5-8033-00ac06be3045'),
(275, 'b051cecc-52f0-11e5-8033-00ac06be3045'),
(276, 'b051cecd-52f0-11e5-8033-00ac06be3045'),
(277, 'b051cece-52f0-11e5-8033-00ac06be3045'),
(278, 'b051cecf-52f0-11e5-8033-00ac06be3045'),
(279, 'b051ced1-52f0-11e5-8033-00ac06be3045'),
(280, 'b051ced2-52f0-11e5-8033-00ac06be3045'),
(281, 'b051ced3-52f0-11e5-8033-00ac06be3045'),
(282, 'b051ced4-52f0-11e5-8033-00ac06be3045'),
(283, 'b051ced7-52f0-11e5-8033-00ac06be3045'),
(284, 'b051ced8-52f0-11e5-8033-00ac06be3045'),
(285, 'b051ceda-52f0-11e5-8033-00ac06be3045'),
(286, 'b051cedb-52f0-11e5-8033-00ac06be3045'),
(287, 'b051cedd-52f0-11e5-8033-00ac06be3045'),
(288, 'b051cede-52f0-11e5-8033-00ac06be3045'),
(289, 'b051cee0-52f0-11e5-8033-00ac06be3045'),
(290, 'b051cee1-52f0-11e5-8033-00ac06be3045'),
(291, 'b051cee3-52f0-11e5-8033-00ac06be3045'),
(292, 'b051cee4-52f0-11e5-8033-00ac06be3045'),
(293, 'f9387096-53cf-11e5-8033-00ac06be3045'),
(294, 'f9387097-53cf-11e5-8033-00ac06be3045'),
(295, 'f9387098-53cf-11e5-8033-00ac06be3045'),
(296, 'f9387099-53cf-11e5-8033-00ac06be3045'),
(297, 'f938709a-53cf-11e5-8033-00ac06be3045'),
(298, 'b051cee6-52f0-11e5-8033-00ac06be3045'),
(299, 'b051cee7-52f0-11e5-8033-00ac06be3045'),
(300, 'b051cee8-52f0-11e5-8033-00ac06be3045'),
(301, 'b051cee9-52f0-11e5-8033-00ac06be3045'),
(302, '9ac78bf4-530b-11e5-8033-00ac06be3045'),
(303, '9ac78bf5-530b-11e5-8033-00ac06be3045'),
(304, '9ac78bf6-530b-11e5-8033-00ac06be3045'),
(305, '9ac78bf7-530b-11e5-8033-00ac06be3045'),
(306, '9ac78bf8-530b-11e5-8033-00ac06be3045'),
(307, '9ac78bf9-530b-11e5-8033-00ac06be3045'),
(308, '9ac78bfb-530b-11e5-8033-00ac06be3045'),
(309, '9ac78bfc-530b-11e5-8033-00ac06be3045'),
(310, '9ac78bfd-530b-11e5-8033-00ac06be3045'),
(311, '9ac78bfe-530b-11e5-8033-00ac06be3045'),
(312, '9ac78bff-530b-11e5-8033-00ac06be3045'),
(313, '9ac78c01-530b-11e5-8033-00ac06be3045'),
(314, '9ac78c02-530b-11e5-8033-00ac06be3045'),
(315, '9ac78c03-530b-11e5-8033-00ac06be3045'),
(316, '9ac78c04-530b-11e5-8033-00ac06be3045'),
(317, '9ac78c05-530b-11e5-8033-00ac06be3045'),
(318, '9ac78c07-530b-11e5-8033-00ac06be3045'),
(319, '9ac78c08-530b-11e5-8033-00ac06be3045'),
(320, '9ac78c09-530b-11e5-8033-00ac06be3045'),
(321, '9ac78c0a-530b-11e5-8033-00ac06be3045'),
(322, '9ac78c0b-530b-11e5-8033-00ac06be3045'),
(323, '9ac78c0c-530b-11e5-8033-00ac06be3045'),
(324, '9a662f25-5388-11e5-8033-00ac06be3045'),
(325, '9a662f26-5388-11e5-8033-00ac06be3045'),
(326, '9a662f27-5388-11e5-8033-00ac06be3045'),
(327, '9a662f28-5388-11e5-8033-00ac06be3045'),
(328, '9a662f29-5388-11e5-8033-00ac06be3045'),
(329, '9a662f2a-5388-11e5-8033-00ac06be3045'),
(330, '9a662f2b-5388-11e5-8033-00ac06be3045'),
(331, '9a662f2c-5388-11e5-8033-00ac06be3045'),
(332, '9a662f2d-5388-11e5-8033-00ac06be3045'),
(333, '9a662f2e-5388-11e5-8033-00ac06be3045'),
(334, '9a662f30-5388-11e5-8033-00ac06be3045'),
(335, '9a662f31-5388-11e5-8033-00ac06be3045'),
(336, '9a662f32-5388-11e5-8033-00ac06be3045'),
(337, '9a662f33-5388-11e5-8033-00ac06be3045'),
(338, '9a662f34-5388-11e5-8033-00ac06be3045'),
(339, '9a662f35-5388-11e5-8033-00ac06be3045'),
(340, '9a662f36-5388-11e5-8033-00ac06be3045'),
(341, '9a662f37-5388-11e5-8033-00ac06be3045'),
(342, '9a662f38-5388-11e5-8033-00ac06be3045'),
(343, '9a662f39-5388-11e5-8033-00ac06be3045'),
(344, '9a662f3b-5388-11e5-8033-00ac06be3045'),
(345, '9a662f3c-5388-11e5-8033-00ac06be3045'),
(346, '9a662f3d-5388-11e5-8033-00ac06be3045'),
(347, '9a662f3e-5388-11e5-8033-00ac06be3045'),
(348, '9a662f3f-5388-11e5-8033-00ac06be3045'),
(349, '9a662f40-5388-11e5-8033-00ac06be3045'),
(350, '9a662f41-5388-11e5-8033-00ac06be3045'),
(351, '9a662f42-5388-11e5-8033-00ac06be3045'),
(352, '9a662f43-5388-11e5-8033-00ac06be3045'),
(353, '9a662f44-5388-11e5-8033-00ac06be3045'),
(354, '9a662f45-5388-11e5-8033-00ac06be3045'),
(355, '9a662f46-5388-11e5-8033-00ac06be3045'),
(356, '9a662f47-5388-11e5-8033-00ac06be3045'),
(357, '9a662f48-5388-11e5-8033-00ac06be3045'),
(358, '9a662f49-5388-11e5-8033-00ac06be3045'),
(359, '9a662f4b-5388-11e5-8033-00ac06be3045'),
(360, '9a662f4c-5388-11e5-8033-00ac06be3045'),
(361, '9a662f4e-5388-11e5-8033-00ac06be3045'),
(362, '9a662f4f-5388-11e5-8033-00ac06be3045'),
(363, '9a662f51-5388-11e5-8033-00ac06be3045'),
(364, '9a662f52-5388-11e5-8033-00ac06be3045'),
(365, '9a662f53-5388-11e5-8033-00ac06be3045'),
(366, '9a662f54-5388-11e5-8033-00ac06be3045'),
(367, '9a662f55-5388-11e5-8033-00ac06be3045'),
(368, '9a662f57-5388-11e5-8033-00ac06be3045'),
(369, '9a662f58-5388-11e5-8033-00ac06be3045'),
(370, '9a662f59-5388-11e5-8033-00ac06be3045'),
(371, '9a662f5a-5388-11e5-8033-00ac06be3045'),
(372, '9a662f5b-5388-11e5-8033-00ac06be3045'),
(373, '9a662f5e-5388-11e5-8033-00ac06be3045'),
(374, '9a662f5f-5388-11e5-8033-00ac06be3045'),
(375, '9a662f61-5388-11e5-8033-00ac06be3045'),
(376, '9a662f62-5388-11e5-8033-00ac06be3045'),
(377, '6b849bd9-5390-11e5-8033-00ac06be3045'),
(378, '6b849bda-5390-11e5-8033-00ac06be3045'),
(379, '6b849bdc-5390-11e5-8033-00ac06be3045'),
(380, '6b849bdd-5390-11e5-8033-00ac06be3045'),
(381, '6b849bdf-5390-11e5-8033-00ac06be3045'),
(382, '6b849be0-5390-11e5-8033-00ac06be3045'),
(383, 'b5fd3078-6beb-11e5-b374-00ac06be3045'),
(384, 'b5fd3079-6beb-11e5-b374-00ac06be3045'),
(385, 'b5fd307a-6beb-11e5-b374-00ac06be3045'),
(386, 'b5fd307b-6beb-11e5-b374-00ac06be3045'),
(387, 'b5fd307c-6beb-11e5-b374-00ac06be3045'),
(388, '6b849be2-5390-11e5-8033-00ac06be3045'),
(389, '6b849be3-5390-11e5-8033-00ac06be3045'),
(390, '6b849be4-5390-11e5-8033-00ac06be3045'),
(391, '6b849be5-5390-11e5-8033-00ac06be3045'),
(392, '6b849be6-5390-11e5-8033-00ac06be3045'),
(393, '6b849be7-5390-11e5-8033-00ac06be3045'),
(394, '6b849be9-5390-11e5-8033-00ac06be3045'),
(395, '6b849bea-5390-11e5-8033-00ac06be3045'),
(396, '6b849beb-5390-11e5-8033-00ac06be3045'),
(397, '6b849bec-5390-11e5-8033-00ac06be3045'),
(398, '6b849bed-5390-11e5-8033-00ac06be3045'),
(399, '6b849bf1-5390-11e5-8033-00ac06be3045'),
(400, '6b849bf2-5390-11e5-8033-00ac06be3045'),
(401, '6b849bf4-5390-11e5-8033-00ac06be3045'),
(402, '6b849bf5-5390-11e5-8033-00ac06be3045'),
(403, '6b849bf7-5390-11e5-8033-00ac06be3045'),
(404, '6b849bf8-5390-11e5-8033-00ac06be3045'),
(405, '6b849bfa-5390-11e5-8033-00ac06be3045'),
(406, '6b849bfb-5390-11e5-8033-00ac06be3045'),
(407, '6b849bfd-5390-11e5-8033-00ac06be3045'),
(408, '6b849bfe-5390-11e5-8033-00ac06be3045'),
(409, '0170455f-7990-11e5-aacb-00ac06be3045'),
(410, 'b5fd3059-6beb-11e5-b374-00ac06be3045'),
(411, 'b5fd305a-6beb-11e5-b374-00ac06be3045'),
(412, 'b5fd305b-6beb-11e5-b374-00ac06be3045'),
(413, 'b5fd305c-6beb-11e5-b374-00ac06be3045'),
(414, '6b849c00-5390-11e5-8033-00ac06be3045'),
(415, '6b849c01-5390-11e5-8033-00ac06be3045'),
(416, '6b849c02-5390-11e5-8033-00ac06be3045'),
(417, '6b849c03-5390-11e5-8033-00ac06be3045'),
(418, '6b849c04-5390-11e5-8033-00ac06be3045'),
(419, '6b849c06-5390-11e5-8033-00ac06be3045'),
(420, '6b849c07-5390-11e5-8033-00ac06be3045'),
(421, '6b849c08-5390-11e5-8033-00ac06be3045'),
(422, '6b849c09-5390-11e5-8033-00ac06be3045'),
(423, '6b849c0b-5390-11e5-8033-00ac06be3045'),
(424, '6b849c0c-5390-11e5-8033-00ac06be3045'),
(425, '6b849c0d-5390-11e5-8033-00ac06be3045'),
(426, '6b849c0e-5390-11e5-8033-00ac06be3045'),
(427, '6b849c0f-5390-11e5-8033-00ac06be3045'),
(428, '6b849c12-5390-11e5-8033-00ac06be3045'),
(429, '6b849c13-5390-11e5-8033-00ac06be3045'),
(430, '6b849c15-5390-11e5-8033-00ac06be3045'),
(431, '6b849c16-5390-11e5-8033-00ac06be3045'),
(432, '6b849c18-5390-11e5-8033-00ac06be3045'),
(433, 'b153d69b-5398-11e5-8033-00ac06be3045'),
(434, 'b153d69d-5398-11e5-8033-00ac06be3045'),
(435, 'b153d69e-5398-11e5-8033-00ac06be3045'),
(436, 'b153d6a0-5398-11e5-8033-00ac06be3045'),
(437, 'b153d6a1-5398-11e5-8033-00ac06be3045'),
(438, 'ff640545-681a-11e5-b7e9-00ac06be3045'),
(439, 'ff640546-681a-11e5-b7e9-00ac06be3045'),
(440, 'ff640547-681a-11e5-b7e9-00ac06be3045'),
(441, 'ff640548-681a-11e5-b7e9-00ac06be3045'),
(442, 'ff640549-681a-11e5-b7e9-00ac06be3045'),
(443, 'b153d6a3-5398-11e5-8033-00ac06be3045'),
(444, 'b153d6a4-5398-11e5-8033-00ac06be3045'),
(445, 'b153d6a5-5398-11e5-8033-00ac06be3045'),
(446, 'b153d6a6-5398-11e5-8033-00ac06be3045'),
(447, 'b153d6a8-5398-11e5-8033-00ac06be3045'),
(448, 'b153d6a9-5398-11e5-8033-00ac06be3045'),
(449, 'b153d6aa-5398-11e5-8033-00ac06be3045'),
(450, 'b153d6ab-5398-11e5-8033-00ac06be3045'),
(451, 'b153d6ac-5398-11e5-8033-00ac06be3045'),
(452, 'b153d6ad-5398-11e5-8033-00ac06be3045'),
(453, 'b153d6ae-5398-11e5-8033-00ac06be3045'),
(454, 'b153d6b0-5398-11e5-8033-00ac06be3045'),
(455, 'b153d6b1-5398-11e5-8033-00ac06be3045'),
(456, 'b153d6b2-5398-11e5-8033-00ac06be3045'),
(457, 'b153d6b3-5398-11e5-8033-00ac06be3045'),
(458, 'b153d6b4-5398-11e5-8033-00ac06be3045'),
(459, 'b153d6b5-5398-11e5-8033-00ac06be3045'),
(460, 'b153d6b6-5398-11e5-8033-00ac06be3045'),
(461, 'b153d6b8-5398-11e5-8033-00ac06be3045'),
(462, 'b153d6b9-5398-11e5-8033-00ac06be3045'),
(463, 'b153d6ba-5398-11e5-8033-00ac06be3045'),
(464, 'b153d6bb-5398-11e5-8033-00ac06be3045'),
(465, 'b153d6bc-5398-11e5-8033-00ac06be3045'),
(466, 'b153d6bd-5398-11e5-8033-00ac06be3045'),
(467, 'b153d6be-5398-11e5-8033-00ac06be3045'),
(468, '760aeee1-53b9-11e5-8033-00ac06be3045'),
(469, '760aeee2-53b9-11e5-8033-00ac06be3045'),
(470, '760aeee3-53b9-11e5-8033-00ac06be3045'),
(471, '760aeee4-53b9-11e5-8033-00ac06be3045'),
(472, '760aeee5-53b9-11e5-8033-00ac06be3045'),
(473, '760aeee6-53b9-11e5-8033-00ac06be3045'),
(474, '760aeee8-53b9-11e5-8033-00ac06be3045'),
(475, '760aeee9-53b9-11e5-8033-00ac06be3045'),
(476, '760aeeea-53b9-11e5-8033-00ac06be3045'),
(477, '760aeeeb-53b9-11e5-8033-00ac06be3045'),
(478, '760aeeed-53b9-11e5-8033-00ac06be3045'),
(479, '760aeeee-53b9-11e5-8033-00ac06be3045'),
(480, '760aeeef-53b9-11e5-8033-00ac06be3045'),
(481, '760aeef0-53b9-11e5-8033-00ac06be3045'),
(482, '760aeef1-53b9-11e5-8033-00ac06be3045'),
(483, '760aeef2-53b9-11e5-8033-00ac06be3045'),
(484, '760aeef3-53b9-11e5-8033-00ac06be3045'),
(485, '760aeef4-53b9-11e5-8033-00ac06be3045'),
(486, '760aeef6-53b9-11e5-8033-00ac06be3045'),
(487, '760aeef7-53b9-11e5-8033-00ac06be3045'),
(488, '760aeef8-53b9-11e5-8033-00ac06be3045'),
(489, '760aeef9-53b9-11e5-8033-00ac06be3045'),
(490, '760aeefb-53b9-11e5-8033-00ac06be3045'),
(491, '760aeefc-53b9-11e5-8033-00ac06be3045'),
(492, '760aeefd-53b9-11e5-8033-00ac06be3045'),
(493, '760aeefe-53b9-11e5-8033-00ac06be3045'),
(494, '760aeeff-53b9-11e5-8033-00ac06be3045'),
(495, '760aef00-53b9-11e5-8033-00ac06be3045'),
(496, '760aef01-53b9-11e5-8033-00ac06be3045'),
(497, '760aef02-53b9-11e5-8033-00ac06be3045'),
(498, '760aef03-53b9-11e5-8033-00ac06be3045'),
(499, '760aef04-53b9-11e5-8033-00ac06be3045'),
(500, '760aef05-53b9-11e5-8033-00ac06be3045'),
(501, '760aef06-53b9-11e5-8033-00ac06be3045'),
(502, '760aef08-53b9-11e5-8033-00ac06be3045'),
(503, '760aef09-53b9-11e5-8033-00ac06be3045'),
(504, '760aef0a-53b9-11e5-8033-00ac06be3045'),
(505, '760aef0b-53b9-11e5-8033-00ac06be3045'),
(506, '1aa5692b-5de2-11e5-9708-00ac06be3045'),
(507, '1aa5692c-5de2-11e5-9708-00ac06be3045'),
(508, '760aef0d-53b9-11e5-8033-00ac06be3045'),
(509, '760aef0e-53b9-11e5-8033-00ac06be3045'),
(510, '760aef0f-53b9-11e5-8033-00ac06be3045'),
(511, '760aef10-53b9-11e5-8033-00ac06be3045'),
(512, '760aef11-53b9-11e5-8033-00ac06be3045'),
(513, '760aef12-53b9-11e5-8033-00ac06be3045'),
(514, '760aef13-53b9-11e5-8033-00ac06be3045'),
(515, '760aef14-53b9-11e5-8033-00ac06be3045'),
(516, '760aef15-53b9-11e5-8033-00ac06be3045'),
(517, '760aef16-53b9-11e5-8033-00ac06be3045'),
(518, '760aef17-53b9-11e5-8033-00ac06be3045'),
(519, '760aef18-53b9-11e5-8033-00ac06be3045'),
(520, '760aef19-53b9-11e5-8033-00ac06be3045'),
(521, '760aef1a-53b9-11e5-8033-00ac06be3045'),
(522, '760aef1b-53b9-11e5-8033-00ac06be3045'),
(523, '760aef1d-53b9-11e5-8033-00ac06be3045'),
(524, '760aef1e-53b9-11e5-8033-00ac06be3045'),
(525, '760aef1f-53b9-11e5-8033-00ac06be3045'),
(526, 'f938708d-53cf-11e5-8033-00ac06be3045'),
(527, 'f938708e-53cf-11e5-8033-00ac06be3045'),
(528, 'f938708f-53cf-11e5-8033-00ac06be3045'),
(529, 'f9387090-53cf-11e5-8033-00ac06be3045'),
(530, 'f9387091-53cf-11e5-8033-00ac06be3045'),
(531, 'f9387092-53cf-11e5-8033-00ac06be3045'),
(532, 'f9387093-53cf-11e5-8033-00ac06be3045'),
(533, 'f9387094-53cf-11e5-8033-00ac06be3045'),
(534, 'f9387095-53cf-11e5-8033-00ac06be3045'),
(535, '5e735a63-3f53-11e5-8a4f-00ac06be3045'),
(536, '5e735a64-3f53-11e5-8a4f-00ac06be3045'),
(537, '5e735a65-3f53-11e5-8a4f-00ac06be3045'),
(538, '5e735a66-3f53-11e5-8a4f-00ac06be3045'),
(539, '5e735a67-3f53-11e5-8a4f-00ac06be3045'),
(540, '5e735a68-3f53-11e5-8a4f-00ac06be3045'),
(541, '476442c6-3f6a-11e5-8a4f-00ac06be3045'),
(542, '476442c7-3f6a-11e5-8a4f-00ac06be3045'),
(543, '476442c9-3f6a-11e5-8a4f-00ac06be3045'),
(544, '476442ca-3f6a-11e5-8a4f-00ac06be3045'),
(545, '476442cb-3f6a-11e5-8a4f-00ac06be3045'),
(546, '476442cc-3f6a-11e5-8a4f-00ac06be3045'),
(547, '476442cd-3f6a-11e5-8a4f-00ac06be3045'),
(548, '476442ce-3f6a-11e5-8a4f-00ac06be3045'),
(549, '476442cf-3f6a-11e5-8a4f-00ac06be3045'),
(550, '476442d0-3f6a-11e5-8a4f-00ac06be3045'),
(551, '9b2fab24-3fe8-11e5-8a4f-00ac06be3045'),
(552, '9b2fab25-3fe8-11e5-8a4f-00ac06be3045'),
(553, '9b2fab26-3fe8-11e5-8a4f-00ac06be3045'),
(554, '9b2fab27-3fe8-11e5-8a4f-00ac06be3045'),
(555, '9b2fab28-3fe8-11e5-8a4f-00ac06be3045'),
(556, '9b2fab29-3fe8-11e5-8a4f-00ac06be3045'),
(557, '9b2fab2a-3fe8-11e5-8a4f-00ac06be3045'),
(558, '9b2fab2b-3fe8-11e5-8a4f-00ac06be3045'),
(559, '7c830c48-4010-11e5-8a4f-00ac06be3045'),
(560, '7c830c49-4010-11e5-8a4f-00ac06be3045'),
(561, '7c830c4a-4010-11e5-8a4f-00ac06be3045'),
(562, '7c830c4b-4010-11e5-8a4f-00ac06be3045'),
(563, '7c830c4c-4010-11e5-8a4f-00ac06be3045'),
(564, '7c830c4e-4010-11e5-8a4f-00ac06be3045'),
(565, '7c830c4f-4010-11e5-8a4f-00ac06be3045'),
(566, '7c830c50-4010-11e5-8a4f-00ac06be3045'),
(567, '7c830c51-4010-11e5-8a4f-00ac06be3045'),
(568, '7c830c52-4010-11e5-8a4f-00ac06be3045'),
(569, '7c830c53-4010-11e5-8a4f-00ac06be3045'),
(570, '7c830c55-4010-11e5-8a4f-00ac06be3045'),
(571, '7c830c57-4010-11e5-8a4f-00ac06be3045'),
(572, '7c830c58-4010-11e5-8a4f-00ac06be3045'),
(573, '7c830c59-4010-11e5-8a4f-00ac06be3045'),
(574, '7c830c5a-4010-11e5-8a4f-00ac06be3045'),
(575, '7c830c5b-4010-11e5-8a4f-00ac06be3045'),
(576, '7c830c5c-4010-11e5-8a4f-00ac06be3045'),
(577, '7c830c5d-4010-11e5-8a4f-00ac06be3045'),
(578, '7c830c5e-4010-11e5-8a4f-00ac06be3045'),
(579, '7c830c5f-4010-11e5-8a4f-00ac06be3045'),
(580, '64053bad-5c7c-11e5-9708-00ac06be3045'),
(581, '64053bae-5c7c-11e5-9708-00ac06be3045'),
(582, '64053bb0-5c7c-11e5-9708-00ac06be3045'),
(583, '64053bb1-5c7c-11e5-9708-00ac06be3045'),
(584, '64053bb2-5c7c-11e5-9708-00ac06be3045'),
(585, '64053bb3-5c7c-11e5-9708-00ac06be3045'),
(586, '64053bb4-5c7c-11e5-9708-00ac06be3045'),
(587, '64053bb5-5c7c-11e5-9708-00ac06be3045'),
(588, '64053bb6-5c7c-11e5-9708-00ac06be3045'),
(589, '64053bb7-5c7c-11e5-9708-00ac06be3045'),
(590, '64053bb8-5c7c-11e5-9708-00ac06be3045'),
(591, '64053bb9-5c7c-11e5-9708-00ac06be3045'),
(592, '64053bba-5c7c-11e5-9708-00ac06be3045'),
(593, '64053bbd-5c7c-11e5-9708-00ac06be3045'),
(594, '64053bbe-5c7c-11e5-9708-00ac06be3045'),
(595, '64053bbf-5c7c-11e5-9708-00ac06be3045'),
(596, '64053bc0-5c7c-11e5-9708-00ac06be3045'),
(597, 'd90ff3b3-5d01-11e5-9708-00ac06be3045'),
(598, 'd90ff3b4-5d01-11e5-9708-00ac06be3045'),
(599, 'd90ff3b5-5d01-11e5-9708-00ac06be3045'),
(600, 'd90ff3b6-5d01-11e5-9708-00ac06be3045'),
(601, 'd90ff3b7-5d01-11e5-9708-00ac06be3045'),
(602, '1aa5692e-5de2-11e5-9708-00ac06be3045'),
(603, '1aa5692f-5de2-11e5-9708-00ac06be3045'),
(604, '1aa56930-5de2-11e5-9708-00ac06be3045'),
(605, '1aa56931-5de2-11e5-9708-00ac06be3045'),
(606, '1aa56932-5de2-11e5-9708-00ac06be3045'),
(607, '1aa56933-5de2-11e5-9708-00ac06be3045'),
(608, '32118fc8-5f83-11e5-9708-00ac06be3045'),
(609, '32118fc9-5f83-11e5-9708-00ac06be3045'),
(610, '32118fca-5f83-11e5-9708-00ac06be3045'),
(611, 'd3ba0c91-6114-11e5-9708-00ac06be3045'),
(612, 'd3ba0c92-6114-11e5-9708-00ac06be3045'),
(613, 'd3ba0c93-6114-11e5-9708-00ac06be3045'),
(614, 'd3ba0c85-6114-11e5-9708-00ac06be3045'),
(615, 'd3ba0c86-6114-11e5-9708-00ac06be3045'),
(616, 'd3ba0c87-6114-11e5-9708-00ac06be3045'),
(617, 'd3ba0c88-6114-11e5-9708-00ac06be3045'),
(618, 'd3ba0c89-6114-11e5-9708-00ac06be3045'),
(619, 'd3ba0c8b-6114-11e5-9708-00ac06be3045'),
(620, 'd3ba0c8c-6114-11e5-9708-00ac06be3045'),
(621, 'd3ba0c8d-6114-11e5-9708-00ac06be3045'),
(622, 'd3ba0c8e-6114-11e5-9708-00ac06be3045'),
(623, 'd3ba0c8f-6114-11e5-9708-00ac06be3045'),
(624, 'd3ba0c90-6114-11e5-9708-00ac06be3045'),
(625, '5dcecf5c-8cf5-11e5-9c35-00ac06be3045'),
(626, '5dcecf5d-8cf5-11e5-9c35-00ac06be3045'),
(627, '5dcecf5e-8cf5-11e5-9c35-00ac06be3045'),
(628, '5dcecf5f-8cf5-11e5-9c35-00ac06be3045'),
(629, '5dcecf60-8cf5-11e5-9c35-00ac06be3045'),
(630, '5dcecf62-8cf5-11e5-9c35-00ac06be3045'),
(631, '5dcecf63-8cf5-11e5-9c35-00ac06be3045'),
(632, 'fd0f9d4e-8d14-11e5-9c35-00ac06be3045'),
(633, 'fd0f9d4f-8d14-11e5-9c35-00ac06be3045'),
(634, 'fd0f9d50-8d14-11e5-9c35-00ac06be3045'),
(635, 'fd0f9d51-8d14-11e5-9c35-00ac06be3045'),
(636, 'fd0f9d52-8d14-11e5-9c35-00ac06be3045'),
(637, 'fd0f9d53-8d14-11e5-9c35-00ac06be3045'),
(638, 'fd0f9d54-8d14-11e5-9c35-00ac06be3045'),
(639, 'fd0f9d55-8d14-11e5-9c35-00ac06be3045'),
(640, 'fd0f9d56-8d14-11e5-9c35-00ac06be3045'),
(641, '8fa576b3-7411-11e5-aacb-00ac06be3045'),
(642, '8fa576b5-7411-11e5-aacb-00ac06be3045'),
(643, '8fa576b7-7411-11e5-aacb-00ac06be3045'),
(644, '8fa576b9-7411-11e5-aacb-00ac06be3045'),
(645, '8fa576bb-7411-11e5-aacb-00ac06be3045'),
(646, '8fa576bd-7411-11e5-aacb-00ac06be3045'),
(647, 'fd154ae1-7ca4-11e5-aacb-00ac06be3045'),
(648, 'fd154ae2-7ca4-11e5-aacb-00ac06be3045'),
(649, 'fd154ae3-7ca4-11e5-aacb-00ac06be3045'),
(650, 'fd154ae4-7ca4-11e5-aacb-00ac06be3045'),
(651, 'fd154ae5-7ca4-11e5-aacb-00ac06be3045'),
(652, 'fd154ae6-7ca4-11e5-aacb-00ac06be3045'),
(653, 'fd154ae7-7ca4-11e5-aacb-00ac06be3045'),
(654, 'fd154ae8-7ca4-11e5-aacb-00ac06be3045'),
(655, 'fd154aea-7ca4-11e5-aacb-00ac06be3045'),
(656, 'fd154aeb-7ca4-11e5-aacb-00ac06be3045'),
(657, 'fd154aec-7ca4-11e5-aacb-00ac06be3045'),
(658, 'fd154aed-7ca4-11e5-aacb-00ac06be3045'),
(659, 'fd154aee-7ca4-11e5-aacb-00ac06be3045'),
(660, 'fd154aef-7ca4-11e5-aacb-00ac06be3045'),
(661, '9d93e129-7e57-11e5-aacb-00ac06be3045'),
(662, '9d93e12a-7e57-11e5-aacb-00ac06be3045'),
(663, '9d93e12b-7e57-11e5-aacb-00ac06be3045'),
(664, '9d93e12c-7e57-11e5-aacb-00ac06be3045'),
(665, '9d93e12d-7e57-11e5-aacb-00ac06be3045'),
(666, '9d93e134-7e57-11e5-aacb-00ac06be3045'),
(667, '9d93e135-7e57-11e5-aacb-00ac06be3045'),
(668, '9d93e136-7e57-11e5-aacb-00ac06be3045'),
(669, '9d93e137-7e57-11e5-aacb-00ac06be3045'),
(670, '9d93e138-7e57-11e5-aacb-00ac06be3045'),
(671, '9d93e139-7e57-11e5-aacb-00ac06be3045'),
(672, '9d93e13a-7e57-11e5-aacb-00ac06be3045'),
(673, '9d93e13b-7e57-11e5-aacb-00ac06be3045'),
(674, '9d93e13c-7e57-11e5-aacb-00ac06be3045'),
(675, '9d93e13d-7e57-11e5-aacb-00ac06be3045'),
(676, 'bdcf7b4e-7ed1-11e5-aacb-00ac06be3045'),
(677, 'bdcf7b4f-7ed1-11e5-aacb-00ac06be3045'),
(678, 'bdcf7b50-7ed1-11e5-aacb-00ac06be3045'),
(679, 'bdcf7b51-7ed1-11e5-aacb-00ac06be3045'),
(680, 'bdcf7b52-7ed1-11e5-aacb-00ac06be3045'),
(681, 'bdcf7b53-7ed1-11e5-aacb-00ac06be3045'),
(682, 'bdcf7b54-7ed1-11e5-aacb-00ac06be3045'),
(683, 'bdcf7b55-7ed1-11e5-aacb-00ac06be3045'),
(684, 'bdcf7b56-7ed1-11e5-aacb-00ac06be3045'),
(685, 'bdcf7b57-7ed1-11e5-aacb-00ac06be3045'),
(686, '835aadab-c04e-11e5-b966-00ac06be3045'),
(687, 'bdcf7b59-7ed1-11e5-aacb-00ac06be3045'),
(688, 'bdcf7b5a-7ed1-11e5-aacb-00ac06be3045'),
(689, 'bdcf7b5b-7ed1-11e5-aacb-00ac06be3045'),
(690, 'bdcf7b5c-7ed1-11e5-aacb-00ac06be3045'),
(691, 'bdcf7b5d-7ed1-11e5-aacb-00ac06be3045'),
(692, 'bdcf7b5e-7ed1-11e5-aacb-00ac06be3045'),
(693, 'bdcf7b5f-7ed1-11e5-aacb-00ac06be3045'),
(694, 'bdcf7b60-7ed1-11e5-aacb-00ac06be3045'),
(695, 'bdcf7b61-7ed1-11e5-aacb-00ac06be3045'),
(696, 'bdcf7b62-7ed1-11e5-aacb-00ac06be3045'),
(697, 'b39873f7-8203-11e5-aacb-00ac06be3045'),
(698, 'b39873f8-8203-11e5-aacb-00ac06be3045'),
(699, 'b39873f9-8203-11e5-aacb-00ac06be3045'),
(700, 'b39873fa-8203-11e5-aacb-00ac06be3045'),
(701, 'b39873fb-8203-11e5-aacb-00ac06be3045'),
(702, 'b39873fc-8203-11e5-aacb-00ac06be3045'),
(703, 'bf11ae73-8475-11e5-aacb-00ac06be3045'),
(704, 'ab3eb16b-86c1-11e5-aacb-00ac06be3045'),
(705, 'ab3eb16c-86c1-11e5-aacb-00ac06be3045'),
(706, 'ab3eb16d-86c1-11e5-aacb-00ac06be3045'),
(707, 'ab3eb16e-86c1-11e5-aacb-00ac06be3045'),
(708, 'ab3eb16f-86c1-11e5-aacb-00ac06be3045'),
(709, 'ab3eb170-86c1-11e5-aacb-00ac06be3045'),
(710, 'ab3eb171-86c1-11e5-aacb-00ac06be3045'),
(711, 'ab3eb172-86c1-11e5-aacb-00ac06be3045'),
(712, 'ab3eb173-86c1-11e5-aacb-00ac06be3045'),
(713, 'ab3eb174-86c1-11e5-aacb-00ac06be3045'),
(714, 'ab3eb175-86c1-11e5-aacb-00ac06be3045'),
(715, 'ab3eb176-86c1-11e5-aacb-00ac06be3045'),
(716, 'ab3eb178-86c1-11e5-aacb-00ac06be3045'),
(717, 'ab3eb179-86c1-11e5-aacb-00ac06be3045'),
(718, 'bf11ae76-8475-11e5-aacb-00ac06be3045'),
(719, 'c5dd1282-87b9-11e5-aacb-00ac06be3045'),
(720, 'c5dd1283-87b9-11e5-aacb-00ac06be3045'),
(721, 'c5dd1284-87b9-11e5-aacb-00ac06be3045'),
(722, 'c5dd1285-87b9-11e5-aacb-00ac06be3045'),
(723, 'c5dd1286-87b9-11e5-aacb-00ac06be3045'),
(724, 'c5dd1287-87b9-11e5-aacb-00ac06be3045'),
(725, 'c5dd1288-87b9-11e5-aacb-00ac06be3045'),
(726, 'c5dd1289-87b9-11e5-aacb-00ac06be3045'),
(727, 'c5dd128a-87b9-11e5-aacb-00ac06be3045'),
(728, 'c5dd128b-87b9-11e5-aacb-00ac06be3045'),
(729, 'c5dd128c-87b9-11e5-aacb-00ac06be3045'),
(730, 'c5dd127e-87b9-11e5-aacb-00ac06be3045'),
(731, 'c5dd127f-87b9-11e5-aacb-00ac06be3045'),
(732, 'c5dd1281-87b9-11e5-aacb-00ac06be3045'),
(733, 'b5fd305e-6beb-11e5-b374-00ac06be3045'),
(734, 'b5fd305f-6beb-11e5-b374-00ac06be3045'),
(735, 'b5fd3060-6beb-11e5-b374-00ac06be3045'),
(736, 'b5fd3061-6beb-11e5-b374-00ac06be3045'),
(737, 'b5fd3062-6beb-11e5-b374-00ac06be3045'),
(738, 'b5fd3063-6beb-11e5-b374-00ac06be3045'),
(739, 'b5fd3064-6beb-11e5-b374-00ac06be3045'),
(740, 'b5fd3065-6beb-11e5-b374-00ac06be3045'),
(741, 'b5fd3066-6beb-11e5-b374-00ac06be3045'),
(742, 'b5fd3067-6beb-11e5-b374-00ac06be3045'),
(743, 'b5fd3068-6beb-11e5-b374-00ac06be3045'),
(744, 'b5fd3069-6beb-11e5-b374-00ac06be3045'),
(745, 'b5fd306b-6beb-11e5-b374-00ac06be3045'),
(746, 'b5fd306c-6beb-11e5-b374-00ac06be3045'),
(747, 'b5fd306d-6beb-11e5-b374-00ac06be3045'),
(748, 'b5fd306e-6beb-11e5-b374-00ac06be3045'),
(749, 'b5fd306f-6beb-11e5-b374-00ac06be3045'),
(750, 'b5fd3070-6beb-11e5-b374-00ac06be3045'),
(751, 'b5fd3072-6beb-11e5-b374-00ac06be3045'),
(752, 'b5fd3073-6beb-11e5-b374-00ac06be3045'),
(753, 'b5fd3074-6beb-11e5-b374-00ac06be3045'),
(754, 'b5fd3075-6beb-11e5-b374-00ac06be3045'),
(755, 'b5fd3076-6beb-11e5-b374-00ac06be3045'),
(756, 'b5fd3077-6beb-11e5-b374-00ac06be3045'),
(757, '2b72975e-70b2-11e5-b374-00ac06be3045'),
(758, '2b72975f-70b2-11e5-b374-00ac06be3045'),
(759, '2b729760-70b2-11e5-b374-00ac06be3045'),
(760, '2b729761-70b2-11e5-b374-00ac06be3045'),
(761, '2b729763-70b2-11e5-b374-00ac06be3045'),
(762, '2b729764-70b2-11e5-b374-00ac06be3045'),
(763, '2b729765-70b2-11e5-b374-00ac06be3045'),
(764, '2b729766-70b2-11e5-b374-00ac06be3045'),
(765, '2b729767-70b2-11e5-b374-00ac06be3045'),
(766, '2b729768-70b2-11e5-b374-00ac06be3045'),
(767, '2b729769-70b2-11e5-b374-00ac06be3045'),
(768, '2b72976a-70b2-11e5-b374-00ac06be3045'),
(769, '482241d8-6298-11e5-b48a-00ac06be3045'),
(770, '482241d9-6298-11e5-b48a-00ac06be3045'),
(771, '482241da-6298-11e5-b48a-00ac06be3045'),
(772, '482241db-6298-11e5-b48a-00ac06be3045'),
(773, '482241dc-6298-11e5-b48a-00ac06be3045'),
(774, '482241dd-6298-11e5-b48a-00ac06be3045'),
(775, '482241de-6298-11e5-b48a-00ac06be3045'),
(776, '482241df-6298-11e5-b48a-00ac06be3045'),
(777, '482241e0-6298-11e5-b48a-00ac06be3045'),
(778, '482241e1-6298-11e5-b48a-00ac06be3045'),
(779, '482241e2-6298-11e5-b48a-00ac06be3045'),
(780, '482241e3-6298-11e5-b48a-00ac06be3045'),
(781, 'da4ae9e8-3480-11e6-b6f5-00ac06be3045'),
(782, 'da4ae9e9-3480-11e6-b6f5-00ac06be3045'),
(783, 'da4ae9ea-3480-11e6-b6f5-00ac06be3045'),
(784, 'da4ae9eb-3480-11e6-b6f5-00ac06be3045'),
(785, 'da4ae9ec-3480-11e6-b6f5-00ac06be3045'),
(786, 'da4ae9ed-3480-11e6-b6f5-00ac06be3045'),
(787, 'da4ae9ee-3480-11e6-b6f5-00ac06be3045'),
(788, 'da4ae9ef-3480-11e6-b6f5-00ac06be3045'),
(789, 'ff640539-681a-11e5-b7e9-00ac06be3045'),
(790, 'ff64053a-681a-11e5-b7e9-00ac06be3045'),
(791, 'ff64053b-681a-11e5-b7e9-00ac06be3045'),
(792, 'ff64053c-681a-11e5-b7e9-00ac06be3045'),
(793, 'ff64053d-681a-11e5-b7e9-00ac06be3045'),
(794, 'ff64053e-681a-11e5-b7e9-00ac06be3045'),
(795, 'ff64053f-681a-11e5-b7e9-00ac06be3045'),
(796, 'ff640540-681a-11e5-b7e9-00ac06be3045'),
(797, 'ff640541-681a-11e5-b7e9-00ac06be3045'),
(798, 'ff640542-681a-11e5-b7e9-00ac06be3045'),
(799, 'ff640543-681a-11e5-b7e9-00ac06be3045'),
(800, 'ff640544-681a-11e5-b7e9-00ac06be3045'),
(801, '482241d4-6298-11e5-b48a-00ac06be3045'),
(802, '71150849-40f5-11e5-bd34-00ac06be3045'),
(803, '7115084a-40f5-11e5-bd34-00ac06be3045'),
(804, '7115084b-40f5-11e5-bd34-00ac06be3045'),
(805, '7115084c-40f5-11e5-bd34-00ac06be3045'),
(806, '7115084d-40f5-11e5-bd34-00ac06be3045'),
(807, '7115084e-40f5-11e5-bd34-00ac06be3045'),
(808, '5c59b37f-613c-11e5-9708-00ac06be3045'),
(809, '71150850-40f5-11e5-bd34-00ac06be3045'),
(810, '71150851-40f5-11e5-bd34-00ac06be3045'),
(811, '71150852-40f5-11e5-bd34-00ac06be3045'),
(812, '71150853-40f5-11e5-bd34-00ac06be3045'),
(813, '71150854-40f5-11e5-bd34-00ac06be3045'),
(814, '71150855-40f5-11e5-bd34-00ac06be3045'),
(815, 'fa1b4de9-7316-11e5-bd5c-00ac06be3045'),
(816, 'fa1b4dea-7316-11e5-bd5c-00ac06be3045'),
(817, 'fa1b4deb-7316-11e5-bd5c-00ac06be3045'),
(818, 'fa1b4dec-7316-11e5-bd5c-00ac06be3045'),
(819, 'fa1b4ded-7316-11e5-bd5c-00ac06be3045'),
(820, 'fa1b4dee-7316-11e5-bd5c-00ac06be3045'),
(821, 'fa1b4df0-7316-11e5-bd5c-00ac06be3045'),
(822, 'fa1b4df1-7316-11e5-bd5c-00ac06be3045'),
(823, 'fa1b4df2-7316-11e5-bd5c-00ac06be3045'),
(824, 'fa1b4df3-7316-11e5-bd5c-00ac06be3045'),
(825, 'fa1b4df5-7316-11e5-bd5c-00ac06be3045'),
(826, 'fa1b4df6-7316-11e5-bd5c-00ac06be3045'),
(827, 'cd4ce158-675b-11e5-be92-00ac06be3045'),
(828, 'cd4ce159-675b-11e5-be92-00ac06be3045'),
(829, 'cd4ce15a-675b-11e5-be92-00ac06be3045'),
(830, 'cd4ce15c-675b-11e5-be92-00ac06be3045'),
(831, 'cd4ce15e-675b-11e5-be92-00ac06be3045'),
(832, 'cd4ce15f-675b-11e5-be92-00ac06be3045'),
(833, 'cd4ce161-675b-11e5-be92-00ac06be3045'),
(834, 'cd4ce162-675b-11e5-be92-00ac06be3045'),
(835, 'cd4ce163-675b-11e5-be92-00ac06be3045'),
(836, '5ff07d50-680b-11e5-be92-00ac06be3045'),
(837, '5ff07d51-680b-11e5-be92-00ac06be3045'),
(838, '5ff07d52-680b-11e5-be92-00ac06be3045'),
(839, '5ff07d53-680b-11e5-be92-00ac06be3045'),
(840, '5ff07d54-680b-11e5-be92-00ac06be3045'),
(841, '5ff07d55-680b-11e5-be92-00ac06be3045'),
(842, '5ff07d56-680b-11e5-be92-00ac06be3045'),
(843, '5ff07d57-680b-11e5-be92-00ac06be3045'),
(844, '5ff07d58-680b-11e5-be92-00ac06be3045'),
(845, 'ff64052a-681a-11e5-b7e9-00ac06be3045'),
(846, 'ff64052b-681a-11e5-b7e9-00ac06be3045'),
(847, '38006f82-6818-11e5-be92-00ac06be3045'),
(848, '38006f83-6818-11e5-be92-00ac06be3045');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions_variant`
--

CREATE TABLE `oc_relatedoptions_variant` (
  `relatedoptions_variant_id` int(11) NOT NULL,
  `relatedoptions_variant_name` char(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_relatedoptions_variant`
--

INSERT INTO `oc_relatedoptions_variant` (`relatedoptions_variant_id`, `relatedoptions_variant_name`) VALUES
(1, 'Розмір, Колір');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions_variant_option`
--

CREATE TABLE `oc_relatedoptions_variant_option` (
  `relatedoptions_variant_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_relatedoptions_variant_option`
--

INSERT INTO `oc_relatedoptions_variant_option` (`relatedoptions_variant_id`, `option_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_relatedoptions_variant_product`
--

CREATE TABLE `oc_relatedoptions_variant_product` (
  `relatedoptions_variant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `relatedoptions_use` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_relatedoptions_variant_product`
--

INSERT INTO `oc_relatedoptions_variant_product` (`relatedoptions_variant_id`, `product_id`, `relatedoptions_use`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(1, 5, 1),
(1, 6, 1),
(1, 7, 1),
(1, 8, 1),
(1, 9, 1),
(1, 10, 1),
(0, 11, 0),
(1, 12, 1),
(1, 13, 1),
(1, 14, 1),
(1, 15, 1),
(1, 16, 1),
(1, 17, 1),
(1, 18, 1),
(1, 19, 1),
(1, 20, 1),
(1, 21, 1),
(1, 22, 1),
(1, 23, 1),
(1, 24, 1),
(1, 25, 1),
(1, 26, 1),
(1, 27, 1),
(1, 28, 1),
(1, 29, 1),
(1, 30, 1),
(1, 31, 1),
(1, 32, 1),
(1, 33, 1),
(1, 34, 1),
(1, 35, 1),
(1, 36, 1),
(1, 37, 1),
(1, 38, 1),
(1, 39, 1),
(1, 40, 1),
(1, 41, 1),
(1, 42, 1),
(1, 43, 1),
(1, 44, 1),
(1, 45, 1),
(1, 46, 1),
(1, 47, 1),
(1, 48, 1),
(1, 49, 1),
(1, 50, 1),
(1, 51, 1),
(1, 52, 1),
(1, 53, 1),
(1, 54, 1),
(1, 55, 1),
(1, 56, 1),
(1, 57, 1),
(1, 58, 1),
(1, 59, 1),
(1, 60, 1),
(1, 61, 1),
(1, 62, 1),
(1, 63, 1),
(1, 64, 1),
(1, 65, 1),
(1, 66, 1),
(1, 67, 1),
(1, 68, 1),
(1, 69, 1),
(1, 70, 1),
(0, 71, 0),
(0, 72, 0),
(1, 73, 1),
(1, 74, 1),
(1, 75, 1),
(1, 76, 1),
(1, 77, 1),
(1, 78, 1),
(1, 79, 1),
(0, 80, 0),
(0, 81, 0),
(0, 82, 0),
(1, 83, 1),
(1, 84, 1),
(0, 85, 0),
(0, 86, 0),
(1, 87, 1),
(1, 88, 1),
(1, 89, 1),
(1, 90, 1),
(1, 91, 1),
(1, 92, 1),
(1, 93, 1),
(1, 94, 1),
(1, 95, 1),
(0, 96, 0),
(1, 97, 1),
(1, 98, 1),
(0, 99, 0),
(0, 100, 0),
(1, 101, 1),
(1, 102, 1),
(1, 103, 1),
(1, 104, 1),
(1, 105, 1),
(1, 106, 1),
(1, 107, 1),
(1, 108, 1),
(1, 109, 1),
(1, 110, 1),
(1, 111, 1),
(1, 112, 1),
(1, 113, 1),
(1, 114, 1),
(1, 115, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Возвращены средства'),
(2, 1, 'Выдан в кредит'),
(3, 1, 'Отправлена замена (отправлен другой товар для замены)'),
(1, 2, 'Refunded'),
(2, 2, 'Credit Issued'),
(3, 2, 'Replacement Sent'),
(1, 3, 'Возвращены средства'),
(2, 3, 'Выдан в кредит'),
(3, 3, 'Отправлена замена (отправлен другой товар для замены)'),
(1, 4, 'Возвращены средства'),
(2, 4, 'Выдан в кредит'),
(3, 4, 'Отправлена замена (отправлен другой товар для замены)');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Получен/доставлен неисправным (сломанным)'),
(2, 1, 'Получен не тот (ошибочный) товар'),
(4, 1, 'Ошибочный, пожалуйста, укажите/приложите подробности'),
(5, 1, 'Другое (другая причина), пожалуйста, укажите/приложите подробности'),
(1, 2, 'Dead On Arrival'),
(2, 2, 'Received Wrong Item'),
(4, 2, 'Faulty, please supply details'),
(5, 2, 'Other, please supply details'),
(1, 3, 'Получен/доставлен неисправным (сломанным)'),
(2, 3, 'Получен не тот (ошибочный) товар'),
(4, 3, 'Ошибочный, пожалуйста, укажите/приложите подробности'),
(5, 3, 'Другое (другая причина), пожалуйста, укажите/приложите подробности'),
(1, 4, 'Получен/доставлен неисправным (сломанным)'),
(2, 4, 'Получен не тот (ошибочный) товар'),
(4, 4, 'Ошибочный, пожалуйста, укажите/приложите подробности'),
(5, 4, 'Другое (другая причина), пожалуйста, укажите/приложите подробности');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Рассматриваемый / находящийся в '),
(3, 1, 'Готов (к отправке) / или Заверше'),
(2, 1, 'Заказ "висит" в ожидании поступл'),
(1, 2, 'Pending'),
(3, 2, 'Complete'),
(2, 2, 'Awaiting Products'),
(1, 3, 'Рассматриваемый / находящийся в '),
(3, 3, 'Готов (к отправке) / или Заверше'),
(2, 3, 'Заказ "висит" в ожидании поступл'),
(1, 4, 'Рассматриваемый / находящийся в '),
(3, 4, 'Готов (к отправке) / или Заверше'),
(2, 4, 'Заказ "висит" в ожидании поступл');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` longtext NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(2, 0, 'sub_total', 'sub_total_sort_order', '1', 0),
(3, 0, 'sub_total', 'sub_total_status', '1', 0),
(5, 0, 'total', 'total_sort_order', '9', 0),
(6, 0, 'total', 'total_status', '1', 0),
(3410, 0, 'free', 'free_status', '1', 0),
(8, 0, 'free_checkout', 'free_checkout_sort_order', '1', 0),
(9, 0, 'cod', 'cod_sort_order', '5', 0),
(10, 0, 'cod', 'cod_total', '0.01', 0),
(11, 0, 'cod', 'cod_order_status_id', '1', 0),
(12, 0, 'cod', 'cod_geo_zone_id', '0', 0),
(13, 0, 'cod', 'cod_status', '1', 0),
(3411, 0, 'free', 'free_sort_order', '', 0),
(3486, 0, 'flat', 'flat_sort_order', '1', 0),
(3485, 0, 'flat', 'flat_status', '0', 0),
(3484, 0, 'flat', 'flat_geo_zone_id', '0', 0),
(3483, 0, 'flat', 'flat_tax_class_id', '9', 0),
(3482, 0, 'flat', 'flat_cost', '5.00', 0),
(146, 0, 'category', 'category_status', '1', 0),
(158, 0, 'account', 'account_status', '1', 0),
(159, 0, 'affiliate', 'affiliate_status', '1', 0),
(3409, 0, 'free', 'free_geo_zone_id', '0', 0),
(3408, 0, 'free', 'free_total', '0.01', 0),
(103, 0, 'free_checkout', 'free_checkout_status', '1', 0),
(104, 0, 'free_checkout', 'free_checkout_order_status_id', '1', 0),
(4131, 0, 'related_options', 'related_options', '{"update_quantity":"1","update_options":"1","subtract_stock":"0","required":"0","show_clear_options":"0","select_first":"0","ro_use_variants":"1","related_options_version":"2.1.2"}', 1),
(4044, 0, 'coupon', 'coupon_sort_order', '2', 0),
(4043, 0, 'coupon', 'coupon_status', '1', 0),
(4905, 0, 'config', 'config_address', '', 0),
(4903, 0, 'config', 'config_name', 'Мой Магазин', 0),
(4904, 0, 'config', 'config_owner', 'Мое Имя', 0),
(5059, 0, 'exchange1c', 'exchange1c_allow_ip', '', 0),
(5060, 0, 'exchange1c', 'exchange1c_price_type', '[{"keyword":"\\u0406\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d (\\u0420\\u043e\\u0437\\u0434\\u0440\\u0456\\u0431\\u043d\\u0430)","customer_group_id":"0","quantity":"0","priority":"0"},{"keyword":"\\u0406\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d (\\u0420\\u043e\\u0437\\u0434\\u0440\\u0456\\u0431\\u043d\\u0430)","customer_group_id":"1","quantity":"","priority":"0"},{"keyword":"\\u0406\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d (\\u041e\\u043f\\u0442\\u043e\\u0432\\u0430)","customer_group_id":"2","quantity":"","priority":"0"}]', 1),
(4902, 0, 'config', 'config_meta_keyword', '', 0),
(4883, 0, 'config', 'config_file_max_size', '300000', 0),
(4884, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(4885, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(4901, 0, 'config', 'config_meta_description', 'Мой Магазин', 0),
(4900, 0, 'config', 'config_meta_title', 'Мой Магазин', 0),
(4899, 0, 'config', 'config_sms_gate_password', '', 0),
(4898, 0, 'config', 'config_sms_gate_username', '', 0),
(4897, 0, 'config', 'config_sms_message', '', 0),
(4890, 0, 'config', 'config_google_captcha_secret', '', 0),
(4891, 0, 'config', 'config_google_captcha_status', '0', 0),
(4892, 0, 'config', 'config_sms_alert', '0', 0),
(4893, 0, 'config', 'config_sms_gatename', 'testsms', 0),
(4894, 0, 'config', 'config_sms_from', '', 0),
(4895, 0, 'config', 'config_sms_to', '', 0),
(4896, 0, 'config', 'config_sms_copy', '', 0),
(4873, 0, 'config', 'config_seo_url_postfix', '', 0),
(4874, 0, 'config', 'config_seo_url_include_path', '1', 0),
(4875, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(4889, 0, 'config', 'config_google_captcha_public', '', 0),
(4888, 0, 'config', 'config_error_filename', 'error.log', 0),
(4887, 0, 'config', 'config_error_log', '1', 0),
(4886, 0, 'config', 'config_error_display', '1', 0),
(4879, 0, 'config', 'config_secure', '0', 0),
(4880, 0, 'config', 'config_password', '1', 0),
(4881, 0, 'config', 'config_shared', '0', 0),
(4882, 0, 'config', 'config_encryption', 'c698qDhTJWH0uhEZPqO85Ck9BE4yK3DBffr1l76zAI0EZf7ARcdvD9yQNiKJ5t1Ax7PAcNGkn0tLifiqxk5tZGSY0ind0DktwQbpeEI3FjVCRgORKuKw3NbauOkRNSvyXyuzSPQpwFlYOjs3FvLkDhLw6IhERb6pNTL3zY3KzhxgspOrigBh4QOlETj0Uojwt1jxLx94R3bRnXNMtst3Yn0zKp3LjFZsrUgma8LYajENIX5gC7fyiNNLbSIsfSMVwPuU70N9loMHjF7P5wuPrKuyzJgsiBf0d5CqzNkVYL6ea2gR4RTJZ4vTHSScLVFCOmBn9tbRWJc0t8i5cbotrvlvCfpIWMJYVtcICQlu03QI3MpzRG9JttGGi4vS3G2BM2Xfrb26uDZQ80gBRHUpLpmxo5BO4v3503MwqIeQ9p6Zr6GT5vQrIPWzxI7sINxf1IvsH1GW4xGveJGCuZZFEzfltQqdjl2fEjDyAMLks2rqTf8VI6yZrxaOVCXXLDB5lRwbZK6QLr2L8rnvDwRCgBm07qBtuZniNjjkSCGKqJy4sw8OHcf4J4pcnBEcrQvcpCbil136fV6kkLw2gVxrdOgdLLqpRD00z4KhTMqe6R8xUfPM0biCvYa7tqp7RqDicfCXLBIdYxykb7Ok2OvvmC2KIMtK4ZybxKj8grRJ0W9eBRwUN1cVPK3wz3oTyYuFE6cAimsAF3q4Vw7VdJlk9Yfnef6KWoagkbscDqP5pVXkQskpBYu3MZAFfTacHWcGAO1gNBUdV3Lxr8SdlUodva8M4eDEWBY01w9jHj2BHgeqN1dU7BkVrAICVYEYfJjv3PiOuJoCjgW7EGZtmzluVNTEGMxxNgnrgHOtQuk7EPyKRnDrkUwFCm4zodE1M1ORNPeQOJWH1muKqPebAejGYN1Gx2hHjluS1If9mgRb31pQNpaxqCBt95caqwmsaPLRlFHMIxEfGn1LV2tm06TK5nIkAlDK3eSPGlJQFKbvk92ay1BE04u8Kxi3fnOROqB0', 0),
(4876, 0, 'config', 'config_compression', '0', 0),
(4877, 0, 'config', 'config_mail_regexp', '/^[^@]+@.*.[a-z]{2,15}$/i', 0),
(4878, 0, 'config', 'config_editor_default', '0', 0),
(4872, 0, 'config', 'config_seo_url_type', 'seo_pro', 0),
(4871, 0, 'config', 'config_seo_url', '1', 0),
(4869, 0, 'config', 'config_mail_alert', '', 0),
(4870, 0, 'config', 'config_maintenance', '0', 0),
(4868, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(4867, 0, 'config', 'config_mail_smtp_port', '25', 0),
(4866, 0, 'config', 'config_mail_smtp_password', '', 0),
(4865, 0, 'config', 'config_mail_smtp_username', '', 0),
(4864, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(4863, 0, 'config', 'config_mail_parameter', '', 0),
(4862, 0, 'config', 'config_mail_protocol', 'mail', 0),
(4860, 0, 'config', 'config_ftp_root', '', 0),
(4861, 0, 'config', 'config_ftp_status', '0', 0),
(4859, 0, 'config', 'config_ftp_password', '', 0),
(4858, 0, 'config', 'config_ftp_username', '', 0),
(4857, 0, 'config', 'config_ftp_port', '21', 0),
(430, 0, 'ocjazz_seopro', 'ocjazz_seopro_hide_default', '1', 0),
(4974, 0, 'mega_filter_module', 'mega_filter_module', '{"1":{"attribs":{"3":{"sort_order":"","items":{"4":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"5":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"6":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"7":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"8":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"9":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"10":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"11":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""}}},"6":{"sort_order":"","items":{"3":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"1":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"2":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""}}},"7":{"sort_order":"","items":{"15":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":"1"},"13":{"enabled":"1","type":"slider","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":"5"},"14":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":"4"},"12":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":"6"},"16":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":"3"}}},"1":{"sort_order":"","items":{"18":{"enabled":"0","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"17":{"enabled":"0","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"19":{"enabled":"0","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""}}}},"options":{"1":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"2":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"5":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"11":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"13":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":"7"},"14":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":"8"}},"base_attribs":{"price":{"enabled":"1","sort_order":"2","collapsed":"0"},"search":{"enabled":"0","sort_order":"","collapsed":"","refresh_delay":"1000","button":"0"},"manufacturers":{"enabled":"1","sort_order":"","display_list_of_items":"","display_as_type":"checkbox","collapsed":"0","display_live_filter":""},"model":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"sku":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"upc":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"ean":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"jan":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"isbn":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"mpn":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"location":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"length":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"slider","collapsed":"0","display_live_filter":""},"width":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"slider","collapsed":"0","display_live_filter":""},"height":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"slider","collapsed":"0","display_live_filter":""},"stock_status":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"checkbox","collapsed":"0"},"rating":{"enabled":"0","sort_order":"","collapsed":"0"}},"filters":{"based_on_category":"1"},"name":"","title":{"1":"","3":"","2":"","4":""},"layout_id":["3"],"store_id":["0"],"position":"content_top","status":"1","sort_order":""}}', 1),
(4975, 0, 'mega_filter_settings', 'mega_filter_settings', '{"attribs":{"search":{"enabled":"0","sort_order":"","collapsed":"","refresh_delay":"1000","button":"0"},"price":{"enabled":"1","sort_order":"-1","collapsed":"0"},"manufacturers":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"checkbox","collapsed":"0","display_live_filter":""},"model":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"sku":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"upc":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"ean":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"jan":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"isbn":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"mpn":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"location":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"text","collapsed":"0","display_live_filter":""},"length":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"slider","collapsed":"0","display_live_filter":""},"width":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"slider","collapsed":"0","display_live_filter":""},"height":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"slider","collapsed":"0","display_live_filter":""},"stock_status":{"enabled":"0","sort_order":"","display_list_of_items":"","display_as_type":"checkbox","collapsed":"0"},"rating":{"enabled":"0","sort_order":"","collapsed":"0"}},"show_loader_over_results":"1","add_pixels_from_top":"0","refresh_results":"using_button","place_button":"bottom","display_list_of_items":{"type":"scroll","max_height":"155","limit_of_items":"4"},"display_live_filter":{"items":"1"},"background_color_counter":"#428bca","text_color_counter":"#ffffff","background_color_search_button":"#428bca","background_color_slider":"","background_color_header":"","text_color_header":"","border_bottom_color_header":"","image_size_width":"20","image_size_height":"20","background_color_widget_button":"","css_style":"","javascript":"MegaFilter.prototype.beforeRequest = function() {\\r\\n\\tvar self = this;\\r\\n};\\r\\n\\r\\nMegaFilter.prototype.beforeRender = function( htmlResponse, htmlContent, json ) {\\r\\n\\tvar self = this;\\r\\n};\\r\\n\\r\\nMegaFilter.prototype.afterRender = function( htmlResponse, htmlContent, json ) {\\r\\n\\tvar self = this;\\r\\n};\\r\\n","content_selector":"#mfilter-content-container","home_page_content_selector":"#content","type_of_condition":"or","show_number_of_products":"1","level_products_from_subcategories":"1","layout_c":"3","in_stock_status":"7","attribute_separator":""}', 1),
(433, 0, 'mega_filter_seo', 'mega_filter_seo', '{"enabled":"0"}', 1),
(434, 0, 'mega_filter_status', 'mega_filter_status', '1', 0),
(435, 0, 'mfilter_version', 'mfilter_version', '2.0.4.3', 0),
(436, 0, 'mega_filter_options', 'mega_filter_options', '{"1":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"2":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"5":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"11":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""}}', 1),
(437, 0, 'mega_filter_attribs', 'mega_filter_attribs', '{"3":{"sort_order":"","items":{"4":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"5":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"6":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"7":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"8":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"9":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"10":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"11":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""}}},"6":{"sort_order":"","items":{"3":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"1":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""},"2":{"enabled":"1","type":"checkbox","display_live_filter":"","collapsed":"0","display_list_of_items":"","sort_order_values":"","sort_order":""}}}}', 1),
(3119, 0, 'popup_purchase', 'popup_purchase_data', '{"status":"1","allow_page":"1","firstname":"2","email":"2","telephone":"2","comment":"0","quantity":"0","stock_check":"1","description":"0","description_max":"255","image":"1","image_width":"200","image_height":"230","allowed_options":["14","13"],"color_h1":"","color_price":"","color_special_price":"","color_description":"","color_checkout_button":"","color_close_button":"","background_checkout_button":"","background_close_button":"","background_checkout_button_hover":"","background_close_button_hover":"","border_checkout_button":"","border_close_button":"","border_checkout_button_hover":"","border_close_button_hover":""}', 1),
(4965, 0, 'simple', 'simple_address_format', '{firstname} {lastname}, {city}, {address_1}', 0),
(4966, 0, 'simple', 'simple_replace_cart', '1', 0),
(4967, 0, 'simple', 'simple_replace_checkout', '1', 0),
(4968, 0, 'simple', 'simple_replace_register', '1', 0),
(3952, 0, 'ulogin_sets', 'ulogin_sets_status', '1', 0),
(3951, 0, 'ulogin_sets', 'ulogin_sets_group', '1', 0),
(3950, 0, 'ulogin_sets', 'ulogin_sets_uloginid', 'c5572c89', 0),
(3941, 0, 'ncategory', 'ncategory_bnews_twitter_tags', '0', 0),
(3940, 0, 'ncategory', 'ncategory_bnews_facebook_tags', '0', 0),
(3939, 0, 'ncategory', 'ncategory_bnews_fbcom_theme', 'dark', 0),
(4856, 0, 'config', 'config_ftp_hostname', 'ocstore', 0),
(3938, 0, 'ncategory', 'ncategory_bnews_fbcom_posts', '10', 0),
(3937, 0, 'ncategory', 'ncategory_bnews_fbcom_appid', '', 0),
(3936, 0, 'ncategory', 'ncategory_bnews_fbcom_status', '0', 0),
(3932, 0, 'ncategory', 'ncategory_bnews_headlines_url', 'blog-headlines', 0),
(3935, 0, 'ncategory', 'ncategory_bnews_disqus_sname', 'short_name', 0),
(3934, 0, 'ncategory', 'ncategory_bnews_disqus_status', '0', 0),
(3933, 0, 'ncategory', 'ncategory_bnews_desc_length', '600', 0),
(3931, 0, 'ncategory', 'ncategory_bnews_admin_limit', '20', 0),
(3930, 0, 'ncategory', 'ncategory_bnews_catalog_limit', '14', 0),
(3929, 0, 'ncategory', 'ncategory_bnews_thumb_height', '230', 0),
(3928, 0, 'ncategory', 'ncategory_bnews_thumb_width', '230', 0),
(3927, 0, 'ncategory', 'ncategory_bnews_image_height', '370', 0),
(3926, 0, 'ncategory', 'ncategory_bnews_image_width', '370', 0),
(3925, 0, 'ncategory', 'ncategory_bnews_display_elements', '["name","image","da","du","author","category","desc","button","com","custom1","custom2","custom3","custom4"]', 1),
(3924, 0, 'ncategory', 'ncategory_bnews_tplpick', '0', 0),
(3923, 0, 'ncategory', 'ncategory_bnews_display_style', '0', 0),
(3922, 0, 'ncategory', 'ncategory_bnews_order', '0', 0),
(3921, 0, 'ncategory', 'ncategory_status', '1', 0),
(4855, 0, 'config', 'config_image_location_height', '50', 0),
(4854, 0, 'config', 'config_image_location_width', '268', 0),
(4853, 0, 'config', 'config_image_cart_height', '230', 0),
(4852, 0, 'config', 'config_image_cart_width', '200', 0),
(4850, 0, 'config', 'config_image_wishlist_width', '228', 0),
(4851, 0, 'config', 'config_image_wishlist_height', '228', 0),
(4849, 0, 'config', 'config_image_compare_height', '90', 0),
(4848, 0, 'config', 'config_image_compare_width', '90', 0),
(4847, 0, 'config', 'config_image_related_height', '228', 0),
(4846, 0, 'config', 'config_image_related_width', '228', 0),
(4845, 0, 'config', 'config_image_additional_height', '500', 0),
(4844, 0, 'config', 'config_image_additional_width', '400', 0),
(4842, 0, 'config', 'config_image_product_width', '228', 0),
(4843, 0, 'config', 'config_image_product_height', '228', 0),
(4837, 0, 'config', 'config_image_category_height', '80', 0),
(4838, 0, 'config', 'config_image_thumb_width', '400', 0),
(4839, 0, 'config', 'config_image_thumb_height', '500', 0),
(4841, 0, 'config', 'config_image_popup_height', '500', 0),
(4840, 0, 'config', 'config_image_popup_width', '500', 0),
(4836, 0, 'config', 'config_image_category_width', '80', 0),
(4835, 0, 'config', 'config_icon', 'catalog/favicon2.png', 0),
(4832, 0, 'config', 'config_captcha', '', 0),
(4833, 0, 'config', 'config_captcha_page', '["review","return","contact"]', 1),
(4834, 0, 'config', 'config_logo', 'catalog/nicola-logo-print-01.jpg', 0),
(4829, 0, 'config', 'config_affiliate_mail', '0', 0),
(4830, 0, 'config', 'config_return_id', '0', 0),
(4831, 0, 'config', 'config_return_status_id', '2', 0),
(4828, 0, 'config', 'config_affiliate_id', '4', 0),
(4827, 0, 'config', 'config_affiliate_commission', '5', 0),
(4826, 0, 'config', 'config_affiliate_auto', '0', 0),
(4825, 0, 'config', 'config_affiliate_approval', '0', 0),
(4824, 0, 'config', 'config_stock_checkout', '0', 0),
(4823, 0, 'config', 'config_stock_warning', '0', 0),
(4822, 0, 'config', 'config_stock_display', '0', 0),
(4821, 0, 'config', 'config_api_id', '1', 0),
(4820, 0, 'config', 'config_order_mail', '0', 0),
(4819, 0, 'config', 'config_fraud_status_id', '2', 0),
(4818, 0, 'config', 'config_complete_status', '["3","5"]', 1),
(4817, 0, 'config', 'config_processing_status', '["2","3","1","12","5"]', 1),
(4816, 0, 'config', 'config_order_status_id', '1', 0),
(4813, 0, 'config', 'config_cart_weight', '1', 0),
(4814, 0, 'config', 'config_checkout_guest', '1', 0),
(4815, 0, 'config', 'config_checkout_id', '5', 0),
(4812, 0, 'config', 'config_invoice_prefix', 'INV-2016-00', 0),
(4811, 0, 'config', 'config_account_mail', '0', 0),
(4809, 0, 'config', 'config_login_attempts', '5', 0),
(4923, 0, 'faq', 'faq_module', '{"items":{"1":{"question":{"1":"\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441 \\u0442\\u0430\\u043a\\u043e\\u0439","3":"\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441 \\u0442\\u0430\\u043a\\u043e\\u0439","2":"\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441 \\u0442\\u0430\\u043a\\u043e\\u0439","4":"\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441 \\u0442\\u0430\\u043a\\u043e\\u0439"},"answer":{"1":"&lt;p&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;br&gt;&lt;\\/p&gt;","3":"&lt;p&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;br&gt;&lt;\\/p&gt;","2":"&lt;p&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;br&gt;&lt;\\/p&gt;","4":"&lt;p&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;span style=&quot;background-color: transparent;&quot;&gt;\\u0410 \\u044d\\u0442\\u043e \\u043e\\u0442\\u0432\\u0435\\u0442&amp;nbsp;&lt;\\/span&gt;&lt;br&gt;&lt;\\/p&gt;"},"section_id":"59787c349fdb9","order":"1"}},"sections":{"1":{"id":"59787c349fdb9","title":{"1":"FAQ","3":"FAQ","2":"FAQ","4":"FAQ"},"hidden":"1","order":"1"}},"settings":{"title":"FAQ","collapse":"0"}}', 1),
(4810, 0, 'config', 'config_account_id', '0', 0),
(4808, 0, 'config', 'config_customer_price', '0', 0),
(4807, 0, 'config', 'config_customer_group_display', '["1","2"]', 1),
(4806, 0, 'config', 'config_customer_group_id', '1', 0),
(4803, 0, 'config', 'config_tax_default', 'shipping', 0),
(4804, 0, 'config', 'config_tax_customer', 'shipping', 0),
(4805, 0, 'config', 'config_customer_online', '0', 0),
(4802, 0, 'config', 'config_tax', '0', 0),
(4799, 0, 'config', 'config_review_mail', '0', 0),
(4800, 0, 'config', 'config_voucher_min', '1', 0),
(4801, 0, 'config', 'config_voucher_max', '1000', 0),
(4798, 0, 'config', 'config_review_guest', '1', 0),
(4797, 0, 'config', 'config_review_status', '1', 0),
(4796, 0, 'config', 'config_product_mpn_hide', '0', 0),
(4795, 0, 'config', 'config_product_isbn_hide', '0', 0),
(4794, 0, 'config', 'config_product_jan_hide', '0', 0),
(4793, 0, 'config', 'config_product_ean_hide', '0', 0),
(4792, 0, 'config', 'config_product_upc_hide', '0', 0),
(4791, 0, 'config', 'config_limit_admin', '100', 0),
(4790, 0, 'config', 'config_product_description_length', '100', 0),
(4789, 0, 'config', 'config_product_limit', '12', 0),
(4788, 0, 'config', 'config_product_count', '0', 0),
(4784, 0, 'config', 'config_currency', 'RUB', 0),
(4785, 0, 'config', 'config_currency_auto', '1', 0),
(4787, 0, 'config', 'config_weight_class_id', '1', 0),
(4786, 0, 'config', 'config_length_class_id', '1', 0),
(4783, 0, 'config', 'config_admin_language', 'ru', 0),
(4782, 0, 'config', 'config_language', 'ru', 0),
(976, 0, 'newsletters', 'newsletters_status', '1', 0),
(4781, 0, 'config', 'config_zone_id', '2761', 0),
(4780, 0, 'config', 'config_country_id', '176', 0),
(4779, 0, 'config', 'config_image', '', 0),
(4778, 0, 'config', 'config_fax', '', 0),
(4765, 0, 'config', 'config_template', 'default', 0),
(4766, 0, 'config', 'config_layout_id', '4', 0),
(4767, 0, 'config', 'config_geocode', '54.718681,20.499113', 0),
(4768, 0, 'config', 'config_email', 'melnykserhiy1@gmail.com', 0),
(4769, 0, 'config', 'config_telephone', '+38 097 942 26 42', 0),
(4770, 0, 'config', 'config_telephone2', '+38 097 407 04 11', 0),
(4771, 0, 'config', 'config_fb', '', 0),
(4772, 0, 'config', 'config_odn', '', 0),
(4773, 0, 'config', 'config_vk', '', 0),
(4774, 0, 'config', 'config_gp', '', 0),
(4775, 0, 'config', 'config_youtube', '', 0),
(4776, 0, 'config', 'config_twitter', '2', 0),
(4777, 0, 'config', 'config_facebook', '1', 0),
(5070, 0, 'exchange1c', 'exchange1c_order_status', '2', 0),
(5071, 0, 'exchange1c', 'exchange1c_order_currency', '', 0),
(5072, 0, 'exchange1c', 'exchange1c_order_date', '', 0),
(5069, 0, 'exchange1c', 'exchange1c_order_status_to_exchange', '0', 0),
(5061, 0, 'exchange1c', 'exchange1c_flush_product', '1', 0),
(5062, 0, 'exchange1c', 'exchange1c_flush_category', '1', 0),
(5063, 0, 'exchange1c', 'exchange1c_flush_quantity', '1', 0),
(5064, 0, 'exchange1c', 'exchange1c_fill_parent_cats', '1', 0),
(4764, 0, 'config', 'config_langdata', '{"1":{"meta_title":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d","meta_description":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d","meta_keyword":"","name":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d","owner":"\\u041c\\u043e\\u0435 \\u0418\\u043c\\u044f","address":"\\u0410\\u0434\\u0440\\u0435\\u0441","open":"\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u043d\\u0442\\u044b Nikola \\u0441 \\u0443\\u0434\\u043e\\u0432\\u043e\\u043b\\u044c\\u0441\\u0442\\u0432\\u0438\\u0435\\u043c \\u043e\\u0442\\u0432\\u0435\\u0442\\u044f\\u0442 \\u043d\\u0430 \\u0432\\u0441\\u0435 \\u0412\\u0430\\u0448\\u0438 \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b.\\r\\n\\u0420\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u043c \\u0435\\u0436\\u0435\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e \\u0441 08:00 \\u0434\\u043e 20:00.","comment":""},"3":{"meta_title":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d","meta_description":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d22323","meta_keyword":"","name":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d","owner":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d","address":"\\u041c\\u043e\\u0439 \\u041c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d","open":"\\u041a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u043d\\u0442\\u0438 Nikola \\u0437 \\u0437\\u0430\\u0434\\u043e\\u0432\\u043e\\u043b\\u0435\\u043d\\u043d\\u044f\\u043c \\u0434\\u0430\\u0434\\u0443\\u0442\\u044c \\u0432\\u0456\\u0434\\u043f\\u043e\\u0432\\u0456\\u0434\\u044c \\u043d\\u0430 \\u0432\\u0441\\u0456 \\u0412\\u0430\\u0448\\u0456 \\u0437\\u0430\\u043f\\u0438\\u0442\\u0430\\u043d\\u043d\\u044f.\\r\\n\\u041f\\u0440\\u0430\\u0446\\u044e\\u0454\\u043c\\u043e \\u0449\\u043e\\u0434\\u043d\\u044f \\u0437 08:00 \\u0434\\u043e 20:00.","comment":""},"2":{"meta_title":"Your Store","meta_description":"My Store","meta_keyword":"","name":"Your Store","owner":"Your Name","address":"Address 1","open":"Nikola consultants will be happy to answer all your questions.\\r\\nWe work daily from 08:00 to 20:00.","comment":""}}', 1),
(5065, 0, 'exchange1c', 'exchange1c_relatedoptions', '1', 0),
(5066, 0, 'exchange1c', 'exchange1c_seo_url', '0', 0),
(5067, 0, 'exchange1c', 'exchange1c_full_log', '1', 0),
(5068, 0, 'exchange1c', 'exchange1c_watermark', '', 0);
INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(4964, 0, 'simple', 'simple_settings', '{"address":{"geoIpMode":1,"rows":{"default":[{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"firstname","masterField":"","requireWhen":{},"required":"1","sortOrder":1,"type":"field","$$hashKey":"1AH"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"lastname","masterField":"","requireWhen":{},"required":"1","sortOrder":2,"type":"field","$$hashKey":"1AI"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"country_id","masterField":"","requireWhen":{},"required":"1","sortOrder":3,"type":"field","$$hashKey":"1AJ"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"zone_id","masterField":"","requireWhen":{},"required":"1","sortOrder":4,"type":"field","$$hashKey":"1AK"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"city","masterField":"","requireWhen":{},"required":"1","sortOrder":5,"type":"field","$$hashKey":"1AL"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"postcode","masterField":"","requireWhen":{},"required":"1","sortOrder":6,"type":"field","$$hashKey":"1AM"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"address_1","masterField":"","requireWhen":{},"required":"1","sortOrder":7,"type":"field","$$hashKey":"1AN"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"default","masterField":"","requireWhen":{},"required":0,"sortOrder":9,"type":"field","$$hashKey":"1AO"}]},"scrollToError":true},"addressFormat":"{firstname} {lastname}, {city}, {address_1}","checkout":[{"agreement":{"displayHeader":true},"agreementId":0,"asapForGuests":true,"asapForLogged":true,"cart":{"displayHeader":true,"displayModel":true,"maxAmount":{},"maxQuantity":{},"maxWeight":{},"minAmount":{},"minQuantity":{},"minWeight":{}},"comment":{"displayHeader":true,"label":{"en":"Comment","ru":"Комментарий","ua":"Коментар","pl":"Komentarz"},"placeholder":{"en":"Comment","ru":"Комментарий","ua":"Коментар","pl":"Komentarz"}},"customer":{"addressSameInit":true,"addressSelectionFormat":"{firstname} {lastname}, {city}, {address_1}","displayAddressSame":false,"displayAddressSelection":true,"displayHeader":true,"displayLogin":false,"displayYouWillRegistered":false,"rows":{"default":[{"displayWhen":{"0":true,"1":true},"hideForGuest":false,"hideForLogged":true,"id":"email","masterField":"register","requireWhen":{"1":true},"required":"2","sortOrder":2,"type":"field","$$hashKey":"04O"},{"displayWhen":{"1":true},"hideForGuest":false,"hideForLogged":true,"id":"password","masterField":"register","requireWhen":{},"required":"1","sortOrder":3,"type":"field","$$hashKey":"04P"},{"displayWhen":{"1":true},"hideForGuest":false,"hideForLogged":true,"id":"confirm_password","masterField":"register","requireWhen":{"1":false},"required":"1","sortOrder":4,"type":"field","$$hashKey":"04Q"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"firstname","masterField":"","requireWhen":{},"required":"1","sortOrder":6,"type":"field","$$hashKey":"04R"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"lastname","masterField":"","requireWhen":{},"required":"1","sortOrder":7,"type":"field","$$hashKey":"04S"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"telephone","masterField":"","requireWhen":{},"required":"1","sortOrder":8,"type":"field","$$hashKey":"04T"}]}},"displayProceedText":true,"displayWeight":false,"geoIpMode":1,"help":{"displayHeader":true},"helpId":0,"leftColumnWidth":"","loginType":"popup","payment":{"displayHeader":true,"hideForGuest":false,"methods":{"cod":{"code":"cod","title":{"ru":"Оплата при доставке"},"hideForStatuses":{"guest":false,"logged":false},"forGroups":{},"forAllGroups":1,"forMethods":{},"forAllMethods":1,"description":{}},"free_checkout":{"code":"free_checkout","title":{"ru":"Бесплатный заказ"},"hideForStatuses":{"guest":false,"logged":false},"forGroups":{},"forAllGroups":1,"forMethods":{},"forAllMethods":1,"description":{}}},"rows":{"|cod":[],"|free_checkout":[]},"selectFirst":true,"displayType":1},"paymentAddress":{"addressSameInit":true,"displayAddressSame":true,"displayHeader":true,"rows":{"default":[{"displayWhen":{},"hideForGuest":true,"hideForLogged":false,"id":"address_id","masterField":"","requireWhen":{},"required":"1","sortOrder":1,"type":"field","$$hashKey":"0CF"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"country_id","masterField":"","requireWhen":{},"required":"1","sortOrder":4,"type":"field","$$hashKey":"0CI"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"zone_id","masterField":"","requireWhen":{},"required":"1","sortOrder":6,"type":"field","$$hashKey":"0CJ"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"city","masterField":"","requireWhen":{},"required":"1","sortOrder":7,"type":"field","$$hashKey":"0CK"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"address_1","masterField":"","requireWhen":{},"required":"1","sortOrder":9,"type":"field","$$hashKey":"0CM"}]},"hideForMethods":{}},"rightColumnWidth":"","scrollToError":true,"scrollToPaymentForm":true,"settingsId":0,"shipping":{"displayHeader":true,"hideForGuest":false,"methods":{"free":{"code":"free","title":{"ru":"Бесплатная доставка"},"methods":{"free.free":{"code":"free.free","title":{"ru":"Бесплатная Доставка"},"hideForStatuses":{"guest":false,"logged":false},"forGroups":{},"forAllGroups":1,"forMethods":{},"forAllMethods":1,"description":{}}}}},"rows":{"flat.*|":[],"flat.flat|":[],"free.free|":[]},"selectFirst":true,"displayType":1},"shippingAddress":{"addressSelectionFormat":"{firstname} {lastname}, {city}, {address_1}","displayAddressSelection":true,"displayHeader":true,"hideForGuest":false,"hideForLogged":false,"rows":{"default":[{"displayWhen":{},"hideForGuest":true,"hideForLogged":false,"id":"address_id","masterField":"","requireWhen":{},"required":0,"sortOrder":1,"type":"field","$$hashKey":"0KG"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"country_id","masterField":"","requireWhen":{},"required":"1","sortOrder":4,"type":"field","$$hashKey":"0KJ"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"zone_id","masterField":"","requireWhen":{},"required":"1","sortOrder":5,"type":"field","$$hashKey":"0KK"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"city","masterField":"","requireWhen":{},"required":"1","sortOrder":6,"type":"field","$$hashKey":"0KL"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"address_1","masterField":"","requireWhen":{},"required":"1","sortOrder":8,"type":"field","$$hashKey":"0KN"}]},"hideForMethods":{}},"shipping_address":{"hideForGuest":true,"hideForLogged":true},"steps":[{"id":"step_0","label":{"en":"Step 1"},"manual":true,"template":"{left_column}{shipping}{payment}{shipping_address}{customer}{comment}{/left_column}{right_column}{cart}{/right_column}{payment_form}","$$hashKey":"03U"}],"summary":{"displayHeader":true},"$$hashKey":"025"}],"colorbox":true,"edit":{"rows":{"default":[{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"email","masterField":"","requireWhen":{},"required":"1","sortOrder":2,"type":"field","$$hashKey":"16C"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"firstname","masterField":"","requireWhen":{},"required":"1","sortOrder":3,"type":"field","$$hashKey":"16D"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"lastname","masterField":"","requireWhen":{},"required":"1","sortOrder":4,"type":"field","$$hashKey":"16E"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"telephone","masterField":"","requireWhen":{},"required":"1","sortOrder":5,"type":"field","$$hashKey":"16F"}]},"scrollToError":true},"fields":[{"autoreload":false,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"0","source":"saved"},"description":{},"id":"register","label":{"en":"Register account","ru":"Зарегистрироваться"},"mask":{"source":"saved"},"objects":{"address":false,"customer":true,"order":false},"placeholder":[],"rules":{"api":{},"byLength":[],"equal":{},"notEmpty":{},"regexp":[]},"saveToComment":false,"type":"radio","values":{"method":"getYesNo","saved":[],"source":"model"},"valuesList":{"ru":[{"id":"1","text":"Tak","$$hashKey":"3HP"},{"id":"0","text":"nie","$$hashKey":"3HQ"}]},"$$hashKey":"06J"},{"autoreload":false,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{"en":""},"id":"email","label":{"en":"Email","ru":"Email","ua":"Email","pl":"Email"},"mask":{"source":"saved"},"masterField":"register","objects":{"customer":true},"placeholder":{},"rules":{"api":{"enabled":true,"errorText":{"en":"E-Mail Address is already registered!","ru":"Адрес уже зарегистрирован!","pl":"Adres e-mail jest już zarejestrowany!"},"filter":"register","method":"checkEmailForUniqueness"},"byLength":{},"equal":{},"notEmpty":{"enabled":false,"errorText":{}},"regexp":{"enabled":true,"errorText":{"en":"E-Mail Address does not appear to be valid!","ru":"Некорректный адрес электронной почты!","pl":"Nieprawidłowy adres e-mail!","ua":"Некоректний адресу електронної пошти!"},"value":".+@.+"}},"saveToComment":false,"type":"email","valuesList":{},"$$hashKey":"06K"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"method":"getRandomPassword","saved":"","source":"saved"},"description":{},"id":"password","label":{"en":"Password","ru":"Пароль","ua":"Пароль"},"mask":{"source":"saved"},"masterField":"register","objects":{"customer":true},"placeholder":{},"rules":{"api":{},"byLength":{"enabled":true,"errorText":{"en":"Password must be between 4 and 20 characters!","ru":"Пароль должен быть от 4 до 20 символов!"},"max":"20","min":"4"},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"password","valuesList":{},"$$hashKey":"06L"},{"autoreload":false,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"method":"getRandomPassword","saved":"","source":"saved"},"description":{},"id":"confirm_password","label":{"en":"Confirm password","ru":"Подтвердите пароль"},"mask":{"source":"saved"},"objects":{"customer":true},"placeholder":{},"rules":{"api":{"enabled":false,"errorText":[]},"byLength":{},"equal":{"enabled":true,"errorText":{"en":"Password confirmation does not match password!","ru":"Подтверждение пароля не соответствует паролю!"},"fieldId":"password"},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"password","valuesList":{},"$$hashKey":"06M"},{"autoreload":false,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"1","source":"saved"},"description":{},"id":"newsletter","label":{"en":"Subscribe","ru":"Подписаться на новости"},"mask":{"source":"saved"},"objects":{"customer":true},"placeholder":[],"rules":{"api":{},"byLength":[],"equal":{},"notEmpty":{},"regexp":[]},"saveToComment":false,"type":"radio","values":{"method":"getYesNo","saved":[],"source":"model"},"valuesList":{"ru":[{"id":"1","text":"Tak","$$hashKey":"44Z"},{"id":"0","text":"nie","$$hashKey":"450"}]},"$$hashKey":"05T"},{"autoreload":true,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"method":"getDefaultGroup","saved":"","source":"model"},"description":{},"id":"customer_group_id","label":{"en":"Customer group","ru":"Группа покупателя","ua":"Тип покупця"},"mask":{"source":"saved"},"objects":{"customer":true},"placeholder":[],"rules":{"api":{},"byLength":[],"equal":{},"notEmpty":{},"regexp":[]},"saveToComment":false,"type":"radio","values":{"method":"getCustomerGroups","saved":[],"source":"model"},"valuesList":{"ru":[{"id":"1","text":"Default"},{"id":"2","text":"Jestem hurtowym nabywcą"}]},"$$hashKey":"05U"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"firstname","label":{"en":"Firstname","ru":"Имя","ua":"І\'мя","pl":"Nazwa jest"},"mask":{"source":"saved"},"objects":{"address":true,"customer":true},"placeholder":{},"rules":{"api":{},"byLength":{"enabled":true,"errorText":{"en":"First Name must be between 1 and 32 characters!","ru":"Имя должно быть от 1 до 32 символов!","pl":"Nazwa musi wynosić od 1 do 32 znaków!","ua":"Ім\'я повинно бути від 1 до 32 символів!"},"max":"32","min":"1"},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"06N"},{"autoreload":false,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"lastname","label":{"en":"Lastname","ru":"Фамилия","ua":"Прізвище","pl":"Nazwisko"},"mask":{"source":"saved"},"objects":{"address":true,"customer":true},"placeholder":{"en":""},"rules":{"api":{},"byLength":{"enabled":true,"errorText":{"en":"Last Name must be between 1 and 32 characters!","ru":"Фамилия должна быть от 1 до 32 символов!","ua":"Прізвище повинна бути від 1 до 32 символів!","pl":"Nazwisko musi wynosić od 1 do 32 znaków!"},"max":"32","min":"1"},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"06O"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"telephone","label":{"en":"Telephone","ru":"Телефон","ua":"Телефон","pl":"Telefon"},"mask":{"filter":"country_id","method":"getTelephoneMask","saved":"+38 (999) 999 99 99","source":"saved"},"masterField":"","objects":{"customer":true},"placeholder":{},"rules":{"api":{},"byLength":{"enabled":true,"errorText":{"en":"Telephone must be between 3 and 32 characters!","ru":"Телефон должен быть от 3 до 32 символов!","pl":"Telefon musi wynosić od 3 do 32 znaków!","ua":"Телефон повинен бути від 3 до 32 символів!"},"max":"32","min":"3"},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"tel","valuesList":{},"$$hashKey":"06P"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"fax","label":{"en":"Fax","ru":"Факс"},"mask":{"source":"saved"},"objects":{"customer":true},"placeholder":{},"rules":{"api":{},"byLength":{},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"05V"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"company","label":{"en":"Company","ru":"Компания"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":{},"rules":{"api":{},"byLength":{},"equal":{},"notEmpty":{"enabled":false,"errorText":{"en":""}},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"06Q"},{"autoreload":false,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"address_1","label":{"en":"Address","ru":"Адрес","ua":"Адреса","pl":"Adres"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":{},"rules":{"api":{},"byLength":{"enabled":true,"errorText":{"en":"Address 1 must be between 3 and 128 characters!","ru":"Адрес должен быть от 3 до 128 символов!","ua":"Адреса повинен бути від 3 до 128 символів!","pl":"Adres musi wynosić od 3 do 128 znaków!"},"max":"128","min":"3"},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"06R"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"address_2","label":{"en":"Address 2","ru":"Продолжение адреса"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":{},"rules":{"api":{},"byLength":{},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"06S"},{"autoreload":true,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"city","label":{"en":"City","ru":"Город","ua":"Місто","pl":"Miasto"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":{},"rules":{"api":{},"byLength":{"enabled":true,"errorText":{"en":"City must be between 2 and 128 characters!","ru":"Город должен быть от 2 до 128 символов!","pl":"Miasto musi mieć od 2 do 128 znaków!","ua":"Місто повинно бути від 2 до 128 символів!"},"max":"128","min":"2"},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"06T"},{"autoreload":true,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"postcode","label":{"en":"Postcode","ru":"Индекс"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":{},"rules":{"api":{},"byLength":{"enabled":true,"errorText":{"en":"Postcode must be between 2 and 10 characters!","ru":"Индекс должен быть от 2 до 10 символов!"},"max":"10","min":"2"},"equal":{},"notEmpty":{},"regexp":{}},"saveToComment":false,"type":"text","valuesList":{},"$$hashKey":"06U"},{"autoreload":true,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"country_id","label":{"en":"Country","ru":"Страна","ua":"Країна","pl":"Kraj"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":[],"rules":{"api":{},"byLength":[],"equal":{},"notEmpty":{"enabled":true,"errorText":{"en":"Please select a country!","ru":"Выберите страну!","ua":"Виберіть країну!","pl":"Wybierz kraj!"}},"regexp":[]},"saveToComment":false,"type":"select","values":{"method":"getCountries","saved":[],"source":"model"},"valuesList":{"ru":[{"id":"","text":" ---Wybierz--- ","$$hashKey":"3IN"},{"id":"13","text":"Австралия","$$hashKey":"3IO"},{"id":"14","text":"Австрия","$$hashKey":"3IP"},{"id":"15","text":"Азербайджан","$$hashKey":"3IQ"},{"id":"244","text":"Аландские острова","$$hashKey":"3IR"},{"id":"2","text":"Албания","$$hashKey":"3IS"},{"id":"3","text":"Алжир","$$hashKey":"3IT"},{"id":"7","text":"Ангилья","$$hashKey":"3IU"},{"id":"6","text":"Ангола","$$hashKey":"3IV"},{"id":"5","text":"Андорра","$$hashKey":"3IW"},{"id":"8","text":"Антарктида","$$hashKey":"3IX"},{"id":"9","text":"Антигуа и Барбуда","$$hashKey":"3IY"},{"id":"151","text":"Антильские (Нидерландские) острова","$$hashKey":"3IZ"},{"id":"10","text":"Аргентина","$$hashKey":"3J0"},{"id":"11","text":"Армения","$$hashKey":"3J1"},{"id":"12","text":"Аруба","$$hashKey":"3J2"},{"id":"1","text":"Афганистан","$$hashKey":"3J3"},{"id":"16","text":"Багамские острова","$$hashKey":"3J4"},{"id":"18","text":"Бангладеш","$$hashKey":"3J5"},{"id":"19","text":"Барбадос","$$hashKey":"3J6"},{"id":"17","text":"Бахрейн","$$hashKey":"3J7"},{"id":"22","text":"Белиз","$$hashKey":"3J8"},{"id":"20","text":"Белоруссия (Беларусь)","$$hashKey":"3J9"},{"id":"21","text":"Бельгия","$$hashKey":"3JA"},{"id":"23","text":"Бенин","$$hashKey":"3JB"},{"id":"24","text":"Бермудские острова","$$hashKey":"3JC"},{"id":"33","text":"Болгария","$$hashKey":"3JD"},{"id":"26","text":"Боливия","$$hashKey":"3JE"},{"id":"245","text":"Бонайре, Синт-Эстатиус и Саба","$$hashKey":"3JF"},{"id":"27","text":"Босния и Герцеговина","$$hashKey":"3JG"},{"id":"28","text":"Ботсвана","$$hashKey":"3JH"},{"id":"30","text":"Бразилия","$$hashKey":"3JI"},{"id":"31","text":"Британская территория в Индийском океане","$$hashKey":"3JJ"},{"id":"32","text":"Бруней","$$hashKey":"3JK"},{"id":"34","text":"Буркина-Фасо","$$hashKey":"3JL"},{"id":"35","text":"Бурунди","$$hashKey":"3JM"},{"id":"25","text":"Бутан","$$hashKey":"3JN"},{"id":"227","text":"Вануату","$$hashKey":"3JO"},{"id":"228","text":"Ватикан","$$hashKey":"3JP"},{"id":"222","text":"Великобритания","$$hashKey":"3JQ"},{"id":"97","text":"Венгрия","$$hashKey":"3JR"},{"id":"229","text":"Венесуэла","$$hashKey":"3JS"},{"id":"231","text":"Виргинские острова (Британские)","$$hashKey":"3JT"},{"id":"232","text":"Виргинские острова (США)","$$hashKey":"3JU"},{"id":"4","text":"Восточное Самоа","$$hashKey":"3JV"},{"id":"61","text":"Восточный Тимор","$$hashKey":"3JW"},{"id":"230","text":"Вьетнам","$$hashKey":"3JX"},{"id":"78","text":"Габон","$$hashKey":"3JY"},{"id":"93","text":"Гаити","$$hashKey":"3JZ"},{"id":"92","text":"Гайана","$$hashKey":"3K0"},{"id":"79","text":"Гамбия","$$hashKey":"3K1"},{"id":"82","text":"Гана","$$hashKey":"3K2"},{"id":"87","text":"Гваделупа","$$hashKey":"3K3"},{"id":"89","text":"Гватемала","$$hashKey":"3K4"},{"id":"90","text":"Гвинея","$$hashKey":"3K5"},{"id":"91","text":"Гвинея-Бисау","$$hashKey":"3K6"},{"id":"81","text":"Германия","$$hashKey":"3K7"},{"id":"83","text":"Гибралтар","$$hashKey":"3K8"},{"id":"95","text":"Гондурас","$$hashKey":"3K9"},{"id":"96","text":"Гонконг","$$hashKey":"3KA"},{"id":"86","text":"Гренада","$$hashKey":"3KB"},{"id":"85","text":"Гренландия","$$hashKey":"3KC"},{"id":"84","text":"Греция","$$hashKey":"3KD"},{"id":"80","text":"Грузия","$$hashKey":"3KE"},{"id":"88","text":"Гуам","$$hashKey":"3KF"},{"id":"57","text":"Дания","$$hashKey":"3KG"},{"id":"58","text":"Джибути","$$hashKey":"3KH"},{"id":"59","text":"Доминика","$$hashKey":"3KI"},{"id":"60","text":"Доминиканская Республика","$$hashKey":"3KJ"},{"id":"63","text":"Египет","$$hashKey":"3KK"},{"id":"237","text":"Заир","$$hashKey":"3KL"},{"id":"238","text":"Замбия","$$hashKey":"3KM"},{"id":"234","text":"Западная Сахара","$$hashKey":"3KN"},{"id":"181","text":"Западное Самоа","$$hashKey":"3KO"},{"id":"239","text":"Зимбабве","$$hashKey":"3KP"},{"id":"104","text":"Израиль","$$hashKey":"3KQ"},{"id":"99","text":"Индия","$$hashKey":"3KR"},{"id":"100","text":"Индонезия","$$hashKey":"3KS"},{"id":"108","text":"Иордания","$$hashKey":"3KT"},{"id":"102","text":"Ирак","$$hashKey":"3KU"},{"id":"101","text":"Иран","$$hashKey":"3KV"},{"id":"103","text":"Ирландия","$$hashKey":"3KW"},{"id":"98","text":"Исландия","$$hashKey":"3KX"},{"id":"195","text":"Испания","$$hashKey":"3KY"},{"id":"105","text":"Италия","$$hashKey":"3KZ"},{"id":"235","text":"Йемен","$$hashKey":"3L0"},{"id":"39","text":"Кабо-Верде","$$hashKey":"3L1"},{"id":"109","text":"Казахстан","$$hashKey":"3L2"},{"id":"40","text":"Каймановы острова","$$hashKey":"3L3"},{"id":"36","text":"Камбоджа","$$hashKey":"3L4"},{"id":"37","text":"Камерун","$$hashKey":"3L5"},{"id":"38","text":"Канада","$$hashKey":"3L6"},{"id":"251","text":"Канарские Острова","$$hashKey":"3L7"},{"id":"173","text":"Катар","$$hashKey":"3L8"},{"id":"110","text":"Кения","$$hashKey":"3L9"},{"id":"55","text":"Кипр","$$hashKey":"3LA"},{"id":"115","text":"Киргизия (Кыргызстан)","$$hashKey":"3LB"},{"id":"111","text":"Кирибати","$$hashKey":"3LC"},{"id":"44","text":"Китайская Народная Республика","$$hashKey":"3LD"},{"id":"46","text":"Кокосовые острова","$$hashKey":"3LE"},{"id":"47","text":"Колумбия","$$hashKey":"3LF"},{"id":"48","text":"Коморские острова","$$hashKey":"3LG"},{"id":"49","text":"Конго","$$hashKey":"3LH"},{"id":"112","text":"Корейская Народно-Демократическая Республика","$$hashKey":"3LI"},{"id":"253","text":"Косово, Республика","$$hashKey":"3LJ"},{"id":"51","text":"Коста-Рика","$$hashKey":"3LK"},{"id":"52","text":"Кот д\'Ивуар","$$hashKey":"3LL"},{"id":"54","text":"Куба","$$hashKey":"3LM"},{"id":"114","text":"Кувейт","$$hashKey":"3LN"},{"id":"246","text":"Кюрасао","$$hashKey":"3LO"},{"id":"116","text":"Лаос","$$hashKey":"3LP"},{"id":"117","text":"Латвия","$$hashKey":"3LQ"},{"id":"119","text":"Лесото","$$hashKey":"3LR"},{"id":"120","text":"Либерия","$$hashKey":"3LS"},{"id":"118","text":"Ливан","$$hashKey":"3LT"},{"id":"121","text":"Ливия","$$hashKey":"3LU"},{"id":"123","text":"Литва","$$hashKey":"3LV"},{"id":"122","text":"Лихтенштейн","$$hashKey":"3LW"},{"id":"124","text":"Люксембург","$$hashKey":"3LX"},{"id":"136","text":"Маврикий","$$hashKey":"3LY"},{"id":"135","text":"Мавритания","$$hashKey":"3LZ"},{"id":"127","text":"Мадагаскар","$$hashKey":"3M0"},{"id":"137","text":"Майотта","$$hashKey":"3M1"},{"id":"125","text":"Макао","$$hashKey":"3M2"},{"id":"126","text":"Македония","$$hashKey":"3M3"},{"id":"128","text":"Малави","$$hashKey":"3M4"},{"id":"129","text":"Малайзия","$$hashKey":"3M5"},{"id":"131","text":"Мали","$$hashKey":"3M6"},{"id":"130","text":"Мальдивы","$$hashKey":"3M7"},{"id":"132","text":"Мальта","$$hashKey":"3M8"},{"id":"144","text":"Марокко","$$hashKey":"3M9"},{"id":"134","text":"Мартиника","$$hashKey":"3MA"},{"id":"133","text":"Маршалловы острова","$$hashKey":"3MB"},{"id":"138","text":"Мексика","$$hashKey":"3MC"},{"id":"224","text":"Мелкие отдаленные острова США","$$hashKey":"3MD"},{"id":"139","text":"Микронезия","$$hashKey":"3ME"},{"id":"145","text":"Мозамбик","$$hashKey":"3MF"},{"id":"140","text":"Молдова","$$hashKey":"3MG"},{"id":"141","text":"Монако","$$hashKey":"3MH"},{"id":"142","text":"Монголия","$$hashKey":"3MI"},{"id":"143","text":"Монтсеррат","$$hashKey":"3MJ"},{"id":"146","text":"Мьянма","$$hashKey":"3MK"},{"id":"147","text":"Намибия","$$hashKey":"3ML"},{"id":"148","text":"Науру","$$hashKey":"3MM"},{"id":"149","text":"Непал","$$hashKey":"3MN"},{"id":"155","text":"Нигер","$$hashKey":"3MO"},{"id":"156","text":"Нигерия","$$hashKey":"3MP"},{"id":"150","text":"Нидерланды","$$hashKey":"3MQ"},{"id":"154","text":"Никарагуа","$$hashKey":"3MR"},{"id":"157","text":"Ниуэ","$$hashKey":"3MS"},{"id":"153","text":"Новая Зеландия","$$hashKey":"3MT"},{"id":"152","text":"Новая Каледония","$$hashKey":"3MU"},{"id":"160","text":"Норвегия","$$hashKey":"3MV"},{"id":"221","text":"Объединенные Арабские Эмираты","$$hashKey":"3MW"},{"id":"161","text":"Оман","$$hashKey":"3MX"},{"id":"29","text":"Остров Буве","$$hashKey":"3MY"},{"id":"252","text":"Остров Вознесения (Великобритания)","$$hashKey":"3MZ"},{"id":"256","text":"Остров Гернси","$$hashKey":"3N0"},{"id":"257","text":"Остров Джерси","$$hashKey":"3N1"},{"id":"254","text":"Остров Мэн","$$hashKey":"3N2"},{"id":"158","text":"Остров Норфолк","$$hashKey":"3N3"},{"id":"45","text":"Остров Рождества","$$hashKey":"3N4"},{"id":"197","text":"Остров Святой Елены","$$hashKey":"3N5"},{"id":"50","text":"Острова Кука","$$hashKey":"3N6"},{"id":"169","text":"Острова Питкэрн","$$hashKey":"3N7"},{"id":"217","text":"Острова Теркс и Кайкос","$$hashKey":"3N8"},{"id":"162","text":"Пакистан","$$hashKey":"3N9"},{"id":"163","text":"Палау","$$hashKey":"3NA"},{"id":"247","text":"Палестинская территория, оккупированная","$$hashKey":"3NB"},{"id":"164","text":"Панама","$$hashKey":"3NC"},{"id":"165","text":"Папуа - Новая Гвинея","$$hashKey":"3ND"},{"id":"166","text":"Парагвай","$$hashKey":"3NE"},{"id":"167","text":"Перу","$$hashKey":"3NF"},{"id":"170","text":"Польша","$$hashKey":"3NG"},{"id":"171","text":"Португалия","$$hashKey":"3NH"},{"id":"172","text":"Пуэрто-Рико","$$hashKey":"3NI"},{"id":"113","text":"Республика Корея","$$hashKey":"3NJ"},{"id":"174","text":"Реюньон","$$hashKey":"3NK"},{"id":"176","text":"Российская Федерация","$$hashKey":"3NL"},{"id":"177","text":"Руанда","$$hashKey":"3NM"},{"id":"175","text":"Румыния","$$hashKey":"3NN"},{"id":"64","text":"Сальвадор","$$hashKey":"3NO"},{"id":"182","text":"Сан-Марино","$$hashKey":"3NP"},{"id":"183","text":"Сан-Томе и Принсипи","$$hashKey":"3NQ"},{"id":"249","text":"Санкт-Бартелеми","$$hashKey":"3NR"},{"id":"250","text":"Санкт-Мартин (французская часть)","$$hashKey":"3NS"},{"id":"184","text":"Саудовская Аравия","$$hashKey":"3NT"},{"id":"202","text":"Свазиленд","$$hashKey":"3NU"},{"id":"159","text":"Северные Марианские острова","$$hashKey":"3NV"},{"id":"186","text":"Сейшельские острова","$$hashKey":"3NW"},{"id":"198","text":"Сен-Пьер и Микелон","$$hashKey":"3NX"},{"id":"185","text":"Сенегал","$$hashKey":"3NY"},{"id":"180","text":"Сент-Винсент и Гренадины","$$hashKey":"3NZ"},{"id":"178","text":"Сент-Китс и Невис","$$hashKey":"3O0"},{"id":"179","text":"Сент-Люсия","$$hashKey":"3O1"},{"id":"243","text":"Сербия","$$hashKey":"3O2"},{"id":"236","text":"Сербия и Черногория","$$hashKey":"3O3"},{"id":"188","text":"Сингапур","$$hashKey":"3O4"},{"id":"205","text":"Сирия","$$hashKey":"3O5"},{"id":"189","text":"Словакия","$$hashKey":"3O6"},{"id":"190","text":"Словения","$$hashKey":"3O7"},{"id":"223","text":"Соединенные Штаты Америки","$$hashKey":"3O8"},{"id":"191","text":"Соломоновы острова","$$hashKey":"3O9"},{"id":"192","text":"Сомали","$$hashKey":"3OA"},{"id":"199","text":"Судан","$$hashKey":"3OB"},{"id":"200","text":"Суринам","$$hashKey":"3OC"},{"id":"187","text":"Сьерра-Леоне","$$hashKey":"3OD"},{"id":"207","text":"Таджикистан","$$hashKey":"3OE"},{"id":"209","text":"Таиланд","$$hashKey":"3OF"},{"id":"206","text":"Тайвань (провинция Китая)","$$hashKey":"3OG"},{"id":"208","text":"Танзания","$$hashKey":"3OH"},{"id":"210","text":"Того","$$hashKey":"3OI"},{"id":"211","text":"Токелау","$$hashKey":"3OJ"},{"id":"212","text":"Тонга","$$hashKey":"3OK"},{"id":"213","text":"Тринидад и Тобаго","$$hashKey":"3OL"},{"id":"255","text":"Тристан-да-Кунья","$$hashKey":"3OM"},{"id":"218","text":"Тувалу","$$hashKey":"3ON"},{"id":"214","text":"Тунис","$$hashKey":"3OO"},{"id":"216","text":"Туркменистан","$$hashKey":"3OP"},{"id":"215","text":"Турция","$$hashKey":"3OQ"},{"id":"219","text":"Уганда","$$hashKey":"3OR"},{"id":"226","text":"Узбекистан","$$hashKey":"3OS"},{"id":"220","text":"Украина","$$hashKey":"3OT"},{"id":"233","text":"Уоллис и Футуна","$$hashKey":"3OU"},{"id":"225","text":"Уругвай","$$hashKey":"3OV"},{"id":"70","text":"Фарерские острова","$$hashKey":"3OW"},{"id":"71","text":"Фиджи","$$hashKey":"3OX"},{"id":"168","text":"Филиппины","$$hashKey":"3OY"},{"id":"72","text":"Финляндия","$$hashKey":"3OZ"},{"id":"69","text":"Фолклендские (Мальвинские) острова","$$hashKey":"3P0"},{"id":"73","text":"Франция","$$hashKey":"3P1"},{"id":"74","text":"Франция, Метрополия","$$hashKey":"3P2"},{"id":"75","text":"Французская Гвиана","$$hashKey":"3P3"},{"id":"76","text":"Французская Полинезия","$$hashKey":"3P4"},{"id":"77","text":"Французские Южные территории","$$hashKey":"3P5"},{"id":"94","text":"Херд и Макдональд, острова","$$hashKey":"3P6"},{"id":"53","text":"Хорватия","$$hashKey":"3P7"},{"id":"41","text":"Центрально-Африканская Республика","$$hashKey":"3P8"},{"id":"42","text":"Чад","$$hashKey":"3P9"},{"id":"242","text":"Черногория","$$hashKey":"3PA"},{"id":"56","text":"Чехия","$$hashKey":"3PB"},{"id":"43","text":"Чили","$$hashKey":"3PC"},{"id":"204","text":"Швейцария","$$hashKey":"3PD"},{"id":"203","text":"Швеция","$$hashKey":"3PE"},{"id":"201","text":"Шпицберген и Ян Майен","$$hashKey":"3PF"},{"id":"196","text":"Шри-Ланка","$$hashKey":"3PG"},{"id":"62","text":"Эквадор","$$hashKey":"3PH"},{"id":"65","text":"Экваториальная Гвинея","$$hashKey":"3PI"},{"id":"66","text":"Эритрея","$$hashKey":"3PJ"},{"id":"67","text":"Эстония","$$hashKey":"3PK"},{"id":"68","text":"Эфиопия","$$hashKey":"3PL"},{"id":"194","text":"Южная Джорджия и Южные Сандвичевы острова","$$hashKey":"3PM"},{"id":"193","text":"Южно-Африканская Республика","$$hashKey":"3PN"},{"id":"248","text":"Южный Судан","$$hashKey":"3PO"},{"id":"106","text":"Ямайка","$$hashKey":"3PP"},{"id":"107","text":"Япония","$$hashKey":"3PQ"}]},"$$hashKey":"06V"},{"autoreload":true,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"zone_id","label":{"en":"Region / state","ru":"Регион","ua":"Область","pl":"Obszar"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":[],"rules":{"api":{},"byLength":[],"equal":{},"notEmpty":{"enabled":true,"errorText":{"en":"Please select a zone!","ru":"Выберите регион!"}},"regexp":[]},"saveToComment":false,"type":"select","values":{"filter":"country_id","method":"getZones","saved":[],"source":"model"},"valuesList":{"ru":[{"id":"","text":" ---Wybierz--- ","$$hashKey":"44R"},{"id":0,"text":" ---Brak--- ","$$hashKey":"44S"}]},"$$hashKey":"06W"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"saved":"","source":"saved"},"description":{},"id":"captcha","label":{"en":"Verification code","ru":"Защитный код"},"mask":{"source":"saved"},"objects":{"customer":true,"order":false},"placeholder":{},"rules":{"api":{"enabled":true,"errorText":{"en":"Verification code does not match the image!","ru":"Защитный код не соответствует изображению!"},"method":"checkCaptcha"},"byLength":{},"equal":{},"notEmpty":{"enabled":true,"errorText":{"en":"Please enter verification code!","ru":"Введите защитный код!"}},"regexp":{}},"saveToComment":false,"type":"captcha","valuesList":{},"$$hashKey":"05W"},{"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"filter":"address_id","method":"isDefaultAddress","saved":"","source":"model"},"description":{},"id":"default","label":{"en":"Default address","ru":"Основной адрес"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":[],"rules":{"api":{},"byLength":[],"equal":{},"notEmpty":{},"regexp":[]},"saveToComment":false,"type":"radio","values":{"method":"getYesNo","saved":[],"source":"model"},"valuesList":{"ru":[{"id":"1","text":"Tak"},{"id":"0","text":"nie"}]},"$$hashKey":"06X"},{"autoreload":true,"custom":false,"dateEndType":"calculated","dateSelected":{},"dateStartType":"calculated","default":{"method":"getDefaultAddressId","saved":"","source":"model"},"description":{},"id":"address_id","label":{"en":"Select address","ru":"Выберите адрес"},"mask":{"source":"saved"},"objects":{"address":true},"placeholder":[],"rules":{"api":{},"byLength":[],"equal":{},"notEmpty":{},"regexp":[]},"saveToComment":false,"type":"select","values":{"method":"getAddresses","saved":[],"source":"model"},"valuesList":{"ru":[]},"$$hashKey":"06Y"}],"headers":[{"custom":true,"id":"main","label":{"en":"Your Personal Details","ru":"Основная информация","ua":"Основна інформація","pl":"Twoje dane osobowe"},"$$hashKey":"061"},{"custom":true,"id":"address","label":{"en":"Your Address","ru":"Ваш адрес","ua":"Ваша адреса","pl":"Twój adres"},"$$hashKey":"062"}],"modules":[],"register":{"agreementCheckboxInit":false,"agreementId":0,"displayAgreementCheckbox":false,"geoIpMode":1,"rows":{"default":[{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"main","masterField":"","requireWhen":{},"required":0,"sortOrder":1,"type":"header","$$hashKey":"0UF"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"email","masterField":"","requireWhen":{},"required":"1","sortOrder":2,"type":"field","$$hashKey":"0UG"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"firstname","masterField":"","requireWhen":{},"required":"1","sortOrder":3,"type":"field","$$hashKey":"0UH"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"lastname","masterField":"","requireWhen":{},"required":"1","sortOrder":4,"type":"field","$$hashKey":"0UI"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"telephone","masterField":"","requireWhen":{},"required":"1","sortOrder":5,"type":"field","$$hashKey":"0UJ"},{"type":"field","id":"customer_group_id","sortOrder":6,"$$hashKey":"474","hideForLogged":false,"hideForGuest":false,"masterField":"","displayWhen":{},"required":0,"requireWhen":{}},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"address","masterField":"","requireWhen":{},"required":0,"sortOrder":7,"type":"header","$$hashKey":"0UK"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"country_id","masterField":"","requireWhen":{},"required":"1","sortOrder":8,"type":"field","$$hashKey":"0UL"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"zone_id","masterField":"","requireWhen":{},"required":"1","sortOrder":9,"type":"field","$$hashKey":"0UM"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"city","masterField":"","requireWhen":{},"required":"1","sortOrder":10,"type":"field","$$hashKey":"0UN"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"postcode","masterField":"","requireWhen":{},"required":"1","sortOrder":11,"type":"field","$$hashKey":"0UO"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"address_1","masterField":"","requireWhen":{},"required":"1","sortOrder":12,"type":"field","$$hashKey":"0UP"},{"displayWhen":{},"hideForGuest":false,"hideForLogged":false,"id":"newsletter","masterField":"","requireWhen":{},"required":0,"sortOrder":13,"type":"field","$$hashKey":"0UQ"}]},"scrollToError":true,"useGeoIp":false,"useGoogleApi":false},"replaceAddress":true,"replaceCart":true,"replaceCheckout":true,"replaceEdit":true,"replaceRegister":true,"javascriptCallback":"$(\'#cart > ul\').load(\'index.php?route=common/cart/info ul li\');/*$(\'nav#top\').load(\'index.php?route=common/simple_connector/header #top > div\');*/","addressFormats":{"1":{},"2":{},"3":{}}}', 0),
(4969, 0, 'simple', 'simple_replace_edit', '1', 0),
(4970, 0, 'simple', 'simple_replace_address', '1', 0),
(4971, 0, 'simple', 'simple_module', '[]', 1),
(4972, 0, 'simple', 'simple_license', '', 0),
(4973, 0, 'simple', 'simple_custom_fields', '[]', 1),
(5058, 0, 'exchange1c', 'exchange1c_status', '1', 0),
(5057, 0, 'exchange1c', 'exchange1c_password', '1c', 0),
(5056, 0, 'exchange1c', 'exchange1c_username', '1c', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'В наличии'),
(8, 1, 'Предзаказ'),
(5, 1, 'Нет в наличии'),
(6, 1, 'Ожидание 2-3 дня'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2 - 3 Days'),
(7, 3, 'В наличии'),
(8, 3, 'Предзаказ'),
(5, 3, 'Нет в наличии'),
(6, 3, 'Ожидание 2-3 дня'),
(7, 4, 'В наличии'),
(8, 4, 'Предзаказ'),
(5, 4, 'Нет в наличии'),
(6, 4, 'Ожидание 2-3 дня');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Налоги', 'Облагаемые налогом', '2009-01-06 23:21:53', '2011-03-09 21:17:10'),
(10, 'Цифровые товары', 'Цифровые', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'НДС (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Экологический налог (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_ulogin`
--

CREATE TABLE `oc_ulogin` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `identity` varchar(255) NOT NULL,
  `network` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_ulogin`
--

INSERT INTO `oc_ulogin` (`id`, `user_id`, `identity`, `network`) VALUES
(3, 25, 'https://plus.google.com/u/0/107024457275535277296/', 'google');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_url_alias`
--

CREATE TABLE `oc_url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_url_alias`
--

INSERT INTO `oc_url_alias` (`url_alias_id`, `query`, `keyword`) VALUES
(601, 'account/voucher', 'vouchers'),
(602, 'account/wishlist', 'wishlist'),
(603, 'account/account', 'my-account'),
(604, 'checkout/cart', 'cart'),
(605, 'checkout/checkout', 'checkout'),
(606, 'account/login', 'login'),
(607, 'account/logout', 'logout'),
(608, 'account/order', 'order-history'),
(609, 'account/newsletter', 'newsletter'),
(610, 'product/special', 'specials'),
(611, 'affiliate/account', 'affiliates'),
(612, 'checkout/voucher', 'gift-vouchers'),
(613, 'product/manufacturer', 'brands'),
(614, 'information/contact', 'contact-us'),
(615, 'account/return/insert', 'request-return'),
(616, 'information/sitemap', 'sitemap'),
(617, 'account/forgotten', 'forgot-password'),
(618, 'account/download', 'downloads'),
(619, 'account/return', 'returns'),
(620, 'account/transaction', 'transactions'),
(621, 'account/register', 'create-account'),
(622, 'product/compare', 'compare-products'),
(623, 'product/search', 'search'),
(624, 'account/edit', 'edit-account'),
(625, 'account/password', 'change-password'),
(626, 'account/address', 'address-book'),
(627, 'account/reward', 'reward-points'),
(628, 'affiliate/edit', 'edit-affiliate-account'),
(629, 'affiliate/password', 'change-affiliate-password'),
(630, 'affiliate/payment', 'affiliate-payment-options'),
(631, 'affiliate/tracking', 'affiliate-tracking-code'),
(632, 'affiliate/transaction', 'affiliate-transactions'),
(633, 'affiliate/logout', 'affiliate-logout'),
(634, 'affiliate/forgotten', 'affiliate-forgot-password'),
(635, 'affiliate/register', 'create-affiliate-account'),
(636, 'affiliate/login', 'affiliate-login'),
(637, 'account/return/add', 'add-return'),
(638, 'common/home', ''),
(730, 'manufacturer_id=8', 'apple'),
(871, 'information_id=4', 'about_company'),
(828, 'manufacturer_id=9', 'canon'),
(829, 'manufacturer_id=5', 'htc'),
(830, 'manufacturer_id=7', 'hewlett-packard'),
(831, 'manufacturer_id=6', 'palm'),
(832, 'manufacturer_id=10', 'sony'),
(841, 'information_id=6', 'delivery'),
(842, 'information_id=3', 'privacy'),
(843, 'information_id=5', 'terms'),
(844, 'news/headlines', 'blogspage'),
(870, 'news_id=12', 'Lorem-ipsum-12'),
(848, 'news/ncategory', 'nblog'),
(12021, 'ncat=59', 'gallery1'),
(872, 'information_id=7', 'cooperation'),
(873, 'information_id=7', 'cooperation'),
(3687, 'information_id=10', ''),
(4401, 'news_id=18', 'osen---vesna-2017-18-18'),
(6578, 'module/faq', 'faq'),
(4405, 'news_id=16', 'novaya-kollektsiya-zimnih-shapochek--Nikola-copy-copy-16'),
(4404, 'news_id=15', 'novaya-kollektsiya-zimnih-shapochek--Nikola-copy-15'),
(3688, 'information_id=10', 'adresa-futer-10'),
(12022, 'news_id=13', 'novaya-kollektsiya-zimnih-shapochek--Nikola-13'),
(6579, 'information_id=9', 'otpravka-9'),
(4402, 'news_id=14', 'novaya-kollektsiya-zimnih-shapochek--Nikola-copy-14'),
(1932, 'information_id=8', ''),
(1933, 'information_id=8', 'adresa-8'),
(20364, 'product_id=115', '15-Z-211-Fulham-115'),
(20363, 'product_id=114', '15-Z-114-WILD-114'),
(20362, 'product_id=113', '15-Z-08-SOFFIO-113'),
(20361, 'product_id=112', '15-Z-59K-SOFFIO-112'),
(20360, 'product_id=111', '15-Z-206K-SOFFIO-111'),
(20359, 'product_id=110', '15-Z-202-SOFFIO-110'),
(20358, 'product_id=109', '15-Z-208-SOFFIO-109'),
(20357, 'product_id=108', '15-Z-107-COFIL-108'),
(20356, 'product_id=107', '15-Z-60-107'),
(20355, 'product_id=106', '15-Z-251-SOFFIO-106'),
(20354, 'product_id=105', '15-Z-259-Fulham-105'),
(20353, 'product_id=104', '15-Z-226-PL-104'),
(20352, 'product_id=103', '15-Z-14-SOFFIO-103'),
(20351, 'product_id=102', '15-Z-13-SOFFIO-102'),
(20350, 'product_id=101', '15-Z-02-SOFFIO-101'),
(20245, 'product_id=100', '15-Z-141-SOFFIO-100'),
(20244, 'product_id=99', '15-Z-131-SOFFIO-99'),
(20349, 'product_id=98', '15-Z-217K-SOFFIO-98'),
(20348, 'product_id=97', '15-Z-267K-SOFFIO-97'),
(20241, 'product_id=96', '15-Z-269-96'),
(20347, 'product_id=95', '15-Z-266-Gemini-95'),
(20346, 'product_id=94', '15-Z-113-Gemini-94'),
(20345, 'product_id=93', '15-Z-265-Fulham-93'),
(20344, 'product_id=92', '15-Z-264-Fulham-92'),
(20343, 'product_id=91', '15-Z-263-Fulham-91'),
(20342, 'product_id=90', '15-Z-262-Fulham-90'),
(20341, 'product_id=89', '15-Z-261-SOFFIO-89'),
(20340, 'product_id=88', '15-Z-260-SOFFIO-88'),
(20339, 'product_id=87', '15-Z-112-COFIL-87'),
(20231, 'product_id=86', '15-Z-301-86'),
(20230, 'product_id=85', '15-Z-61-85'),
(20338, 'product_id=84', '15-Z-114K-WILD-84'),
(20337, 'product_id=83', '15-Z-270-Marilyn-83'),
(20227, 'product_id=82', '15-Z-113K-Gemini-82'),
(20226, 'product_id=81', '15-Z-03-1-81'),
(20225, 'product_id=80', '15-Z-121-SOFFIO-80'),
(20336, 'product_id=79', '15-Z-216-SOFFIO-79'),
(20335, 'product_id=78', '15-Z-223-SOFFIO-78'),
(20334, 'product_id=77', '15-Z-258-SOFFIO-77'),
(20333, 'product_id=76', '15-Z-203-SOFFIO-76'),
(20332, 'product_id=75', '15-Z-12-SOFFIO-75'),
(20331, 'product_id=74', '15-Z-210-Fulham-74'),
(20330, 'product_id=73', '15-Z-249-SOFFIO-73'),
(20217, 'product_id=72', '15-Z-63-72'),
(20216, 'product_id=71', '15-Z-62-71'),
(20329, 'product_id=70', '15-Z-102-Fulham-70'),
(20328, 'product_id=69', '15-Z-50-SOFFIO-69'),
(20327, 'product_id=68', '15-Z-55-SOFFIO-68'),
(20326, 'product_id=67', '15-Z-222-SOFFIO-67'),
(20325, 'product_id=66', '15-Z-59-SOFFIO-66'),
(20324, 'product_id=65', '15-Z-49-SOFFIO-65'),
(20323, 'product_id=64', '15-Z-10-SOFFIO-64'),
(20322, 'product_id=63', '15-Z-09-SOFFIO-63'),
(20321, 'product_id=62', '15-Z-53-SOFFIO-62'),
(20320, 'product_id=61', '15-Z-07-SOFFIO-61'),
(20319, 'product_id=60', '15-Z-58-Fulham-60'),
(20318, 'product_id=59', '15-Z-252-SOFFIO-59'),
(20317, 'product_id=58', '15-Z-256-SOFFIO-58'),
(20316, 'product_id=57', '15-Z-207-SOFFIO-57'),
(20315, 'product_id=56', '15-Z-111-Fulham-56'),
(20314, 'product_id=55', '15-Z-110-Fulham-55'),
(20313, 'product_id=54', '15-Z-109-Fulham-54'),
(20312, 'product_id=53', '15-Z-254-SOFFIO-53'),
(20311, 'product_id=52', '15-Z-244-SOFFIO-52'),
(20310, 'product_id=51', '15-Z-231-SOFFIO-51'),
(20309, 'product_id=50', '15-Z-104-Fulham-50'),
(20308, 'product_id=49', '15-Z-236-RX-49'),
(20307, 'product_id=48', '15-Z-241-SOFFIO-48'),
(20306, 'product_id=47', '15-Z-257-SOFFIO-47'),
(20305, 'product_id=46', '15-Z-230-SOFFIO-46'),
(20304, 'product_id=45', '15-Z-240-SOFFIO-45'),
(20303, 'product_id=44', '15-Z-11-SOFFIO-44'),
(20302, 'product_id=43', '15-Z-238-SOFFIO-43'),
(20301, 'product_id=42', '15-Z-234-SOFFIO-42'),
(20300, 'product_id=41', '15-Z-242-SOFFIO-41'),
(20299, 'product_id=40', '15-Z-214-SOFFIO-40'),
(20298, 'product_id=39', '15-Z-215-SOFFIO-39'),
(20297, 'product_id=38', '15-Z-209-SOFFIO-38'),
(20296, 'product_id=37', '15-Z-205-SOFFIO-37'),
(20295, 'product_id=36', '15-Z-204-SOFFIO-36'),
(20294, 'product_id=35', '15-Z-201-SOFFIO-35'),
(20293, 'product_id=34', '15-Z-225-Fulham-34'),
(20292, 'product_id=33', '15-Z-245-SOFFIO-33'),
(20291, 'product_id=32', '15-Z-239-SOFFIO-32'),
(20290, 'product_id=31', '15-Z-243-SOFFIO-31'),
(20289, 'product_id=30', '15-Z-247-SOFFIO-30'),
(20288, 'product_id=29', '15-Z-246-SOFFIO-29'),
(20287, 'product_id=28', '15-Z-250-SOFFIO-28'),
(20286, 'product_id=27', '15-Z-248-SOFFIO-27'),
(20285, 'product_id=26', '15-Z-253-SOFFIO-26'),
(20145, 'category_id=67', 'shapki-osen-zima-2015-67'),
(20261, 'product_id=1', '15-Z-54-SOFFIO-1'),
(20262, 'product_id=2', '15-Z-108-Cofil-2'),
(20263, 'product_id=3', '15-Z-206-SOFFIO-3'),
(20264, 'product_id=4', '15-Z-04-SOFFIO-izosoftova-podkl-4'),
(20265, 'product_id=5', '15-Z-52-SOFFIO-5'),
(20266, 'product_id=6', '15-Z-51-SOFFIO-6'),
(20267, 'product_id=7', '15-Z-217-SOFFIO-7'),
(20268, 'product_id=8', '15-Z-57-SOFFIO-8'),
(20269, 'product_id=9', '15-Z-220-Gemini-9'),
(20270, 'product_id=10', '15-Z-56-SOFFIO-10'),
(20156, 'product_id=11', '15-Z-56-SOFFIO-11'),
(20271, 'product_id=12', '15-Z-106-Fulham-12'),
(20272, 'product_id=13', '15-Z-103-Fulham-13'),
(20273, 'product_id=14', '15-Z-01-SOFFIO-14'),
(20274, 'product_id=15', '15-Z-218-Fulham-15'),
(20275, 'product_id=16', '15-Z-212-SOFFIO-16'),
(20276, 'product_id=17', '15-Z-05-SOFFIO-17'),
(20277, 'product_id=18', '15-Z-105-Fulham-18'),
(20278, 'product_id=19', '15-Z-101-Fulham-19'),
(20279, 'product_id=20', '15-Z-06-SOFFIO-20'),
(20280, 'product_id=21', '15-Z-03-Australia-21'),
(20281, 'product_id=22', '15-Z-255-SOFFIO-22'),
(20282, 'product_id=23', '15-Z-237-RX-23'),
(20283, 'product_id=24', '15-Z-235-SOFFIO-24'),
(20284, 'product_id=25', '15-Z-233-SOFFIO-25'),
(20144, 'category_id=59', 'all_products'),
(20365, 'category_id=68', '1123-68');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', '51ec5dbc7e6ce6a0715a51d0ce83cbe010e58adc', 'JCqfw5BdR', 'John', 'Doe', 'gmkostya@gmail.com', '', '', '127.0.0.1', 1, '2016-11-06 17:15:44');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{"access":["analytics\\/google_analytics","captcha\\/basic_captcha","captcha\\/google_captcha","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/nauthor","catalog\\/ncategory","catalog\\/ncomments","catalog\\/news","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","common\\/column_left","common\\/filemanager","common\\/menu","common\\/newsletter","common\\/newspanel","common\\/profile","common\\/sass","common\\/stats","csvprice_pro\\/app_about","csvprice_pro\\/app_category","csvprice_pro\\/app_cli","csvprice_pro\\/app_crontab","csvprice_pro\\/app_customer","csvprice_pro\\/app_footer","csvprice_pro\\/app_header","csvprice_pro\\/app_manufacturer","csvprice_pro\\/app_order","csvprice_pro\\/app_product","customer\\/custom_field","customer\\/customer","customer\\/customer_group","design\\/banner","design\\/layout","extension\\/analytics","extension\\/captcha","extension\\/feed","extension\\/fraud","extension\\/installer","extension\\/modification","extension\\/module","extension\\/openbay","extension\\/payment","extension\\/shipping","extension\\/total","feed\\/articles_google_base","feed\\/articles_google_sitemap","feed\\/google_base","feed\\/google_sitemap","feed\\/openbaypro","fraud\\/fraudlabspro","fraud\\/ip","fraud\\/maxmind","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","marketing\\/affiliate","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","module\\/account","module\\/affiliate","module\\/amazon_login","module\\/amazon_pay","module\\/banner","module\\/bestseller","module\\/carousel","module\\/category","module\\/csvprice_pro","module\\/ebay_listing","module\\/exchange1c","module\\/faq","module\\/featured","module\\/filter","module\\/google_hangouts","module\\/html","module\\/information","module\\/lang_editor","module\\/latest","module\\/mega_filter","module\\/metatags_create","module\\/ncategory","module\\/news","module\\/newsletters","module\\/ocjazz_seopro","module\\/popup_purchase","module\\/pp_button","module\\/pp_login","module\\/simple","module\\/slideshow","module\\/special","module\\/store","module\\/unisender","module\\/watermark","octeam\\/toolset","octeam_tools\\/cache","octeam_tools\\/seo_manager","openbay\\/amazon","openbay\\/amazon_listing","openbay\\/amazon_product","openbay\\/amazonus","openbay\\/amazonus_listing","openbay\\/amazonus_product","openbay\\/ebay","openbay\\/ebay_profile","openbay\\/ebay_template","openbay\\/etsy","openbay\\/etsy_product","openbay\\/etsy_shipping","openbay\\/etsy_shop","payment\\/amazon_login_pay","payment\\/authorizenet_aim","payment\\/authorizenet_sim","payment\\/bank_transfer","payment\\/bluepay_hosted","payment\\/bluepay_redirect","payment\\/cheque","payment\\/cod","payment\\/firstdata","payment\\/firstdata_remote","payment\\/free_checkout","payment\\/g2apay","payment\\/globalpay","payment\\/globalpay_remote","payment\\/klarna_account","payment\\/klarna_invoice","payment\\/liqpay","payment\\/nochex","payment\\/paymate","payment\\/paypoint","payment\\/payza","payment\\/perpetual_payments","payment\\/pp_express","payment\\/pp_payflow","payment\\/pp_payflow_iframe","payment\\/pp_pro","payment\\/pp_pro_iframe","payment\\/pp_standard","payment\\/qiwi_rest","payment\\/realex","payment\\/realex_remote","payment\\/sagepay_direct","payment\\/sagepay_server","payment\\/sagepay_us","payment\\/sberbank_transfer","payment\\/securetrading_pp","payment\\/securetrading_ws","payment\\/skrill","payment\\/twocheckout","payment\\/web_payment_software","payment\\/worldpay","report\\/affiliate","report\\/affiliate_activity","report\\/affiliate_login","report\\/customer_activity","report\\/customer_credit","report\\/customer_login","report\\/customer_online","report\\/customer_order","report\\/customer_reward","report\\/marketing","report\\/product_purchased","report\\/product_viewed","report\\/sale_coupon","report\\/sale_order","report\\/sale_return","report\\/sale_shipping","report\\/sale_tax","sale\\/order","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","setting\\/setting","setting\\/store","shipping\\/auspost","shipping\\/by_total","shipping\\/citylink","shipping\\/fedex","shipping\\/flat","shipping\\/free","shipping\\/item","shipping\\/parcelforce_48","shipping\\/pickup","shipping\\/royal_mail","shipping\\/ups","shipping\\/usps","shipping\\/weight","tool\\/backup","tool\\/error_log","tool\\/upload","total\\/coupon","total\\/credit","total\\/handling","total\\/klarna_fee","total\\/low_order_fee","total\\/reward","total\\/shipping","total\\/sub_total","total\\/tax","total\\/total","total\\/voucher","user\\/api","user\\/user","user\\/user_permission","module\\/exchange1c","shipping\\/free","module\\/ulogin_sets","module\\/ulogin","total\\/coupon","module\\/related_options"],"modify":["analytics\\/google_analytics","captcha\\/basic_captcha","captcha\\/google_captcha","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/nauthor","catalog\\/ncategory","catalog\\/ncomments","catalog\\/news","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","common\\/column_left","common\\/filemanager","common\\/menu","common\\/newsletter","common\\/newspanel","common\\/profile","common\\/sass","common\\/stats","csvprice_pro\\/app_about","csvprice_pro\\/app_category","csvprice_pro\\/app_cli","csvprice_pro\\/app_crontab","csvprice_pro\\/app_customer","csvprice_pro\\/app_footer","csvprice_pro\\/app_header","csvprice_pro\\/app_manufacturer","csvprice_pro\\/app_order","csvprice_pro\\/app_product","customer\\/custom_field","customer\\/customer","customer\\/customer_group","design\\/banner","design\\/layout","extension\\/analytics","extension\\/captcha","extension\\/feed","extension\\/fraud","extension\\/installer","extension\\/modification","extension\\/module","extension\\/openbay","extension\\/payment","extension\\/shipping","extension\\/total","feed\\/articles_google_base","feed\\/articles_google_sitemap","feed\\/google_base","feed\\/google_sitemap","feed\\/openbaypro","fraud\\/fraudlabspro","fraud\\/ip","fraud\\/maxmind","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","marketing\\/affiliate","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","module\\/account","module\\/affiliate","module\\/amazon_login","module\\/amazon_pay","module\\/banner","module\\/bestseller","module\\/carousel","module\\/category","module\\/csvprice_pro","module\\/ebay_listing","module\\/exchange1c","module\\/faq","module\\/featured","module\\/filter","module\\/google_hangouts","module\\/html","module\\/information","module\\/lang_editor","module\\/latest","module\\/mega_filter","module\\/metatags_create","module\\/ncategory","module\\/news","module\\/newsletters","module\\/ocjazz_seopro","module\\/popup_purchase","module\\/pp_button","module\\/pp_login","module\\/simple","module\\/slideshow","module\\/special","module\\/store","module\\/unisender","module\\/watermark","octeam\\/toolset","octeam_tools\\/cache","octeam_tools\\/seo_manager","openbay\\/amazon","openbay\\/amazon_listing","openbay\\/amazon_product","openbay\\/amazonus","openbay\\/amazonus_listing","openbay\\/amazonus_product","openbay\\/ebay","openbay\\/ebay_profile","openbay\\/ebay_template","openbay\\/etsy","openbay\\/etsy_product","openbay\\/etsy_shipping","openbay\\/etsy_shop","payment\\/amazon_login_pay","payment\\/authorizenet_aim","payment\\/authorizenet_sim","payment\\/bank_transfer","payment\\/bluepay_hosted","payment\\/bluepay_redirect","payment\\/cheque","payment\\/cod","payment\\/firstdata","payment\\/firstdata_remote","payment\\/free_checkout","payment\\/g2apay","payment\\/globalpay","payment\\/globalpay_remote","payment\\/klarna_account","payment\\/klarna_invoice","payment\\/liqpay","payment\\/nochex","payment\\/paymate","payment\\/paypoint","payment\\/payza","payment\\/perpetual_payments","payment\\/pp_express","payment\\/pp_payflow","payment\\/pp_payflow_iframe","payment\\/pp_pro","payment\\/pp_pro_iframe","payment\\/pp_standard","payment\\/qiwi_rest","payment\\/realex","payment\\/realex_remote","payment\\/sagepay_direct","payment\\/sagepay_server","payment\\/sagepay_us","payment\\/sberbank_transfer","payment\\/securetrading_pp","payment\\/securetrading_ws","payment\\/skrill","payment\\/twocheckout","payment\\/web_payment_software","payment\\/worldpay","report\\/affiliate","report\\/affiliate_activity","report\\/affiliate_login","report\\/customer_activity","report\\/customer_credit","report\\/customer_login","report\\/customer_online","report\\/customer_order","report\\/customer_reward","report\\/marketing","report\\/product_purchased","report\\/product_viewed","report\\/sale_coupon","report\\/sale_order","report\\/sale_return","report\\/sale_shipping","report\\/sale_tax","sale\\/order","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","setting\\/setting","setting\\/store","shipping\\/auspost","shipping\\/by_total","shipping\\/citylink","shipping\\/fedex","shipping\\/flat","shipping\\/free","shipping\\/item","shipping\\/parcelforce_48","shipping\\/pickup","shipping\\/royal_mail","shipping\\/ups","shipping\\/usps","shipping\\/weight","tool\\/backup","tool\\/error_log","tool\\/upload","total\\/coupon","total\\/credit","total\\/handling","total\\/klarna_fee","total\\/low_order_fee","total\\/reward","total\\/shipping","total\\/sub_total","total\\/tax","total\\/total","total\\/voucher","user\\/api","user\\/user","user\\/user_permission","module\\/exchange1c","shipping\\/free","module\\/ulogin_sets","module\\/ulogin","total\\/coupon","module\\/related_options"],"hiden":["module\\/amazon_login","module\\/amazon_pay","module\\/ebay_listing","module\\/pp_button","module\\/pp_login","payment\\/amazon_login_pay","payment\\/authorizenet_aim","payment\\/authorizenet_sim","payment\\/bluepay_hosted","payment\\/bluepay_redirect","payment\\/cheque","payment\\/firstdata","payment\\/firstdata_remote","payment\\/g2apay","payment\\/globalpay","payment\\/globalpay_remote","payment\\/klarna_account","payment\\/klarna_invoice","payment\\/liqpay","payment\\/nochex","payment\\/paymate","payment\\/paypoint","payment\\/payza","payment\\/perpetual_payments","payment\\/pp_express","payment\\/pp_payflow","payment\\/pp_payflow_iframe","payment\\/pp_pro","payment\\/pp_pro_iframe","payment\\/realex","payment\\/realex_remote","payment\\/sagepay_direct","payment\\/sagepay_server","payment\\/sagepay_us","payment\\/securetrading_pp","payment\\/securetrading_ws","payment\\/skrill","payment\\/twocheckout","payment\\/web_payment_software","payment\\/worldpay","shipping\\/auspost","shipping\\/by_total","shipping\\/citylink","shipping\\/fedex","shipping\\/royal_mail","shipping\\/ups","shipping\\/usps"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Рождество'),
(7, 1, 'День рождения'),
(8, 1, 'Основной'),
(6, 2, 'Christmas'),
(7, 2, 'Birthday'),
(8, 2, 'General'),
(6, 3, 'Рождество'),
(7, 3, 'День рождения'),
(8, 3, 'Основной'),
(6, 4, 'Рождество'),
(7, 4, 'День рождения'),
(8, 4, 'Основной');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_watermark_settings`
--

CREATE TABLE `oc_watermark_settings` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(1024) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_watermark_settings`
--

INSERT INTO `oc_watermark_settings` (`id`, `key`, `value`) VALUES
(1, 'active', '0'),
(2, 'image', 'catalog/logo.png'),
(3, 'size_x', '187'),
(4, 'size_y', '50'),
(5, 'zoom', '0.2'),
(6, 'pos_x', '-20'),
(7, 'pos_x_center', '0'),
(8, 'pos_y', '-20'),
(9, 'pos_y_center', '0'),
(10, 'opacity', '0.8'),
(11, 'category_image', '0'),
(12, 'product_thumb', '0'),
(13, 'product_popup', '1'),
(14, 'product_list', '0'),
(15, 'product_additional', '0'),
(16, 'product_related', '0'),
(17, 'product_in_compare', '0'),
(18, 'product_in_wish_list', '0'),
(19, 'product_in_cart', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00'),
(2, '1000.00'),
(5, '2.20'),
(6, '35.27');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Килограммы', 'кг'),
(2, 1, 'Граммы', 'г'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(1, 3, 'Килограммы', 'кг'),
(2, 3, 'Граммы', 'г'),
(1, 4, 'Килограммы', 'кг'),
(2, 4, 'Граммы', 'г');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Бадахшан', 'BDS', 1),
(2, 1, 'Бадгис', 'BDG', 1),
(3, 1, 'Баглан', 'BGL', 1),
(4, 1, 'Балх', 'BAL', 1),
(5, 1, 'Бамиан', 'BAM', 1),
(6, 1, 'Фарах', 'FRA', 1),
(7, 1, 'Фарьяб', 'FYB', 1),
(8, 1, 'Газни', 'GHA', 1),
(9, 1, 'Гор', 'GHO', 1),
(10, 1, 'Гильменд', 'HEL', 1),
(11, 1, 'Герат', 'HER', 1),
(12, 1, 'Джаузджан', 'JOW', 1),
(13, 1, 'Кабул', 'KAB', 1),
(14, 1, 'Кандагар', 'KAN', 1),
(15, 1, 'Каписа', 'KAP', 1),
(16, 1, 'Хост', 'KHO', 1),
(17, 1, 'Кунар', 'KNR', 1),
(18, 1, 'Кундуз', 'KDZ', 1),
(19, 1, 'Лагман', 'LAG', 1),
(20, 1, 'Логар', 'LOW', 1),
(21, 1, 'Нангархар', 'NAN', 1),
(22, 1, 'Нимроз', 'NIM', 1),
(23, 1, 'Нуристан', 'NUR', 1),
(24, 1, 'Урузган', 'ORU', 1),
(25, 1, 'Пактия', 'PIA', 1),
(26, 1, 'Пактика', 'PKA', 1),
(27, 1, 'Парван', 'PAR', 1),
(28, 1, 'Саманган', 'SAM', 1),
(29, 1, 'Сари-Пуль', 'SAR', 1),
(30, 1, 'Тахар', 'TAK', 1),
(31, 1, 'Вардак', 'WAR', 1),
(32, 1, 'Забуль', 'ZAB', 1),
(33, 2, 'Берат', 'BR', 1),
(34, 2, 'Булькиза', 'BU', 1),
(35, 2, 'Дельвина', 'DL', 1),
(36, 2, 'Девол', 'DV', 1),
(37, 2, 'Дибра', 'DI', 1),
(38, 2, 'Дуррес', 'DR', 1),
(39, 2, 'Эльбасан', 'EL', 1),
(40, 2, 'Колёня', 'ER', 1),
(41, 2, 'Фиери', 'FR', 1),
(42, 2, 'Гирокастра', 'GJ', 1),
(43, 2, 'Грамши', 'GR', 1),
(44, 2, 'Хас', 'HA', 1),
(45, 2, 'Кавая', 'KA', 1),
(46, 2, 'Курбин', 'KB', 1),
(47, 2, 'Кучова', 'KC', 1),
(48, 2, 'Корча', 'KO', 1),
(49, 2, 'Круя', 'KR', 1),
(50, 2, 'Кукес', 'KU', 1),
(51, 2, 'Либражди', 'LB', 1),
(52, 2, 'Лежа', 'LE', 1),
(53, 2, 'Люшня', 'LU', 1),
(54, 2, 'Мальси-э-Мади', 'MM', 1),
(55, 2, 'Малакастра', 'MK', 1),
(56, 2, 'Мати', 'MT', 1),
(57, 2, 'Мирдита', 'MR', 1),
(58, 2, 'Пекини', 'PQ', 1),
(59, 2, 'Пермети', 'PR', 1),
(60, 2, 'Поградец', 'PG', 1),
(61, 2, 'Пука', 'PU', 1),
(62, 2, 'Шкодер', 'SH', 1),
(63, 2, 'Скрапари', 'SK', 1),
(64, 2, 'Саранда', 'SR', 1),
(65, 2, 'Тепелена', 'TE', 1),
(66, 2, 'Тропоя', 'TP', 1),
(67, 2, 'Тирана', 'TR', 1),
(68, 2, 'Влёра', 'VL', 1),
(69, 3, 'Адрар', 'ADR', 1),
(70, 3, 'Айн-Дефла', 'ADE', 1),
(71, 3, 'Айн-Темухент', 'ATE', 1),
(72, 3, 'Алжир', 'ALG', 1),
(73, 3, 'Аннаба', 'ANN', 1),
(74, 3, 'Батна', 'BAT', 1),
(75, 3, 'Бешар', 'BEC', 1),
(76, 3, 'Беджая', 'BEJ', 1),
(77, 3, 'Бискра', 'BIS', 1),
(78, 3, 'Блида', 'BLI', 1),
(79, 3, 'Бордж-Бу-Арреридж', 'BBA', 1),
(80, 3, 'Буйра', 'BOA', 1),
(81, 3, 'Бумердес', 'BMD', 1),
(82, 3, 'Шлеф', 'CHL', 1),
(83, 3, 'Константина', 'CON', 1),
(84, 3, 'Джельфа', 'DJE', 1),
(85, 3, 'Эль-Баяд', 'EBA', 1),
(86, 3, 'Эль-Уэд', 'EOU', 1),
(87, 3, 'Эль-Тарф', 'ETA', 1),
(88, 3, 'Гардая', 'GHA', 1),
(89, 3, 'Гуэльма', 'GUE', 1),
(90, 3, 'Иллизи', 'ILL', 1),
(91, 3, 'Джиджель', 'JIJ', 1),
(92, 3, 'Хеншела', 'KHE', 1),
(93, 3, 'Лагуат', 'LAG', 1),
(94, 3, 'Маскара', 'MUA', 1),
(95, 3, 'Медеа', 'MED', 1),
(96, 3, 'Мила', 'MIL', 1),
(97, 3, 'Мостаганем', 'MOS', 1),
(98, 3, 'Мсила', 'MSI', 1),
(99, 3, 'Наама', 'NAA', 1),
(100, 3, 'Оран', 'ORA', 1),
(101, 3, 'Уаргла', 'OUA', 1),
(102, 3, 'Ум Эль-Буахи', 'OEB', 1),
(103, 3, 'Релизан', 'REL', 1),
(104, 3, 'Саида', 'SAI', 1),
(105, 3, 'Сетиф', 'SET', 1),
(106, 3, 'Сиди-Бель-Аббес', 'SBA', 1),
(107, 3, 'Скикда', 'SKI', 1),
(108, 3, 'Сук-Ахрас', 'SAH', 1),
(109, 3, 'Таменрассет', 'TAM', 1),
(110, 3, 'Тебесса', 'TEB', 1),
(111, 3, 'Тиарет', 'TIA', 1),
(112, 3, 'Тиндуф', 'TIN', 1),
(113, 3, 'Типаза', 'TIP', 1),
(114, 3, 'Тиссемсилт', 'TIS', 1),
(115, 3, 'Тизи-Узу', 'TOU', 1),
(116, 3, 'Тлемсен', 'TLE', 1),
(117, 4, 'Восточный округ', 'E', 1),
(118, 4, 'Мануа', 'M', 1),
(119, 4, 'Остров Роз', 'R', 1),
(120, 4, 'Остров Суэйнс', 'S', 1),
(121, 4, 'Западный округ', 'W', 1),
(122, 5, 'Андорра-ла-Велья', 'ALV', 1),
(123, 5, 'Канильо', 'CAN', 1),
(124, 5, 'Энкамп', 'ENC', 1),
(125, 5, 'Эскальдес-Энгордань', 'ESE', 1),
(126, 5, 'Ла-Массана', 'LMA', 1),
(127, 5, 'Ордино', 'ORD', 1),
(128, 5, 'Сант-Жулия-де-Лория', 'SJL', 1),
(129, 6, 'Бенго', 'BGO', 1),
(130, 6, 'Бенгела', 'BGU', 1),
(131, 6, 'Бие', 'BIE', 1),
(132, 6, 'Кабинда', 'CAB', 1),
(133, 6, 'Квандо-Кубанго', 'CCU', 1),
(134, 6, 'Северная Кванза', 'CNO', 1),
(135, 6, 'Южная Кванза', 'CUS', 1),
(136, 6, 'Кунене', 'CNN', 1),
(137, 6, 'Уамбо', 'HUA', 1),
(138, 6, 'Уила', 'HUI', 1),
(139, 6, 'Луанда', 'LUA', 1),
(140, 6, 'Северная Лунда', 'LNO', 1),
(141, 6, 'Южная Лунда', 'LSU', 1),
(142, 6, 'Маланже', 'MAL', 1),
(143, 6, 'Мошико', 'MOX', 1),
(144, 6, 'Намибе', 'NAM', 1),
(145, 6, 'Уиже', 'UIG', 1),
(146, 6, 'Заире', 'ZAI', 1),
(147, 9, 'Сент-Джордж', 'ASG', 1),
(148, 9, 'Сент-Джон', 'ASJ', 1),
(149, 9, 'Сент-Мери', 'ASM', 1),
(150, 9, 'Сент-Пол', 'ASL', 1),
(151, 9, 'Сент-Петер', 'ASR', 1),
(152, 9, 'Сент-Филип', 'ASH', 1),
(153, 9, 'Барбуда', 'BAR', 1),
(154, 9, 'Редонда', 'RED', 1),
(155, 10, 'Антарктида и острова Южной Атлантики', 'AN', 1),
(156, 10, 'Буэнос-Айрес', 'BA', 1),
(157, 10, 'Катамарка', 'CA', 1),
(158, 10, 'Чако', 'CH', 1),
(159, 10, 'Чубут', 'CU', 1),
(160, 10, 'Кордова', 'CO', 1),
(161, 10, 'Корриентес', 'CR', 1),
(162, 10, 'Федеральный округ', 'DF', 1),
(163, 10, 'Энтре-Риос', 'ER', 1),
(164, 10, 'Формоса', 'FO', 1),
(165, 10, 'Жужуй', 'JU', 1),
(166, 10, 'Ла-Пампа', 'LP', 1),
(167, 10, 'Ла-Риоха', 'LR', 1),
(168, 10, 'Мендоса', 'ME', 1),
(169, 10, 'Мисьонес', 'MI', 1),
(170, 10, 'Неукен', 'NE', 1),
(171, 10, 'Рио-Негро', 'RN', 1),
(172, 10, 'Сальта', 'SA', 1),
(173, 10, 'Сан-Хуан', 'SJ', 1),
(174, 10, 'Сан-Луис', 'SL', 1),
(175, 10, 'Санта-Крус', 'SC', 1),
(176, 10, 'Санта-Фе', 'SF', 1),
(177, 10, 'Сантьяго-дель-Эстеро', 'SD', 1),
(178, 10, 'Тьерра-дель-Фуэго', 'TF', 1),
(179, 10, 'Тукуман', 'TU', 1),
(180, 11, 'Арагацотн', 'AGT', 1),
(181, 11, 'Арарат', 'ARR', 1),
(182, 11, 'Армавир', 'ARM', 1),
(183, 11, 'Гегаркуник', 'GEG', 1),
(184, 11, 'Котайк', 'KOT', 1),
(185, 11, 'Лори', 'LOR', 1),
(186, 11, 'Ширак', 'SHI', 1),
(187, 11, 'Сюник', 'SYU', 1),
(188, 11, 'Тавуш', 'TAV', 1),
(189, 11, 'Вайоц Дзор', 'VAY', 1),
(190, 11, 'Ереван', 'YER', 1),
(191, 13, 'Австралийская столичная территория', 'ACT', 1),
(192, 13, 'Новый Южный Уэльс', 'NSW', 1),
(193, 13, 'Северная территория', 'NT', 1),
(194, 13, 'Квинсленд', 'QLD', 1),
(195, 13, 'Южная Австралия', 'SA', 1),
(196, 13, 'Тасмания', 'TAS', 1),
(197, 13, 'Виктория', 'VIC', 1),
(198, 13, 'Западная Австралия', 'WA', 1),
(199, 14, 'Бургенланд', 'BUR', 1),
(200, 14, 'Каринтия', 'KAR', 1),
(201, 14, 'Нижняя Австрия', 'NOS', 1),
(202, 14, 'Верхняя Австрия', 'OOS', 1),
(203, 14, 'Зальцбург', 'SAL', 1),
(204, 14, 'Штирия', 'STE', 1),
(205, 14, 'Тироль', 'TIR', 1),
(206, 14, 'Форарльберг', 'VOR', 1),
(207, 14, 'Вена', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Брест', 'BR', 1),
(338, 20, 'Гомель', 'HO', 1),
(339, 20, 'Минск', 'HM', 1),
(340, 20, 'Гродно', 'HR', 1),
(341, 20, 'Могилев', 'MA', 1),
(342, 20, 'Минская область', 'MI', 1),
(343, 20, 'Витебск', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liege', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapa', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceara', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espirito Santo', 'ES', 1),
(448, 30, 'Goias', 'GO', 1),
(449, 30, 'Maranhao', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Para', 'PA', 1),
(454, 30, 'Paraiba', 'PB', 1),
(455, 30, 'Parana', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piaui', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondonia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'Sao Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Hi', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chi', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovar-Bilogora', 'BB', 1),
(848, 53, 'City of Zagreb', 'CZ', 1),
(849, 53, 'Dubrovnik-Neretva', 'DN', 1),
(850, 53, 'Istra', 'IS', 1),
(851, 53, 'Karlovac', 'KA', 1),
(852, 53, 'Koprivnica-Krizevci', 'KK', 1),
(853, 53, 'Krapina-Zagorje', 'KZ', 1),
(854, 53, 'Lika-Senj', 'LS', 1),
(855, 53, 'Medimurje', 'ME', 1),
(856, 53, 'Osijek-Baranja', 'OB', 1),
(857, 53, 'Pozega-Slavonia', 'PS', 1),
(858, 53, 'Primorje-Gorski Kotar', 'PG', 1),
(859, 53, 'Sibenik', 'SI', 1),
(860, 53, 'Sisak-Moslavina', 'SM', 1),
(861, 53, 'Slavonski Brod-Posavina', 'SB', 1),
(862, 53, 'Split-Dalmatia', 'SD', 1),
(863, 53, 'Varazdin', 'VA', 1),
(864, 53, 'Virovitica-Podravina', 'VP', 1),
(865, 53, 'Vukovar-Srijem', 'VS', 1),
(866, 53, 'Zadar-Knin', 'ZK', 1),
(867, 53, 'Zagreb', 'ZA', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan Laani', 'AL', 1),
(1086, 72, 'Etela-Suomen Laani', 'ES', 1),
(1087, 72, 'Ita-Suomen Laani', 'IS', 1),
(1088, 72, 'Lansi-Suomen Laani', 'LS', 1),
(1089, 72, 'Lapin Lanani', 'LA', 1),
(1090, 72, 'Oulun Laani', 'OU', 1),
(1091, 73, 'Alsace', 'AL', 1),
(1092, 73, 'Aquitaine', 'AQ', 1),
(1093, 73, 'Auvergne', 'AU', 1),
(1094, 73, 'Brittany', 'BR', 1),
(1095, 73, 'Burgundy', 'BU', 1),
(1096, 73, 'Center Loire Valley', 'CE', 1),
(1097, 73, 'Champagne', 'CH', 1),
(1098, 73, 'Corse', 'CO', 1),
(1099, 73, 'France Comte', 'FR', 1),
(1100, 73, 'Languedoc Roussillon', 'LA', 1),
(1101, 73, 'Limousin', 'LI', 1),
(1102, 73, 'Lorraine', 'LO', 1),
(1103, 73, 'Midi Pyrenees', 'MI', 1),
(1104, 73, 'Nord Pas de Calais', 'NO', 1),
(1105, 73, 'Normandy', 'NR', 1),
(1106, 73, 'Paris / Ill de France', 'PA', 1),
(1107, 73, 'Picardie', 'PI', 1),
(1108, 73, 'Poitou Charente', 'PO', 1),
(1109, 73, 'Provence', 'PR', 1),
(1110, 73, 'Rhone Alps', 'RH', 1),
(1111, 73, 'Riviera', 'RI', 1),
(1112, 73, 'Western Loire Valley', 'WE', 1),
(1113, 74, 'Etranger', 'Et', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1428, 97, 'Bacs-Kiskun', 'BK', 1),
(1429, 97, 'Baranya', 'BA', 1),
(1430, 97, 'Bekes', 'BE', 1),
(1431, 97, 'Bekescsaba', 'BS', 1),
(1432, 97, 'Borsod-Abauj-Zemplen', 'BZ', 1),
(1433, 97, 'Budapest', 'BU', 1),
(1434, 97, 'Csongrad', 'CS', 1),
(1435, 97, 'Debrecen', 'DE', 1),
(1436, 97, 'Dunaujvaros', 'DU', 1),
(1437, 97, 'Eger', 'EG', 1),
(1438, 97, 'Fejer', 'FE', 1),
(1439, 97, 'Gyor', 'GY', 1),
(1440, 97, 'Gyor-Moson-Sopron', 'GM', 1),
(1441, 97, 'Hajdu-Bihar', 'HB', 1),
(1442, 97, 'Heves', 'HE', 1),
(1443, 97, 'Hodmezovasarhely', 'HO', 1),
(1444, 97, 'Jasz-Nagykun-Szolnok', 'JN', 1),
(1445, 97, 'Kaposvar', 'KA', 1),
(1446, 97, 'Kecskemet', 'KE', 1),
(1447, 97, 'Komarom-Esztergom', 'KO', 1),
(1448, 97, 'Miskolc', 'MI', 1),
(1449, 97, 'Nagykanizsa', 'NA', 1),
(1450, 97, 'Nograd', 'NO', 1),
(1451, 97, 'Nyiregyhaza', 'NY', 1),
(1452, 97, 'Pecs', 'PE', 1),
(1453, 97, 'Pest', 'PS', 1),
(1454, 97, 'Somogy', 'SO', 1),
(1455, 97, 'Sopron', 'SP', 1),
(1456, 97, 'Szabolcs-Szatmar-Bereg', 'SS', 1),
(1457, 97, 'Szeged', 'SZ', 1),
(1458, 97, 'Szekesfehervar', 'SE', 1),
(1459, 97, 'Szolnok', 'SL', 1),
(1460, 97, 'Szombathely', 'SM', 1),
(1461, 97, 'Tatabanya', 'TA', 1),
(1462, 97, 'Tolna', 'TO', 1),
(1463, 97, 'Vas', 'VA', 1),
(1464, 97, 'Veszprem', 'VE', 1),
(1465, 97, 'Zala', 'ZA', 1),
(1466, 97, 'Zalaegerszeg', 'ZZ', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Pondicherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Алматинская область', 'AL', 1),
(1717, 109, 'Алматы - город республ-го значения', 'AC', 1),
(1718, 109, 'Акмолинская область', 'AM', 1),
(1719, 109, 'Актюбинская область', 'AQ', 1),
(1720, 109, 'Астана - город республ-го значения', 'AS', 1),
(1721, 109, 'Атырауская область', 'AT', 1),
(1722, 109, 'Западно-Казахстанская область', 'BA', 1),
(1723, 109, 'Байконур - город республ-го значения', 'BY', 1),
(1724, 109, 'Мангистауская область', 'MA', 1),
(1725, 109, 'Южно-Казахстанская область', 'ON', 1),
(1726, 109, 'Павлодарская область', 'PA', 1),
(1727, 109, 'Карагандинская область', 'QA', 1),
(1728, 109, 'Костанайская область', 'QO', 1),
(1729, 109, 'Кызылординская область', 'QY', 1),
(1730, 109, 'Восточно-Казахстанская область', 'SH', 1),
(1731, 109, 'Северо-Казахстанская область', 'SO', 1),
(1732, 109, 'Жамбылская область', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1819, 117, 'Aizkraukles Rajons', 'AIZ', 1),
(1820, 117, 'Aluksnes Rajons', 'ALU', 1),
(1821, 117, 'Balvu Rajons', 'BAL', 1),
(1822, 117, 'Bauskas Rajons', 'BAU', 1),
(1823, 117, 'Cesu Rajons', 'CES', 1),
(1824, 117, 'Daugavpils Rajons', 'DGR', 1),
(1825, 117, 'Dobeles Rajons', 'DOB', 1),
(1826, 117, 'Gulbenes Rajons', 'GUL', 1),
(1827, 117, 'Jekabpils Rajons', 'JEK', 1),
(1828, 117, 'Jelgavas Rajons', 'JGR', 1),
(1829, 117, 'Kraslavas Rajons', 'KRA', 1),
(1830, 117, 'Kuldigas Rajons', 'KUL', 1),
(1831, 117, 'Liepajas Rajons', 'LPR', 1),
(1832, 117, 'Limbazu Rajons', 'LIM', 1),
(1833, 117, 'Ludzas Rajons', 'LUD', 1),
(1834, 117, 'Madonas Rajons', 'MAD', 1),
(1835, 117, 'Ogres Rajons', 'OGR', 1),
(1836, 117, 'Preilu Rajons', 'PRE', 1),
(1837, 117, 'Rezeknes Rajons', 'RZR', 1),
(1838, 117, 'Rigas Rajons', 'RGR', 1),
(1839, 117, 'Saldus Rajons', 'SAL', 1),
(1840, 117, 'Talsu Rajons', 'TAL', 1),
(1841, 117, 'Tukuma Rajons', 'TUK', 1),
(1842, 117, 'Valkas Rajons', 'VLK', 1),
(1843, 117, 'Valmieras Rajons', 'VLM', 1),
(1844, 117, 'Ventspils Rajons', 'VSR', 1),
(1845, 117, 'Daugavpils', 'DGV', 1),
(1846, 117, 'Jelgava', 'JGV', 1),
(1847, 117, 'Jurmala', 'JUR', 1),
(1848, 117, 'Liepaja', 'LPK', 1),
(1849, 117, 'Rezekne', 'RZK', 1),
(1850, 117, 'Riga', 'RGA', 1),
(1851, 117, 'Ventspils', 'VSL', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'JO', 1),
(1972, 129, 'Kedah', 'KE', 1),
(1973, 129, 'Kelantan', 'KL', 1),
(1974, 129, 'Labuan', 'LA', 1),
(1975, 129, 'Melaka', 'ME', 1),
(1976, 129, 'Negeri Sembilan', 'NS', 1),
(1977, 129, 'Pahang', 'PA', 1),
(1978, 129, 'Perak', 'PE', 1),
(1979, 129, 'Perlis', 'PR', 1),
(1980, 129, 'Pulau Pinang', 'PP', 1),
(1981, 129, 'Sabah', 'SA', 1),
(1982, 129, 'Sarawak', 'SR', 1),
(1983, 129, 'Selangor', 'SE', 1),
(1984, 129, 'Terengganu', 'TE', 1),
(1985, 129, 'Wilayah Persekutuan', 'WP', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairprarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Республика Хакасия', 'KK', 1),
(2722, 176, 'Московская область', 'MOS', 1),
(2723, 176, 'Чукотский АО', 'CHU', 1),
(2724, 176, 'Архангельская область', 'ARK', 1),
(2725, 176, 'Астраханская область', 'AST', 1),
(2726, 176, 'Алтайский край', 'ALT', 1),
(2727, 176, 'Белгородская область', 'BEL', 1),
(2728, 176, 'Еврейская АО', 'YEV', 1),
(2729, 176, 'Амурская область', 'AMU', 1),
(2730, 176, 'Брянская область', 'BRY', 1),
(2731, 176, 'Чувашская Республика', 'CU', 1),
(2732, 176, 'Челябинская область', 'CHE', 1),
(2733, 176, 'Карачаево-Черкеcсия', 'KC', 1),
(2734, 176, 'Забайкальский край', 'ZAB', 1),
(2735, 176, 'Ленинградская область', 'LEN', 1),
(2736, 176, 'Республика Калмыкия', 'KL', 1),
(2737, 176, 'Сахалинская область', 'SAK', 1),
(2738, 176, 'Республика Алтай', 'AL', 1),
(2739, 176, 'Чеченская Республика', 'CE', 1),
(2740, 176, 'Иркутская область', 'IRK', 1),
(2741, 176, 'Ивановская область', 'IVA', 1),
(2742, 176, 'Удмуртская Республика', 'UD', 1),
(2743, 176, 'Калининградская область', 'KGD', 1),
(2744, 176, 'Калужская область', 'KLU', 1),
(2746, 176, 'Республика Татарстан', 'TA', 1),
(2747, 176, 'Кемеровская область', 'KEM', 1),
(2748, 176, 'Хабаровский край', 'KHA', 1),
(2749, 176, 'Ханты-Мансийский АО - Югра', 'KHM', 1),
(2750, 176, 'Костромская область', 'KOS', 1),
(2751, 176, 'Краснодарский край', 'KDA', 1),
(2752, 176, 'Красноярский край', 'KYA', 1),
(2754, 176, 'Курганская область', 'KGN', 1),
(2755, 176, 'Курская область', 'KRS', 1),
(2756, 176, 'Республика Тыва', 'TY', 1),
(2757, 176, 'Липецкая область', 'LIP', 1),
(2758, 176, 'Магаданская область', 'MAG', 1),
(2759, 176, 'Республика Дагестан', 'DA', 1),
(2760, 176, 'Республика Адыгея', 'AD', 1),
(2761, 176, 'Москва', 'MOW', 1),
(2762, 176, 'Мурманская область', 'MUR', 1),
(2763, 176, 'Республика Кабардино-Балкария', 'KB', 1),
(2764, 176, 'Ненецкий АО', 'NEN', 1),
(2765, 176, 'Республика Ингушетия', 'IN', 1),
(2766, 176, 'Нижегородская область', 'NIZ', 1),
(2767, 176, 'Новгородская область', 'NGR', 1),
(2768, 176, 'Новосибирская область', 'NVS', 1),
(2769, 176, 'Омская область', 'OMS', 1),
(2770, 176, 'Орловская область', 'ORL', 1),
(2771, 176, 'Оренбургская область', 'ORE', 1),
(2773, 176, 'Пензенская область', 'PNZ', 1),
(2774, 176, 'Пермский край', 'PER', 1),
(2775, 176, 'Камчатский край', 'KAM', 1),
(2776, 176, 'Республика Карелия', 'KR', 1),
(2777, 176, 'Псковская область', 'PSK', 1),
(2778, 176, 'Ростовская область', 'ROS', 1),
(2779, 176, 'Рязанская область', 'RYA', 1),
(2780, 176, 'Ямало-Ненецкий АО', 'YAN', 1),
(2781, 176, 'Самарская область', 'SAM', 1),
(2782, 176, 'Республика Мордовия', 'MO', 1),
(2783, 176, 'Саратовская область', 'SAR', 1),
(2784, 176, 'Смоленская область', 'SMO', 1),
(2785, 176, 'Санкт-Петербург', 'SPE', 1),
(2786, 176, 'Ставропольский край', 'STA', 1),
(2787, 176, 'Республика Коми', 'KO', 1),
(2788, 176, 'Тамбовская область', 'TAM', 1),
(2789, 176, 'Томская область', 'TOM', 1),
(2790, 176, 'Тульская область', 'TUL', 1),
(2792, 176, 'Тверская область', 'TVE', 1),
(2793, 176, 'Тюменская область', 'TYU', 1),
(2794, 176, 'Республика Башкортостан', 'BA', 1),
(2795, 176, 'Ульяновская область', 'ULY', 1),
(2796, 176, 'Республика Бурятия', 'BU', 1),
(2798, 176, 'Республика Северная Осетия', 'SE', 1),
(2799, 176, 'Владимирская область', 'VLA', 1),
(2800, 176, 'Приморский край', 'PRI', 1),
(2801, 176, 'Волгоградская область', 'VGG', 1),
(2802, 176, 'Вологодская область', 'VLG', 1),
(2803, 176, 'Воронежская область', 'VOR', 1),
(2804, 176, 'Кировская область', 'KIR', 1),
(2805, 176, 'Республика Саха', 'SA', 1),
(2806, 176, 'Ярославская область', 'YAR', 1),
(2807, 176, 'Свердловская область', 'SVE', 1),
(2808, 176, 'Республика Марий Эл', 'ME', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3031, 197, 'Ascension', 'A', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3033, 197, 'Tristan da Cunha', 'T', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakir', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazig', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kirikkale', 'KRK', 1),
(3364, 215, 'Kirklareli', 'KLR', 1),
(3365, 215, 'Kirsehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Черкассы', 'CK', 1),
(3481, 220, 'Чернигов', 'CH', 1),
(3482, 220, 'Черновцы', 'CV', 1),
(3483, 220, 'Крым', 'CR', 1),
(3484, 220, 'Днепропетровск', 'DN', 1),
(3485, 220, 'Донецк', 'DO', 1),
(3486, 220, 'Ивано-Франковск', 'IV', 1),
(3487, 220, 'Харьков', 'KH', 1),
(3488, 220, 'Хмельницкий', 'KM', 1),
(3489, 220, 'Кировоград', 'KR', 1),
(3490, 220, 'Киевская область', 'KV', 1),
(3491, 220, 'Киев', 'KY', 1),
(3492, 220, 'Луганск', 'LU', 1),
(3493, 220, 'Львов', 'LV', 1),
(3494, 220, 'Николаев', 'MY', 1),
(3495, 220, 'Одесса', 'OD', 1),
(3496, 220, 'Полтава', 'PO', 1),
(3497, 220, 'Ровно', 'RI', 1),
(3498, 220, 'Севастополь', 'SE', 1),
(3499, 220, 'Сумы', 'SU', 1),
(3500, 220, 'Тернополь', 'TE', 1),
(3501, 220, 'Винница', 'VI', 1),
(3502, 220, 'Луцк', 'VO', 1),
(3503, 220, 'Ужгород', 'ZK', 1),
(3504, 220, 'Запорожье', 'ZA', 1),
(3505, 220, 'Житомир', 'ZH', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubayy', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3808, 236, 'Kosovo', 'KOS', 1),
(3809, 236, 'Montenegro', 'MON', 1),
(3810, 236, 'Serbia', 'SER', 1),
(3811, 236, 'Vojvodina', 'VOJ', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 220, 'Херсон', 'KE', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(57, 176, 0, 3, '2015-11-11 15:56:24', '0000-00-00 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Индексы таблицы `oc_address_simple_fields`
--
ALTER TABLE `oc_address_simple_fields`
  ADD PRIMARY KEY (`address_id`);

--
-- Индексы таблицы `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  ADD PRIMARY KEY (`affiliate_id`);

--
-- Индексы таблицы `oc_affiliate_activity`
--
ALTER TABLE `oc_affiliate_activity`
  ADD PRIMARY KEY (`activity_id`);

--
-- Индексы таблицы `oc_affiliate_login`
--
ALTER TABLE `oc_affiliate_login`
  ADD PRIMARY KEY (`affiliate_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  ADD PRIMARY KEY (`affiliate_transaction_id`);

--
-- Индексы таблицы `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Индексы таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Индексы таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Индексы таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Индексы таблицы `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Индексы таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Индексы таблицы `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Индексы таблицы `oc_attribute_to_1c`
--
ALTER TABLE `oc_attribute_to_1c`
  ADD KEY `attribute_id` (`attribute_id`),
  ADD KEY `1c_id` (`1c_attribute_id`);

--
-- Индексы таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Индексы таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Индексы таблицы `oc_banner_image_description`
--
ALTER TABLE `oc_banner_image_description`
  ADD PRIMARY KEY (`banner_image_id`,`language_id`);

--
-- Индексы таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Индексы таблицы `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Индексы таблицы `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Индексы таблицы `oc_category_to_1c`
--
ALTER TABLE `oc_category_to_1c`
  ADD KEY `category_id` (`category_id`),
  ADD KEY `1c_id` (`1c_category_id`);

--
-- Индексы таблицы `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Индексы таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Индексы таблицы `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Индексы таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Индексы таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Индексы таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Индексы таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`activity_id`);

--
-- Индексы таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Индексы таблицы `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Индексы таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Индексы таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Индексы таблицы `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Индексы таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Индексы таблицы `oc_customer_simple_fields`
--
ALTER TABLE `oc_customer_simple_fields`
  ADD PRIMARY KEY (`customer_id`);

--
-- Индексы таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Индексы таблицы `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Индексы таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Индексы таблицы `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Индексы таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Индексы таблицы `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Индексы таблицы `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Индексы таблицы `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Индексы таблицы `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Индексы таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Индексы таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Индексы таблицы `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Индексы таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Индексы таблицы `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Индексы таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Индексы таблицы `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Индексы таблицы `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Индексы таблицы `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Индексы таблицы `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Индексы таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Индексы таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Индексы таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Индексы таблицы `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Индексы таблицы `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Индексы таблицы `oc_manufacturer_description`
--
ALTER TABLE `oc_manufacturer_description`
  ADD PRIMARY KEY (`manufacturer_id`,`language_id`);

--
-- Индексы таблицы `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Индексы таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Индексы таблицы `oc_mfilter_url_alias`
--
ALTER TABLE `oc_mfilter_url_alias`
  ADD PRIMARY KEY (`mfilter_url_alias_id`);

--
-- Индексы таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Индексы таблицы `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Индексы таблицы `oc_nauthor`
--
ALTER TABLE `oc_nauthor`
  ADD PRIMARY KEY (`nauthor_id`);

--
-- Индексы таблицы `oc_nauthor_description`
--
ALTER TABLE `oc_nauthor_description`
  ADD PRIMARY KEY (`nauthor_id`,`language_id`);

--
-- Индексы таблицы `oc_ncategory`
--
ALTER TABLE `oc_ncategory`
  ADD PRIMARY KEY (`ncategory_id`);

--
-- Индексы таблицы `oc_ncategory_description`
--
ALTER TABLE `oc_ncategory_description`
  ADD PRIMARY KEY (`ncategory_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_ncategory_to_layout`
--
ALTER TABLE `oc_ncategory_to_layout`
  ADD PRIMARY KEY (`ncategory_id`,`store_id`);

--
-- Индексы таблицы `oc_ncategory_to_store`
--
ALTER TABLE `oc_ncategory_to_store`
  ADD PRIMARY KEY (`ncategory_id`,`store_id`);

--
-- Индексы таблицы `oc_ncomments`
--
ALTER TABLE `oc_ncomments`
  ADD PRIMARY KEY (`ncomment_id`),
  ADD KEY `news_id` (`news_id`);

--
-- Индексы таблицы `oc_news`
--
ALTER TABLE `oc_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Индексы таблицы `oc_newsletter`
--
ALTER TABLE `oc_newsletter`
  ADD PRIMARY KEY (`news_id`);

--
-- Индексы таблицы `oc_news_description`
--
ALTER TABLE `oc_news_description`
  ADD PRIMARY KEY (`news_id`,`language_id`);

--
-- Индексы таблицы `oc_news_gallery`
--
ALTER TABLE `oc_news_gallery`
  ADD PRIMARY KEY (`news_image_id`);

--
-- Индексы таблицы `oc_news_related`
--
ALTER TABLE `oc_news_related`
  ADD PRIMARY KEY (`news_id`,`product_id`);

--
-- Индексы таблицы `oc_news_to_layout`
--
ALTER TABLE `oc_news_to_layout`
  ADD PRIMARY KEY (`news_id`,`store_id`);

--
-- Индексы таблицы `oc_news_to_ncategory`
--
ALTER TABLE `oc_news_to_ncategory`
  ADD PRIMARY KEY (`news_id`,`ncategory_id`);

--
-- Индексы таблицы `oc_news_to_store`
--
ALTER TABLE `oc_news_to_store`
  ADD PRIMARY KEY (`news_id`,`store_id`);

--
-- Индексы таблицы `oc_news_video`
--
ALTER TABLE `oc_news_video`
  ADD PRIMARY KEY (`news_video_id`);

--
-- Индексы таблицы `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Индексы таблицы `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Индексы таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Индексы таблицы `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Индексы таблицы `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `oc_order_custom_field`
--
ALTER TABLE `oc_order_custom_field`
  ADD PRIMARY KEY (`order_custom_field_id`);

--
-- Индексы таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Индексы таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Индексы таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`);

--
-- Индексы таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Индексы таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Индексы таблицы `oc_order_simple_fields`
--
ALTER TABLE `oc_order_simple_fields`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Индексы таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Индексы таблицы `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Индексы таблицы `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Индексы таблицы `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Индексы таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Индексы таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Индексы таблицы `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Индексы таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Индексы таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_product_to_1c`
--
ALTER TABLE `oc_product_to_1c`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `1c_id` (`1c_id`);

--
-- Индексы таблицы `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Индексы таблицы `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Индексы таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Индексы таблицы `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Индексы таблицы `oc_relatedoptions`
--
ALTER TABLE `oc_relatedoptions`
  ADD PRIMARY KEY (`relatedoptions_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_relatedoptions_discount`
--
ALTER TABLE `oc_relatedoptions_discount`
  ADD KEY `relatedoptions_id` (`relatedoptions_id`),
  ADD KEY `customer_group_id` (`customer_group_id`);

--
-- Индексы таблицы `oc_relatedoptions_option`
--
ALTER TABLE `oc_relatedoptions_option`
  ADD KEY `relatedoptions_id` (`relatedoptions_id`),
  ADD KEY `option_value_id` (`option_value_id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_relatedoptions_special`
--
ALTER TABLE `oc_relatedoptions_special`
  ADD KEY `relatedoptions_id` (`relatedoptions_id`),
  ADD KEY `customer_group_id` (`customer_group_id`);

--
-- Индексы таблицы `oc_relatedoptions_to_char`
--
ALTER TABLE `oc_relatedoptions_to_char`
  ADD KEY `relatedoptions_id` (`relatedoptions_id`),
  ADD KEY `char_id` (`char_id`);

--
-- Индексы таблицы `oc_relatedoptions_variant`
--
ALTER TABLE `oc_relatedoptions_variant`
  ADD PRIMARY KEY (`relatedoptions_variant_id`);

--
-- Индексы таблицы `oc_relatedoptions_variant_option`
--
ALTER TABLE `oc_relatedoptions_variant_option`
  ADD KEY `option_id` (`option_id`),
  ADD KEY `relatedoptions_variant_id` (`relatedoptions_variant_id`);

--
-- Индексы таблицы `oc_relatedoptions_variant_product`
--
ALTER TABLE `oc_relatedoptions_variant_product`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `relatedoptions_variant_id` (`relatedoptions_variant_id`);

--
-- Индексы таблицы `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Индексы таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Индексы таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Индексы таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Индексы таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Индексы таблицы `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Индексы таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Индексы таблицы `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Индексы таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Индексы таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Индексы таблицы `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Индексы таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Индексы таблицы `oc_ulogin`
--
ALTER TABLE `oc_ulogin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `identity` (`identity`);

--
-- Индексы таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Индексы таблицы `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  ADD PRIMARY KEY (`url_alias_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Индексы таблицы `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Индексы таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Индексы таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Индексы таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Индексы таблицы `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Индексы таблицы `oc_watermark_settings`
--
ALTER TABLE `oc_watermark_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Индексы таблицы `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Индексы таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Индексы таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  MODIFY `affiliate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `oc_affiliate_activity`
--
ALTER TABLE `oc_affiliate_activity`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `oc_affiliate_login`
--
ALTER TABLE `oc_affiliate_login`
  MODIFY `affiliate_login_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  MODIFY `affiliate_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT для таблицы `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT для таблицы `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT для таблицы `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT для таблицы `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT для таблицы `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT для таблицы `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT для таблицы `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT для таблицы `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_mfilter_url_alias`
--
ALTER TABLE `oc_mfilter_url_alias`
  MODIFY `mfilter_url_alias_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `oc_nauthor`
--
ALTER TABLE `oc_nauthor`
  MODIFY `nauthor_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_ncategory`
--
ALTER TABLE `oc_ncategory`
  MODIFY `ncategory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT для таблицы `oc_ncomments`
--
ALTER TABLE `oc_ncomments`
  MODIFY `ncomment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `oc_news`
--
ALTER TABLE `oc_news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `oc_newsletter`
--
ALTER TABLE `oc_newsletter`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `oc_news_gallery`
--
ALTER TABLE `oc_news_gallery`
  MODIFY `news_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `oc_news_video`
--
ALTER TABLE `oc_news_video`
  MODIFY `news_video_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT для таблицы `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `oc_order_custom_field`
--
ALTER TABLE `oc_order_custom_field`
  MODIFY `order_custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT для таблицы `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;
--
-- AUTO_INCREMENT для таблицы `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=839;
--
-- AUTO_INCREMENT для таблицы `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT для таблицы `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=783;
--
-- AUTO_INCREMENT для таблицы `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_relatedoptions`
--
ALTER TABLE `oc_relatedoptions`
  MODIFY `relatedoptions_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=849;
--
-- AUTO_INCREMENT для таблицы `oc_relatedoptions_variant`
--
ALTER TABLE `oc_relatedoptions_variant`
  MODIFY `relatedoptions_variant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5073;
--
-- AUTO_INCREMENT для таблицы `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT для таблицы `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT для таблицы `oc_ulogin`
--
ALTER TABLE `oc_ulogin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20366;
--
-- AUTO_INCREMENT для таблицы `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `oc_watermark_settings`
--
ALTER TABLE `oc_watermark_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3971;
--
-- AUTO_INCREMENT для таблицы `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
