<?php
class ModelModuleNewsletters extends Model {
	public function createNewsletter()
	{
		$res0 = $this->db->query("SHOW TABLES LIKE '".DB_PREFIX."newsletter'");
		if($res0->num_rows == 0){
			$this->db->query("
				CREATE TABLE IF NOT EXISTS `". DB_PREFIX. "newsletter` (
				  `news_id` int(11) NOT NULL AUTO_INCREMENT,
				  `news_email` varchar(255) NOT NULL,
				  PRIMARY KEY (`news_id`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
			");
		}
	}
	public function subscribes($data) {
        $this->load->language('module/newsletter');

        $res = $this->db->query("select * from ". DB_PREFIX ."newsletter where news_email='".$data['email']."'");
		if($res->num_rows == 1)
		{
			return $this->language->get('isset_email');
		}
		else
		{
            $code = substr(sha1(uniqid(mt_rand(), true)), 0, 10);

            if($this->db->query("INSERT INTO " . DB_PREFIX . "newsletter(news_email, news_name, token_key) values ('".$data['email']."', '".$data['name']."', '". $code ."')"))
			{
                $html = $data['name'].', Спасибо за подписку!'.'<br><br>';
                $html .= 'Для подтверждения подписки перейдите по ссылке:'.'<br><br>';
                $html .= $this->url->link('module/newsletters_success', 'code=' . $code, 'SSL');
                $html .= '<br><br>';
                $html .= 'Если Вы не подписывались, просто проигнорируйте письмо.';

                $mail = new Mail();
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                $mail->setTo($data['email']);
                $mail->setFrom($this->config->get('config_name'));
                $mail->setSender(html_entity_decode($data['name'], ENT_QUOTES, 'UTF-8'));
                $mail->setSubject("Подписка");
                $mail->setHtml($html);
                $mail->send();

                return $this->language->get('success_subscribe');
			}
			else
			{
				//$this->response->redirect($this->url->link('common/home', '', 'SSL'));
                return $this->language->get('error_subscribe');
			}
		}
	}

	public function confirmkey($key){
	    $query = $this->db->query("SELECT * from ". DB_PREFIX ."newsletter WHERE token_key='". $key ."'");
        if($query->num_rows == 1){
            return true;
        }else{
            return false;
        }
    }
    public function createcoupon($coupon_code){

        $this->db->query("INSERT INTO " . DB_PREFIX . "coupon SET name = 'Скидка 5%', code = '" . $coupon_code . "', discount = '5.0000', type = 'P', total = '', logged = '0', shipping = '0', date_start = '', date_end = '', uses_total = '0', uses_customer = '0', status = '1', date_added = NOW()");

        return true;
    }
    public function getCouponByCode($code) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "coupon WHERE code = '" . $this->db->escape($code) . "'");

        if($query->num_rows == 1){
            return true;
        }else{
            return false;
        }
    }

    public function getEmail($code){

        $email = $this->db->query("SELECT news_email FROM " .DB_PREFIX. "newsletter WHERE token_key = '" . $code . "'");

        return $email->row;
    }
}