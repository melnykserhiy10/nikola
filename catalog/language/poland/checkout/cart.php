<?php
// Nagłówek
$_['heading_title'] = 'Order Basket';

// tekst
$_['text_success'] = 'Element <a href="%s">%s </a> jest dodawany do <a href="%s"> koszyka </a>!';
$_['text_remove'] = 'Koszyk został pomyślnie uaktualniony!';
$_['text_login'] = 'Musisz się zalogować <a href="%s"> </a> <a href="%s"> lub Utwórz konto aby zobaczyć ceny </a>!';
$_['text_items'] = '%s<br><span>Zobacz koszyk</span>';
$_['text_points'] = 'Punkty bonusowe: %s';
$_['text_next'] = 'Użyj zaawansowanych funkcji';
$_['text_next_choice'] = 'Jeśli masz kupon zniżkowy lub punkty bonusowe, które chcesz użyć, wybierz odpowiedni element. Co więcej, możesz (w przybliżeniu) sprawdzić koszt wysyłki w Twoim regionie. ';
$_['text_empty'] = 'Koszyk jest pusty';
$_['text_day'] = 'dzień';
$_['text_week'] = 'tydzień';
$_['text_semi_month'] = 'półksiężyc';
$_['text_month'] = 'miesiąc';
$_['text_year'] = 'rok';
$_['text_trial'] = '%s każdy %s %s dla %s płatności';
$_['text_recurring'] = '%s każdy %s %s';
$_['text_length'] = 'dla płatności %s';
$_['text_until_cancelled'] = 'przed anulowaniem';
$_['text_recurring_item'] = 'Powtarzalne';
$_['text_payment_recurring'] = 'Profil płatności';
$_['text_trial_description'] = '%s każdy %d %s (s) dla %d płatności (ów), a następnie';
$_['text_payment_description'] = '%s każdy %d %s (s) dla %d płatności (s)';
$_['text_payment_cancel'] = '%s każdy %d %s (s) do czasu jego anulowania';

// kolumna
$_['column_image'] = 'Obrazki';
$_['column_name'] = 'Nazwa produktu';
$_['column_model'] = 'Model';
$_['column_quantity'] = 'Numer';
$_['column_price'] = 'Cena za szt.';
$_['column_total'] = 'Razem';

// Błąd
$_['error_stock'] = 'Produkty oznaczone *** nie są w wymaganym numerze lub nie są dostępne!';
$_['error_minimum'] = 'Minimalna ilość zamówienia %s to %s!';
$_['error_required'] = 'Wymagany jest %s!';
$_['error_product'] = 'Brak produktów w koszyku!';
$_['error_recurring_required'] = 'Wybierz profil płatności!';