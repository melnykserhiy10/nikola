<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/javascript/jquery/slick/slick-theme.css" rel="stylesheet">
<link href="catalog/view/javascript/jquery/slick/slick.css" rel="stylesheet">
<link href="catalog/view/javascript/jquery/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/hamburgers.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
    <script src="//ulogin.ru/js/ulogin.js"></script>
    <script src="catalog/view/javascript/ulogin.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/jquery.cookie.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/popup_purchase/jquery.magnific-popup.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/popup_purchase/magnific-popup.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/animate.css" rel="stylesheet" media="screen" />
<link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/main2.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

    <?php if ( isset($popup_purchase_data['status']) && $popup_purchase_data['status'] ) { ?>
        <script type="text/javascript">
            $(function() {
                <?php if ($popup_purchase_data['allow_page']) { ?>
                $.each($("[onclick^='cart.add']"), function() {
                    var product_id = $(this).attr('onclick').match(/[0-9]+/);
//                    $(this).before("<div class='button-group popup-purchase-button'><button><?php //echo $popup_purchase_text['button_purchase_now']; ?>//</button></div>").prev().attr('onclick', 'get_popup_purchase(\'' + product_id + '\');');
                });
                $.each($(".product-thumb [onclick^='get_popup_cart']"), function() {
                    var product_id = $(this).attr('onclick').match(/[0-9]+/);
                    $(this).parent().before("<div class='button-group popup-purchase-button'><button><?php echo $popup_purchase_text['button_purchase_now']; ?></button></div>").prev().attr('onclick', 'get_popup_purchase(\'' + product_id + '\');');
                });
                <?php } ?>
                var main_product_id = $('input[name=\'product_id\']').val();
                $('#button-cart').after( "<button class='one_click btn btn-primary btn-lg btn-block'><?php echo $popup_purchase_text['button_purchase_now']; ?></button>" ).next().attr('onclick', 'get_popup_purchase(\'' + main_product_id + '\');');
            });
            function get_popup_purchase(product_id) {
                $.magnificPopup.open({
                    tLoading: '<img src="catalog/view/theme/default/stylesheet/popup_purchase/ring-alt.svg" />',
                    items: {
                        src: 'index.php?route=module/popup_purchase&product_id='+product_id,
                        type: 'ajax'
                    }
                });
            }
        </script>
    <?php } ?>

</head>
<body class="<?php echo $class; ?> resize mob_resize">
<div class="f_wrapper">
<div class="f_content">
<div id="right_header">
    <?php echo $cart; ?>
    <div class="wishlist<?php echo ($count_wshst > 0)? ' has_product':''?>">
        <a href="<?php echo $wishlist; ?>" id="wishlist-total" class="wishlist-total">
            <div class="info"><?php echo $text_wishlist; ?></div>
        </a>
    </div>
    <?php echo $language; ?>
    <?php echo $currency; ?>
    <?php echo $search; ?>
    <div class="login_wrapp">
        <?php if(!$logged) { ?>
            <a href="#login_register-wrapp" class="login_link"><span class="icon icon-login-1"></span></a>
        <?php }else{ ?>
            <a href="<?php echo $account; ?>" class="login_link"><span class="icon icon-login-1"></span></a>
        <?php } ?>
        <div class="info"><?php echo $text_account; ?></div>
    </div>
</div>
    <div class="clearfix"></div>
<header>
    <div class="container">
        <div class="left_logo">
            <div class="text">From<br>2008</div>
        </div>
        <div class="logo_wrapp">
            <?php if ($logo) { ?>
                <?php if ($home == $og_url) { ?>
                    <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                    <img src="/image/catalog/logo-mobile.svg" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive mobile_logo" />
                <?php } else { ?>
                    <a href="<?php echo $home; ?>">
                        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                        <img src="/image/catalog/logo-mobile.svg" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive mobile_logo" />
                    </a>
                <?php } ?>
            <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
            <?php } ?></div>
        <div class="right_logo">
            <div class="text">To<br>2017</div>
        </div>
    </div>
</header>

<div class="main_menu<?php echo($home != $og_url)?' fine_menu':''; ?>">
    <div class="container">
        <nav id="menu">
            <div class="hamburger hamburger--collapse">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            <ul class="first_level">
                <?php if ($categories) { ?>
                    <li class="dropdown"><a href="<?php echo $online_shop; ?>" class="dropdown-toggle"><?php echo $text_shop; ?><div class="open_child"></div></a>
                        <div class="dropdown-menu">
                            <ul class="secound_level">
                                <?php foreach ($categories as $category) { ?>
                                    <?php if ($category['children']) { ?>
                                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                            <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                                <ul class="list-unstyled third_level">
                                                    <?php foreach ($children as $child) { ?>
                                                        <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                    <?php }else{ ?>
                                        <li class="menu_item"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </li>
                <?php } ?>
                <li><a href="<?php echo $special_products; ?>">SALE</a></li>
                <li><a href="<?php echo $about_us; ?>"><?php echo $text_company; ?></a></li>
                <li><a href="<?php echo $gallery; ?>"><?php echo $text_gallery; ?></a></li>
                <li><a href="<?php echo $sotrudnichestvo; ?>"><?php echo $text_sot; ?></a></li>
                <li><a href="<?php echo $faq; ?>">FAQ</a></li>
                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
            </ul>
            <div class="shecter" data-fall="0"></div>
        </nav>
    </div>
</div>
<div class="fixed_menu">
    <div class="container">
        <div class="fixed_logo">
            <?php if ($home == $og_url) { ?>
                <img src="/image/catalog/fixed-logo.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
            <?php } else { ?>
                <a href="<?php echo $home; ?>"><img src="/image/catalog/fixed-logo.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
            <?php } ?>
        </div>
        <ul class="first_level">
            <?php if ($categories) { ?>
                <li class="dropdown"><a href="<?php echo $online_shop; ?>" class="dropdown-toggle"><?php echo $text_shop; ?></a></li>
            <?php } ?>
            <li><a href="<?php echo $special_products; ?>">SALE</a></li>
            <li><a href="<?php echo $about_us; ?>"><?php echo $text_company; ?></a></li>
            <li><a href="<?php echo $gallery; ?>"><?php echo $text_gallery; ?></a></li>
            <li><a href="<?php echo $sotrudnichestvo; ?>"><?php echo $text_sot; ?></a></li>
            <li><a href="<?php echo $faq; ?>">FAQ</a></li>
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
        </ul>
    </div>
</div>
