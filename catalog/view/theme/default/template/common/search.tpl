<a href="#search_popup" class="search_link"><span class="icon icon-search-1"></span>
    <div class="info"><?php echo $search_button; ?></div>
</a>
<div id="search_popup">
    <div class="fancy_head"><?php echo $search_head; ?></div>
    <div class="fancy_content">
        <div id="search">
            <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="" />
            <div class="text-right">
                <button type="button" class="btn-default-1"><?php echo $search_button; ?></button>
            </div>
        </div>
    </div>
</div>
