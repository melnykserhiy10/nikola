<?php
// Nagłówek
$_['heading_title'] = 'Zamawianie';

$_['button_login'] = 'Zaloguj się';
$_['button_order'] = 'Checkout';
$_['button_save_changes'] = 'Zapisz zmiany';
$_['button_prev'] = 'Wstecz';
$_['button_next'] = 'Do przodu';

// tekst
$_['text_cart'] = 'Kosz';
$_['text_checkout_customer'] = 'Kupujący';
$_['text_checkout_customer_login'] = 'Jestem zarejestrowany';
$_['text_checkout_customer_cancel'] = 'Anuluj';
$_['text_checkout_shipping_method'] = 'Metoda dostarczania';
$_['text_checkout_payment_method'] = 'Metoda płatności';
$_['text_city'] = 'Zacznij wpisywać swoje miasto i wybierz go z listy';
$_['text_shipping_address'] = 'Podaj swój adres w celu uzyskania pełnych informacji dotyczących wysyłki';
$_['text_payment_address'] = 'Podaj swój adres w celu uzyskania pełnej informacji o płatnościach';
$_['text_forgotten'] = 'Nie pamiętasz hasła?';
$_['text_only'] = 'Region: %s';
$_['text_reward'] = 'Punkty bonusowe: %s';
$_['text_items'] = '%s<br><span>Zobacz koszyk</span>';
$_['text_empty'] = 'Twój koszyk jest pusty!';
$_['text_login'] = 'Musisz być zalogowany <a href="%s">Szafka osobista</a> konta osobistego lub załóż konto <a href="%s">Konto</a> widzieć ceny!';
$_['text_agree'] = 'Przeczytałem i zgadzam się z zasadami <a class="colorbox fancybox agree" href="%s" alt="%s"> <b>%s</b> </a>';
$_['text_agree_shoppica'] = 'Przeczytałem i zgadzam się z zasadami <a class="s_main_color" rel="prettyPhoto[ajax]" href="%s&iframe=true" alt="%s"> <b>%s</b> </a>'.
$_['text_private'] = 'Fiz. Osoba';
$_['text_company'] = 'Yur. Osoba';
$_['text_add_new'] = 'Dodaj nowy adres';
$_['text_your_company'] = 'Dane organizacji';
$_['text_select_address'] = 'Wybierz adres';
$_['text_proceed_payment'] = 'Poczekaj ... Przejście na płatność';
$_['text_checkout_shipping_address'] = 'adres dostawy';
$_['text_payment_form_title'] = 'Płatność';
$_['text_account_created'] = 'Twoje konto zostało utworzone! Możesz zamówić płatność! ';
$_['text_need_save_changes'] = 'Musisz zapisać zmiany przed dokonaniem płatności!';
$_['text_saving_changes'] = 'Zapisywanie zmian ...';
$_['text_you_will_be_registered'] = 'Będziesz zalogowany!';
$_['main_checkout_heading'] = 'Zamawianie';


// kolumna
$_['column_image'] = 'Zdjęcia';
$_['column_name'] = 'Nazwa produktu';
$_['column_model'] = 'Model';
$_['column_quantity'] = '<span title = "Number"> Number </span>';
$_['column_price'] = 'Cena';
$_['column_total'] = 'Razem';

// wpis
$_['entry_email'] = 'Email:';
$_['entry_password'] = 'Hasło:';
$_['entry_password_confirm'] = 'Potwierdź hasło:';
$_['entry_register'] = 'Zarejestruj się na stronie:';
$_['entry_coupon'] = 'Kupon:';
$_['entry_reward'] = 'Użyj punktów (nie więcej niż %s):';
$_['entry_voucher'] = 'Certyfikat podarunkowy:';
$_['entry_newsletter'] = 'Subskrybuj nowości:';
$_['entry_address_same'] = 'Adres dostawy jest adresem płatnika';
$_['entry_customer_type'] = 'Typ nabywcy';

// Błąd
$_['error_login'] = 'Błąd: nieprawidłowa wiadomość e-mail lub hasło!';
$_['error_exists'] = 'E-Mail jest już zarejestrowany! Zaloguj się, aby złożyć zamówienie! ';
$_['error_no_shipping'] = 'Dostawa na ten adres nie jest możliwa! Zobacz <a href="%s"> administracja </a>! ';
$_['error_no_payment'] = 'Płatność za ten adres nie jest możliwy! Zobacz <a href="%s"> administracja </a>! ';
$_['error_stock'] = 'Produkty oznaczone *** nie są dostępne w odpowiedniej ilości lub nie są w magazynie!';
$_['error_minimum'] = 'Minimalna liczba zamówień %s to %s!';
$_['error_agree'] = 'Musisz zaakceptować warunki transakcji "%s"!';
$_['error_password'] = 'Hasło musi zawierać się od 4 do 20 znaków!';
$_['error_password_confirm'] = 'Potwierdzenie hasła i hasła musi się zgadzać!';
$_['error_coupon'] = 'Błąd: kupon lub nieważny, wygasł lub został osiągnięty!';
$_['error_voucher'] = 'Nieprawidłowo określony kod dla bonu podarunkowego, został już użyty lub wygasł!';
$_['error_reward'] = 'Podaj liczbę punktów bonusowych, których chcesz użyć!';
$_['error_points'] = 'Nie masz';

?>