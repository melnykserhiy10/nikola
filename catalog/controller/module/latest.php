<?php
class ControllerModuleLatest extends Controller {
	public function index($setting) {
		$this->load->language('module/latest');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

        $this->load->model('account/wishlist');

        $data['products'] = array();

		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_catalog_product->getProducts($filter_data);

        $wishlist_mass = $this->model_account_wishlist->getIdWishlist();

        if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && (($this->customer->getGroupId()==$this->config->get('config_customer_group_id2')) || ($this->customer->getGroupId()==$this->config->get('config_customer_group_id3')))) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

                $images = $this->model_catalog_product->getProductImages($result['product_id']);
                if(!empty($images)){
                    $image2 = $this->model_tool_image->resize(array_shift($images)['image'], $setting['width'], $setting['height']);
                }else{
                    $image2 = '';
                }

                $data['options'] = array();

                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                    $product_option_value_data = array();

                    if($option['option_id'] == 1   ){
                        foreach ($option['product_option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                $product_option_value_data[] = array(
                                    'name'  => $option_value['name'],
                                );
                            }
                        }
                        $data['options'][] = array(
                            'product_option_id'    => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id'            => $option['option_id'],
                        );
                    }

                }
                if(!$this->customer->isLogged()){
                    if(isset($this->session->data['wishlist'])){
                        if (in_array($result['product_id'], $this->session->data['wishlist'])){
                            $wishlist = true;
                        }else{
                            $wishlist = false;
                        }
                    }else{
                        $wishlist = false;
                    }
                }else{
                    if(in_array($result['product_id'], $wishlist_mass)){
                        $wishlist = true;
                    }else{
                        $wishlist = false;
                    }
                }

                $data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'thumb2'      => $image2,
					'name'        => $result['name'],
					'sizes'       => $data['options'],
                    'wishlist'    => $wishlist,
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/latest.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/latest.tpl', $data);
			} else {
				return $this->load->view('default/template/module/latest.tpl', $data);
			}
		}
	}
}
