<?php
$_['heading_title'] = 'Zapisz się na newsletter i otrzymaj rabat<span>-5% z Nikoli</span>';
$_['text_subscribe'] = 'Subskrybuj';
$_['text_email'] = 'Twój e-mail';
$_['text_name'] = 'Twoje imię';

$_['error_name'] = 'Nazwa musi wynosić od 3 do 32 znaków!';
$_['error_email'] = 'Adres e-mail nie jest poprawny!';

$_['isset_email'] = 'Ten e-mail już istnieje w subskrybentach';
$_['success_subscribe'] = 'List z potwierdzeniem został wysłany na Twój adres e-mail';
$_['error_subscribe'] = 'Błąd subskrypcji';