</div>
<div class="f_footer">
<footer>
  <div class="container">
      <div class="col-sm-4 addressa">
        <?php echo $addressa; ?>
      </div>
      <div class="col-sm-3 contacts">
          <div class="telephone">tel: <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>"><?php echo $telephone2; ?></a> - <?php echo $text_office; ?></div>
          <div class="telephone">tel: <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>"><?php echo $telephone1; ?></a> - <?php echo $text_opt; ?></div>
          <div class="email">e-mail: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
          <ul class="social_links">
             <li><a href=""><span class="icon icon-facebook-logo"></span></a></li>
             <li><a href=""><span class="icon icon-instagram-logo"></span></a></li>
          </ul>
      </div>
      <div class="col-sm-5 subscribe_wrapp">
          <?php echo $newsletters; ?>
      </div>
      <div class="clearfix"></div>
      <div class="copyright"><p>Copyright © 2017 NIKOLA</p></div>
      <div class="bottom_footer">
          <a href="http://web-systems.solutions" target="_blank">Bring your ideas to life</a>
      </div>
  </div>
</footer>
</div>
<div id="thanks_modal">
    <div class="fancy_head"><p></p></div>
    <div class="fancy_content">
    </div>
</div>

<div id="cookie_fancy">
    <div class="fancy_content">
        <div class="title"><?php echo $title_cookie_fancy; ?></div>
        <div class="logo">
            <img src="/image/catalog/nicola-logo-web.png" alt="nicola-logo-web">
        </div>
        <div class="text"><?php echo $text_cookie_fancy; ?></div>
        <div class="wrapp_buttons row">
            <div class="col-sm-6">
                <a href="" id="wholesale_buyer"><?php echo $text_wholesale_buyer; ?></a>
            </div>
            <div class="col-sm-6">
                <a href="" id="retail_buyer"><?php echo $text_retail_buyer; ?></a>
            </div>
        </div>
    </div>
</div>

<script src="catalog/view/javascript/jquery/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="catalog/view/javascript/match.Height-min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/slick/slick.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/main.js" type="text/javascript"></script>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
</div>
</body></html>