<?php
// tekst
$_['text_search'] = 'Szukaj';
$_['text_brand'] = 'Marka';
$_['text_manufacturer'] = 'Producent:';
$_['text_model'] = 'Model:';
$_['text_reward'] = 'Punkty bonusowe:';
$_['text_points'] = 'Cena w punktach bonusowych:';
$_['text_stock'] = 'Dostępność:';
$_['text_instock'] = 'W magazynie';
$_['text_tax'] = 'Brak podatku VAT:';
$_['text_discount'] = 'lub więcej';
$_['text_option'] = 'Dostępne opcje';
$_['text_minimum'] = 'Minimalne zamówienie dla tego produktu: %s.';
$_['text_reviews'] = '%s opinie';
$_['text_write'] = 'Napisz recenzję';
$_['text_login'] = 'Proszę <a href="%s"> zaloguj się </a> lub <a href="%s"> zarejestruj się </a>, aby go wyświetlić.';
$_['text_no_reviews'] = 'Brak opinii o tym produkcie.';
$_['text_note'] = '<span class = "text-danger"> Uwaga: </span> HTML nie jest obsługiwany!';
$_['text_success'] = 'Dziękujemy za opinie. Został skierowany do umiaru. ';
$_['text_related'] = 'Zobacz także';
$_['text_tags'] = 'Tagi:';
$_['text_error'] = 'Nie znaleziono produktu!';
$_['text_payment_recurring'] = 'Profile płatności';
$_['text_trial_description'] = '%s każdy %d %s dla %d płatności (i),';
$_['text_payment_description'] = '%s każdy% d %s (s) z %d płatności (w)';
$_['text_payment_cancel'] = '%s co %d %s(s) do czasu anulowania';
$_['text_day'] = 'dzień';
$_['text_week'] = 'tydzień';
$_['text_semi_month'] = 'półksiężyc';
$_['text_month'] = 'miesiąc';
$_['text_year'] = 'rok';
$_['info_text'] = 'Z<span> rodziny Nikola </span> oszczędzasz <span> %</span>';
$_['text_desciption'] = 'Opis produktu';
$_['text_otpravka'] = 'Wysyłanie';
$_['text_share'] = 'Udostępnij';


// wpis
$_['entry_qty'] = 'Numer';
$_['entry_name'] = 'Twoje imię:';
$_['entry_review'] = 'Twoja opinia';
$_['entry_rating'] = 'Ocena';
$_['entry_good'] = 'OK';
$_['entry_bad'] = 'Złe';

// karty
$_['tab_description'] = 'Opis';
$_['tab_attribute'] = 'Charakterystyka';
$_['tab_review'] = 'Recenzje (%s)';

// Błąd
$_['error_name'] = 'Nazwa musi wynosić od 3 do 25 znaków!';
$_['error_text'] = 'Tekst przeglądu musi zawierać się między 25 a 1000 znaków!';
$_['error_rating'] = 'Oceń!';