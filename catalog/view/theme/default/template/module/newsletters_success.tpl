<?php echo $header; ?>
<div class="container">
    <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if($i+1<count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="container">
    <h1><?php echo $heading_title; ?></h1>
    <p><?php echo $message; ?></p>
    <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
    </div>
</div>
<?php echo $footer; ?>