<?php
// Heading
$_['heading_title']  = 'Связаться с нами';

// Text
$_['text_location']  = 'Наш адрес';
$_['text_store']     = 'Наши магазины';
$_['text_contact']   = 'Форма связи';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Время работы';
$_['text_comment']   = 'Комментарий';
$_['text_success']   = '<p>Ваш запрос был успешно отправлен администрации магазина!</p>';


$_['text_contacts'] = 'Контакты <span>Nikola.ua</span>';
$_['text_contact_with'] = 'Контакт с <span>Nikola.ua</span>';
$_['text_shops'] = 'Магазины <span>Nikola</span>';

// Entry
$_['entry_first_name']     = 'Ваше имя';
$_['entry_last_name']     = 'Фамилия';
$_['entry_email']    = 'E-Mail для связи';
$_['entry_enquiry']  = 'Сообщение';
$_['entry_telephone']  = 'Контактный телефон';

// Email
$_['email_subject']  = 'Сообщение %s';

// Errors
$_['error_first_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_last_name']     = 'Фамилия должна быть от 3 до 32 символов!';
$_['error_email']    = 'E-Mail указан некорректно!';
$_['error_enquiry']  = 'Сообщение должно быть от 10 до 3000 символов!';
$_['error_telephone']  = 'Телефон должен быть от 3 до 32 символов!';
