<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

$_['text_contacts'] = 'Contacts <span>Nikola.ua</span>';
$_['text_contact_with'] = 'Contact with <span>Nikola.ua</span>';
$_['text_shops'] = 'Stores <span>Nikola</span>';

// Entry
$_['entry_first_name']     = 'Your First Name';
$_['entry_last_name']     = 'Your Last Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';
$_['entry_telephone']  = 'Contact telephone';


// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_first_name']     = 'First name must be between 3 and 32 characters!';
$_['error_last_name']     = 'Last name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
$_['error_telephone'] = 'The phone should be from 3 to 32 characters!';
