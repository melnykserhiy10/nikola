<?php echo $header; ?>
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
    <?php echo $column_left; ?>
    <div id="content"><?php echo $content_top; ?>
      <h1 style="display: none;"><?php echo $heading_title; ?></h1>
        <div class="row">
            <div class="col-sm-6">
                <div class="contacts_nikola">
                    <div class="title"><h2><?php echo $text_contacts; ?></h2><span class="line"></span></div>
                    <ul class="telephones">
                        <li><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">tel: <?php echo $telephone2; ?> - <?php echo $text_office; ?></a></li>
                        <li><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>">tel: <?php echo $telephone1; ?> - <?php echo $text_opt; ?></a></li>
                    </ul>
                    <div class="email"><a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a></div>
                </div>
                <div class="contacts_with_nikola">
                    <div class="title"><h2><?php echo $text_contact_with; ?></h2><span class="line"></span></div>
                    <div class="text_info"><?php echo $open; ?></div>

                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group required">
                                <input type="text" name="first_name" value="<?php echo $first_name; ?>" placeholder="<?php echo $entry_first_name; ?>" id="input-first_name" class="form-control" />
                                <?php if ($error_first_name) { ?>
                                    <div class="text-danger"><?php echo $error_first_name; ?></div>
                                <?php } ?>
                            </div>
                            <div class="form-group required">
                                <input type="text" name="last_name" value="<?php echo $last_name; ?>" placeholder="<?php echo $entry_last_name; ?>" id="input-last_name" class="form-control" />
                                <?php if ($error_last_name) { ?>
                                    <div class="text-danger"><?php echo $error_last_name; ?></div>
                                <?php } ?>
                            </div>
                            <div class="form-group required">
                                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                                <?php if ($error_email) { ?>
                                    <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>
                            </div>
                            <div class="form-group required">
                                <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                                <?php if ($error_telephone) { ?>
                                    <div class="text-danger"><?php echo $error_telephone; ?></div>
                                <?php } ?>
                            </div>
                            <div class="form-group required">
                                <textarea name="enquiry" id="input-enquiry" placeholder="<?php echo $entry_enquiry; ?>" class="form-control"><?php echo $enquiry; ?></textarea>
                                <?php if ($error_enquiry) { ?>
                                    <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                <?php } ?>
                            </div>
                            <?php echo $captcha; ?>
                        </fieldset>
                        <div class="buttons">
                            <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6 shops_nikola">
                <div class="title"><h2><?php echo $text_shops; ?></h2><span class="line"></span></div>
                <?php echo $addressa; ?>
            </div>
        </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
<?php echo $footer; ?>
