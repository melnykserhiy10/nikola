<?php echo $header; $s = 1; ?>
<div class="container">
    <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if($i+1<count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <?php if(!empty($sections)) { ?>
        <?php foreach($sections as $section) { ?>
                <div class="title"><h1><?php echo $section['title']; ?> <span>Nikola.ua</span></h1><span
                            class="line"></span></div>
        <div class="row-faq row" >
            <div class="col-faq-left col-md-6 col-sm-12 col-xs-12">
                          <?php $array_count = count($section['items']); $k=0; foreach($section['items'] as $item) { $k++; ?>
                            <?php if($k <= round($array_count/2)) { ?>
                                  <div class="col-faq ">
                                      <div class="faq-curier-title"><span class="faq-number"><?php if($k<10) echo 0;  echo $k; ?></span><span class="faq-title"><?php echo  $item['question']; ?>
                                      </div>
                                      <div class="faq-description"><?php echo $item['answer']; ?>
                                      </div>
                                  </div>
                              <?php } ?>
                            <?php } ?>
                      </div>
                      <div class="col-faq-right col-md-6 col-sm-12 col-xs-12">
                          <?php $array_count = count($section['items']); $k=0; foreach($section['items'] as $item) { $k++; ?>
                            <?php if($k > round($array_count/2)) { ?>
                                  <div class="col-faq ">
                                      <div class="faq-curier-title"><span class="faq-number"><?php if($k<10) echo 0;  echo $k; ?></span><span class="faq-title"><?php echo  $item['question']; ?>
                                      </div>
                                      <div class="faq-description"><?php echo $item['answer']; ?>
                                      </div>
                                  </div>
                              <?php } ?>
                            <?php } ?>
                      </div>
        </div>
        <?php } ?>
        <?php } ?>
    <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>