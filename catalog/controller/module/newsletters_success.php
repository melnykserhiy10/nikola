<?php
class ControllerModuleNewslettersSuccess extends Controller{
    public function index()
    {
        $this->load->language('module/newsletters_success');

        $this->load->model('module/newsletters');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/newsletters_success')
        );

        if(isset($this->request->get['code'])){
           $code = $this->request->get['code'];
        }else{
           $code = false;
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['message'] = $this->language->get('message');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['continue'] = $this->url->link('common/home');

        $get_email = $this->model_module_newsletters->getEmail($code);

        $result = $this->model_module_newsletters->confirmkey($code);

        if($result){
            $coupon_code = $this->generate();

            if($coupon_code){

                $this->model_module_newsletters->createcoupon($coupon_code);

//                $html = 'Купон на скидку в магазине' . $this->config->get('config_name') . ' : '.'<br><br>';
                $html = sprintf($this->language->get('text_message'), $this->config->get('config_name')) .'<br><br>';
                $html .= '<b>' . $coupon_code . '</b>';

                $mail = new Mail();
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                $mail->setTo($get_email);
                $mail->setFrom($this->config->get('config_name'));
                $mail->setSender($this->config->get('config_name'));
                $mail->setSubject("Купон");
                $mail->setHtml($html);
                $mail->send();
            }
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/newsletters_success.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/newsletters_success.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/module/newsletters_success.tpl', $data));
        }

    }
    public function generate(){
        $this->load->model('module/newsletters');

        $ran_mass = array();
        for($i=1; $i<=7; $i++){
            $ran_mass[] = rand(0,9);
        }

        if($this->model_module_newsletters->getCouponByCode((int)implode($ran_mass))){
            $this->generate();
        }else{
            return (int)implode($ran_mass);
        }
    }
}