<?php
// Text
$_['text_signin_register']    = 'Sign In/Register';
$_['text_login']   			  = 'Sign In';
$_['text_register']    		  = 'Register';

$_['text_new_customer']       = 'New Customer';
$_['text_returning']          = 'Returning Customer';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'First name';
$_['entry_last_name']         = 'Last name';
$_['entry_password']          = 'Password';
$_['entry_confpassword']      = 'Confirm password';
$_['entry_telephone']         = '+38(ххх)ххх-хх-хх';
$_['text_forgotten']          = 'Forgotten Password';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['text_all_field'] = '* All fields are required';


$_['text_login_title1'] = 'I\'m already registered';
$_['text_login_title2'] = 'Enter your e-mail and password to login';
$_['text_register_title1'] = 'New client';
$_['text_register_title2'] = 'Complete information about yourself';

//Button
$_['button_login']            = 'Login';

//Error
$_['error_first_name']     = 'First name must be between 1 and 32 characters!';
$_['error_last_name']      = 'Last name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confpassword']   = 'Passwords do not match';
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_agree']          = 'Warning: You must agree to the %s!';
$_['error_warning']        = 'Warning: Please check the form carefully for errors!';
$_['error_approved']       = 'Warning: Your account requires approval before you can login.';
$_['error_login']          = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_country']        = 'Warning: Select country';