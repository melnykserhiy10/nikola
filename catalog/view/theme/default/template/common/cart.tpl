<div class="cart_wrapp<?php if ($products || $vouchers) { ?> has-product<?php } ?>">
    <a href="<?php echo $checkout; ?>" class="cart_link"><?php echo $text_items; ?><br><span><?php echo $cart_text; ?></span></a>
    <div id="cart_popup">
    <div class="fancy_head"><?php echo $text_cart2; ?></div>
    <div class="fancy_content">
      <ul>
        <?php if ($products || $vouchers) { ?>
        <li>

          <table class="table table-striped" style="display: none;">
            <?php $i=1; foreach ($products as $product) { ?>
                <?php if($i == count($products)){ ?>
            <tr>
              <td class="text-center">
                  <?php if ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                <?php } ?>
              </td>
              <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <?php if ($product['option']) { ?>
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                <?php } ?>
                <?php } ?>
                <?php if ($product['recurring']) { ?>
                <br />
                - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                <?php } ?></td>
              <td class="text-right">x <?php echo $product['quantity']; ?></td>
              <td class="text-right"><?php echo $product['total']; ?></td>
              <td class="text-center"><button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
            </tr>
                    <?php } ?>
                <?php $i++; } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
              <td class="text-center"></td>
              <td class="text-left"><?php echo $voucher['description']; ?></td>
              <td class="text-right">x&nbsp;1</td>
              <td class="text-right"><?php echo $voucher['amount']; ?></td>
              <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
            </tr>
            <?php } ?>
          </table>
            <div class="wrapp_products">
                <?php $i=1; foreach ($products as $product) { ?>
                    <?php if($i == count($products)){ ?>
                        <div class="product">
                            <?php if ($product['thumb']) { ?>
                                <div class="thumb">
                                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"/></a>
                                </div>
                            <?php } ?>
                            <div class="right_info">
                            <a class="name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            <?php if ($product['option']) { ?>
                                <div class="options">
                                    <?php foreach ($product['option'] as $option) { ?>
                                    - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                                <div class="price">
                                    <?php echo $product['total']; ?>
                                </div>
                            </div>
                            <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="remove"></button>
                        </div>
                    <?php } ?>
                <?php $i++; } ?>
            </div>
        </li>
        <li>
          <div>
            <table class="table table-bordered">
              <?php foreach ($totals as $total) { ?>
              <tr>
                <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
                <td class="text-right"><?php echo $total['text']; ?></td>
              </tr>
              <?php } ?>
            </table>
              <div class="wrapp_btn">
                  <a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
              </div>
          </div>
        </li>
        <?php } else { ?>
        <li>
          <p class="text-empty"><?php echo $text_empty; ?></p>
        </li>
        <?php } ?>
      </ul>
    </div>
    </div>
</div>