<?php
// tekst
$_['text_signin_register'] = 'Zaloguj się / zarejestruj';
$_['text_login'] = 'Autoryzacja';
$_['text_register'] = 'Zarejestruj';

$_['text_new_customer'] = 'Nowy klient';
$_['text_returning'] = 'powrót klienta';
$_['text_returning_customer'] = 'Jestem z powrotem klientem';
$_['text_details'] = 'Twoje dane osobowe';
$_['entry_email'] = 'Email';
$_['entry_name'] = 'Im';
$_['entry_last_name'] = 'Nazwisko';
$_['entry_password'] = 'Hasło';
$_['entry_confpassword'] = 'Powtórz hasło';
$_['entry_telephone'] = '+38 (xxx) xxx-xx-xx';
$_['text_forgotten'] = 'Zapomniałem hasła';
$_['text_agree'] = 'Zgadzam się z <a href="%s" class="agree"> <b>% s </ b> </a>';
$_['text_all_field'] = '* Wszystkie pola są wymagane do wypełnienia';


$_['text_login_title1'] = 'Jestem już zarejestrowany';
$_['text_login_title2'] = 'Podaj swój adres e-mail i hasło, aby się zalogować';
$_['text_register_title1'] = 'Nowy klient';
$_['text_register_title2'] = 'Wypełnij informacje o sobie';

//
$_['button_login'] = 'Zaloguj się';

// błąd
$_['error_first_name'] = 'Nazwa musi zawierać od 1 do 32 znaków!';
$_['error_last_name'] = 'Ostatnie nazwisko musi zawierać od 1 do 32 znaków!';
$_['error_email'] = 'Adres e-mail jest nieprawidłowy!';
$_['error_telephone'] = 'Telefon musi zawierać od 3 do 32 znaków!';
$_['error_password'] = 'Hasło musi zawierać od 4 do 20 znaków!';
$_['error_confpassword'] = 'Hasła nie pasują do siebie';
$_['error_exists'] = 'Ostrzeżenie: adres e-mail już zarejestrowany!';
$_['error_agree'] = 'Ostrzeżenie: musisz zaakceptować% s!';
$_['error_warning'] = 'Ostrzeżenie: uważnie sprawdź formularz błędu!';
$_['error_approved'] = 'Ostrzeżenie: Twoje konto wymaga zatwierdzenia zanim będziesz mógł się zalogować';
$_['error_login'] = 'Ostrzeżenie: nie dotyczy adresu e-mail i / lub hasła.';