<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = '%s товаров';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'История платежей';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Показать все';
$_['text_page']          = 'страница';
$_['text_gallery']       = 'Галерея';

$_['text_shop']          = 'Онлайн магазин';
$_['text_sot']           = 'Сотрудничество';
$_['text_company']       = 'О компании';
$_['text_contact']       = 'Контакты';
$_['total_wishlist']     = '%s товаров';

