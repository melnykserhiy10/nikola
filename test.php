<div class="row-aboute row">
    <div class="col-aboute-left col-lg-4  col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 col-lg-offset-2 ">
        <div class="col-aboute">
            <div class="aboute-curier-title"><span class="aboute-title-icon icon-rocket"></span><span
                        class="aboute-title">15 лет на рынке</div>
            <div class="aboute-description"><p>За 15 лет работы мы изучили вкусы и потребности наших
                    клиентов, и нам приятно, что спрос на нашу продукцию растет с каждым годом. Это говорит
                    о том, что мы работаем не зря, мотивирует нас расти и становится еще лучше для вас!</p>
            </div>
        </div>
        <div class="col-aboute">
            <div class="aboute-curier-title"><span class="aboute-title-icon icon-good"></span><span
                        class="aboute-title">Кредит доверия</div>
            <div class="aboute-description"><p>Мы работаем как с оптовыми, так и розничными покупателями.
                    Уже много лет мы сотрудничаем с крупными компаниями страны. С одними клиентами мы
                    работаем уже не первый год, с другими - только начинаем сотрудничество, но мы гордимся
                    тем, что покупатели всегда возвращаются к нам, лично оценив качество нашей
                    продукции.</p>
            </div>
        </div>
        <div class="col-aboute">
            <div class="aboute-curier-title"><span class="aboute-title-icon icon-quality"></span><span
                        class="aboute-title">100% материалы</div>
            <div class="aboute-description"><p>Мы заботимся о качестве продукции и используем только
                    сертифицированные материалы. Покупая товары нашего производства или же продукцию
                    брендов, представленных в нашем магазине, вы можете быть уверены в качестве этой вещи.
                    Мы ценим доверие наших клиентов и предлагаем только проверенные товары!</p>
            </div>
        </div>
    </div>
    <div class="col-aboute-right col-lg-4  col-md-12 col-sm-12 col-xs-12">
        <div class="col-aboute">
            <div class="aboute-curier-title"><span class="aboute-title-icon icon-garanty"></span><span
                        class="aboute-title">Гарантия качества</div>
            <div class="aboute-description"><p>Наша команда контролирует весь цикл производства каждого
                    изделия - начиная от его дизайна и заканчивая реализацией.
                    Кроме продукта личного производства, мы также представляем широкий ассортимент польских
                    шапок от брендов, поддерживающих высокий стандарт качества своей продукции.</p>
            </div>
        </div>
        <div class="col-aboute">
            <div class="aboute-curier-title"><span class="aboute-title-icon icon-assortment"></span><span
                        class="aboute-title">Большой ассортимент</div>
            <div class="aboute-description"><p>Мы постоянно развиваемся, следим за последними трендами,
                    экспериментируем с цветами, технологиями, удивляя наших покупателей оригинальными
                    моделями каждый сезон. Дизайнеры Nikola разрабатывают шапки на любой вкус, вкладывая
                    душу в каждое изделие, для того, чтобы посетив магазин с нашей продукцией, клиент
                    обязательно нашел что-то интересное для самого дорогого человека - для своего
                    ребенка!</p>
            </div>
        </div>

    </div>
</div>


<div class="row-faq row" style="display: none">
    <div class="col-faq-left col-md-6 col-sm-12 col-xs-12">
        <div class="col-faq ">
            <div class="faq-curier-title"><span class="faq-number">01</span><span class="faq-title">НУЖНО ЛИ РЕГИСТРИРОВАТЬСЯ, ЧТОБЫ СДЕЛАТЬ ЗАКАЗ?
            </div>
            <div class="faq-description">Нет, Вы можете сделать заказ с помощью опции "Быстрые покупки".
                Если Вы хотите использовать привилегии (VIP-скидки, ночные распродажи, промо-акции),
                иметь доступ к истории своих покупок и покупать еще дешевле - Вы можете создать аккаунт
                ANSWEAR.ua.
            </div>
        </div>
        <div class=" col-faq">
            <div class="faq-curier-title"><span class="faq-number">2</span><span class="faq-title">КАКИЕ СРОКИ ДОСТАВКИ ТОВАРА?
            </div>
            <div class="faq-description">Ваша посылка будет доставлена в течении 150 часов от момента
                потверждения заказа в оператора.
            </div>
        </div>
        <div class=" col-faq">
            <div class="faq-curier-title"><span class="faq-number">03</span><span class="faq-title">КАКАЯ СТОИМОСТЬ ДОСТАВКИ?
            </div>
            <div class="faq-description">На данный момент доставка всех заказов происходит БЕСПЛАТНО до
                конца 2016 года!
            </div>
        </div>
        <div class=" col-faq">
            <div class="faq-curier-title"><span class="faq-number">04</span><span class="faq-title">КАКИЕ ДОКУМЕНТЫ БУДУТ ПРЕДОСТАВЛЕНЫ ВМЕСТЕ С ЗАКАЗОМ?
            </div>
            <div class="faq-description">Вместе с заказанными товарами Вы получите клиентский инвойс и
                форму на возврат.
            </div>
        </div>
        <div class=" col-faq">
            <div class="faq-curier-title"><span class="faq-number">05</span><span class="faq-title">КАКОЙ ТЕЛЕФОН КУРЬЕРСКОЙ СЛУЖБЫ??
            </div>
            <div class="faq-description">(050) 432 77 07, (067) 432 77 07
            </div>
        </div>
        <div class=" col-faq">
            <div class="faq-curier-title"><span class="faq-number">06</span><span class="faq-title">КАКИЕ УСЛОВИЯ ДОСТАВКИ ЗАКАЗА?
            </div>
            <div class="faq-description">В момент получения заказа перед оплатой товара Вы можете открыть
                посылку и осмотреть доставленный товар, проверить на наличие всех позиций и оценить качество
                товара.
            </div>
        </div>
    </div>
    <div class="col-faq-right col-md-6 col-sm-12 col-xs-12">
        <div class=" col-faq">
            <div class="faq-curier-title"><span class="faq-number">07</span><span class="faq-title">КАКОЙ ГРАФИК РАБОТЫ КУРЬЕРСКОЙ СЛУЖБЫ?
            </div>
            <div class="faq-description">С понедельника по пятницу курьерская служба работает с 09:00 до
                20:00, в субботу доставка возможна с 09:00 до 16:00, в воскресенье и выходные дни доставка
                не осуществляется.
            </div>
        </div>
        <div class=" col-faq">
            <div class="faq-curier-title"><span class="faq-number">08</span><span class="faq-title">Я СМОГУ ВЕРНУТЬ ЗАКАЗ КУРЬЕРУ, ЕСЛИ МЕНЯ ЧТО ЛИБО НЕ УСТРОИТ?
            </div>
            <div class="faq-description">Да, вы сможете вернуть заказ курьеру, при этом ничего не оплачивая.
            </div>
            <div class=" col-faq">
                <div class="faq-curier-title"><span class="faq-number">09</span><span class="faq-title">ВЕРНУТ ЛИ МНЕ СРЕДСТВА ЗА ДОСТАВКУ, ЕСЛИ Я ВЕРНУ ТОВАР?
                </div>
                <div class="faq-description">При возврате товара вы получите денежные средства в полном
                    обьеме. Стоимость доставки полностью компенсируется.
                </div>
            </div>
            <div class=" col-faq">
                <div class="faq-curier-title"><span class="faq-number">10</span><span class="faq-title">УСЛОВИЯ ВОЗВРАТА ТОВАРА?
                </div>
                <div class="faq-description">Заполните заявление на возврат, обязательные поля: ФИО, адрес,
                    отметьте позиции, которые возвращаете сумму и причину возврата.
                </div>
            </div>
            <div class=" col-faq">
                <div class="faq-curier-title"><span class="faq-number">11</span><span class="faq-title">КАКОЙ АДРЕС ВОЗВРАТА?
                </div>
                <div class="faq-description">а/я ANSWEAR, ул. Пасечная, 102, г. Львов, 79032, Украина.
                </div>
            </div>
            <div class=" col-faq">
                <div class="faq-curier-title"><span class="faq-number">12</span><span class="faq-title">КАК МНЕ ВЕРНУТ ДЕНЕЖНЫЕ СРЕДСТВА ЗА ВОЗВРАТ?
                </div>
                <div class="faq-description">Средства будут возвращены на отделение Укрпочты, которое вы
                    укажете в заявлении на возврат, в течение 14 дней с момента поступления товара на наш
                    склад. О поступлении товара на склад и о переводе средств Вы будете проинформированы
                    представителем ANSWEAR.ua.
                </div>
            </div>
        </div>
    </div>
</div>
