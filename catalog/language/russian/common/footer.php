<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Связаться с нами';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Товары со скидкой';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Мои Закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = 'Работает на <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';

$_['entry_name']      	= 'Имя';
$_['entry_phone']       = 'Номер мобильного телефона';
$_['text_call']      	= 'Заказать звонок';
$_['text_send']      	= 'Отправить';
$_['text_loading']      = 'Обработка';

$_['text_cookie_fancy']  = 'Пожалуйста, укажите Ваш тип покупателя';
$_['title_cookie_fancy'] = 'Добро пожаловать<br>в наш интернет-магазин!';
$_['text_wholesale_buyer'] = 'Я оптовый покупатель';
$_['text_retail_buyer']    = 'Я розничный покупатель';