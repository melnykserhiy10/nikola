<div class="latest_wrapp clearfix">
    <div class="title">
        <h3><a href="/latest"><?php echo $heading_title; ?></a></h3>
    </div>
    <div class="product_wrapp">
      <?php foreach ($products as $product) { ?>
      <div class="product-layout">
        <div class="product-thumb">
          <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
          <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
          <?php if ($product['price']) { ?>
            <div class="price">
              <?php if (!$product['special']) { ?>
                <div class="price-new"><?php echo $product['price']; ?></div>
              <?php } else { ?>
                <div class="price-new"><?php echo $product['special']; ?></div>
                <div class="price-old"><?php echo $product['price']; ?></div>
              <?php } ?>
              <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
              <?php } ?>
            </div>
          <?php } ?>
            <?php if($product['sizes']){ ?>
          <div class="sizes">
              <ul>
                  <?php foreach ($product['sizes'] as $sizes) { ?>
                      <?php foreach ($sizes['product_option_value'] as $size) { ?>
                          <li><?php echo $size['name']; ?></li>
                      <?php } ?>
                  <?php } ?>
              </ul>
          </div>
            <?php } ?>
            <div class="wrapp_whbtn">
                <input type="hidden" id="pr-<?php echo $product['product_id']; ?>">
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" <?php echo $product['wishlist']? 'disabled' : '' ?> onclick="wishlist.add('<?php echo $product['product_id']; ?>'); return false;"></button>
            </div>
        </div>
      </div>
      <?php } ?>
    </div>
</div>
